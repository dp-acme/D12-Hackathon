package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.RegionRepository;
import domain.Region;

@Component
@Transactional
public class StringToRegionConverter implements Converter<String, Region>{
	
	@Autowired
	RegionRepository regionRepository;
	
	@Override
	public Region convert(String source) {
		Region result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = regionRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
