package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import repositories.CommentRepository;
import domain.Comment;

public class StringToCommentConverter implements Converter<String, Comment>{
	
	@Autowired
	CommentRepository commentRepository;
	
	@Override
	public Comment convert(String source) {
		Comment result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = commentRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}
}
