package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.VideogameRepository;
import domain.Videogame;

@Component
@Transactional
public class StringToVideogameConverter implements Converter<String, Videogame>{
	
	@Autowired
	VideogameRepository videogameRepository;
	
	@Override
	public Videogame convert(String source) {
		Videogame result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = videogameRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
