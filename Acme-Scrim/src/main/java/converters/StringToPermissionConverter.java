package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.PermissionRepository;
import domain.Permission;

@Component
@Transactional
public class StringToPermissionConverter implements Converter<String, Permission>{
	
	@Autowired
	PermissionRepository permissionRepository;
	
	@Override
	public Permission convert(String source) {
		Permission result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = permissionRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
