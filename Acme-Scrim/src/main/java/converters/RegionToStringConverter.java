package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Region;

@Component
@Transactional
public class RegionToStringConverter implements Converter<Region, String>{
	
	@Override
	public String convert(Region source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}
	
}
