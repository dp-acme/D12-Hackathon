package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.SeasonRepository;
import domain.Season;

@Component
@Transactional
public class StringToSeasonConverter implements Converter<String, Season>{
	
	@Autowired
	SeasonRepository seasonRepository;
	
	@Override
	public Season convert(String source) {
		Season result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = seasonRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
