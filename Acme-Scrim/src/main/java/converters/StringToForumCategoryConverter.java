package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.ForumCategoryRepository;
import domain.ForumCategory;

@Component
@Transactional
public class StringToForumCategoryConverter implements Converter<String, ForumCategory>{
	
	@Autowired
	ForumCategoryRepository forumCategoryRepository;
	
	@Override
	public ForumCategory convert(String source) {
		ForumCategory result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = forumCategoryRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
