package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.InvitationRepository;
import domain.Invitation;

@Component
@Transactional
public class StringToInvitationConverter implements Converter<String, Invitation>{
	
	@Autowired
	InvitationRepository invitationRepository;
	
	@Override
	public Invitation convert(String source) {
		Invitation result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = invitationRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
