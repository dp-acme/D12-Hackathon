package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.ConfigurationRepository;
import domain.Configuration;

@Component
@Transactional
public class StringToConfigurationConverter implements Converter<String, Configuration>{
	
	@Autowired
	ConfigurationRepository configurationRepository;
	
	@Override
	public Configuration convert(String source) {
		Configuration result;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				result = configurationRepository.find();
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
