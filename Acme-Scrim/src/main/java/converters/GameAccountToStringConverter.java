package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.GameAccount;

@Component
@Transactional
public class GameAccountToStringConverter implements Converter<GameAccount, String>{
	
	@Override
	public String convert(GameAccount source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}
	
}
