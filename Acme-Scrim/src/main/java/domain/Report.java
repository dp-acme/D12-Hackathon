package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Report extends DomainEntity {

	// Constructor
	
	public Report() {
		super();
	}

	// Attributes
	
	private String comments;

	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

	// Relationships
	
	private Player player;
	private Scrim scrim;

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Scrim getScrim() {
		return scrim;
	}
	public void setScrim(Scrim scrim) {
		this.scrim = scrim;
	}

}
