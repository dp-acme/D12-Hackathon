package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class ForumMessage extends DomainEntity {

	// Constructor
	
	public ForumMessage() {
		super();
	}
	
	// Attributes
	
	private String body;
	private Date moment;

	@NotBlank
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return moment;
	}

	public void setMoment(Date moment) {
		this.moment = moment;
	}

	// Relationships
	private Forum forum;
	private Actor actor;
	private Collection<ForumMessage> childrenMessages;
	private ForumMessage parent;


	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Forum getForum() {
		return forum;
	}
	public void setForum(Forum forum) {
		this.forum = forum;
	}
	
	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Actor getActor() {
		return actor;
	}
	public void setActor(Actor actor) {
		this.actor = actor;
	}
	
	@NotNull
	@Valid
	@OneToMany(mappedBy = "parent")
	public Collection<ForumMessage> getChildrenMessages() {
		return childrenMessages;
	}
	
	public void setChildrenMessages(Collection<ForumMessage> childrenMessages) {
		this.childrenMessages = childrenMessages;
	}

	@Valid
	@ManyToOne(optional = true)
	public ForumMessage getParent() {
		return parent;
	}

	public void setParent(ForumMessage parent) {
		this.parent = parent;
	}
	
	
}
