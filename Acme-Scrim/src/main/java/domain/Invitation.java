package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="moment")})
public class Invitation extends DomainEntity {

	// Constructor
	
	public Invitation() {
		super();
	}

	// Attributes

	private String message;
	private Date moment;
	
	@NotBlank
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return moment;
	}
	public void setMoment(Date moment) {
		this.moment = moment;
	}
	
	// Relationships
	
	private GameAccount teammate;
	private Team team;

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public GameAccount getTeammate() {
		return teammate;
	}
	public void setTeammate(GameAccount teammate) {
		this.teammate = teammate;
	}
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	
}
