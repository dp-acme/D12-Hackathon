package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Resolution extends DomainEntity{
	
	public Resolution(){
		super();
	}
	
	private String comments;
	private boolean attended;
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	public boolean isAttended() {
		return attended;
	}
	public void setAttended(boolean attended) {
		this.attended = attended;
	}
	
	private Referee referee;
	private Scrim scrim;

	@Valid
	@ManyToOne(optional = true)
	public Referee getReferee() {
		return referee;
	}
	public void setReferee(Referee referee) {
		this.referee = referee;
	}
	
	@Valid
	@OneToOne(optional = false)
	public Scrim getScrim() {
		return scrim;
	}
	public void setScrim(Scrim scrim) {
		this.scrim = scrim;
	}
	
}
