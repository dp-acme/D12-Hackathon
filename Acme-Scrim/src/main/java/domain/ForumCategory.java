package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="name")})
public class ForumCategory extends DomainEntity {

	// Constructor
	
	public ForumCategory() {
		super();
	}
	
	// Attributes
	
	private String name;

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	// Relationships
	
	private ForumCategory parent;
	private Collection<ForumCategory> childrenCategories;
	private Videogame videogame;

	
	@NotNull
	@Valid
	@OneToMany(mappedBy="parent")
	public Collection<ForumCategory> getChildrenCategories() {
		return childrenCategories;
	}
	
	public void setChildrenCategories(Collection<ForumCategory> childrenCategories) {
		this.childrenCategories = childrenCategories;
	}
	

	@Valid
	@ManyToOne(optional=true)
	public Videogame getVideogame() {
		return videogame;
	}
	public void setVideogame(Videogame videogame) {
		this.videogame = videogame;
	}

	@Valid
	@ManyToOne
	public ForumCategory getParent() {
		return parent;
	}
	
	public void setParent(ForumCategory parent) {
		this.parent=parent;
	}
}
