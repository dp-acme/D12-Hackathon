package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Review extends DomainEntity {

	// Constructor
	
	public Review() {
		super ();
	}
	
	// Attributes
	
	private boolean positive;

	public boolean isPositive() {
		return positive;
	}
	public void setPositive(boolean positive) {
		this.positive = positive;
	}
	
	// Relationships
	
	private Team team;
	private Scrim scrim;

	@Valid
	@NotNull
	@ManyToOne(optional = true)
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = true)
	public Scrim getScrim() {
		return scrim;
	}
	public void setScrim(Scrim scrim) {
		this.scrim = scrim;
	}
	
}
