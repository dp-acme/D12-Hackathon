package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class GameAccount extends DomainEntity {

	// Constructor
	
	public GameAccount() {
		super();
	}
	
	// Attributes
	
	private String nickname;
	
	@NotBlank
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	// Relationships
	
	private Player player;
	private Region region;
	private Videogame videogame;
	private Collection<Team> teams;
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Videogame getVideogame() {
		return videogame;
	}
	public void setVideogame(Videogame videogame) {
		this.videogame = videogame;
	}
	
	@Valid
	@NotNull
	@OneToMany(mappedBy = "leader")
	public Collection<Team> getTeams() {
		return teams;
	}
	public void setTeams(Collection<Team> teams) {
		this.teams = teams;
	}
	
}
