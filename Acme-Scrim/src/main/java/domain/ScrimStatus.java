package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Embeddable
@Access(AccessType.PROPERTY)
public class ScrimStatus {

	// Constructor
	
	public ScrimStatus() {
		super ();
	}
	
	// Values -----------------------------------------------------------------

	public static final String	PENDING		= "PENDING";
	public static final String	CANCELLED	= "CANCELLED";
	public static final String	EXPIRED		= "EXPIRED";
	public static final String	CONFIRMED	= "CONFIRMED";
	public static final String	CLOSED		= "CLOSED";
	public static final String	FINISHED	= "FINISHED";

	// Attributes -------------------------------------------------------------

	private String scrimStatus;

	@NotBlank
	@Pattern(regexp = "^" + ScrimStatus.PENDING + "|" + ScrimStatus.CANCELLED + "|" + ScrimStatus.EXPIRED + "|" + ScrimStatus.CONFIRMED + "|" + ScrimStatus.CLOSED + "|" + ScrimStatus.FINISHED + "$")
	public String getScrimStatus() {
		return this.scrimStatus;
	}
	public void setScrimStatus(String scrimStatus) {
		this.scrimStatus = scrimStatus;
	}
}
