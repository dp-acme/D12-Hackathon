package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Configuration extends DomainEntity {
	// Constructor
	
	public Configuration() {
		super();
	}
	
	// Attributes
	
	private String banner;
	private Collection<String> tabooWords;
	private Integer minimumReports;
	private Double adRatio;
	private String adImage;
	private Integer finderCacheTime;
	private Integer finderResults;
	
	@NotNull
	@URL
	public String getBanner() {
		return banner;
	}
	
	public void setBanner(String banner) {
		this.banner = banner;
	}
	
	@NotNull
	@ElementCollection
	public Collection<String> getTabooWords() {
		return tabooWords;
	}
	
	public void setTabooWords(Collection<String> tabooWords) {
		this.tabooWords = tabooWords;
	}
	
	@NotNull
	@DecimalMin("0")
	public Integer getMinimumReports() {
		return minimumReports;
	}
	
	public void setMinimumReports(Integer minimumReports) {
		this.minimumReports = minimumReports;
	}
	
	@NotNull
	@DecimalMin("0")	
	@DecimalMax("50")	
	public Double getAdRatio() {
		return adRatio;
	}
	
	public void setAdRatio(Double adRatio) {
		this.adRatio = adRatio;
	}
	
	@NotNull
	@URL
	public String getAdImage() {
		return adImage;
	}
	
	public void setAdImage(String adImage) {
		this.adImage = adImage;
	}
	
	@NotNull
	@DecimalMin("1")
	@DecimalMax("24")
	public Integer getFinderCacheTime() {
		return finderCacheTime;
	}
	
	public void setFinderCacheTime(Integer finderCacheTime) {
		this.finderCacheTime = finderCacheTime;
	}
	
	@NotNull
	@DecimalMin("5")	
	@DecimalMax("50")
	public Integer getFinderResults() {
		return finderResults;
	}
	public void setFinderResults(Integer finderResults) {
		this.finderResults = finderResults;
	}
}
