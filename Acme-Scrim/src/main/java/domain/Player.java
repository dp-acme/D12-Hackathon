package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Player extends Actor {

	// Constructor
	
	public Player() {
		super();
	}

	// Attributes
	PremiumSubscription premiumSubscription;

	@Valid
	@OneToOne(optional=true)
	public PremiumSubscription getPremiumSubscription() {
		return premiumSubscription;
	}

	public void setPremiumSubscription(PremiumSubscription premiumSubscription) {
		this.premiumSubscription = premiumSubscription;
	}
}
