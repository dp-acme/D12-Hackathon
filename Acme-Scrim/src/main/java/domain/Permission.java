package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Permission extends DomainEntity {

	// Constructor
	public Permission() {
		super();
	}

	// Attributes
	private Date expirationDate;
	private Boolean isReferee;

	// Methods GET/ SET
	
	@NotNull
	public Boolean getIsReferee() {
		return isReferee;
	}
	public void setIsReferee(Boolean isReferee) {
		this.isReferee = isReferee;
	}
	
	@NotNull
	@Future
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	// Relationships
	private Player player;

	@Valid
	@NotNull
	@OneToOne(optional = false)
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

}
