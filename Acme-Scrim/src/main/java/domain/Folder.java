package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Folder extends DomainEntity {
	// Attributes
	private String name;
	private FolderType type;
	
	// Constructor
	public Folder() {
		super();
	}

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Valid
	@NotNull
	public FolderType getType() {
		return type;
	}

	public void setType(FolderType type) {
		this.type = type;
	}
	
	// Relationships

	private Actor actor; 
	private Collection<Message> messages;
	private Folder parent;
	private Collection<Folder> children;

	@Valid
	@NotNull
	@ManyToOne
	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	@Valid
	@NotNull
	@ManyToMany
	public Collection<Message> getMessages() {
		return messages;
	}

	public void setMessages(Collection<Message> messages) {
		this.messages = messages;
	}
	
	@Valid
	@ManyToOne
	public Folder getParent() {
		return parent;
	}

	public void setParent(Folder parent) {
		this.parent = parent;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "parent")
	public Collection<Folder> getChildren() {
		return children;
	}

	public void setChildren(Collection<Folder> children) {
		this.children = children;
	}
	
	
}
