package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.SocialIdentity;
import domain.SocialNetwork;

@Repository
public interface SocialIdentityRepository extends JpaRepository<SocialIdentity, Integer>{
	
	@Query("select sn from SocialNetwork sn where sn not in (select si.socialNetwork from SocialIdentity si where si.actor.id = ?1)")
	Collection<SocialNetwork> getAvSocialNetwork(int actorId);
	
	@Query("select si from SocialIdentity si where si.actor.id = ?1")
	Collection<SocialIdentity> getSocialIdentitiesByActor(int actorId);
	
	@Query("select si from SocialIdentity si where si.socialNetwork.id = ?1")
	Collection<SocialIdentity> getSocialIdentitiesBySocialNetwork(int socialNetworkId);
	
}