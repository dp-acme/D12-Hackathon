package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.Resolution;

public interface ResolutionRepository extends JpaRepository<Resolution, Integer>{

	
	@Query("select r from Resolution r where r.attended = true")
	Collection<Resolution> getSolvedResolutions();
	
	@Query("select r from Resolution r where r.attended = false")
	Collection<Resolution> getPendingResolutions();
	
}
