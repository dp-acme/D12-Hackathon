package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.GameCategory;
import domain.Videogame;

@Repository
public interface GameCategoryRepository extends JpaRepository<GameCategory, Integer>{

	@Query("select c from GameCategory c where c.parent is null")
	Collection<GameCategory> findParentCategories();
	
	@Query("select g.children from GameCategory g where g.id = ?1")
	Collection<GameCategory> findRootGameCategory(int gameCategoryId);
	
	@Query("select g.videogames from GameCategory g where g.id = ?1")
	Collection<Videogame> findVideogamesOfGameCategory(int gameCategoryId);

	@Query("select f from GameCategory f where (?1 member of f.children)")
	GameCategory parentCategory(int gameCategoryId);
}
