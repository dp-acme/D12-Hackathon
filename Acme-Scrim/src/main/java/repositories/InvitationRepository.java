package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Invitation;

@Repository
public interface InvitationRepository extends JpaRepository<Invitation, Integer>{

	@Query("select inv from Invitation inv where inv.team.id = ?1 AND inv.teammate.id = ?2")
	Invitation getGameAccountTeamInvitation(int teamId, int gameAccountId);
	
}