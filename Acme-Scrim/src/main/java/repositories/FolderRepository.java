package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Folder;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Integer>{
	@Query("select f from Folder f where f.actor.id = ?1 AND f.type.folderType = 'TRASHBOX'")
	Folder findTrashboxByActorId(Integer actorId);
	
	@Query("select f from Folder f where f.actor.id = ?1 AND f.type.folderType = 'INBOX'")
	Folder findInboxByActorId(Integer actorId);

	@Query("select f from Folder f where f.actor.id = ?1 AND f.type.folderType = 'NOTIFICATIONBOX'")
	Folder findNotificationBoxByActorId(Integer actorId);

	@Query("select f from Folder f where f.actor.id = ?1 AND f.type.folderType = 'OUTBOX'")
	Folder findOutboxByActorId(Integer actorId);

	@Query("select f from Folder f where f.actor.id = ?1 AND f.type.folderType = 'SPAMBOX'")
	Folder findSpamboxByActorId(Integer actorId);

	@Query("select f from Folder f where f.actor.id = ?1 AND f.parent = null")
	Collection<Folder> findParentFoldersByActorId(Integer actorId);
}