package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Forum;

@Repository
public interface ForumRepository extends JpaRepository<Forum, Integer>{

	@Query("select f from Forum f where (?1 member of f.forumCategories)")
	Collection<Forum> getForumsByCategory(Integer category);

}