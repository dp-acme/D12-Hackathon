package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Scrim;

@Repository
public interface ScrimRepository extends JpaRepository<Scrim, Integer>{

	@Query("select scrim from Scrim scrim where scrim.creator.id = ?1 OR (scrim.guest != null AND scrim.guest.id = ?1) ORDER BY scrim.moment desc")
	public Collection<Scrim> getTeamScrims(int teamId);
	
}