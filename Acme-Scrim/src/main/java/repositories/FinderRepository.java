package repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Finder;
import domain.Region;
import domain.Scrim;
import domain.Videogame;

@Repository
public interface FinderRepository extends JpaRepository<Finder, Integer>{
	 @Query("select s from Scrim s where (s.guest.name like ?1 OR s.creator.name like ?1) AND ?2 <= s.moment AND s.moment <= ?3 AND s.season.videogame=?4 AND s.desiredGuestRegion=?5")	
		Page<Scrim> filterScrimsByCriteriaFinder(String keyWord, Date date1, Date date2, Videogame videogame, Region region, Pageable pageable);
	 
	 @Query("select s from Scrim s where (s.guest.name like ?1 OR s.creator.name like ?1) AND ?2 <= s.moment AND s.moment <= ?3 AND s.season.videogame=?4")	
		Page<Scrim> filterScrimsByCriteriaFinder(String keyWord, Date date1, Date date2, Videogame videogame, Pageable pageable);
	 
	 @Query("select s from Scrim s where (s.guest.name like ?1 OR s.creator.name like ?1) AND ?2 <= s.moment AND s.moment <= ?3 AND s.desiredGuestRegion=?4")	
		Page<Scrim> filterScrimsByCriteriaFinder(String keyWord, Date date1, Date date2, Region region, Pageable pageable);
	 
	 @Query("select s from Scrim s where (s.guest.name like ?1 OR s.creator.name like ?1) AND ?2 <= s.moment AND s.moment <= ?3")	
		Page<Scrim> filterScrimsByCriteriaFinder(String keyWord, Date date1, Date date2, Pageable pageable);
	 
	 @Query("select f from Finder f where f.actor=?1")
	 	Finder getFinderFromActor(Actor actor);

}
