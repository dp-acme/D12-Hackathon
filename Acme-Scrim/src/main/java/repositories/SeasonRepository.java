package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Scrim;
import domain.Season;

@Repository
public interface SeasonRepository extends JpaRepository<Season, Integer>{
	
	// Scrim asociado a una temporada especifica
	@Query("select s from Scrim s where s.season.id = ?1 AND s.moment < CURRENT_TIMESTAMP ORDER BY s.moment DESC")
	Collection<Scrim> findScrimsPassedOfSeason(int seasonId);
	
	@Query("select s from Scrim s where s.season.id = ?1 AND s.moment > CURRENT_TIMESTAMP ORDER BY s.moment ASC")
	Collection<Scrim> findScrimsNoPassedOfSeason(int seasonId);
	
	// Obtener la �ltima season de un videgame 
	@Query("select MAX(s) from Season s where s.videogame.id = ?1 AND s.finished = false ORDER BY s.startDate ASC")
	Season getLastSeasonOfVideogame(int videogameId);
	
	// Seasons de un videogame dado
	@Query("select s from Season s where s.videogame.id = ?1")
	Collection<Season> seasonsOfVideoGame(int videogameId);
}