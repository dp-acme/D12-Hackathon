package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Permission;
import domain.Player;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Integer>{
	
	@Query("select p from Permission p where p.player.id = ?1")
	Permission findPermission(int playerId);
	
	@Query("select p from Permission p where p.player.id = ?1 AND p.isReferee = ?2")
	Permission findTypePermission(int playerId, Boolean isReferee);
	
	@Query("select p from Player p where p NOT IN (select per.player from Permission per)")
	Collection<Player> findPlayersWithNoPermission();	
}