package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Player;
import domain.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer>{

	@Query("select r from Report r where r.player.id = ?1")
	Collection<Report> getReportsByPlayer(int playerId);
	
	@Query("select r from Report r where r.player.id = ?1 and r.scrim.id = ?2")
	Collection<Report> getReportsByPlayerAndScrim(int playerId, int scrimId);
	
	@Query("select report.player from Report report group by report.player having count(report) >= ?1")
	Collection<Player> getPlayersByNumOfReports(long i);
	
	@Query("select report.player from Report report where report.scrim.id = ?1")
	Collection<Player> getReportedPlayersOfScrim(int scrimId);
	
	@Query("select report from Report report where report.scrim.id = ?1")
	Collection<Report> getReportsOfScrim(int scrimId);
	
}