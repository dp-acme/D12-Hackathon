package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.ForumMessage;

@Repository
public interface ForumMessageRepository extends JpaRepository<ForumMessage, Integer>{
	@Query("select fm from ForumMessage fm where fm.forum.id=?1")
	Collection<ForumMessage> forumMessages(int forumId);
	
	@Query("select fm from ForumMessage fm where fm.forum.id=?1 AND fm.parent=null")
	Collection<ForumMessage> findByForum(int forumId);

}