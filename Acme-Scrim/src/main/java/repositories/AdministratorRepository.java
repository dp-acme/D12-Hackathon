package repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import domain.Administrator;
import domain.Player;
import domain.Team;
import domain.Videogame;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer>{
	
	/*	i. The average, minimum, maximum, and the standard deviation of the number of videogames per manager.*/
	@Query("select avg(cast((select count (*) from Videogame v where v.manager = m) as float)), " +
			"max(cast((select count (*) from Videogame v where v.manager = m) as float)), " +
			"min(cast((select count (*) from Videogame v where v.manager = m) as float)), " +
			"sqrt(sum((select count (*) from Videogame v where v.manager = m)*(select count (*) from Videogame v where v.manager = m))/(select count (*) from Manager m2) - (avg(cast((select count (*) from Videogame v where v.manager = m) as float)))*(avg(cast((select count (*) from Videogame v where v.manager = m) as float)))) " +
			"from Manager m")
	Double[] getAvgMaxMinSdVideogamesPerManager();
	
	/*	ii. The average, minimum, maximum, and the standard deviation of the number of seasons per videogame.*/
	@Query("select avg(cast((select count (*) from Season s where s.videogame = v) as float)), " +
			"max(cast((select count (*) from Season s where s.videogame = v) as float)), " +
			"min(cast((select count (*) from Season s where s.videogame = v) as float)), " +
			"sqrt(sum((select count (*) from Season s where s.videogame = v)*(select count (*) from Season s where s.videogame = v))/(select count (*) from Videogame v2) - (avg(cast((select count (*) from Season s where s.videogame = v) as float)))*(avg(cast((select count (*) from Season s where s.videogame = v) as float)))) " +
			"from Videogame v")
	Double[] getAvgMaxMinSdSeasonsPerVideogame();
	
	/*	iii. The average, minimum, maximum, and the standard deviation of the number of scrims per team.*/
	@Query("select avg(cast((select count (*) from Scrim s where s.creator = t or s.guest=t) as float)), " +
			"max(cast((select count (*) from Scrim s where s.creator = t or s.guest=t) as float)), " +
			"min(cast((select count (*) from Scrim s where s.creator = t or s.guest=t) as float)), " +
			"sqrt(sum((select count (*) from Scrim s where s.creator = t or s.guest=t)*(select count (*) from Scrim s where s.creator = t or s.guest=t))/(select count (*) from Team t2) - (avg(cast((select count (*) from Scrim s where s.creator = t or s.guest=t) as float)))*(avg(cast((select count (*) from Scrim s where s.creator = t or s.guest=t) as float)))) " +
			"from Team t")
	Double[] getAvgMaxMinSdScrimsPerTeam();
	
	/* iv. The average, minimum, maximum, and the standard deviation of the number of teams per player.*/
	@Query("select avg(cast((select count(t) from Team t where (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0)as float)), " +
			"max(cast((select count(t) from Team t where (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0)as float)), " +
			"min(cast((select count(t) from Team t where (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0)as float)), " +
			"sqrt(sum((select count(t) from Team t where (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0)*(select count(t) from Team t where (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0))/(select count (*) from Player p2) - (avg(cast((select count(t) from Team t where (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0) as float)))*(avg(cast((select count(t) from Team t where (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0) as float)))) " +			
			"from Player p")
	Double[] getAvgMaxMinSdTeamsPerPlayer();
	
	/*	v. The average, minimum, maximum, and the standard deviation of the number of players per team.*/
	@Query("select avg(cast((select count (*) from Invitation i where i.team = t and NOT i.moment=null) as float)), " +
			"max(cast((select count (*) from Invitation i where i.team = t and NOT i.moment=null) as float)), " +
			"min(cast((select count (*) from Invitation i where i.team = t and NOT i.moment=null) as float)), " +
			"sqrt(sum((select count (*) from Invitation i where i.team = t and NOT i.moment=null)*(select count (*) from Invitation i where i.team = t and NOT i.moment=null))/(select count (*) from Team t2) - (avg(cast((select count (*) from Invitation i where i.team = t and NOT i.moment=null) as float)))*(avg(cast((select count (*) from Invitation i where i.team = t and NOT i.moment=null) as float)))) " +
			"from Team t")
	Double[] getAvgMaxMinSdPlayersPerTeam();
	
	/*	vi. Top 5 teams with more scrims.*/
	@Query("select t from Team t order by coalesce((select count (*) from Scrim s where s.creator = t or s.guest=t),0) DESC")
	Page<Team> getTeamsWithMoreScrims(Pageable pageable);
	
	/*	ix. Top 5 players with more messages*/
	@Query("select p from Player p order by coalesce((select count(fm) from ForumMessage fm where fm.actor=p),0) DESC")
	Page<Player> getPlayersWithMoreForumMessages(Pageable pageable);
	
	/*	x. The ratio of players who have commented at any videogame forum.*/
	@Query("select cast((select count(*) from Player p where (coalesce((select count(fm) from ForumMessage fm where fm.actor=p),0)>0))as float)/count(*) from Player p")
	Double ratioOfPlayersWithForumMessages();
	
	@Query("select cast((select count(s) from Scrim s where NOT s.guest = null AND s.confirmed=false)as float)/count(s) from Scrim s")
	Double ratioOfCancelledScrims();
	
	@Query("select p from Player p order by (cast((select count(t) from Team t where (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0)as float)) DESC")
	Page<Player> playersWithMoreTeams(Pageable pageable);
	
	/*	viii. Top 5 players with more teams per videogame.*/
	@Query(value= "select p from Player p order by (cast((select count(t) from Team t where t.videogame.id= :videogameId AND (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0)as float)) DESC",
			countQuery="select count(p) from Player p order by (cast((select count(t) from Team t where t.videogame.id= :videogameId AND (coalesce((select count(*) from GameAccount ga where t IN ELEMENTS (ga.teams) AND ga.player=p),0)>0) OR (select count(*) from Invitation i where i.team=t AND i.teammate.player=p AND NOT i.moment=NULL)>0)as float)) DESC")
	Page<Player> playersWithMoreTeamsPerVideogame(  @Param("videogameId")Integer videogameId, Pageable pageable);

	@Query("select v from Videogame v order by v.name ASC")
	Page<Videogame> getVideogames(Pageable pageable);
	
}