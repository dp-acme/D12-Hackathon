package controllers.gameCategory;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.GameCategoryService;
import services.VideogameService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.GameCategory;
import domain.Videogame;

@Controller
@RequestMapping("/gameCategory/admin")
public class GameCategoryAdminController extends AbstractController {
	// Services ---------------------------------------------------------------
	@Autowired
	private GameCategoryService gameCategoryService;

	@Autowired
	private VideogameService videogameService;

	@Autowired
	private ActorService actorService;

	// Constructor -----------------------------------------------------------

	public GameCategoryAdminController() {
		super();
	}

	// Create -----------------------------------
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam int parentId) {
		Assert.isTrue(parentId != 0);

		GameCategory parent;
		parent = this.gameCategoryService.findOne(parentId);

		ModelAndView result;
		GameCategory gameCategory;
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());

		Assert.isTrue(actor instanceof Administrator);

		// Comprobamos
		gameCategory = gameCategoryService.create(parent);
		Collection<GameCategory> parentCategoryToSelect = new ArrayList<GameCategory>();
		parentCategoryToSelect.add(parent);
		// Collection<GameCategory> childrenCategoryToSelect =
		// this.gameCategoryService
		// .findAll();
		// childrenCategoryToSelect.remove(parent);

		Collection<Videogame> videogameToSelect = this.videogameService
				.findAll();
		result = createEditModelAndView(gameCategory);
		result.addObject("gameCategory", gameCategory);
		result.addObject("parentCategoryToSelect", parentCategoryToSelect);
		result.addObject("videogameToSelect", videogameToSelect);
		result.addObject("cancel","/gameCategory/list.do?gameCategoryId="+ parentId);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int gameCategoryId) {
		ModelAndView result;
		GameCategory category;
		
		category = this.gameCategoryService.findOne(gameCategoryId);
		
		// Comprobamos
		Collection<GameCategory> parentCategoryToSelect = this.gameCategoryService.findAll();

		Collection<Videogame> videogameToSelect = this.videogameService.findAll();

		result = new ModelAndView("gameCategory/admin/edit");
		category = gameCategoryService.findOne(gameCategoryId);

		Assert.notNull(category);

		result = createEditModelAndView(category);
		result.addObject("parentCategoryToSelect", parentCategoryToSelect);
		result.addObject("videogameToSelect", videogameToSelect);
		result.addObject("cancel","/gameCategory/list.do?gameCategoryId="+ gameCategoryId);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value = "gameCategory") GameCategory gameCategory, BindingResult binding) {
		ModelAndView result;

		GameCategory gameCategoryR = this.gameCategoryService.reconstruct(gameCategory, gameCategory.getParent(), binding);
		Collection<GameCategory> allCategories = this.gameCategoryService.findAll();
		
		if(gameCategoryR.getId() != 0)
			allCategories.remove(this.gameCategoryService.findOne(gameCategoryR.getId()));
		for (GameCategory fc : allCategories) { // No se pueden crear
			// categorķas con el mismo nombre y el mismo padre
			if (fc.getName().equals(gameCategory.getName())) {
				result = createEditModelAndView(gameCategory);

				result.addObject("gameCategory", gameCategory);
				result.addObject("nameMessage", "gameCategory.commit.error.nameMessage");

				return result;
			}
		}

		if (binding.hasErrors()) {
			result = createEditModelAndView(gameCategory);
		} else {
			try {
				this.gameCategoryService.save(gameCategoryR);
				result = new ModelAndView(
						"redirect:/gameCategory/list.do?gameCategoryId="
								+ gameCategory.getParent().getId());
			} catch (ObjectOptimisticLockingFailureException oops) {
				result = this.createEditModelAndView(gameCategory,
						"gameCategory.commit.errorTransaction");

			} catch (Throwable oops) {
				result = createEditModelAndView(gameCategory,
						"gameCategory.commit.error");
			}
		}

		return result;
	}

	// - Edit (Delete) -
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam int gameCategoryId) {
		ModelAndView result;

		GameCategory category = this.gameCategoryService
				.findOne(gameCategoryId);

		this.gameCategoryService.delete(category);
		result = new ModelAndView("redirect:/gameCategory/list.do?gameCategoryId="
				+ category.getParent().getId());

		return result;
	}

	private ModelAndView createEditModelAndView(GameCategory gameCategory) {
		ModelAndView result;

		result = createEditModelAndView(gameCategory, null);

		return result;
	}

	private ModelAndView createEditModelAndView(GameCategory gameCategory,
			String messageCode) {
		ModelAndView result;

		GameCategory parent;
		parent = gameCategory.getParent();
		
		// Comprobamos
		Collection<GameCategory> parentCategoryToSelect = new ArrayList<GameCategory>();
		parentCategoryToSelect.add(parent);

		Collection<Videogame> videogameToSelect = this.videogameService
				.findAll();

		result = new ModelAndView("gameCategory/admin/edit");

		result.addObject("gameCategory", gameCategory);
		result.addObject("message", messageCode);
		result.addObject("parentCategoryToSelect", parentCategoryToSelect);
		result.addObject("videogameToSelect", videogameToSelect);
		
		return result;
	}

}