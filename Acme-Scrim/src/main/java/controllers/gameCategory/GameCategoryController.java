package controllers.gameCategory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.GameCategoryService;
import controllers.AbstractController;
import domain.GameCategory;

@Controller
@RequestMapping("/gameCategory")
public class GameCategoryController extends AbstractController {
	// Services ---------------------------------------------------------------

	@Autowired
	private GameCategoryService gameCategoryService;

	// Constructor -----------------------------------------------------------

	public GameCategoryController() {
		super();
	}

	@RequestMapping("/list")
	public ModelAndView list(
			@RequestParam(required = false) Integer gameCategoryId) {

		ModelAndView result;

		// Todas categorias
		Collection<GameCategory> categories = new ArrayList<GameCategory>();

		GameCategory categ;
		boolean booleanParent;
		
		// Si no hay id en la url mostramos siempre la categoria padre de todas
		if (gameCategoryId == null) {
			List<GameCategory> categoryParent = new ArrayList<GameCategory>();
			categoryParent.addAll(this.gameCategoryService
					.findParentCategories());
			categ = categoryParent.get(0);
			categories = categoryParent;
			booleanParent = true;
		} else {
			Assert.isTrue(gameCategoryId != 0);

			// COmprobamos si existe la gameCategory
			categ = this.gameCategoryService.findOne(gameCategoryId);

			Assert.notNull(categ);
			categories = this.gameCategoryService.findRootGameCategory(gameCategoryId);
			booleanParent = false;
		}

		result = new ModelAndView("gameCategory/list");

		result.addObject("gameCategories", categories);
		result.addObject("requestURI", "gameCategory/list.do");
		result.addObject("categ", categ);
		result.addObject("booleanParent", booleanParent);
		return result;
	}
}