package controllers.advertisement;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.AdvertisementService;
import controllers.AbstractController;
import domain.Actor;
import domain.Advertisement;
import domain.Sponsor;

@Controller
@RequestMapping("/advertisement/sponsor")
public class AdvertisementSponsorController extends AbstractController {
	// Services------------------------------------------------------------
	@Autowired
	private AdvertisementService advertisementService;

	@Autowired
	private ActorService actorService;

	// Constructor--------------------------------------------------------------------
	public AdvertisementSponsorController() {
		super();
	}

	// Display ----------------------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam int advertisementId) {
		ModelAndView result;
		Advertisement advertisement;

		advertisement = this.advertisementService.findOne(advertisementId);
		Assert.notNull(advertisement);

		result = new ModelAndView("advertisement/sponsor/display");
		result.addObject("advertisement", advertisement);

		return result;
	}

	// List My advertisement ----------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		// Creamos el objeto a devolver
		ModelAndView result;

		// Comprobamos que el usuario logeado es un sponsor
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());
		Assert.isTrue(actor instanceof Sponsor);

		// Si es sponsor, lo cogemos
		Sponsor sponsor;
		sponsor = (Sponsor) actor;

		// Creamos una colleccion para guardar los advertisement
		Collection<Advertisement> myAdvertisement;

		// Inicializamos los anuncios del sponsor
		myAdvertisement = sponsor.getMyAdvertisements();

		result = new ModelAndView("advertisement/sponsor/list");

		// Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("advertisements", myAdvertisement);
		result.addObject("requestURI", "advertisement/sponsor/list.do");

		return result;
	}

	// Create -----------------------------------
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Advertisement advertisement;
		Sponsor sponsor;

		sponsor = (Sponsor) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(sponsor);
		advertisement = advertisementService.create();

		result = createEditModelAndView(advertisement);
		result.addObject("advertisement", advertisement);

		return result;
	}

	// Edit------------------------------------------------------------
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam int advertisementId) {
		ModelAndView result;
		Advertisement advertisement;
		Sponsor user;

		user = (Sponsor) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(user);

		advertisement = advertisementService.findOne(advertisementId);

		Assert.notNull(advertisement);
		Assert.isTrue(advertisement.getSponsor().getUserAccount()
				.equals(LoginService.getPrincipal()));

		result = createEditModelAndView(advertisement);

		return result;
	}

	// Delete--------------------------------------------------------
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam int advertisementId) {
		ModelAndView result;

		Advertisement advertisement = advertisementService
				.findOne(advertisementId);

		advertisementService.delete(advertisement);

		result = new ModelAndView("redirect:/advertisement/sponsor/list.do");

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(
			@ModelAttribute(value = "advertisement") Advertisement advertisement,
			BindingResult binding) {
		ModelAndView result;
		Advertisement advertisementRes;

		Sponsor sponsor;

		sponsor = (Sponsor) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(sponsor);

		advertisementRes = advertisementService.reconstruct(advertisement,
				binding);

		if (binding.hasErrors()) {
			result = createEditModelAndView(advertisementRes);
		} else {
			try {
				advertisementRes = this.advertisementService
						.save(advertisementRes);
				result = new ModelAndView(
						"redirect:/advertisement/sponsor/display.do?advertisementId="
								+ advertisementRes.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(advertisementRes,
						"advertisement.commit.error");
			}
		}

		return result;
	}

	private ModelAndView createEditModelAndView(Advertisement advertisement) {
		ModelAndView result;

		result = createEditModelAndView(advertisement, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Advertisement advertisement,
			String messageCode) {
		ModelAndView result;

		result = new ModelAndView("advertisement/sponsor/edit");

		result.addObject("advertisement", advertisement);
		result.addObject("message", messageCode);

		return result;
	}

}