package controllers.resolution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.PlayerService;
import services.ResolutionService;
import services.ScrimService;
import controllers.AbstractController;
import domain.Player;
import domain.Resolution;
import domain.Result;
import domain.Scrim;
import domain.ScrimStatus;

@Controller
@RequestMapping("/resolution/player")
public class ResolutionUserController extends AbstractController{
	
	@Autowired
	private ResolutionService resolutionService;
	
	@Autowired
	private ScrimService scrimService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PlayerService playerService;
	
	@RequestMapping("/create")
	public ModelAndView create(@ModelAttribute("scrimId") int scrimId) {
		ModelAndView result;
		Resolution resolution;
		Player principal;
		Scrim scrim; 
		
		scrim = scrimService.findOne(scrimId);
		principal = actorService.checkIsPlayer();
		resolution = resolutionService.create(scrimId);
		
		Assert.notNull(scrim);
		
		Assert.isTrue(scrim.getStatus().getScrimStatus().equals(ScrimStatus.CLOSED));
		Assert.isTrue(scrim.getCreatorTeamResult().getResult().equals(Result.ILLOGICAL) && scrim.getGuestTeamResult().getResult().equals(Result.ILLOGICAL));
		Assert.isTrue(scrim.getCreator().getLeader().getPlayer().equals(principal) || scrim.getGuest().getLeader().getPlayer().equals(principal));	

		if (scrim.getCreator().getLeader().getPlayer().equals(principal)) {
			Assert.isTrue(this.playerService.getPremiumPlayersInTeam(scrim.getCreator().getId()).size() != 0);
		} else {
			Assert.isTrue(this.playerService.getPremiumPlayersInTeam(scrim.getGuest().getId()).size() != 0);
		}
		
		if (scrim.getResolution() == null){
			result = new ModelAndView("resolution/create");
		} else {
			result = new ModelAndView("redirect:/scrim/display.do?scrimId="+resolution.getScrim().getId());
		}
		
		result = new ModelAndView("resolution/create");
		
		result.addObject("resolution", resolution);

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("scrimId") int scrimId, Resolution resolution, BindingResult binding){
		ModelAndView result;
		Scrim scrim; 
		
		scrim = scrimService.findOne(scrimId);
		
		resolution.setScrim(scrim);
		
		result = new ModelAndView("resolution/create");
		
		if(binding.hasErrors()){			
			result.addObject("message", "resolution.create.error");
		} else {
			try{
				resolutionService.save(resolution);

				result = new ModelAndView("redirect:/scrim/display.do?scrimId="+resolution.getScrim().getId());
				
			} catch (Throwable oops) {				
				result = new ModelAndView("resolution/create");
				result.addObject("message", "resolution.commit.error");				
			}
		}
		
		return result;
	}
}
