package controllers.review;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ReviewService;

import controllers.AbstractController;
import domain.Review;

@Controller
@RequestMapping("/review/player")
public class ReviewController extends AbstractController {
	
	@Autowired
	private ReviewService reviewService;
	

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@ModelAttribute("scrimId") int scrimId, @ModelAttribute("teamId") int teamId, @ModelAttribute("positive") boolean positive){
		ModelAndView result;
		Review review;
		
		review = reviewService.create(scrimId, teamId);
		review.setPositive(positive);
		
		result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrimId);

		reviewService.save(review);

		return result;
	}

	
}
