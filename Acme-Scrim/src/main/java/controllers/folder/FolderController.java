package controllers.folder;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.FolderService;
import services.MessageService;
import controllers.AbstractController;
import domain.Actor;
import domain.Folder;
import domain.Message;

@Controller
@RequestMapping("/folder")
public class FolderController extends AbstractController{
	// Services ---------------------------------------------------------------

	@Autowired
	private FolderService folderService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor -----------------------------------------------------------

	public FolderController() {
		super();
	}
	
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(value = "folderId", required = false) Integer folderId,
						 	 @RequestParam(value = "page", required = false) Integer page,
					   		 @RequestParam(value = "messageId", required = false) Integer messageId){
		Assert.isTrue(LoginService.isAuthenticated());
		Assert.isTrue((page == null) == (folderId == null)); //O los dos son null o ninguno
		Assert.isTrue(messageId == null || folderId != null); //Si hay mensaje debe haber carpeta
		Assert.isTrue(page == null || page > 0); //La p�gina debe ser mayor que 0
		
		ModelAndView result;
		Actor principal;
		
		principal = actorService.findPrincipal();
						
		result = new ModelAndView("folder/list");
		result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
		result.addObject("folderWithoutParent", folderService.create(null, null));
		result.addObject("folderWithParent", folderService.create(null, null));
		result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
		result.addObject("actors", actorService.findAll());
		
		if(folderId != null){ //Obtener mensajes paginados
			Folder folder;		
			Double pages;
			folder = folderService.findOne(folderId);
			
			Assert.notNull(folder);
			Assert.isTrue(folder.getActor().equals(principal)); //Comprobar credenciales
			
			pages = Math.ceil(folder.getMessages().size() / 10.0);
			
			result.addObject("messages", messageService.findPaginatedMessagesByFolderId(folderId, Math.min(page - 1, pages.intValue())));
			result.addObject("pages", pages);			
		}
		
		if(messageId != null){
			Message message;
			message = messageService.findOne(messageId);
			
			Assert.notNull(message);
			Assert.isTrue(message.getFolder().getActor().equals(principal)); //Comprobar credenciales
			Assert.isTrue(message.getFolder().getId() == folderId); //Comprobar que el mensaje est� en la carpeta actual
			
			result.addObject("currentMessage", message);			
		}
		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "createWithoutParent")
	public ModelAndView create(@ModelAttribute("folderWithoutParent") Folder folder, BindingResult binding){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Actor principal;
		
		principal = actorService.findPrincipal();
		
		folder = folderService.reconstruct(folder, principal, null, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("folder/list");
			result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
			result.addObject("folderWithoutParent", folder);
			result.addObject("folderWithParent", folderService.create(null, null));
			result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
			result.addObject("actors", actorService.findAll());
			result.addObject("shownForms", "withoutParent");
			
		} else{
			try{
				folderService.save(folder);
				
				result = new ModelAndView("redirect:/folder/list.do");
	
			} catch (Throwable oops) {
				result = new ModelAndView("folder/list");
				result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
				result.addObject("folderWithoutParent", folder);
				result.addObject("folderWithParent", folderService.create(null, null));
				result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
				result.addObject("actors", actorService.findAll());
				result.addObject("message", "folder.commit.error");
			}
		}						
		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "createWithParent")
	public ModelAndView create2(@RequestParam(value = "parentId", required = false) Integer parentId,
							    @ModelAttribute("folderWithParent") Folder folder, BindingResult binding){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Actor principal;
		Folder parent;
		
		principal = actorService.findPrincipal();
		
		parent = folderService.findOne(parentId);
		Assert.notNull(parent);
		
		folder = folderService.reconstruct(folder, principal, parent, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("folder/list");
			result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
			result.addObject("folderWithoutParent", folderService.create(null, null));
			result.addObject("folderWithParent", folder);
			result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
			result.addObject("actors", actorService.findAll());
			result.addObject("shownForms", "withParent");

		} else{
			try{
				folderService.save(folder);
				
				result = new ModelAndView("redirect:/folder/list.do");
	
			} catch (Throwable oops) {
				result = new ModelAndView("folder/list");
				result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
				result.addObject("folderWithoutParent", folderService.create(null, null));
				result.addObject("folderWithParent", folder);
				result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
				result.addObject("actors", actorService.findAll());
				result.addObject("message", "folder.commit.error");
			}
		}						
		
		return result;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(value = "folderId", required = true) Integer folderId){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Actor principal;
		Folder folder;
		
		principal = actorService.findPrincipal();
		folder = folderService.findOne(folderId);
		Assert.notNull(folder);
		Assert.isTrue(principal.equals(folder.getActor())); //Comprobar credenciales
		
		try{
			folderService.delete(folder);
			result = new ModelAndView("redirect:/folder/list.do");
	
		} catch (Throwable oops) {
			result = new ModelAndView("folder/list");
			result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
			result.addObject("folderWithoutParent", folderService.create(null, null));
			result.addObject("folderWithParent", folderService.create(null, null));
			result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
			result.addObject("actors", actorService.findAll());
			result.addObject("message", "folder.commit.error");
		}
		
		return result;
	}
	
	@RequestMapping("/move")
	public ModelAndView move(@RequestParam(value = "folderId", required = true) Integer folderId,
							 @RequestParam(value = "parentId", required = false) Integer parentId){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		
		try{
			folderService.moveFolder(folderId, parentId);
			result = new ModelAndView("redirect:/folder/list.do");
	
		} catch (Throwable oops) {
			result = new ModelAndView("folder/list");
			result.addObject("parentFolders", folderService.findParentFoldersByActorId(actorService.findPrincipal().getId()));
			result.addObject("folderWithoutParent", folderService.create(null, null));
			result.addObject("folderWithParent", folderService.create(null, null));
			result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
			result.addObject("actors", actorService.findAll());
			result.addObject("message", "folder.commit.error");
		}
		
		return result;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Folder folder, BindingResult binding){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Actor principal;
		
		principal = actorService.findPrincipal();
		
		Assert.notNull(folder);
				
		folder = folderService.reconstruct(folder, null, null, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("folder/list");
			result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
			result.addObject("folderWithoutParent", folderService.create(null, null));
			result.addObject("folderWithParent", folderService.create(null, null));
			result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
			result.addObject("actors", actorService.findAll());
			result.addObject("message", "org.hibernate.validator.constraints.NotBlank.message");
			
		} else{
			try{
				folderService.save(folder);
				result = new ModelAndView("redirect:/folder/list.do");
		
			} catch (Throwable oops) {
				result = new ModelAndView("folder/list");
				result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
				result.addObject("folderWithoutParent", folderService.create(null, null));
				result.addObject("folderWithParent", folderService.create(null, null));
				result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
				result.addObject("actors", actorService.findAll());
				result.addObject("message", "folder.commit.error");
			}	
		}
		
		return result;
	}
}
