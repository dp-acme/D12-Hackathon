package controllers.region;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.RegionService;
import controllers.AbstractController;
import domain.Region;

@Controller
@RequestMapping("/region")
public class RegionController extends AbstractController{
	
	// Services ---------------------------------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RegionService regionService;
	
	// Constructor -----------------------------------------------------------

	public RegionController() {
		super();
	}
	
	// Create -----------------------------------
		
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Region region;

		this.actorService.checkIsAdministrator();

		region = this.regionService.create();

		result = createEditModelAndView(region);

		return result;
	}

	// Edit------------------------------------------------------------
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required = true, value = "regionId") int regionId) {
		ModelAndView result;
		Region region;

		this.actorService.checkIsAdministrator();
		
		region = this.regionService.findOne(regionId);
		Assert.notNull(region);

		Assert.isTrue(!region.getAbbreviation().equals("INT") && !region.getEnglishName().equals("International")
				&& !region.getSpanishName().equals("Internacional"));

		result = createEditModelAndView(region);

		return result;
	}

	// Delete--------------------------------------------------------
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@ModelAttribute(value = "region") Region region) {
		ModelAndView result;

		region = this.regionService.findOne(region.getId());
		Assert.notNull(region);
		
		try {
			regionService.delete(region);
			result = new ModelAndView("redirect:/configuration/display.do");
		} catch (Throwable oops) {
			result = createEditModelAndView(region, "region.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid @ModelAttribute(value = "region") Region region, BindingResult binding) {
		ModelAndView result;

		actorService.checkIsAdministrator();

		if (binding.hasErrors()) {
			result = createEditModelAndView(region);
		} else {
			try {
				region = this.regionService.save(region);
				result = new ModelAndView("redirect:/configuration/display.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(region, "region.commit.error");
				
				if (!this.regionService.uniqueEnglishName(region)) {
					result.addObject("uniqueEnglishName", "region.edit.uniqueEnglishName");
				}
				if (!this.regionService.uniqueSpanishName(region)) {
					result.addObject("uniqueSpanishName", "region.edit.uniqueSpanishName");
				}
				if (!this.regionService.uniqueAbbreviation(region)) {
					result.addObject("uniqueAbbreviation", "region.edit.uniqueAbbreviation");
				}
				if (!this.regionService.uniqueColor(region)) {
					result.addObject("uniqueColor", "region.edit.uniqueColor");
				}
			}
		}

		return result;
	}

	// Ancillary methods ------------------------------------------------------
		
	private ModelAndView createEditModelAndView(Region region) {
		ModelAndView result;

		result = createEditModelAndView(region, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Region region, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("region/edit");

		result.addObject("region", region);
		result.addObject("message", messageCode);

		return result;
	}
	
}
