package controllers.scrim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.CommentService;
import services.FinderService;
import services.GameAccountService;
import services.PlayerService;
import services.RegionService;
import services.ReportService;
import services.ReviewService;
import services.ScrimService;
import services.TeamService;
import services.VideogameService;
import controllers.AbstractController;
import domain.Actor;
import domain.Comment;
import domain.Finder;
import domain.GameAccount;
import domain.Player;
import domain.Region;
import domain.Result;
import domain.Review;
import domain.Scrim;
import domain.Team;
import domain.Videogame;

@Controller
@RequestMapping("/scrim")
public class ScrimController extends AbstractController{

	// Services ---------------------------------------------------------------
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private ScrimService scrimService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private ReviewService reviewService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private VideogameService videogameService;
	
	@Autowired
	private RegionService regionService;
	
	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private FinderService finderService;
	
	@Autowired
	private ReportService reportService;
	
	// Constructor -----------------------------------------------------------
	
	public ScrimController() {
		super();
	}
	
	// Create -----------------------------------
	
	@RequestMapping("/display")
	public ModelAndView display(@RequestParam(required = true, value = "scrimId") int scrimId) {
		ModelAndView result;
		Collection<Comment> comments;
		Actor principal;
		Scrim scrim;
		List<Review> reviews;
		Collection<GameAccount> creatorTeammates;
		Collection<GameAccount> guestTeammates;
		Collection<Player> reportedPlayers;
		boolean hasPremiumPlayerInTeam = false;
		
		// Comentarios
		boolean isGuestPlayer = false;
		boolean isCreatorPlayer = false;
		boolean isScrimPlayer = false;
		String creatorReviewValue = "null";
		String guestReviewValue = "null";
		int teamToLike = 0;
		
		comments = commentService.getCommentsByScrim(scrimId);
		scrim = scrimService.findOne(scrimId);
		reviews = (List<Review>) reviewService.getLikesByScrim(scrimId);
		
		result = new ModelAndView("scrim/display");

		principal = null;
		if(LoginService.isAuthenticated()){
			principal = actorService.findPrincipal();
			if(principal instanceof Player){
				result.addObject("comment", commentService.create(scrimId));
				
				//Es lider?
				isCreatorPlayer = scrim.getCreator().getLeader().getPlayer().equals(principal);
				isGuestPlayer = scrim.getGuest() != null && scrim.getGuest().getLeader().getPlayer().equals(principal);
				isScrimPlayer = playerService.getPlayersByScrim(scrimId).contains(principal);
				
				//Team al que voy a votar
				if(playerService.getPlayersInTeam(scrim.getCreator().getId()).contains(principal) && scrim.getGuest() != null) {
					teamToLike = scrim.getGuest().getId();
				} else {
					teamToLike = scrim.getCreator().getId();
				}
			}
		}
		
		//Los likes que ya existen
		for (Review r : reviews) { 
			if (r.getTeam().equals(scrim.getCreator()))
				creatorReviewValue = String.valueOf(r.isPositive());
			else if (r.getTeam().equals(scrim.getGuest()))
				guestReviewValue = String.valueOf(r.isPositive());
		}
		
		// Integrantes de cada equipo
		creatorTeammates = this.teamService.getRealTeammates(scrim.getCreator(), true);
		if (scrim.getGuest() != null)
			guestTeammates = this.teamService.getRealTeammates(scrim.getGuest(), true);
		else
			guestTeammates = null;
	
		// ReportedPlayers
		reportedPlayers = this.reportService.getReportedPlayersOfScrim(scrim);
		
		// Premium players
		if (principal != null && scrim.getCreator().getLeader().getPlayer().equals(principal)) {
			hasPremiumPlayerInTeam = this.playerService.getPremiumPlayersInTeam(scrim.getCreator().getId()).size() != 0;
		} else if (principal != null && scrim.getGuest() != null && scrim.getGuest().getLeader().getPlayer().equals(principal)) {
			hasPremiumPlayerInTeam = this.playerService.getPremiumPlayersInTeam(scrim.getGuest().getId()).size() != 0;
		}
		
		result.addObject("comments", comments);
		result.addObject("isGuestPlayer", isGuestPlayer);
		result.addObject("isCreatorPlayer", isCreatorPlayer);
		result.addObject("isScrimPlayer", isScrimPlayer);
		result.addObject("creatorReviewValue", creatorReviewValue);
		result.addObject("guestReviewValue", guestReviewValue);
		result.addObject("teamToLike", teamToLike);
		result.addObject("reportedPlayers", reportedPlayers);
		result.addObject("hasPremiumPlayerInTeam", hasPremiumPlayerInTeam);
		
		result.addObject("scrim", scrim);
		result.addObject("creatorTeammates", creatorTeammates);
		result.addObject("guestTeammates", guestTeammates);

		return result;
	}
	
	// Create -----------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam(required = true, value = "videogameId") int videogameId) {
		ModelAndView result;
		Scrim scrim;
		Videogame videogame;
		
		this.actorService.checkIsPlayer();

		videogame = this.videogameService.findOne(videogameId);
		Assert.notNull(videogame);
		
		scrim = this.scrimService.create(videogame);

		result = createEditModelAndView(scrim);

		return result;
	}
	
	// Create Invitation -----------------------------------
	
	@RequestMapping("/invite")
	public ModelAndView invite(@RequestParam(required = true, value = "teamId") int teamId) {
		ModelAndView result;
		Scrim scrim;
		Team guest;
		
		this.actorService.checkIsPlayer();
		
		guest = this.teamService.findOne(teamId);
		Assert.notNull(guest);

		scrim = this.scrimService.create(guest.getVideogame());
		scrim.setGuest(guest);

		result = createEditModelAndView(scrim);

		return result;
	}

	// Edit------------------------------------------------------------
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required = true, value = "scrimId") int scrimId) {
		ModelAndView result;
		Scrim scrim;
		Player player;
		
		player = this.actorService.checkIsPlayer();
		
		scrim = this.scrimService.findOne(scrimId);
		Assert.notNull(scrim);

		Assert.isTrue(scrim.getCreator().getLeader().getPlayer().equals(player));

		result = createEditModelAndView(scrim);

		return result;
	}

	// Save Create ----------------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value = "scrim") Scrim scrim, BindingResult binding) {
		ModelAndView result;
		Player player;
		
		player = actorService.checkIsPlayer();
		
		Assert.isTrue(scrim.getCreator().getLeader().getPlayer().equals(player));

		scrim = this.scrimService.reconstructCreateEdit(scrim, binding);
		
		if (binding.hasErrors()) {
			result = createEditModelAndView(scrim);
		} else {
			try {
				scrim = this.scrimService.createScrimAnnouncement(scrim.getCreator(), scrim.getMoment(), scrim.getDesiredGuestRegion());
				Assert.notNull(scrim);
				result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrim.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(scrim, "scrim.commit.error");
					
				if (scrim.getMoment().before(new Date()))
					result.addObject("pastMoment", "pastMoment");
			}
		}

		return result;
	}
	
	// Save Invitation ----------------------------------------------------------------------

	@RequestMapping(value = "/invite", method = RequestMethod.POST, params = "save")
	public ModelAndView saveInvite(@ModelAttribute(value = "scrim") Scrim scrim, BindingResult binding) {
		ModelAndView result;
		Player player;
		
		player = actorService.checkIsPlayer();
		
		Assert.isTrue(scrim.getCreator().getLeader().getPlayer().equals(player));

		scrim = this.scrimService.reconstructInvite(scrim, binding);
		
		if (binding.hasErrors()) {
			result = createEditModelAndView(scrim);
		} else {
			try {
				scrim = this.scrimService.createScrimInvitation(scrim.getCreator(), scrim.getGuest(), scrim.getMoment());
				Assert.notNull(scrim);
				result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrim.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(scrim, "scrim.commit.error");
					
				if (scrim.getMoment().before(new Date()))
					result.addObject("pastMoment", "pastMoment");
			}
		}

		return result;
	}
	
	// Join  ----------------------------------------------------------------------
	
	@RequestMapping("/join")
	public ModelAndView join(@RequestParam(required = true, value = "scrimId") int scrimId) {
		ModelAndView result;
		Scrim scrim;
		
		this.actorService.checkIsPlayer();

		scrim = this.scrimService.findOne(scrimId);

		result = joinModelAndView(scrim);

		return result;
	}
	
	@RequestMapping(value = "/join", method = RequestMethod.POST, params = "save")
	public ModelAndView joinSave(@ModelAttribute(value = "scrim") Scrim scrim, BindingResult binding) {
		ModelAndView result;
		Player player;
		Scrim scrimOld;
		
		player = actorService.checkIsPlayer();
		
		Assert.isTrue(scrim.getGuest().getLeader().getPlayer().equals(player));

		scrim = this.scrimService.reconstructJoin(scrim, binding);
		
		if (binding.hasErrors()) {
			result = createEditModelAndView(scrim);
		} else {
			try {
				scrimOld = this.scrimService.findOne(scrim.getId());
				scrim = this.scrimService.acceptScrim(scrimOld, scrim.getGuest());
				Assert.notNull(scrim);
				result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrim.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(scrim, "scrim.commit.error");
			}
		}

		return result;
	}
	
	// Accept Invitation  ----------------------------------------------------------------------
	
	@RequestMapping("/acceptInvitation")
	public ModelAndView acceptInvitation(@RequestParam(required = true, value = "scrimId") int scrimId) {
		ModelAndView result;
		Scrim scrim;
		Player player;
		
		player = this.actorService.checkIsPlayer();

		scrim = this.scrimService.findOne(scrimId);
		Assert.notNull(scrim);
		
		Assert.isNull(scrim.isConfirmed());
		Assert.isTrue(scrim.getGuest().getLeader().getPlayer().equals(player));
		
		scrim = this.scrimService.acceptScrim(scrim, scrim.getGuest());
		Assert.notNull(scrim);
		
		result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrim.getId());

		return result;
	}
	
	// Accept Invitation  ----------------------------------------------------------------------
	
	@RequestMapping("/rejectInvitation")
	public ModelAndView rejectInvitation(@RequestParam(required = true, value = "scrimId") int scrimId) {
		ModelAndView result;
		Scrim scrim;
		Player player;
		
		player = this.actorService.checkIsPlayer();

		scrim = this.scrimService.findOne(scrimId);
		Assert.notNull(scrim);
		
		Assert.isNull(scrim.isConfirmed());
		Assert.isTrue(scrim.getGuest().getLeader().getPlayer().equals(player));
		
		scrim = this.scrimService.rejectScrim(scrim, scrim.getGuest());
		Assert.notNull(scrim);
		
		result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrim.getId());

		return result;
	}
	
	// Enter result ---------------------------
	
	@RequestMapping(value = "/win")
	public ModelAndView enterWin(@RequestParam(required = true, value = "scrimId") int scrimId){
		ModelAndView result;
		
		Result scoreResult;
		Scrim scrim;
		Player player;
		
		scrim = this.scrimService.findOne(scrimId);
		Assert.notNull(scrim);
		
		player = this.actorService.checkIsPlayer();
		Assert.isTrue(scrim.getCreator().getLeader().getPlayer().equals(player) ||
				scrim.getGuest().getLeader().getPlayer().equals(player));
		
		Team team;
		if (scrim.getCreator().getLeader().getPlayer().equals(player)) {
			team = scrim.getCreator();
		} else {
			team = scrim.getGuest();
		}
		
		scoreResult = new Result();
		scoreResult.setResult(Result.WIN);
		
		scrim = this.scrimService.insertResult(scrim, team, scoreResult);
		
		result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrimId);
		
		return result;
	}
	
	@RequestMapping(value = "/lose")
	public ModelAndView enterLose(@RequestParam(required = true, value = "scrimId") int scrimId){
		ModelAndView result;
		
		Result scoreResult;
		Scrim scrim;
		Player player;
		
		scrim = this.scrimService.findOne(scrimId);
		Assert.notNull(scrim);
		
		player = this.actorService.checkIsPlayer();
		Assert.isTrue(scrim.getCreator().getLeader().getPlayer().equals(player) ||
				scrim.getGuest().getLeader().getPlayer().equals(player));
		
		Team team;
		if (scrim.getCreator().getLeader().getPlayer().equals(player)) {
			team = scrim.getCreator();
		} else {
			team = scrim.getGuest();
		}
		
		scoreResult = new Result();
		scoreResult.setResult(Result.LOSE);
		
		scrim = this.scrimService.insertResult(scrim, team, scoreResult);
		
		result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrimId);
		
		return result;
	}
	
	@RequestMapping(value = "/draw")
	public ModelAndView enterDraw(@RequestParam(required = true, value = "scrimId") int scrimId){
		ModelAndView result;
		
		Result scoreResult;
		Scrim scrim;
		Player player;
		
		scrim = this.scrimService.findOne(scrimId);
		Assert.notNull(scrim);
		
		player = this.actorService.checkIsPlayer();
		Assert.isTrue(scrim.getCreator().getLeader().getPlayer().equals(player) ||
				scrim.getGuest().getLeader().getPlayer().equals(player));
		
		Team team;
		if (scrim.getCreator().getLeader().getPlayer().equals(player)) {
			team = scrim.getCreator();
		} else {
			team = scrim.getGuest();
		}
		
		scoreResult = new Result();
		scoreResult.setResult(Result.DRAW);
		
		scrim = this.scrimService.insertResult(scrim, team, scoreResult);
		
		result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrimId);
		
		return result;
	}
	
	//Listing--------------------------------
		
	@RequestMapping(value = "/list")
	public ModelAndView list(@ModelAttribute("finder") @Valid Finder finder, BindingResult binding){
		ModelAndView result;
		
		Assert.isTrue(LoginService.isAuthenticated());
		
			Actor principal;
			Finder principalFinder;
			
			principal = actorService.findPrincipal();
			
			Assert.isTrue(principal instanceof Actor);
			
			if(finderService.getFinderFromActor()!=null){
				principalFinder = finderService.getFinderFromActor();
			}else{
				principalFinder = finderService.create();
				principalFinder= finderService.save(principalFinder);
			}
			
			if(binding.hasErrors()){
				result = new ModelAndView("scrim/list");
				
				result.addObject("scrims", principalFinder.getScrims());
				result.addObject("videogames", videogameService.findAll());
				result.addObject("regions", regionService.findAll());

				result.addObject("requestURI", "scrim/list.do");
				result.addObject("finder", principalFinder);
				
			}else{
				try{				
					principalFinder = finderService.updateFinder(principalFinder, finder);
					
					result = new ModelAndView("scrim/list");

					result.addObject("scrims", principalFinder.getScrims());
					result.addObject("regions", regionService.findAll());
					result.addObject("videogames", videogameService.findAll());
					result.addObject("requestURI", "scrim/list.do");
					result.addObject("finder", finder);

				}catch(Throwable oops){
					result = new ModelAndView("scrim/list");
					
					result.addObject("scrims", finderService.filterScrimsByCriteria(null, null, null));
					result.addObject("regions", regionService.findAll());
					result.addObject("videogames", videogameService.findAll());
					result.addObject("requestURI", "scrim/list.do");
					result.addObject("finder", principalFinder);
				}
			}		
		return result;
	}

	// Ancillary methods ------------------------------------------------------
		
	private ModelAndView createEditModelAndView(Scrim scrim) {
		ModelAndView result;

		result = createEditModelAndView(scrim, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Scrim scrim, String messageCode) {
		ModelAndView result;
		Collection<Region> regions;
		Collection<Team> teams;
		Player player;
		GameAccount gameAccount;
		
		player = this.actorService.checkIsPlayer();
		
		regions = this.regionService.findAll();
		gameAccount = this.gameAccountService.getGameAccountForPlayerAndVideogame(player, scrim.getSeason().getVideogame());
		Assert.notNull(gameAccount);
		
		teams = gameAccount.getTeams();
		Assert.notEmpty(teams);
		
		result = new ModelAndView("scrim/edit");

		result.addObject("teams", teams);
		result.addObject("regions", regions);
		result.addObject("scrim", scrim);
		result.addObject("message", messageCode);

		return result;
	}
	
	private ModelAndView joinModelAndView(Scrim scrim) {
		ModelAndView result;

		result = joinModelAndView(scrim, null);

		return result;
	}

	private ModelAndView joinModelAndView(Scrim scrim, String messageCode) {
		ModelAndView result;
		Collection<Team> allTeams;
		Collection<Team> teams;
		Player player;
		GameAccount gameAccount;
		Region international;
		
		player = this.actorService.checkIsPlayer();
		
		gameAccount = this.gameAccountService.getGameAccountForPlayerAndVideogame(player, scrim.getSeason().getVideogame());
		Assert.notNull(gameAccount);
		
		allTeams = gameAccount.getTeams();
		
		international = this.regionService.getInternational();
		
		teams = new ArrayList<Team> ();
		if (!scrim.getDesiredGuestRegion().equals(international)) {
			for (Team team : allTeams) {
				if (team.getRegion().equals(scrim.getDesiredGuestRegion())) {
					teams.add(team);
				}
			}
		} else {
			teams = allTeams;
		}
		
		teams.remove(scrim.getCreator());
		
		result = new ModelAndView("scrim/join");
		
		if (teams.size() == 0) {
			result.addObject("noTeams", "true");
		} else {
			result.addObject("teams", teams);
			result.addObject("scrim", scrim);
			result.addObject("message", messageCode);
		}
		
		return result;
	}
	
}
