package controllers.invitation;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.GameAccountService;
import services.InvitationService;
import services.TeamService;
import controllers.AbstractController;
import domain.GameAccount;
import domain.Invitation;
import domain.Player;
import domain.Team;

@Controller
@RequestMapping("/invitation")
public class InvitationController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private InvitationService invitationService;
	
	// Constructor -----------------------------------------------------------

	public InvitationController() {
		super();
	}
	
	// Invite --------------------------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView invite(@RequestParam(required = true, value = "teamId") int teamId) {
		ModelAndView result;
		Team team;
		Player principal;
		Invitation invitation;
		
		principal = this.actorService.checkIsPlayer();
		
		team = this.teamService.findOne(teamId);
		Assert.notNull(team);
		
		Assert.isTrue(team.getLeader().getPlayer().equals(principal));
		
		invitation = this.invitationService.create(team);

		result = createEditModelAndView(invitation);

		return result;
	}
	
	// Save --------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid @ModelAttribute(value = "invitation") Invitation invitation, BindingResult binding) {
		ModelAndView result;
		Player player;
		
		player = this.actorService.checkIsPlayer();
		
		Assert.isTrue(invitation.getTeam().getLeader().getPlayer().equals(player));

		if (binding.hasErrors()) {
			result = createEditModelAndView(invitation);
		} else {
			try {
				invitation = this.teamService.inviteTeammate(invitation.getTeam(), invitation.getTeammate(), invitation.getMessage(), true);
				result = new ModelAndView("redirect:/team/display.do?teamId=" + invitation.getTeam().getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(invitation, "invitation.commit.error");
			}
		}

		return result;
	}
	
	// Accept --------------------------------------------------------
	
	@RequestMapping("/accept")
	public ModelAndView accept(@RequestParam(required = true, value = "invitationId") int invitationId) {
		ModelAndView result;
		Player principal;
		Invitation invitation;
		
		principal = this.actorService.checkIsPlayer();
		
		invitation = this.invitationService.findOne(invitationId);
		Assert.notNull(invitation);
		
		Assert.isTrue(invitation.getTeammate().getPlayer().equals(principal));
		Assert.isNull(invitation.getMoment());
		
		this.invitationService.acceptInvitation(invitation);
		
		result = new ModelAndView("redirect:/team/display.do?teamId=" + invitation.getTeam().getId());

		return result;
	}
	
	// Reject --------------------------------------------------------
	
	@RequestMapping("/reject")
	public ModelAndView reject(@RequestParam(required = true, value = "invitationId") int invitationId) {
		ModelAndView result;
		Player principal;
		Invitation invitation;
		
		principal = this.actorService.checkIsPlayer();
		
		invitation = this.invitationService.findOne(invitationId);
		Assert.notNull(invitation);
		
		Assert.isTrue(invitation.getTeammate().getPlayer().equals(principal));
		Assert.isNull(invitation.getMoment());
		
		this.invitationService.rejectInvitation(invitation);
		
		result = new ModelAndView("redirect:/player/display.do?playerId=" + principal.getId());

		return result;
	}
		
	// Ancillary methods ------------------------------------------------------
		
	private ModelAndView createEditModelAndView(Invitation invitation) {
		ModelAndView result;

		result = createEditModelAndView(invitation, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Invitation invitation, String messageCode) {
		ModelAndView result;
		Collection<GameAccount> gameAccounts;
		
		gameAccounts = this.gameAccountService.getGameAccountsNotTeam(invitation.getTeam());
		
		result = new ModelAndView("invitation/create");

		result.addObject("gameAccounts", gameAccounts);
		result.addObject("invitation", invitation);
		result.addObject("message", messageCode);

		return result;
	}
}
