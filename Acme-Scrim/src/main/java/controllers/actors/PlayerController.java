package controllers.actors;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.AdministratorService;
import services.GameAccountService;
import services.MessageService;
import services.PermissionService;
import services.PlayerService;
import services.SocialIdentityService;
import services.TeamService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.GameAccount;
import domain.Player;
import domain.Priority;
import domain.Team;
import form.RegisterActor;

@Controller
@RequestMapping("/player")
public class PlayerController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private SocialIdentityService socialIdentityService;
	
	// Constructor -----------------------------------------------------------

	public PlayerController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required = false, value = "playerId") Integer playerId, 
			@RequestParam(required = false, value = "gameAccountId") Integer gameAccountId){
		ModelAndView result;
		Player player;

		player = playerId == null? this.actorService.checkIsPlayer():this.playerService.findOne(playerId);
		
		Assert.notNull(player);
		
		Collection<GameAccount> gameAccounts;
		GameAccount gameAccount;
		Collection<Team> teams;
		
		gameAccounts = this.gameAccountService.getGameAccountsForPlayer(player);
		
		teams = null;
		gameAccount = null;
		if (gameAccountId != null) {
			gameAccount = this.gameAccountService.findOne(gameAccountId);
			Assert.notNull(gameAccount);
			
			teams = this.teamService.getTeamsOfGameAccount(gameAccount);
		}
		
		result = new ModelAndView("actor/player/display");
		
		if(playerId == null || (LoginService.isAuthenticated()&& actorService.findPrincipal().getId() == playerId)){
			result.addObject("profile", true);
		}
		
		if(player.getUserAccount().isAccountNonLocked()){
			result.addObject("isBanned", false);
		}else{
			result.addObject("isBanned", true);
		}
		
		result.addObject("requestURI", "/player/display.do");
		result.addObject("actor", player);
		result.addObject("editURI", "player/edit.do?playerId=");
		result.addObject("socialIdentities", socialIdentityService.getSocialIdentitiesByActor(player.getId()));
		result.addObject("gameAccounts", gameAccounts);
		result.addObject("gameAccount", gameAccount);
		result.addObject("teams", teams);
		
		return result;
	}
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView result;
		Collection<Player> players;
		
		players = playerService.findAll();
		result = new ModelAndView("actor/player/list");
		result.addObject("actors", players);
		result.addObject("headTitle", "Players");
		result.addObject("displayURI", "player/display.do?playerId=");
		result.addObject("requestURI", "player/list.do");

		return result;
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		RegisterActor formObject;
		
		formObject = new RegisterActor();
		result = this.createEditModelAndView(formObject);
		result.addObject("backURI", "");

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("registerActor") RegisterActor registerActor, BindingResult binding){
		ModelAndView result;
		Player player;
		
		if(!registerActor.getTermsAccepted()){
			result = createEditModelAndView(registerActor);
			result.addObject("showMessageTerms", true);
			return result;
		}
		else if(!registerActor.getPassword().equals(registerActor.getUserAccount().getPassword())){
			result = createEditModelAndView(registerActor);
			result.addObject("showMessageNotMatchingPasswords", true);
			return result;
		}
		
		player = playerService.reconstruct(registerActor, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(registerActor);
			
		} else { 
			try {
				playerService.save(player);
				result = new ModelAndView("redirect:/");
				
			} catch (Throwable oops) {
				result = createEditModelAndView(registerActor, "actor.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required = true) Integer playerId) {
		actorService.checkIsPlayer();
		Player player;
		Player principal;
		ModelAndView result;
		
		player = playerService.findOne(playerId);
		principal = (Player)actorService.findPrincipal();
		
		Assert.notNull(player);
		Assert.isTrue(principal.equals(player));
		
		result = new ModelAndView("actor/player/edit");
		result.addObject("registerActor", player);
		result.addObject("requestURI", "player/edit.do");
		result.addObject("backURI", "player/display.do");
		result.addObject("edit", true);

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="registerActor") Player player, BindingResult binding){
		Assert.notNull(player);
		actorService.checkIsPlayer();
		Player principal;
		ModelAndView result;
		
		principal = (Player)actorService.findPrincipal();
		Assert.notNull(player);
		Assert.isTrue(principal.getId() == player.getId());
		
		player = playerService.reconstructEdit(player, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("actor/player/edit");
			result.addObject("registerActor", player);
			result.addObject("requestURI", "player/edit.do");
			result.addObject("edit", true);
		} else { 
			try {
				player = playerService.save(player);
				
				result = new ModelAndView("redirect:/player/display.do");
				
			} catch (Throwable oops) {
				result = new ModelAndView("actor/player/edit");
				result.addObject("registerActor", player);
				result.addObject("requestURI", "player/edit.do");
				result.addObject("message", "actor.commit.error");
				result.addObject("edit", true);
			}
		}
		return result;
	}
	
	@RequestMapping("/askRefereePermission")
	public ModelAndView askRefereePermission() {
		Player player;
		actorService.checkIsPlayer();
		ModelAndView result;
		Collection<Administrator> admins;
		Collection<Actor> recipients;
		
		Assert.isTrue(!permissionService.hasRefereePermission(actorService.findPrincipal().getId()));
		
		player = (Player)actorService.findPrincipal();
		
		String subject;
		String body;
		
		subject = "Player '" + player.getUserAccount().getUsername() + "' referee permission";
		subject += " -- ";
		subject += "Solicitud de �rbitro del jugador '" + player.getUserAccount().getUsername() + "'";
		
		body = "Player '" + player.getUserAccount().getUsername() + "' has requested referee permission.";
		body += "\n -- \n";
		body += "El jugador '" + player.getUserAccount().getUsername() + "' ha solicitado permiso para registrarse como �rbitro.";
		
		admins = administratorService.findAll();
		recipients = new ArrayList<Actor> (admins);
		
		messageService.sendNotification(subject, body, recipients, Priority.HIGH);
		result = new ModelAndView("redirect:/");
		
		return result;
	}
	
	
	@RequestMapping("/askManagerPermission")
	public ModelAndView askManagerPermission() {
		Player player;
		actorService.checkIsPlayer();
		ModelAndView result;
		Collection<Administrator> admins;
		Collection<Actor> recipients;
		
		Assert.isTrue(!permissionService.hasManagerPermission(actorService.findPrincipal().getId()));
		
		player = (Player)actorService.findPrincipal();
		
		String subject;
		String body;
		
		subject = "Player '" + player.getUserAccount().getUsername() + "' manager permission";
		subject += " -- ";
		subject += "Solicitud de gestor del jugador '" + player.getUserAccount().getUsername() + "'";
		
		body = "Player '" + player.getUserAccount().getUsername() + "' has requested manager permission.";
		body += "\n -- \n";
		body += "El jugador '" + player.getUserAccount().getUsername() + "' ha solicitado permiso para registrarse como gestor.";
		
		result = new ModelAndView("redirect:/");

		admins = administratorService.findAll();
		recipients = new ArrayList<Actor> (admins);
		
		messageService.sendNotification(subject, body, recipients, Priority.HIGH);
		
		return result;
	}
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(RegisterActor registerActor) {
		ModelAndView result; 
		
		result = createEditModelAndView(registerActor, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(RegisterActor registerActor, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/player/create");
		result.addObject("registerActor", registerActor);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "player/create.do");
		
		return result;
	}

}
