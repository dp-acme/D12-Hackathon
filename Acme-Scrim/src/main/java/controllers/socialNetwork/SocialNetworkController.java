package controllers.socialNetwork;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.SocialNetworkService;
import controllers.AbstractController;
import domain.SocialNetwork;

@Controller
@RequestMapping("/socialNetwork")
public class SocialNetworkController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private SocialNetworkService socialNetworkService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor -----------------------------------------------------------

	public SocialNetworkController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/list")
	public ModelAndView list(){
		actorService.checkIsAdministrator();
		ModelAndView result;
		Collection<SocialNetwork> socialNetworks;
		
		socialNetworks = socialNetworkService.findAll();
		result = new ModelAndView("socialNetwork/list");
		
		result.addObject("socialNetworks", socialNetworks);
		
		return result;
	}
	
	@RequestMapping(value = "/create")
	public ModelAndView create(@RequestParam(required = false) Integer socialNetworkId) {
		ModelAndView result;
		SocialNetwork socialNetwork;
		
		actorService.checkIsAdministrator();
		if(socialNetworkId != null){
			socialNetwork = socialNetworkService.findOne(socialNetworkId);
		}else{
			socialNetwork = socialNetworkService.create();
		}
		
		Assert.notNull(socialNetwork);
		result = new ModelAndView("socialNetwork/create");
		result.addObject("socialNetwork", socialNetwork);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute(value = "socialNetwork")SocialNetwork socialNetwork, BindingResult binding) {
		ModelAndView result;
		
		actorService.checkIsAdministrator();
		
		if(binding.hasErrors()){
			result = new ModelAndView("socialNetwork/create");
			result.addObject("socialNetwork", socialNetwork);
			
		} else{
			try{
				socialNetworkService.save(socialNetwork);
				
				result = new ModelAndView("redirect:/socialNetwork/list.do");
		
			} catch (Throwable oops) {
				result = new ModelAndView("socialNetwork/create");
				result.addObject("socialNetwork", socialNetwork);
				result.addObject("message", "socialNetwork.commit.error");
			}
		}	
		return result;
	}
	
	@RequestMapping(value = "/delete")
	public ModelAndView delete(@RequestParam(required = true) Integer socialNetworkId) {
		ModelAndView result;
		
		actorService.checkIsAdministrator();
		socialNetworkService.delete(socialNetworkId);
		
		result = new ModelAndView("redirect:/socialNetwork/list.do");
		
		return result;
	}
}
