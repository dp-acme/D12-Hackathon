package controllers.gameAccount;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.GameAccountService;
import services.RegionService;
import services.VideogameService;
import controllers.AbstractController;
import domain.GameAccount;
import domain.Player;
import domain.Region;
import domain.Videogame;

@Controller
@RequestMapping("/gameAccount")
public class GameAccountController extends AbstractController {
	
	// Services ---------------------------------------------------------------

	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RegionService regionService;
	
	@Autowired
	private VideogameService videogameService;
	
	// Constructor -----------------------------------------------------------

	public GameAccountController() {
		super();
	}
	
	// Create -----------------------------------
	
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(required = true, value = "videogameId") int videogameId) {
		ModelAndView result;
		Collection <GameAccount> gameAccounts;
		Videogame videogame;
		
		videogame = this.videogameService.findOne(videogameId);

		gameAccounts = this.videogameService.listOfGameAccountsOfVideogame(videogameId);

		result = new ModelAndView("gameAccount/list");

		result.addObject("videogame", videogame);
		result.addObject("gameAccounts", gameAccounts);
		
		return result;
	}
	
	// Create -----------------------------------
		
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam(required = false, value = "videogameId") Integer videogameId) {
		ModelAndView result;
		GameAccount gameAccount;
		Player player;
		
		player = this.actorService.checkIsPlayer();

		gameAccount = this.gameAccountService.create(player);

		if (videogameId != null) {
			Videogame videogame;
			
			videogame = this.videogameService.findOne(videogameId);
			if (videogame != null) {
				gameAccount.setVideogame(videogame);
			}
		}
		
		result = createEditModelAndView(gameAccount);

		return result;
	}

	// Edit------------------------------------------------------------
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required = true, value = "gameAccountId") int gameAccountId, 
			@RequestParam(required = false, value = "videogameKeyword") String videogameKeyword) {
		ModelAndView result;
		GameAccount gameAccount;
		Player player;
		
		player = this.actorService.checkIsPlayer();
		
		gameAccount = this.gameAccountService.findOne(gameAccountId);
		Assert.notNull(gameAccount);

		Assert.isTrue(gameAccount.getPlayer().equals(player));

		result = createEditModelAndView(gameAccount);

		return result;
	}

	// Delete--------------------------------------------------------
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@ModelAttribute(value = "gameAccount") GameAccount gameAccount) {
		ModelAndView result;
		Player player;
		
		player = this.actorService.checkIsPlayer();
		
		gameAccount = this.gameAccountService.findOne(gameAccount.getId());
		Assert.notNull(gameAccount);
		Assert.isTrue(gameAccount.getPlayer().equals(player));
		
		try {
			this.gameAccountService.delete(gameAccount);
			result = new ModelAndView("redirect:/player/display.do");
		} catch (Throwable oops) {
			result = createEditModelAndView(gameAccount, "gameAccount.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value = "gameAccount") GameAccount gameAccount, BindingResult binding) {
		ModelAndView result;
		Player player;
		
		player = actorService.checkIsPlayer();
		
		gameAccount = this.gameAccountService.reconstruct(gameAccount, binding);
		Assert.notNull(gameAccount);
		
		Assert.isTrue(gameAccount.getPlayer().equals(player));

		if (binding.hasErrors()) {
			result = createEditModelAndView(gameAccount);
		} else {
			try {
				gameAccount = this.gameAccountService.save(gameAccount);
				result = new ModelAndView("redirect:/player/display.do?gameAccountId=" + gameAccount.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(gameAccount, "gameAccount.commit.error");
			}
		}

		return result;
	}

	// Ancillary methods ------------------------------------------------------
		
	private ModelAndView createEditModelAndView(GameAccount gameAccount) {
		ModelAndView result;

		result = createEditModelAndView(gameAccount, null);

		return result;
	}

	private ModelAndView createEditModelAndView(GameAccount gameAccount, String messageCode) {
		ModelAndView result;
		Collection<Region> regions;
		Collection<Videogame> videogames;
		
		regions = this.regionService.findAll();
		videogames = this.videogameService.getVideogamesWithoutGameAccount(gameAccount.getPlayer());
		
		result = new ModelAndView("gameAccount/edit");

		result.addObject("videogames", videogames);
		result.addObject("regions", regions);
		result.addObject("gameAccount", gameAccount);
		result.addObject("message", messageCode);

		return result;
	}
	
}
