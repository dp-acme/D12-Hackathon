package controllers.team;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.GameAccountService;
import services.MessageService;
import services.RegionService;
import services.ReviewService;
import services.ScrimService;
import services.TeamService;
import services.VideogameService;
import controllers.AbstractController;
import domain.Actor;
import domain.GameAccount;
import domain.Invitation;
import domain.Message;
import domain.Player;
import domain.Region;
import domain.Scrim;
import domain.Team;
import domain.Videogame;

@Controller
@RequestMapping("/team")
public class TeamController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RegionService regionService;
	
	@Autowired
	private VideogameService videogameService;
	
	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private ScrimService scrimService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ReviewService reviewService;
	
	// Constructor -----------------------------------------------------------

	public TeamController() {
		super();
	}
	
	// Display -----------------------------------------------------------
	
	@RequestMapping(value = "/display")
	public ModelAndView display(@RequestParam(required = true, value = "teamId") int teamId) {
		ModelAndView result;
		
		Team team;
		List<Invitation> invitations;
		Collection<Scrim> scrims;
		Collection<Player> players;
		Message newMessage;
		int numberLikes;
		int numberDislikes;
		
		team = teamService.findOne(teamId);
		Assert.notNull(team);
		
		invitations = new ArrayList<Invitation> (team.getInvitations());
		
		Invitation leaderInvit = new Invitation();
		leaderInvit.setTeammate(team.getLeader());
		
		invitations.add(0, leaderInvit);
		
		scrims = this.scrimService.getTeamScrims(team);
		players = this.teamService.getPlayerTeammates(team, true);
		
		newMessage = this.messageService.create(null, null, new ArrayList<Actor> (players));;
		
		numberLikes = this.reviewService.getNumberLikesByTeam(teamId);
		numberDislikes = this.reviewService.getNumberDislikesByTeam(teamId);
		
		result = new ModelAndView("team/display");
		
		Actor principal;
		if(LoginService.isAuthenticated()){
			principal = actorService.findPrincipal();
			if(principal instanceof Player && !team.getLeader().getPlayer().equals(principal)){
				GameAccount gameAccount;
				
				gameAccount = this.gameAccountService.getGameAccountForPlayerAndVideogame((Player)principal, team.getVideogame());
				
				if (gameAccount != null) {
					result.addObject("myTeams", gameAccount.getTeams());
				}
			}
			
			if(principal.getTeamsFollowed().contains(team)){
				result.addObject("isFollowed", true);
			}else{
				result.addObject("isFollowed", false);
			}
		}
		result.addObject("team", team);
		result.addObject("numberLikes", numberLikes);
		result.addObject("numberDislikes", numberDislikes);
		result.addObject("invitations", invitations);
		result.addObject("scrims", scrims);
		result.addObject("newMessage", newMessage);
		
		return result;
	}
	
	// List -----------------------------------------------------------
	
	@RequestMapping(value = "/list")
	public ModelAndView list(@RequestParam(required = true, value = "videogameId") int videogameId) {
		ModelAndView result;
		
		Videogame videogame;
		List<Team> teamsLikesList;
		List<Team> teamsWinsList;
		List<Integer> teamsLikesNumber;
		List<Integer> teamsWinsNumber;
		List<Object[]> teamsLikes;
		List<Object[]> teamsWins;
		
		videogame = this.videogameService.findOne(videogameId);
		Assert.notNull(videogame);
		
		teamsLikes = new ArrayList<Object[]> (this.teamService.getTeamsOrderedByLikes(videogame));
		teamsWins = new ArrayList<Object[]> (this.teamService.getTeamsOrderedByWonGames(videogame));
		
		teamsLikesList = new ArrayList<Team> ();
		teamsWinsList = new ArrayList<Team> ();
		teamsLikesNumber = new ArrayList<Integer> ();
		teamsWinsNumber = new ArrayList<Integer> ();
		
		for (Object[] array : teamsLikes) {
			teamsLikesList.add((Team) array[0]);
			teamsLikesNumber.add(Math.round((Float)array[1]));
		}
		
		for (Object[] array : teamsWins) {
			teamsWinsList.add((Team) array[0]);
			teamsWinsNumber.add(Math.round((Float)array[1]));
		}
		
		result = new ModelAndView("team/list");
		
		result.addObject("teamsLikesList", teamsLikesList);
		result.addObject("teamsWinsList", teamsWinsList);
		result.addObject("teamsLikesNumber", teamsLikesNumber);
		result.addObject("teamsWinsNumber", teamsWinsNumber);
		
		return result;
	}
	
	// Create -----------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam(required = false, value = "videogameId") Integer videogameId) {
		ModelAndView result;
		Team team;
		
		this.actorService.checkIsPlayer();

		team = this.teamService.create();

		if (videogameId != null) {
			Videogame videogame;
			
			videogame = this.videogameService.findOne(videogameId);
			if (videogame != null) {
				team.setVideogame(videogame);
			}
		}
		
		result = createEditModelAndView(team);

		return result;
	}

	// Edit ------------------------------------------------------------
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required = true, value = "teamId") int teamId) {
		ModelAndView result;
		Team team;
		Player player;
		
		player = this.actorService.checkIsPlayer();
		
		team = this.teamService.findOne(teamId);
		Assert.notNull(team);

		Assert.isTrue(team.getLeader().getPlayer().equals(player));

		result = createEditModelAndView(team);

		return result;
	}

	// Delete --------------------------------------------------------
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@ModelAttribute(value = "team") Team team) {
		ModelAndView result;
		Player player;
		
		player = this.actorService.checkIsPlayer();
		
		team = this.teamService.findOne(team.getId());
		Assert.notNull(team);
		Assert.isTrue(team.getLeader().getPlayer().equals(player));
		
		try {
			this.teamService.delete(team);
			result = new ModelAndView("redirect:/player/display.do?playerId=" + player.getId() + "&gameAccountId=" + team.getLeader().getId());
		} catch (Throwable oops) {
			result = createEditModelAndView(team, "team.commit.error");
		}

		return result;
	}
	
	// Save --------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value = "team") Team team, BindingResult binding) {
		ModelAndView result;
		Player player;
		
		player = this.actorService.checkIsPlayer();
		
		team = this.teamService.reconstruct(team, binding);
		Assert.notNull(team);
		
		Assert.isTrue(team.getLeader().getPlayer().equals(player));

		if (binding.hasErrors()) {
			result = createEditModelAndView(team);
		} else {
			try {
				team = this.teamService.save(team);
				result = new ModelAndView("redirect:/team/display.do?teamId=" + team.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(team, "team.commit.error");
			}
		}

		return result;
	}
	
	// Kick --------------------------------------------------------

	@RequestMapping("/kick")
	public ModelAndView kick(@RequestParam(required = true, value = "teamId") int teamId,
			@RequestParam(required = true, value = "gameAccountId") int gameAccountId) {
		ModelAndView result;
		Team team;
		Player principal;
		GameAccount teammate;
		
		principal = this.actorService.checkIsPlayer();
		
		team = this.teamService.findOne(teamId);
		Assert.notNull(team);
		
		teammate = this.gameAccountService.findOne(gameAccountId);
		Assert.notNull(teammate);

		Assert.isTrue(team.getLeader().getPlayer().equals(principal));

		this.teamService.kickTeammate(team, teammate);
		
		result = new ModelAndView("redirect:/team/display.do?teamId=" + team.getId());

		return result;
	}
	
	// Change Leader --------------------------------------------------------

	@RequestMapping("/changeLeader")
	public ModelAndView changeLeader(@RequestParam(required = true, value = "teamId") int teamId,
			@RequestParam(required = true, value = "gameAccountId") int gameAccountId) {
		ModelAndView result;
		Team team;
		Player principal;
		GameAccount teammate;
		
		principal = this.actorService.checkIsPlayer();
		
		team = this.teamService.findOne(teamId);
		Assert.notNull(team);
		
		teammate = this.gameAccountService.findOne(gameAccountId);
		Assert.notNull(teammate);

		Assert.isTrue(team.getLeader().getPlayer().equals(principal));

		this.teamService.changeTeammateLeader(team, teammate, true);
		
		result = new ModelAndView("redirect:/team/display.do?teamId=" + team.getId());

		return result;
	}
	
	// Exit --------------------------------------------------------

	@RequestMapping("/exit")
	public ModelAndView exit(@RequestParam(required = true, value = "teamId") int teamId) {
		ModelAndView result;
		Team team;
		Player principal;
		GameAccount gameAccount;
		Collection<GameAccount> teammates;
		
		principal = this.actorService.checkIsPlayer();
		
		team = this.teamService.findOne(teamId);
		Assert.notNull(team);
		
		gameAccount = this.gameAccountService.getGameAccountForPlayerAndVideogame(principal, team.getVideogame());
		Assert.notNull(gameAccount);

		teammates = this.teamService.getRealTeammates(team, false);
		Assert.isTrue(teammates.contains(gameAccount));
		
		this.teamService.exit(team);
		
		result = new ModelAndView("redirect:/team/display.do?teamId=" + team.getId());

		return result;
	}
	
	// Ancillary methods ------------------------------------------------------
		
	private ModelAndView createEditModelAndView(Team team) {
		ModelAndView result;

		result = createEditModelAndView(team, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Team team, String messageCode) {
		ModelAndView result;
		Collection<Region> regions;
		Collection<Videogame> videogames;
		Player player;
		
		player = this.actorService.checkIsPlayer();
		
		regions = this.regionService.findAll();
		videogames = this.videogameService.getVideogamesWithGameAccount(player);
		
		result = new ModelAndView("team/edit");

		result.addObject("videogames", videogames);
		result.addObject("regions", regions);
		result.addObject("team", team);
		result.addObject("message", messageCode);

		return result;
	}
}
