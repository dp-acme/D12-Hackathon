package controllers.report;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.PlayerService;
import services.ReportService;
import services.ScrimService;
import controllers.AbstractController;
import domain.Player;
import domain.Report;
import domain.Scrim;

@Controller
@RequestMapping("/report/player")
public class ReportController extends AbstractController{

	
	@Autowired
	private ReportService reportService;
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private ScrimService scrimService;
	
	@Autowired
	private ActorService actorService;
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("scrimId") int scrimId, @ModelAttribute("playerId") int playerId, Report report, BindingResult binding){
		ModelAndView result;
		Scrim scrim;
		Player player;
		
		scrim = scrimService.findOne(scrimId);
		player = playerService.findOne(playerId);
		
		report.setPlayer(player);
		report.setScrim(scrim);
		
		result = new ModelAndView("report/create");
		
		if(binding.hasErrors()){			
			result.addObject("message", "report.create.error");
		} else {
			try{
				reportService.save(report);

				result = new ModelAndView("redirect:/scrim/display.do?scrimId="+report.getScrim().getId());
				
			} catch (Throwable oops) {				
				result = new ModelAndView("report/create");
				result.addObject("message", "report.commit.error");				
			}
		}
		
		return result;
	}
	
	@RequestMapping("/create")
	public ModelAndView create(@ModelAttribute("scrimId") int scrimId, @ModelAttribute("playerId") int playerId) {
		ModelAndView result;
		Report report;
		Player principal;
		Collection<Player> players;
		Player player;
		
		player = playerService.findOne(playerId);
		Assert.notNull(player);
		
		principal = actorService.checkIsPlayer();
		report = reportService.create(playerId, scrimId);
		
		players = playerService.getPlayersByScrim(report.getScrim().getId());
		
		Assert.isTrue(report.getScrim().getCreator().getLeader().getPlayer().equals(principal) || report.getScrim().getGuest().getLeader().getPlayer().equals(principal));
		Assert.isTrue(players.contains(player));
		Assert.isTrue(reportService.getReportsByPlayerAndScrim(player.getId(), report.getScrim().getId()).size() == 0);

		result = new ModelAndView("report/create");
		
		result.addObject("report", report);

		return result;
	}
	
}
