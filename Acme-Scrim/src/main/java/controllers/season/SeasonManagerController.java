package controllers.season;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.SeasonService;
import services.VideogameService;
import controllers.AbstractController;
import domain.Manager;
import domain.Scrim;
import domain.Season;
import domain.Videogame;

@Controller
@RequestMapping("/season/manager")
public class SeasonManagerController extends AbstractController {

	// Services -------------------------------------
	@Autowired
	private SeasonService seasonService;

	@Autowired
	private ActorService actorService;

	@Autowired
	private VideogameService videogameService;

	// Contructor -----------------------------------
	public SeasonManagerController() {
		super();
	}

	// List -----------------------------------------
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam int videogameId,
			@RequestParam(required = false) Integer seasonId) {
		// Creamos el objeto a devolver
		ModelAndView result;
		Manager manager = this.actorService.checkIsManager();
		// COmporbamo si el videogame no esta en la bd
		Videogame videogame;
		videogame = this.videogameService.findOne(videogameId);
		Assert.notNull(videogame);
		Assert.isTrue(videogame.getManager().equals(manager));

		Collection<Season> temporadas = videogame.getSeasons();
		Season seasonAMostrar = null;
		Collection<Scrim> scrimsActuales = new ArrayList<Scrim>();
		Collection<Scrim> scrimsPasadas = new ArrayList<Scrim>();

		if (seasonId != null) {
			seasonAMostrar = this.seasonService.findOne(seasonId);
			temporadas.remove(seasonAMostrar);
			scrimsActuales = this.seasonService
					.findScrimsNoPassedOfSeason(seasonAMostrar);
			scrimsPasadas = this.seasonService
					.findScrimsPassedOfSeason(seasonAMostrar);

		} else {
			Season lista = this.seasonService
					.getLastSeasonOfVideogame(videogameId);
			if (lista != null) {
				seasonAMostrar = lista;
				scrimsActuales = this.seasonService
						.findScrimsNoPassedOfSeason(seasonAMostrar);
				scrimsPasadas = this.seasonService
						.findScrimsPassedOfSeason(seasonAMostrar);
				temporadas.remove(seasonAMostrar);
			}
		}
		// Inicializamos la variable a devolver
		result = new ModelAndView("season/list");

		// Al modelo y a la vista le a�adimos los siguientes atributos
		result.addObject("seasons", temporadas);
		result.addObject("currentSeason", seasonAMostrar);
		result.addObject("requestURI", "season/manager/list.do");
		result.addObject("scrimsPasadas", scrimsPasadas);
		result.addObject("scrimsNoPasadas", scrimsActuales);
		result.addObject("videogame", videogame);
		// Devolvemos el resultado
		return result;

	}

	// Create -----------------------------------
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam int videogameId) {
		// Comprobamos que el parametro que le pasamos no es nulo
		Assert.isTrue(videogameId != 0);

		ModelAndView result;
		Season season;
		this.actorService.checkIsManager();
		// Comprobamos
		season = this.seasonService.create(videogameId);

		result = createEditModelAndView(season);
		result.addObject("request","season/manager/list.do?videogameId=" + videogameId);
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(
			@ModelAttribute(value = "season") Season season,
			BindingResult binding) {
		ModelAndView result;

		season = seasonService.reconstruct(season,
				binding);

		if (binding.hasErrors()) {

			result = createEditModelAndView(season);

		} else {
			try {
				season = this.seasonService.save(season);
				result = new ModelAndView(
						"redirect:/season/manager/list.do?videogameId="
								+ season.getVideogame().getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(season,
						"season.commit.error");
			}
		}

		return result;
	}

	private ModelAndView createEditModelAndView(Season season) {
		ModelAndView result;

		result = createEditModelAndView(season, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Season season,
			String messageCode) {
		ModelAndView result;

		result = new ModelAndView("season/manager/create");

		result.addObject("season", season);
		result.addObject("message", messageCode);
		return result;
	}

}