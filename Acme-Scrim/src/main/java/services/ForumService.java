
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ForumRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Forum;
import domain.ForumCategory;
import domain.Videogame;

@Service
@Transactional
public class ForumService {

	// Validator ---------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	// Managed repository ------------------------------------
	
	@Autowired
	private	ForumRepository	forumRepository;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService	actorService;
	
	@Autowired
	private ForumMessageService	forumMessageService;
	
	@Autowired
	private ForumCategoryService	forumCategoryService;

	// Constructors --------------------------------------------
	
	public ForumService() {
		super();
	}
	
	// Simple CRUD methods -------------------------------------
	
	public Forum create(Videogame videogame) {
		Forum result;
		ForumCategory rootCategory;
		
		result = new Forum();
		result.setForumCategories(new ArrayList<ForumCategory>());

		rootCategory = forumCategoryService.findRootForumCategory(videogame);
		result.getForumCategories().add(rootCategory);
		
		return result;
	}
	public Collection<Forum> findAll() {
		Collection<Forum> result;

		result = this.forumRepository.findAll();
		Assert.notNull(result);

		return result;
	}
	
	public Forum findOne(int forumId) {
		Assert.isTrue(forumId != 0);
		
		Forum result;

		result = this.forumRepository.findOne(forumId);

		return result;
	}
	public Forum save(Forum forum) {
		Assert.notNull(forum);
		
		Forum result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();

		Assert.isTrue(forum.getForumCategories().size()>0);
		Assert.isTrue(getVideogameFromForum(forum).getManager().getUserAccount().equals(principal));
		
		Boolean first=true;
		Videogame videogame=null;
		for(ForumCategory f: forum.getForumCategories()){
			if(first){
				videogame=f.getVideogame();
			}else{
				Assert.isTrue(f.getVideogame().equals(videogame));
			}
		}

		result = this.forumRepository.save(forum);
		
		return result;
	}
	public void delete(Forum forum){		
		Actor principal;
		
		principal = actorService.findPrincipal();
		
		Assert.isTrue(getVideogameFromForum(forum).getManager().equals(principal));

		forumMessageService.deleteAllForum(forum.getId());
		
		forumRepository.delete(forum);
	}
	
	


	public Forum reconstruct(Forum forum,Videogame videogame, BindingResult binding){
		Forum result;
		Forum auxForum;
		
		result=forum;
	
		auxForum = forumRepository.findOne(forum.getId());	
		if(forum.getId()!=0)
			Assert.isTrue(getVideogameFromForum(auxForum).equals(getVideogameFromForum(result)));
		
		forum.setForumCategories(result.getForumCategories());
		
		validator.validate(forum, binding);
		
		return forum;
	}

	public void setParentCategoryFromServices(ForumCategory category) { 
		ForumCategory parent, rootCategory;
		
		rootCategory = forumCategoryService.findRootForumCategory(category.getVideogame());
		
		Assert.isTrue(!category.equals(rootCategory));
		
		parent = forumCategoryService.findParentForumCategory(category);
		for(Forum f: forumRepository.getForumsByCategory(category.getId())){
			f.getForumCategories().remove(category);

			if(!(f.getForumCategories().size()>1 && parent.equals(rootCategory))){
				f.getForumCategories().add(forumCategoryService.findParentForumCategory(category));
			}
		}
	}
	
	public Videogame getVideogameFromForum(Forum forum) {
		return ((ForumCategory) forum.getForumCategories().toArray()[0]).getVideogame();
	} 
	
	public Collection<Forum> findForumsByCategory(Integer categoryId) {
		Collection<Forum> result;

		result = this.forumRepository.getForumsByCategory(categoryId);
		Assert.notNull(result);

		return result;
	}

		public void flush() {
		forumRepository.flush();
	}
}
