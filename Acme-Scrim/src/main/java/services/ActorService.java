package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.LoginService;
import domain.Actor;
import domain.Administrator;
import domain.Manager;
import domain.Player;
import domain.Referee;
import domain.Sponsor;
import domain.Team;

@Service
@Transactional
public class ActorService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ActorRepository actorRepository;
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ReportService reportService;

	@Autowired
	private TeamService teamService;
	
	// Supporting services ---------------------------------------------------
	
//	@Autowired
//	private FolderService folderService;
	
	// Constructor ---------------------------------------------------
	
	public ActorService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Actor findOne(int actorId) {
		Assert.isTrue(actorId != 0);
		
		Actor result;
		
		result = actorRepository.findOne(actorId);
		
		return result;
	}
	
	public Collection<Actor> findAll() {		
		Collection<Actor> result;
		
		result = actorRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Actor save(Actor actor) {
		Assert.notNull(actor);
		
		Actor result;
		
		result = actorRepository.save(actor);
		
		return result;
	}
	
	// Other business methods ---------------------------------------------------
	
	public Actor findByUserAccountId(int userAccountId) {
		Assert.isTrue(userAccountId != 0);
		
		Actor result;
		
		result = actorRepository.findByUserAccountId(userAccountId);
		
		return result;
	}

	public Actor findPrincipal() {
		return findByUserAccountId(LoginService.getPrincipal().getId());
	}
	
	public Player checkIsPlayer() {
		// Comprueba que el usuario logeado es un player
		Actor actor;
		actor = findPrincipal();

		Assert.isInstanceOf(Player.class, actor);
		
		return (Player) actor;
	}
	
	public Administrator checkIsAdministrator() {
		// Comprueba que el usuario logeado es un admin
		Actor actor;
		actor = findPrincipal();

		Assert.isInstanceOf(Administrator.class, actor);
		
		return (Administrator) actor;
	}
	
	public Manager checkIsManager() {
		// Comprueba que el usuario logeado es un manager
		Actor actor;
		actor = findPrincipal();

		Assert.isInstanceOf(Manager.class, actor);
		
		return (Manager) actor;
	}
	
	public Sponsor checkIsSponsor() {
		// Comprueba que el usuario logeado es un sponsor
		Actor actor;
		actor = findPrincipal();

		Assert.isInstanceOf(Sponsor.class, actor);
		
		return (Sponsor) actor;
	}
	
	public Referee checkIsReferee() {
		// Comprueba que el usuario logeado es un referee
		Actor actor;
		actor = findPrincipal();

		Assert.isInstanceOf(Referee.class, actor);
		
		return (Referee) actor;
	}
	
	public void banActor(final int actorId) {
		Actor actor;
		Player toBan;
		if(findPrincipal() instanceof Referee){
			this.checkIsReferee();
			toBan = (Player) findOne(actorId);
			Assert.isTrue(reportService.getReportsByPlayer(toBan.getId()).size() >= configurationService.find().getMinimumReports());
		}else{
			this.checkIsAdministrator();
		}

		actor = this.findOne(actorId);
		Assert.notNull(actor);

		actor.getUserAccount().setAccountNonLocked(false);

		this.save(actor);
	}

	public void followTeam(Integer teamId) {
		Actor result;
		Team team;
		
		result = this.findPrincipal();
		team = teamService.findOne(teamId);
		
		Assert.notNull(team);
		
		if(!result.getTeamsFollowed().contains(team)){
			result.getTeamsFollowed().add(team);
			save(result);
		}
	}
	
	public void unfollowTeam(Integer teamId) {
		Actor result;
		Team team;
		
		result = this.findPrincipal();
		team = teamService.findOne(teamId);
		
		Assert.notNull(team);
		
		if(result.getTeamsFollowed().contains(team)){
			result.getTeamsFollowed().remove(team);
			save(result);
		}
	}

	public void unbanActor(final int actorId) {
		Actor actor;

		if(findPrincipal() instanceof Referee){
			this.checkIsReferee();
		}else{
			this.checkIsAdministrator();
		}
		
		actor = this.findOne(actorId);

		Assert.notNull(actor);

		actor.getUserAccount().setAccountNonLocked(true);

		this.save(actor);
	}	
	
	public Collection<Actor> getBannedActors(){
		return actorRepository.getBannedActors();
	}
	
}
