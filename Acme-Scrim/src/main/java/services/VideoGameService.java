package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.VideogameRepository;
import domain.ForumCategory;
import domain.GameAccount;
import domain.GameCategory;
import domain.Manager;
import domain.Player;
import domain.Season;
import domain.Team;
import domain.Videogame;
import form.VideogameForm;

@Service
@Transactional
public class VideogameService {

	// Managed repository ----------------------------------------

	@Autowired
	private VideogameRepository videogameRepository;

	// Supporting services ---------------------------------------

	@Autowired
	private ActorService actorService;

	@Autowired
	private ForumCategoryService forumCategoryService;

	@Autowired
	private GameCategoryService gameCategoryService;

	@Autowired
	private SeasonService seasonService;

	// Constructor -----------------------------------------------

	public VideogameService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------

	public Videogame create() {
		// Comprobamos que el usuario logeado es un manager
		Manager manager;
		manager = this.actorService.checkIsManager();

		// Creamos el objeto a devolver
		Videogame result;
		result = new Videogame();

		// Inicializamos los atributos/ relaciones que necesitamos
		result.setForumCategories(new ArrayList<ForumCategory>());
		result.setManager(manager);
		result.setSeasons(new ArrayList<Season>());

		// Devolvemos el resultado
		return result;

	}

	public Collection<Videogame> findAll() {
		// Creamos el objeto a devolver
		Collection<Videogame> result;
		// Inicializamos el objeto a devolver
		result = this.videogameRepository.findAll();
		// Devolvemos el resultado
		return result;
	}

	public Videogame findOne(int videogameId) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.isTrue(videogameId != 0);

		// Creamos el objeto a devolver
		Videogame result;
		result = this.videogameRepository.findOne(videogameId);

		// Devolvemos el resultado
		return result;
	}

	public Videogame save(Videogame videogame) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.notNull(videogame);

		// Comprobamos que el ususario logeado es un manager
		Manager manager;
		manager = this.actorService.checkIsManager();

		// Comprobamos que el usuario logeado es el que creo el videogame
		Assert.isTrue(videogame.getManager().equals(manager));

		// Creamos el objeto a devolver
		Videogame result = this.videogameRepository.save(videogame);

		// Devolvemos el resultado
		return result;
	}

	// Other methods ---------------------------------------------

	public Collection<Videogame> getVideogamesWithoutGameAccount(Player player) {
		Assert.notNull(player);

		Collection<Videogame> result;

		result = this.videogameRepository
				.getVideogamesWithoutGameAccount(player.getId());

		return result;
	}

	public Collection<Videogame> getVideogamesWithGameAccount(Player player) {
		Assert.notNull(player);

		Collection<Videogame> result;

		result = this.videogameRepository.getVideogamesWithGameAccount(player
				.getId());

		return result;
	}

	// Lista de gameCategories que tiene un Videogame
	public Collection<GameCategory> listOfGameCategories(int videogameId) {
		// Comprobamos que no es nulo
		Assert.isTrue(videogameId != 0);
		// Inicializamos el objeto a devolver
		Collection<GameCategory> result;
		result = this.videogameRepository.listOfGameCategories(videogameId);
		// Devolvemos el reusltado
		return result;
	}

	// Lista de teams que tiene un Videogame
	public Collection<Team> listOfTeams(int videogameId) {
		// Comprobamos que no es nulo
		Assert.isTrue(videogameId != 0);
		// Inicializamos el objeto a devolver
		Collection<Team> result;
		result = this.videogameRepository.listOfTeams(videogameId);
		// Devolvemos el reusltado
		return result;
	}

	// GameAccount asociada a un videogame
	public Collection<GameAccount> listOfGameAccountsOfVideogame(int videogameId) {
		// Comprobamos que no sea nulo
		Assert.isTrue(videogameId != 0);

		// Creamos el resultado
		Collection<GameAccount> result;
		result = this.videogameRepository
				.listOfGameAccountsOfVideogame(videogameId);

		// Devolvemos el resultado
		return result;
	}
	
	public Collection<Player> listOfPlayersOfVideogame(int videogameId) {
		// Comprobamos que no sea nulo
		Assert.isTrue(videogameId != 0);

		// Creamos el resultado
		Collection<Player> result;
		result = this.videogameRepository
				.listOfPlayersOfVideogame(videogameId);

		// Devolvemos el resultado
		return result;
	}

	@Autowired
	private Validator validator;

	public Videogame reconstruct(VideogameForm videogameForm,
			BindingResult binding) {
		Videogame result = this.create();

		result.setName(videogameForm.getName());
		result.setDescription(videogameForm.getDescription());
		result.setImage(videogameForm.getImage());

		Collection<ForumCategory> forumCategories;
		forumCategories = new ArrayList<ForumCategory>();
		forumCategories.add(forumCategoryService.createFORUM(result));

		result.setForumCategories(forumCategories);
		validator.validate(videogameForm, binding);

		return result;
	}

	public void flush() {
		videogameRepository.flush();
	}

	public Videogame saveNewVideogame(Videogame videogame, String seasonName,
			Collection<GameCategory> gameCategories) {

		Assert.notNull(videogame);
		Assert.hasLength(seasonName);
		Assert.notEmpty(gameCategories);

		Videogame result;

		result = this.save(videogame);

		Season season;
		season = this.seasonService.create(result.getId());

		season.setName(seasonName);

		season = this.seasonService.save(season);
		Collection<Season> seasons;
		seasons = result.getSeasons();
		seasons.add(season);

		result.setSeasons(seasons);

		result = this.save(result);
		
		forumCategoryService.saveFORUM(result);
		
		this.flush();
		
		// Metemos el videogame en category
		for (GameCategory g : gameCategories) {
			Collection<Videogame> videogames = g.getVideogames();
			videogames.add(result);
			g.setVideogames(videogames);
			this.gameCategoryService.saveReference(g);
		}

		return result;
	}

}
