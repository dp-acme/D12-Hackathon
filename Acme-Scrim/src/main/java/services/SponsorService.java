package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.SponsorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Advertisement;
import domain.Sponsor;
import domain.Team;
import form.RegisterActor;

@Service
@Transactional
public class SponsorService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private SponsorRepository sponsorRepository;

	// Supporting services ---------------------------------------------------
	
	@Autowired
	private FolderService folderService;
	
	@Autowired
	private ActorService actorService;
	// Constructor ---------------------------------------------------

	public SponsorService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------
	
	public Sponsor create() {

		Sponsor result;

		result = new Sponsor();
		result.setMyAdvertisements(new ArrayList<Advertisement>());
		result.setTeamsFollowed(new ArrayList<Team>());

		return result;
	}

	public Sponsor findOne(int sponsorId) {
		Assert.isTrue(sponsorId != 0);

		Sponsor result;

		result = sponsorRepository.findOne(sponsorId);

		return result;
	}

	public Collection<Sponsor> findAll() {
		Collection<Sponsor> result;

		result = sponsorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Sponsor save(Sponsor sponsor) {
		Assert.notNull(sponsor);
		Sponsor result;
		UserAccount userAccount;

		if (sponsor.getId() != 0) {
			userAccount = LoginService.getPrincipal();
			Assert.isTrue(sponsor.getUserAccount().equals(userAccount));
			result = sponsorRepository.save(sponsor);
		} else {
			Md5PasswordEncoder encoder;

			Assert.isTrue(!LoginService.isAuthenticated());
			
			encoder = new Md5PasswordEncoder();
			sponsor.getUserAccount().setPassword(
					encoder.encodePassword(sponsor.getUserAccount().getPassword(),
							null));
			result = sponsorRepository.save(sponsor);
			folderService.createSystemFolders(actorService.findOne(result.getId()));
		}

		
		
		return result;
	}

	// Other business methods -------------------------------------------------

	@Autowired
	private Validator validator;

	public Sponsor reconstruct(RegisterActor formObject, BindingResult binding) {
		Sponsor result;
		Authority authority;

		authority = new Authority();
		result = create();
		result.setEmail(formObject.getEmail());
		result.setName(formObject.getName());
		result.setSurname(formObject.getSurname());
		authority.setAuthority(Authority.SPONSOR);
		formObject.getUserAccount().addAuthority(authority);
		formObject.getUserAccount().setAccountNonLocked(true);
		result.setUserAccount(formObject.getUserAccount());

		validator.validate(formObject, binding);

		return result;
	}
	
	public Sponsor reconstructEdit(Sponsor sponsor, BindingResult binding) {
		Sponsor result;
		
		result = this.findOne(sponsor.getId());
		
		sponsor.setMyAdvertisements(sponsor.getMyAdvertisements());
		sponsor.setTeamsFollowed(result.getTeamsFollowed());
		sponsor.setUserAccount(result.getUserAccount());

		validator.validate(sponsor, binding);

		return sponsor;
	}

	public void flush() {
		sponsorRepository.flush();
	}
}
