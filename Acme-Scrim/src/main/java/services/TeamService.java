package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.TeamRepository;
import domain.Actor;
import domain.Administrator;
import domain.GameAccount;
import domain.Invitation;
import domain.Player;
import domain.Priority;
import domain.Scrim;
import domain.Team;
import domain.Videogame;

@Service
@Transactional
public class TeamService {

	// Managed repository ----------------------------------------

	@Autowired
	private TeamRepository teamRepository;

	// Supporting services ---------------------------------------

	@Autowired
	private InvitationService invitationService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private ScrimService scrimService;
	
	// Constructor -----------------------------------------------
	
	public TeamService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------

	public Team create() {
		Team result;
		
		result = new Team ();

		result.setInvitations(new ArrayList<Invitation> ());
		
		return result;
	}

	public Collection<Team> findAll() {
		Collection<Team> result;

		result = this.teamRepository.findAll();

		return result;
	}

	public Team findOne(int teamId) {
		Assert.isTrue(teamId != 0);

		Team result;

		result = this.teamRepository.findOne(teamId);

		return result;
	}

	public Team save(Team team) {
		Assert.notNull(team);

		Team result;
		
		Actor principal;
		principal = actorService.findPrincipal();
		
		if (!(principal instanceof Administrator)) {
			Assert.isInstanceOf(Player.class, principal);
			principal = (Player) principal;
		}
		
		if (team.getId() == 0) {
			Assert.isTrue(principal.equals(team.getLeader().getPlayer()));
			Assert.isTrue(team.getInvitations().size() == 0);
			Assert.isTrue(team.getVideogame().equals(team.getLeader().getVideogame()));
			
		} else {
			result = this.findOne(team.getId());
			Assert.notNull(result);
			
			Assert.isTrue(team.getVideogame().equals(result.getVideogame()));
		}
		
		result = this.teamRepository.save(team);
		Assert.notNull(result);
		
		return result;
	}

	public void delete(Team team) {
		Assert.notNull(team);

		Collection<Invitation> invitations;
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Team deleted: '" + team.getName() + "'";
		subject += " -- ";
		subject += "Equipo eliminado: '" + team.getName() + "'";
		
		body = "The team '" + team.getName() + "' has been deleted by the leader.";
		body += "\n -- \n";
		body += "El equipo '" + team.getName() + "' ha sido eliminado por el l�der.";
		
		recipients = new ArrayList<Actor>();
		recipients.addAll(this.getPlayerTeammates(team, true));
		
		invitations = new ArrayList<Invitation> (team.getInvitations());
		
		for (Invitation invit : invitations) {
			this.invitationService.delete(invit);
		}
		
		this.teamRepository.flush();
		
		Collection<Scrim> scrims;
		
		scrims = this.scrimService.getTeamScrims(team);
		
		for (Scrim scrim : scrims) {
			this.scrimService.delete(scrim);
		}
		
		this.teamRepository.delete(team);
		
		messageService.sendNotification(subject, body, recipients, Priority.HIGH);
	}

	// Other methods ---------------------------------------------

	@Autowired
	private Validator validator;
	
	public Team reconstruct(Team result, BindingResult binding) {
		Team team;
		
		if (result.getId() != 0) {
			team = this.findOne(result.getId());
			Assert.notNull(team);
			
			result.setLeader(team.getLeader());
			result.setVideogame(team.getVideogame());
		} else {
			Player player;
			GameAccount gameAccount;
			
			player = this.actorService.checkIsPlayer();
			
			gameAccount = this.gameAccountService.getGameAccountForPlayerAndVideogame(player, result.getVideogame());
			Assert.notNull(gameAccount);
			
			team = this.create();
			
			result.setLeader(gameAccount);
		}

		result.setInvitations(team.getInvitations());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Collection<Team> getTeamsOfVideogame (Videogame videogame) {
		Assert.notNull(videogame);
		
		Collection<Team> result;
		
		result = this.teamRepository.getTeamsOfVideogame (videogame.getId());
		
		return result;
	}
	
	public Collection<Team> getTeamsOfGameAccount (GameAccount gameAccount) {
		Assert.notNull(gameAccount);
		
		Collection<Team> result;
		
		result = this.teamRepository.getTeamsOfGameAccount (gameAccount.getId());
		
		return result;
	}
	
	public Collection<GameAccount> getTeammates(Team team, boolean includeLeader) {
		Collection<GameAccount> result;
		
		result = new ArrayList<GameAccount> ();
		
		if (includeLeader) {
			result.add(team.getLeader());
		}
		
		result.addAll(this.teamRepository.getTeammates(team.getId()));
		
		return result;
	}
	
	public Collection<GameAccount> getRealTeammates(Team team, boolean includeLeader) {
		Collection<GameAccount> result;
		
		result = new ArrayList<GameAccount> ();
		
		if (includeLeader) {
			result.add(team.getLeader());
		}
		
		result.addAll(this.teamRepository.getRealTeammates(team.getId()));
		
		return result;
	}
	
	public Collection<Player> getPlayerTeammates(Team team, boolean includeLeader) {
		Collection<Player> result;
		
		result = new ArrayList<Player> ();
		
		if (includeLeader) {
			result.add(team.getLeader().getPlayer());
		}
		
		result.addAll(this.teamRepository.getPlayerTeammates(team.getId()));
		
		return result;
	}
	
	public void kickTeammate(Team team, GameAccount teammate) {
		kickTeammate(team, teammate, true);
	}
	
	public void kickTeammate(Team team, GameAccount teammate, boolean sendNotification) {
		Assert.notNull(team);
		Assert.notNull(teammate);
		
		Player player;
		
		player = this.actorService.checkIsPlayer();
		Assert.isTrue(player.equals(teammate.getPlayer()) || player.equals(team.getLeader().getPlayer()));
		
		Invitation invitation;
		invitation = invitationService.getGameAccountTeamInvitation(team, teammate, true);
		
		invitationService.delete(invitation);
		
		if (sendNotification) {
			String subject;
			String body;
			Collection<Actor> recipients;
			
			subject = "Kicked from: '" + team.getName() + "'";
			subject += " -- ";
			subject += "Expulsado de: '" + team.getName() + "'";
			
			body = "You have been kicked from '" + team.getName() + "'.";
			body += "\n -- \n";
			body += "Te han expulsado de '" + team.getName() + "'.";
			
			recipients = new ArrayList<Actor>();
			recipients.add(teammate.getPlayer());
			
			messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
		}
	}
	
	public void exit(Team team) {
		Assert.notNull(team);
		
		Player player;
		GameAccount gameAccount;
		Invitation invitation;
		
		player = this.actorService.checkIsPlayer();
		
		gameAccount = this.gameAccountService.getGameAccountForPlayerAndVideogame(player, team.getVideogame());
		Assert.notNull(gameAccount);
		
		invitation = this.invitationService.getGameAccountTeamInvitation(team, gameAccount, true);
		Assert.notNull(invitation.getMoment());
		
		this.invitationService.delete(invitation);
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Player left from: '" + team.getName() + "'";
		subject += " -- ";
		subject += "Jugador sali� de: '" + team.getName() + "'";
		
		body = "The player " + player.getName() + " with game account '" + gameAccount.getNickname() + "' has left the team: '" + team.getName() + "'.";
		body += "\n -- \n";
		body += "El jugador " + player.getName() + " con cuenta de juego '" + gameAccount.getNickname() + "' ha abandonado el equipo: '" + team.getName() + "'.";
		
		recipients = new ArrayList<Actor>();
		recipients.addAll(this.getPlayerTeammates(team, true));
		
		messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
	}
	
	public void changeTeammateLeader(Team team, GameAccount newLeader, boolean oldLeaderToTeammate) {
		Assert.notNull(team);
		Assert.notNull(newLeader);
		
		Player principal;
		
		principal = this.actorService.checkIsPlayer();
		Assert.isTrue(principal.equals(team.getLeader().getPlayer()));
		
		GameAccount oldLeader;
		oldLeader = team.getLeader();
		
		Collection<GameAccount> teammates;
		teammates = this.getRealTeammates(team, false);
		Assert.isTrue(teammates.contains(newLeader));
		
		Invitation newLeaderInvit;
		newLeaderInvit = invitationService.getGameAccountTeamInvitation(team, newLeader, true);
		
		invitationService.delete(newLeaderInvit);
		team.setLeader(newLeader);
		
		if (oldLeaderToTeammate) {
			Invitation oldLeaderInvit;
			
			oldLeaderInvit = this.inviteTeammate(team, oldLeader, "Leader has changed -- El l�der ha cambiado", false, false);
			
			invitationService.acceptInvitation(oldLeaderInvit);
		}
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Team leader changed: '" + team.getName() + "'";
		subject += " -- ";
		subject += "Cambio de l�der en equipo: '" + team.getName() + "'";
		
		body = "The team leader has changed for '" + team.getName() + "'.";
		body += "\n -- \n";
		body += "Ha cambiado el l�der para el equipo '" + team.getName() + "'.";
		
		recipients = new ArrayList<Actor>();
		recipients.addAll(this.getPlayerTeammates(team, true));
		
		messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
	}
	
	public Invitation inviteTeammate(Team team, GameAccount gameAccount, String message, boolean sendNotification) {
		return inviteTeammate(team, gameAccount, message, sendNotification, true);
	}
	
	public Invitation inviteTeammate(Team team, GameAccount gameAccount, String message, boolean sendNotification, boolean checkPrincipalLeader) {
		Assert.notNull(team);
		Assert.notNull(gameAccount);
		if (sendNotification) Assert.hasText(message);
		
		Invitation result;
		
		result = invitationService.create(team);
		
		result.setMessage(message);
		result.setTeammate(gameAccount);
		
		result = invitationService.save(result, checkPrincipalLeader);
		Assert.notNull(result);
		
		if (sendNotification) {
			String subject;
			String body;
			Collection<Actor> recipients;
			
			subject = "Team Invitation: '" + team.getName() + "'";
			subject += " -- ";
			subject += "Invitaci�n de equipo: '" + team.getName() + "'";
			
			body = "You have been invited to play in '" + team.getName() + "'.";
			body += "\n -- \n";
			body += "Te han invitado para jugar en '" + team.getName() + "'.";
			body += "\n -- \n\n";
			body += result.getMessage();
			
			recipients = new ArrayList<Actor>();
			recipients.add(gameAccount.getPlayer());
			
			messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
		}
		
		return result;
	}

	public void flush() {
		teamRepository.flush();
	}
	
	public Collection<Object[]> getTeamsOrderedByLikes(Videogame videogame) {
		Assert.notNull(videogame);
		
		Collection<Object[]> result;
		
		result = this.teamRepository.getTeamsOrderedByLikes(videogame.getId());
		
		return result;
	}
	
	public Collection<Object[]> getTeamsOrderedByWonGames(Videogame videogame) {
		Assert.notNull(videogame);
		
		Collection<Object[]> result;
		
		result = this.teamRepository.getTeamsOrderedByWonGames(videogame.getId());
		
		return result;
	}
	
	public Collection<Actor> getTeamFollowers(Team team) {
		Assert.notNull(team);
		
		Collection<Actor> result;
		
		result = this.teamRepository.getTeamFollowers(team.getId());
		
		return result;
	}
	
}
