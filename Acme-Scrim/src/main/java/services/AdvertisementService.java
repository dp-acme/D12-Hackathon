package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.AdvertisementRepository;
import domain.Advertisement;
import domain.Sponsor;

@Service
@Transactional
public class AdvertisementService {

	// Managed repository ----------------------------------------

	@Autowired
	private AdvertisementRepository advertisementRepository;

	// Supporting services ---------------------------------------
	@Autowired
	private ActorService actorService;

	// Constructor -----------------------------------------------
	public AdvertisementService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------

	public Advertisement create() {
		Sponsor sponsor;
		sponsor = this.actorService.checkIsSponsor();

		// Creamos el objeto a devolver
		Advertisement result;

		// Inicializamos el objeto a devolver
		result = new Advertisement();

		// Actualizamos las variables o realciones a introducir
		result.setSponsor(sponsor);

		// Devolvemos el resultado
		return result;
	}

	public Collection<Advertisement> findAll() {
		// Creamos el objeto a devolver
		Collection<Advertisement> result;

		// Inicializamos el objeto con los datos del repositorio
		result = this.advertisementRepository.findAll();

		// Devolvemos el resultado
		return result;
	}

	public Advertisement findOne(int advertisementId) {
		// Comprobamos que el objeto que le pasamos por parametro no es nulo
		Assert.isTrue(advertisementId != 0);

		// Creamos el objeto a devolver
		Advertisement result;

		// Inicializamos el objeto a devolver el dato del respositorio
		result = this.advertisementRepository.findOne(advertisementId);

		// Devolvemos el resultado
		return result;
	}

	public Advertisement save(Advertisement advertisement) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.notNull(advertisement);

		// Comprobamos que el usuario logeado es un sponsor
		Sponsor sponsor;
		sponsor = this.actorService.checkIsSponsor();

		Assert.isTrue(this.checkCreditCard(advertisement.getCreditCard()
				.getExpirationMonth(), advertisement.getCreditCard()
				.getExpirationYear()));

		// Comprobamos que el usuario logeado es el mismo que el que creo el
		// advertisement que le pasamos por parametros
		Assert.isTrue(advertisement.getSponsor().equals(sponsor));

		// Creamos el objeto a devolver
		Advertisement result;

		// Guardamos el objeto
		result = this.advertisementRepository.save(advertisement);

		// Devolvemos el resultado
		return result;
	}

	public void delete(Advertisement advertisement) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.notNull(advertisement);

		// Comprobamos que el ususario logeado es un sponsor y que sea el
		// creador del advertisement
		Sponsor sponsor;
		sponsor = this.actorService.checkIsSponsor();

		// Comprobamos que el usuario que quiere eliminar el advertisement es el
		// mismo que lo creo
		Assert.isTrue(advertisement.getSponsor().equals(sponsor));

		// Eliminamos el advertisement
		this.advertisementRepository.delete(advertisement);
	}

	// Other methods ---------------------------------------------

	// Devuelve un advertisement aleatorio para obtener su imagen
	public Advertisement findOneRandom() {
		// Creamos el objeto a devolver
		Advertisement result;

		// Obtenemos todos los advertisement de la base de datos
		List<Advertisement> allAdvertisement = this.advertisementRepository
				.findAll();

		// De todas los advertisement obtenemos 1 aleatorio
		Random random = new Random();

		// Nos devuelve un numero aleatorio
		result = allAdvertisement.get(random.nextInt(allAdvertisement.size()));

		// Devolvemos el resultado
		return result;
	}

	@Autowired
	private Validator validator;

	public Advertisement reconstruct(Advertisement result,
			BindingResult binding) {
		Advertisement advertisement;

		if (result.getId() == 0) { // Caso de creaci�n

			Sponsor sponsor;
			sponsor = actorService.checkIsSponsor();

			result.setSponsor(sponsor);

		} else {
			advertisement = this.findOne(result.getId());

			result.setSponsor(advertisement.getSponsor());
		}

		validator.validate(result, binding);

		return result;
	}

	@SuppressWarnings("deprecation")
	public boolean checkCreditCard(int month, int year) {
		Date date = new Date();
		year += 2000;
		int nowMonth = date.getMonth() + 1;
		int nowYear = date.getYear() + 1900;

		return year > nowYear || (month > nowMonth && year == nowYear);
	}

	public void flush() {
		advertisementRepository.flush();
	}

}