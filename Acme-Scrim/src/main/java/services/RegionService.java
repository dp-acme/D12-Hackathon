package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.RegionRepository;
import domain.Actor;
import domain.Administrator;
import domain.GameAccount;
import domain.Region;
import domain.Scrim;
import domain.Team;

@Service
@Transactional
public class RegionService {

	// Managed repository ----------------------------------------

	@Autowired
	private RegionRepository regionRepository;

	// Supporting services ---------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private ScrimService scrimService;
	
	// Constructor -----------------------------------------------
	
	public RegionService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------

	public Region create() {
		Region result;
		
		result = new Region ();
		
		return result;
	}

	public Collection<Region> findAll() {
		Collection<Region> result;

		result = this.regionRepository.findAll();

		return result;
	}

	public Region findOne(int regionId) {
		Assert.isTrue(regionId != 0);

		Region result;

		result = this.regionRepository.findOne(regionId);

		return result;
	}

	public Region save(Region region) {
		Assert.notNull(region);

		Actor principal;
		principal = actorService.findPrincipal();
		
		Assert.isInstanceOf(Administrator.class, principal);
		
		Region result;
		
		result = this.regionRepository.save(region);
		Assert.notNull(result);
		
		return result;
	}

	public void delete(Region region) {
		Assert.notNull(region);
		Assert.isTrue(!region.equals(this.getInternational()));

		checkItemsWithRegion(region);
		
		this.regionRepository.delete(region);
	}

	// Other methods ---------------------------------------------

	public Region getInternational() {
		Region result;
		
		result = this.regionRepository.getInternational();
		Assert.notNull(result);
		
		return result;
	}
	
	public boolean uniqueEnglishName(Region region) {
		Region regionRes;
		
		regionRes = this.regionRepository.uniqueEnglishName(region.getId(), region.getEnglishName());
		
		boolean result = regionRes == null;
		
		return result;
	}
	
	public boolean uniqueSpanishName(Region region) {
		Region regionRes;
		
		regionRes = this.regionRepository.uniqueSpanishName(region.getId(), region.getSpanishName());
		
		boolean result = regionRes == null;
		
		return result;
	}
	
	public boolean uniqueAbbreviation(Region region) {
		Region regionRes;
		
		regionRes = this.regionRepository.uniqueAbbreviation(region.getId(), region.getAbbreviation());
		
		boolean result = regionRes == null;
		
		return result;
	}
	
	public boolean uniqueColor(Region region) {
		Region regionRes;
		
		regionRes = this.regionRepository.uniqueColor(region.getId(), region.getColorDisplay());
		
		boolean result = regionRes == null;
		
		return result;
	}
	
	private void checkItemsWithRegion(Region region) {
		Assert.notNull(region);
		
		Collection<Team> teams;
		Collection<GameAccount> gameAccounts;
		Collection<Scrim> scrims;
		
		teams = this.regionRepository.getTeamsWithRegion(region.getId());
		gameAccounts = this.regionRepository.getGameAccountsWithRegion(region.getId());
		scrims = this.regionRepository.getScrimsWithRegion(region.getId());
		
		Region international;
		international = this.getInternational();
		
		for(Team team : teams) {
			team.setRegion(international);
			
			teamService.save(team);
		}
		
		for(GameAccount gameAccount : gameAccounts) {
			gameAccount.setRegion(international);
			
			gameAccountService.save(gameAccount);
		}

		for(Scrim scrim : scrims) {
			scrim.setDesiredGuestRegion(international);
			
			scrimService.save(scrim);
		}
	}

	public void flush() {
		regionRepository.flush();
	}
}
