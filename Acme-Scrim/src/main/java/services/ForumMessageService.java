
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ForumMessageRepository;
import domain.Actor;
import domain.Forum;
import domain.ForumMessage;

@Service
@Transactional
public class ForumMessageService {

	// Validator ---------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	// Managed repository ------------------------------------
	
	@Autowired
	private	ForumMessageRepository	forumMessageRepository;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService	actorService;
	
	@Autowired
	private ForumService	forumService;
	
	// Constructors --------------------------------------------
	
	public ForumMessageService() {
		super();
	}
	
	// Simple CRUD methods -------------------------------------
	
	public ForumMessage create(Forum forum, ForumMessage parent) {
		ForumMessage result;
	
		result = new ForumMessage();
	
		result.setChildrenMessages(new ArrayList<ForumMessage>());
		result.setForum(forum);
		result.setMoment(new Date(System.currentTimeMillis() - 1));
		
		return result;
	}
	public Collection<ForumMessage> findAll() {
		Collection<ForumMessage> result;

		result = this.forumMessageRepository.findAll();

		Assert.notNull(result);

		return result;
	}
	
	public ForumMessage findOne(int forumMessageId) {
		Assert.isTrue(forumMessageId != 0);
		
		ForumMessage result;

		result = this.forumMessageRepository.findOne(forumMessageId);

		return result;
	}
	public ForumMessage save(ForumMessage forumMessage) {
		Assert.notNull(forumMessage);
		
		ForumMessage result;

		Assert.isTrue(forumMessage.getId()==0);
		if(forumMessage.getParent()!=null){
			Assert.isTrue(forumMessage.getForum().equals(forumMessage.getParent().getForum()));
			forumMessage.getParent().getChildrenMessages().add(forumMessage);
		}
		
		result = this.forumMessageRepository.save(forumMessage);
		
		return result;
	}
	public void delete(ForumMessage forumMessage){		
		Actor principal;
		
		principal = actorService.findPrincipal();
		
		Assert.isTrue(forumService.getVideogameFromForum(forumMessage.getForum()).getManager().equals(principal));

		Set<ForumMessage> messagesToDelete;

		messagesToDelete = new HashSet<>();
		
		if(forumMessage.getParent() != null){ //Actualizar hijos del padre
			ForumMessage parent;
			
			parent = forumMessage.getParent();
			parent.getChildrenMessages().remove(forumMessage);
			
			parent = forumMessageRepository.saveAndFlush(parent);
		}
		
		deleteRec(forumMessage, messagesToDelete); //A�adir todas las categor�as a borrar
		
		forumMessageRepository.flush();
		
		forumMessageRepository.delete(messagesToDelete);
		
		
	}
	

	private void deleteRec(ForumMessage forumMessage, Set<ForumMessage> messagesToDelete) {
		for(ForumMessage fm : forumMessage.getChildrenMessages()){
			deleteRec(fm, messagesToDelete);
		}
		messagesToDelete.add(forumMessage);
		
	}

	public void deleteAllForum(int forumId) {
		forumMessageRepository.delete(forumMessageRepository.forumMessages(forumId));
	}
	
	public ForumMessage reconstruct(ForumMessage forumMessage, ForumMessage parent, BindingResult binding){
		ForumMessage result, newMessage;
		
		if(forumMessage.getId() == 0){
			Actor principal;
			
			principal = actorService.findPrincipal();
			newMessage = create(forumMessage.getForum(),forumMessage.getParent());
			
			forumMessage.setChildrenMessages(newMessage.getChildrenMessages());
			forumMessage.setActor(principal);

			forumMessage.setParent(parent);
			forumMessage.setMoment(new Date(System.currentTimeMillis() - 1));	
			result = forumMessage;
		}else{
			result = forumMessageRepository.findOne(forumMessage.getId());	
			
			forumMessage.setChildrenMessages(result.getChildrenMessages());
		
		}
		
		validator.validate(forumMessage, binding);
		
		return forumMessage;
	}

	public Collection<ForumMessage> findByForum(Integer forumId) {

		return forumMessageRepository.findByForum(forumId);
	}

		public void flush() {
		forumMessageRepository.flush();
	}


	
}
