package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ReviewRepository;
import domain.Review;
import domain.Player;
import domain.Scrim;
import domain.ScrimStatus;
import domain.Team;

@Service
@Transactional
public class ReviewService {

	@Autowired
	private ReviewRepository reviewRepository;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ScrimService scrimService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private PlayerService playerService;

	public Review findOne(int id){
		Assert.isTrue(id != 0);
		
		Review result;
		
		result = reviewRepository.findOne(id);
		
		return result;
	}
	
	public Collection<Review> findAll(){
		Collection<Review> result; 
		
		result = reviewRepository.findAll();
		
		Assert.notNull(result);
		
		return result;
	}
	
	public Review create(int scrimId, int teamId){
		Review result;
		Scrim scrim;
		Team team;
		
		Assert.isTrue(scrimId != 0);
		Assert.isTrue(teamId != 0);
		
		scrim = scrimService.findOne(scrimId);
		team = teamService.findOne(teamId);
		
		result = new Review();
		
		result.setScrim(scrim);
		result.setTeam(team);
		
		return result;
	}
	
	public Review save(Review like){
		Review result;
		Player principal;
		
		Assert.notNull(like);
		
		principal = actorService.checkIsPlayer();
		
		Assert.isTrue(like.getScrim().getCreator().getLeader().getPlayer().equals(principal) ^ like.getScrim().getGuest().getLeader().getPlayer().equals(principal));
		Assert.isTrue(playerService.getPlayersByScrim(like.getScrim().getId()).contains(principal));
		Assert.isTrue(getLikesByScrim(like.getScrim().getId()).size() < 2);
		Assert.isTrue(getLikesByScrimAndTeam(like.getScrim().getId(), like.getTeam().getId()).size() == 0);
		Assert.isTrue(like.getScrim().getStatus().getScrimStatus().equals(ScrimStatus.FINISHED));

		result = reviewRepository.save(like);
		
		return result;
	}
	
	public void delete(Review review) {
		Assert.notNull(review);
		
		this.reviewRepository.delete(review);
	}
	
	public 	Collection<Review> getLikesByTeam(int teamId){
		return reviewRepository.getLikesByTeam(teamId);
	}

	public 	Collection<Review> getLikesByScrim(int scrimId){
		return reviewRepository.getLikesByScrim(scrimId);
	}
	
	public Collection<Review> getLikesByScrimAndTeam(int scrimId, int teamId){
		return reviewRepository.getLikesByScrimAndTeam(scrimId, teamId);
	}

	public int getNumberLikesByTeam(int teamId){
		return reviewRepository.getNumberLikesByTeam(teamId);
	}
	
	public int getNumberDislikesByTeam(int teamId){
		return reviewRepository.getNumberDislikesByTeam(teamId);
	}
	
	public void flush() {
		reviewRepository.flush();
	}
	
	
}
