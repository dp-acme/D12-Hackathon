package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.GameCategoryRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Administrator;
import domain.GameCategory;
import domain.Videogame;

@Service
@Transactional
public class GameCategoryService {
	
	// Managed repository ----------------------------------------
	
	@Autowired
	private GameCategoryRepository gameCategoryRepository;

	// Supporting services ---------------------------------------
	
	@Autowired
	private ActorService actorService;

	// Constructor -----------------------------------------------
	
	public GameCategoryService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------
	
	public GameCategory create(GameCategory parent) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.notNull(parent);

		// Comprobamos que el usuario logeado es un administrador
		this.checkAdmin();

		// Creamos el objeto a devolver
		GameCategory result;
		result = new GameCategory();

		result.setParent(parent);
		result.setChildren(new ArrayList<GameCategory>());
		result.setVideogames(new ArrayList<Videogame>());

		// Devolvemos el resultado
		return result;
	}

	public Collection<GameCategory> findAll() {
		// Creamos el objeto a devolver
		Collection<GameCategory> result;
		// Inicializamos el objeto con los datos del repositorio
		result = this.gameCategoryRepository.findAll();
		// Devolvemos el resultado
		return result;
	}

	public GameCategory findOne(int gameCategoryId) {
		// Comprobamos que el objeto que le pasammos no es nulo
		Assert.isTrue(gameCategoryId != 0);
		// Creamos el objeto a devolver
		GameCategory result;
		// Inicializamos el ojeto con el dato del repositorio
		result = this.gameCategoryRepository.findOne(gameCategoryId);
		// Devolvemos el resultado
		return result;
	}

	public GameCategory save(GameCategory gameCategory) {
		// // Comprobamos que el usuario logeado es un administrador
		// this.checkIsAdministrator();
		// Assert.notNull(gameCategory, "The category is null.");
		// Assert.notNull(gameCategory.getParent(),
		// "Only Root can have previous null");
		// // Assert.isTrue(category.getIsRoot() == false,
		// // "Can't edit a category root.");
		// Assert.isTrue(!gameCategory.getParent().equals(gameCategory),
		// "The previous category can't be itself");
		//
		// Assert.isTrue(!gameCategory.getName().isEmpty()
		// && gameCategory.getName() != null, "Can't be empty text");
		//
		// // Copmrobamos que el nombre de la categoria no sea el mismo que la
		// // anterior
		// this.checkNameFatherIfIsTheSame(gameCategory);
		//
		// GameCategory result;
		//
		// result = this.gameCategoryRepository.save(gameCategory);
		// // Comprobamos que se ha guardado
		// Assert.isTrue(result.getId() != 0, "The category didn't save.");
		//
		// // Devolvemos resultado
		// return result;

		Assert.notNull(gameCategory);

		GameCategory result;

		this.checkAdmin();

		// if(this.gameCategoryRepository.gameCategoriesSize(gameCategory.getVideogame().getId())>0){
		// Assert.isTrue(gameCategory.getParent()!=null);
		// }

		Assert.isTrue(!gameCategory.getName().equals("VIDEOGAME"));
		Collection<GameCategory> categ = this.gameCategoryRepository.findAll();
		categ.remove(gameCategory);
			for (GameCategory fc : categ) { // No
																				// se
																				// pueden
																				// renombrar
																				// categor�as
																				// con
																				// el
																				// mismo
																				// nombre
																				// y
																				// el
																				// mismo
																				// padre
				Assert.isTrue(!fc.getName().equals(gameCategory.getName()));
			}

		this.gameCategoryRepository.flush();
		result = this.gameCategoryRepository.save(gameCategory);

		if (!result.getParent().getChildren().contains(result)) { // Actualizar
																	// la lista
																	// de
																	// carpetas
																	// del
																	// padre{
			addChildren(result);
		}

		return result;

	}

	public void delete(GameCategory gameCategory) {
		// Comprobamos que el objeto que queremos eliminar no es nulo
		Assert.notNull(gameCategory);

		// Comprobamos que el usuario logeado es un admin
		this.checkAdmin();

		Set<GameCategory> categoriesToDelete;

		categoriesToDelete = new HashSet<>();

		if (gameCategoryRepository.parentCategory(gameCategory.getId()) != null) { // Actualizar
																					// hijos
																					// del
																					// padre
			GameCategory parent;

			parent = gameCategoryRepository
					.parentCategory(gameCategory.getId());
			parent.getChildren().remove(gameCategory);

			parent = gameCategoryRepository.saveAndFlush(parent);
		}

		deleteRec(gameCategory, categoriesToDelete); // A�adir todas las
														// categor�as a borrar

		gameCategoryRepository.flush();

		gameCategoryRepository.delete(categoriesToDelete);
	}

	void deleteRec(GameCategory category, Set<GameCategory> categoriesToDelete) {

		for (GameCategory c : category.getChildren()) {
			deleteRec(c, categoriesToDelete);
		}

		category.setChildren(new ArrayList<GameCategory>());

		category = gameCategoryRepository.save(category);

		categoriesToDelete.add(category);
	}

	public void addChildren(GameCategory child) {
		GameCategory parent;

		parent = child.getParent();

		Assert.notNull(parent);
		Assert.notNull(child);

		for (GameCategory fc : parent.getChildren()) { // No se pueden crear
														// categor�as con el
														// mismo nombre y el
														// mismo padre
			Assert.isTrue(!fc.getName().equals(child.getName()));
		}

		parent.getChildren().add(child);
		gameCategoryRepository.save(parent);
	}

	// Other methods ---------------------------------------------
	// Comprobamos que el usuario logeado es un admin
	private void checkAdmin() {
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());

		Assert.isTrue(actor instanceof Administrator);
	}
	
	public void saveReference(GameCategory gameCategory) {
		Assert.notNull(gameCategory);
		Assert.isTrue(gameCategory.getId() != 0);
		this.gameCategoryRepository.save(gameCategory);
	}

	public Collection<GameCategory> findParentCategories() {
		// Devolvemos el resultado
		return this.gameCategoryRepository.findParentCategories();
	}

	public Collection<GameCategory> findRootGameCategory(int gameCategoryId) {

		Assert.isTrue(gameCategoryId != 0);
		Collection<GameCategory> result;

		result = gameCategoryRepository.findRootGameCategory(gameCategoryId);

		return result;
	}

	public Collection<Videogame> findVideogamesOfGameCategory(int gameCategoryId) {
		Assert.isTrue(gameCategoryId != 0);
		Collection<Videogame> result;

		result = gameCategoryRepository
				.findVideogamesOfGameCategory(gameCategoryId);

		return result;
	}

	public GameCategory findParentForumCategory(GameCategory gameCategory) {
		GameCategory result;

		result = gameCategoryRepository.parentCategory(gameCategory.getId());

		return result;
	}

	@Autowired
	private Validator validator;

	public GameCategory reconstruct(GameCategory gameCategory,
			GameCategory parent, BindingResult binding) {
		GameCategory result;

		if (gameCategory.getId() == 0) {
			result = this.create(parent);
		} else {
			result = gameCategoryRepository.findOne(gameCategory
					.getId());
		}
		
		result.setName(gameCategory.getName());
		result.setParent(gameCategory.getParent());

		validator.validate(result, binding);

		return result;
	}

	
	
	
	public void flush() {
		gameCategoryRepository.flush();
	}

}