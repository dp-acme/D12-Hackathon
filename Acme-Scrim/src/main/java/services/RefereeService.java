package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.RefereeRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Player;
import domain.Referee;
import domain.Team;
import form.RegisterActor;

@Service
@Transactional
public class RefereeService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private RefereeRepository refereeRepository;

	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private FolderService folderService;
	// Constructor ---------------------------------------------------

	public RefereeService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------
	
	public Referee create() {

		Referee result;

		result = new Referee();
		result.setTeamsFollowed(new ArrayList<Team>());

		return result;
	}

	public Referee findOne(int refereeId) {
		Assert.isTrue(refereeId != 0);

		Referee result;

		result = refereeRepository.findOne(refereeId);

		return result;
	}

	public Collection<Referee> findAll() {
		Collection<Referee> result;

		result = refereeRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Referee save(Referee referee) {
		Assert.notNull(referee);
		
		Player player;
		Referee result;
		UserAccount userAccount;
		
		player = new Player();

		if (referee.getId() != 0) {
			actorService.checkIsReferee();
			userAccount = LoginService.getPrincipal();
			Assert.isTrue(referee.getUserAccount().equals(userAccount));
			result = refereeRepository.save(referee);
		} else {
			
			actorService.checkIsPlayer();
			Assert.isInstanceOf(Player.class, actorService.findPrincipal());
			player = (Player) actorService.findPrincipal();
			Assert.isTrue(permissionService.hasRefereePermission(player.getId()));
			
			String role;
			Md5PasswordEncoder encoder;

			if (SecurityContextHolder.getContext().getAuthentication() != null
					&& SecurityContextHolder.getContext().getAuthentication()
							.getAuthorities().toArray().length > 0) {
				role = SecurityContextHolder.getContext().getAuthentication()
						.getAuthorities().toArray()[0].toString();
				Assert.isTrue(role.equals("PLAYER"));
			}
			encoder = new Md5PasswordEncoder();
			referee.getUserAccount().setPassword(
					encoder.encodePassword(referee.getUserAccount().getPassword(),
							null));
			result = refereeRepository.save(referee);
			folderService.createSystemFolders(actorService.findOne(result.getId()));
		}

		if(result != null && referee.getId() == 0){
			permissionService.delete(permissionService.getPermission(player.getId()).getId());
		}
		
		return result;
	}

	// Other business methods -------------------------------------------------

	@Autowired
	private Validator validator;

	public Referee reconstruct(RegisterActor formObject, BindingResult binding) {
		Referee result;
		Authority authority;

		authority = new Authority();
		result = create();
		result.setEmail(formObject.getEmail());
		result.setName(formObject.getName());
		result.setSurname(formObject.getSurname());
		authority.setAuthority(Authority.REFEREE);
		formObject.getUserAccount().addAuthority(authority);
		formObject.getUserAccount().setAccountNonLocked(true);
		result.setUserAccount(formObject.getUserAccount());

		validator.validate(formObject, binding);

		return result;
	}
	
	public Referee reconstructEdit(Referee referee, BindingResult binding) {
		Referee result;
		
		result = this.findOne(referee.getId());
		
		referee.setTeamsFollowed(result.getTeamsFollowed());
		referee.setUserAccount(result.getUserAccount());

		validator.validate(referee, binding);

		return referee;
	}

	public void flush() {
		refereeRepository.flush();
	}
}
