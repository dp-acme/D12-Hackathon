package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.SocialIdentityRepository;
import security.LoginService;
import domain.SocialIdentity;
import domain.SocialNetwork;

@Service
@Transactional
public class SocialIdentityService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private SocialIdentityRepository socialIdentityRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Constructor ---------------------------------------------------
	
	public SocialIdentityService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public SocialIdentity create(){
		SocialIdentity result;
		
		result = new SocialIdentity();
		
		return result;
	}
	
	public SocialIdentity findOne(int socialIdentityId) {
		Assert.isTrue(socialIdentityId != 0);
		
		SocialIdentity result;
		
		result = socialIdentityRepository.findOne(socialIdentityId);
		
		return result;
	}
	
	public Collection<SocialIdentity> findAll() {		
		Collection<SocialIdentity> result;
		
		try{
			result = socialIdentityRepository.findAll();
		}
		catch(Throwable e){
			result = null;
		}
		
		Assert.notNull(result);
		
		return result;
	}
	
	public SocialIdentity save(SocialIdentity socialIdentity) {
		Assert.notNull(socialIdentity);
		SocialIdentity result;
		Collection<SocialNetwork> socialNetworkAv;
		actorService.findPrincipal();
		
		socialNetworkAv = this.getAvSocialNetwork(socialIdentity.getActor().getId());
		
		if(socialIdentity.getId() != 0){
			socialNetworkAv.add(findOne(socialIdentity.getId()).getSocialNetwork());
		}
		Assert.isTrue(socialNetworkAv.contains(socialIdentity.getSocialNetwork()));
		result = socialIdentityRepository.save(socialIdentity);
		
		return result;
	}
	
	public void delete(int socialIdentityId){
		SocialIdentity socialIdentity;
		
		socialIdentity = this.findOne(socialIdentityId);
		Assert.notNull(socialIdentity);

		LoginService.isAuthenticated();
		
		socialIdentityRepository.delete(socialIdentity);
	}
	
	
	// Other business methods ---------------------------------------------------
	
	public SocialIdentity reconstruct(SocialIdentity socialIdentity, BindingResult binding) {
		SocialIdentity result;
		
		result = socialIdentity.getId()==0? socialIdentity:findOne(socialIdentity.getId());
		if(socialIdentity.getId()==0){
			result.setActor(actorService.findPrincipal());
		}
		result.setSocialNetwork(socialIdentity.getSocialNetwork());
		result.setLink(socialIdentity.getLink());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Collection<SocialNetwork> getAvSocialNetwork(int actorId){
		Collection<SocialNetwork> result;
		
		result = socialIdentityRepository.getAvSocialNetwork(actorId);
		
		return result;
	}
	
	public Collection<SocialIdentity> getSocialIdentitiesByActor(int actorId){
		Collection<SocialIdentity> result;
		
		result = socialIdentityRepository.getSocialIdentitiesByActor(actorId);
		
		return result;
	}
	
	public Collection<SocialIdentity> getSocialIdentitiesBySocialNetwork(int socialNetworkId){
		Collection<SocialIdentity> result;
		
		result = socialIdentityRepository.getSocialIdentitiesBySocialNetwork(socialNetworkId);
		
		return result;
	}
	
	
	public void flush() {
		socialIdentityRepository.flush();
	}
}
