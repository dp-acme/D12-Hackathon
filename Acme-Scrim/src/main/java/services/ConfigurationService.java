package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ConfigurationRepository;
import security.LoginService;
import domain.Actor;
import domain.Administrator;
import domain.Configuration;
import domain.Message;

@Service
@Transactional
public class ConfigurationService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;

	// Validator ---------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	// Constructor ---------------------------------------------------
	
	public ConfigurationService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Configuration find() {		
		Configuration result;
		
		result = configurationRepository.find();
		Assert.notNull(result);
		
		return result;
	}
	
	public Configuration save(Configuration configuration) {
		Assert.notNull(configuration);
		Assert.notNull(LoginService.isAuthenticated());
		
		Actor principal;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);
		
		Configuration result;
		
		result = configurationRepository.save(configuration);
		
		return result;
	}

	public Configuration reconstruct(Configuration configuration, BindingResult binding) {
		validator.validate(configuration, binding);
		
		return configuration;
	}
	
	public Boolean isSpam(String string){		
		for(String word : this.find().getTabooWords()){
			if(string.contains(word)){
				return true;
			}
		}
		
		return false;
	}
	
	public Boolean isSpam(Message message){
		return isSpam(message.getSubject()) || isSpam(message.getBody());
	}

	public void flush() {
		configurationRepository.flush();
	}
}
