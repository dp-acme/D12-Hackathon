package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.PermissionRepository;
import domain.Actor;
import domain.Permission;
import domain.Player;
import domain.Priority;

@Service
@Transactional
public class PermissionService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private PermissionRepository permissionRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private MessageService messageService;
	
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Constructor ---------------------------------------------------
	
	public PermissionService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Permission create(){
		Permission result;
		
		result = new Permission();
		
		return result;
	}
	
	public Permission findOne(int permission) {
		Assert.isTrue(permission != 0);
		
		Permission result;
		
		result = permissionRepository.findOne(permission);
		
		return result;
	}
	
	public Collection<Permission> findAll() {		
		Collection<Permission> result;
		
		try{
			result = permissionRepository.findAll();
		}
		catch(Throwable e){
			result = null;
		}
		
		Assert.notNull(result);
		
		return result;
	}
	
	public Permission save(Permission permission) {
		Assert.notNull(permission);
		Permission result;
		actorService.checkIsAdministrator();//Debes estar autenticado como admin
		
		Assert.isTrue(!this.hasAnyPermission(permission.getPlayer().getId()));
		
		result = permissionRepository.save(permission);
		Assert.notNull(result);
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Permission conceeded";
		subject += " -- ";
		subject += "Permiso concedido";
		
		body = "Permission was conceeded to create a " + (permission.getIsReferee() ? "referee" : "manager") + " account.";
		body += "\n -- \n";
		body += "Permiso concedido para crear una cuenta de " + (permission.getIsReferee() ? "�rbitro" : "gestor") + ".";		
		
		recipients = new ArrayList<Actor> ();
		recipients.add(permission.getPlayer());
		
		messageService.sendNotification(subject, body, recipients, Priority.HIGH);
		
		return result;
	}
	
	public void delete(int permissionId){
		Actor principal;
		Permission permission;
		Player player;
		
		permission = this.findOne(permissionId);
		Assert.notNull(permission);

		principal = actorService.findPrincipal();
		
		Assert.isTrue(principal instanceof Player);
		player = (Player)principal;
		Assert.isTrue(permission.getPlayer().equals(player));
		
		permissionRepository.delete(permission);
	}
	
	
	// Other business methods ---------------------------------------------------
	
	public Permission reconstruct(Permission permission, BindingResult binding) {
		Permission result;
		
		result = permission;
		result.setExpirationDate(permission.getExpirationDate());
		result.setPlayer(playerService.findOne(permission.getPlayer().getId()));
		result.setIsReferee(permission.getIsReferee());
		validator.validate(result, binding);
		
		return result;
	}
	
	
	public void flush() {
		permissionRepository.flush();
	}
	
	public Boolean hasAnyPermission(int playerId){
		Boolean result;
		result = false;	

		if(permissionRepository.findPermission(playerId) != null){
			result = true;
		}
	
		return result;
	}
	
	public Boolean hasRefereePermission(int playerId){
		Boolean result;
		
		result = false;	
		
		if(permissionRepository.findTypePermission(playerId, true) != null){
			result = true;
		}
	
		return result;
	}
	
	public Boolean hasManagerPermission(int playerId){
		Boolean result;
		
		result = false;	
		
		if(permissionRepository.findTypePermission(playerId, false) != null){
			result = true;
		}
	
		return result;
	}
	
	public Permission getPermission(int playerId){
		Permission result;
	
		result = permissionRepository.findPermission(playerId);
		
		return result;
	}
	
	public Collection<Player> findPlayersWithNoPermission(){
		Collection<Player> result;
		
		result = permissionRepository.findPlayersWithNoPermission();
		
		return result;
	}
}
