package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Manager;
import domain.Player;
import domain.Team;
import domain.Videogame;
import form.RegisterActor;

@Service
@Transactional
public class ManagerService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ManagerRepository managerRepository;

	// Supporting services ---------------------------------------------------
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private FolderService folderService;

	// Constructor ---------------------------------------------------

	public ManagerService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------
	
	public Manager create() {
		Manager result;

		result = new Manager();
		result.setmyVideogames(new ArrayList<Videogame>());
		result.setTeamsFollowed(new ArrayList<Team>());

		return result;
	}

	public Manager findOne(int managerId) {
		Assert.isTrue(managerId != 0);

		Manager result;

		result = managerRepository.findOne(managerId);

		return result;
	}

	public Collection<Manager> findAll() {
		Collection<Manager> result;

		result = managerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Manager save(Manager manager) {
		Assert.notNull(manager);
		
		Player player;
		Manager result;
		UserAccount userAccount;
		
		player = new Player();
		
		if (manager.getId() != 0) {
			actorService.checkIsManager();
			userAccount = LoginService.getPrincipal();
			Assert.isTrue(manager.getUserAccount().equals(userAccount));
			result = managerRepository.save(manager);
		} else {
			
			actorService.checkIsPlayer();
			Assert.isInstanceOf(Player.class, actorService.findPrincipal());
			player = (Player) actorService.findPrincipal();
			Assert.isTrue(permissionService.hasManagerPermission(player.getId()));
			
			String role;
			Md5PasswordEncoder encoder;

			if (SecurityContextHolder.getContext().getAuthentication() != null
					&& SecurityContextHolder.getContext().getAuthentication()
							.getAuthorities().toArray().length > 0) {
				role = SecurityContextHolder.getContext().getAuthentication()
						.getAuthorities().toArray()[0].toString();
				Assert.isTrue(role.equals("PLAYER"));
			}
			encoder = new Md5PasswordEncoder();
			manager.getUserAccount().setPassword(
					encoder.encodePassword(manager.getUserAccount().getPassword(),
							null));
			
			result = managerRepository.save(manager);
			folderService.createSystemFolders(actorService.findOne(result.getId()));
		}
		
		if(result != null && manager.getId() == 0){
			permissionService.delete(permissionService.getPermission(player.getId()).getId());
		}
		
		return result;
	}

	// Other business methods -------------------------------------------------

	@Autowired
	private Validator validator;

	public Manager reconstruct(RegisterActor formObject, BindingResult binding) {
		Manager result;
		Authority authority;

		authority = new Authority();
		result = create();
		result.setEmail(formObject.getEmail());
		result.setName(formObject.getName());
		result.setSurname(formObject.getSurname());
		authority.setAuthority(Authority.MANAGER);
		formObject.getUserAccount().addAuthority(authority);
		formObject.getUserAccount().setAccountNonLocked(true);
		result.setUserAccount(formObject.getUserAccount());

		validator.validate(formObject, binding);

		return result;
	}
	
	public Manager reconstructEdit(Manager manager, BindingResult binding) {
		Manager result;
		
		result = this.findOne(manager.getId());
		
		manager.setmyVideogames(result.getmyVideogames());
		manager.setTeamsFollowed(result.getTeamsFollowed());
		manager.setUserAccount(result.getUserAccount());

		validator.validate(manager, binding);

		return manager;
	}

	public void flush() {
		managerRepository.flush();
	}
}
