
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ForumCategoryRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.ForumCategory;
import domain.Videogame;

@Service
@Transactional
public class ForumCategoryService {

	// Validator ---------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	// Managed repository ------------------------------------
	
	@Autowired
	private	ForumCategoryRepository	forumCategoryRepository;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService	actorService;
	
	@Autowired
	private ForumService	forumService;

	// Constructors --------------------------------------------
	
	public ForumCategoryService() {
		super();
	}
	
	// Simple CRUD methods -------------------------------------
	
	public ForumCategory create(Videogame videogame) {
		ForumCategory result;

		result = new ForumCategory();
		result.setVideogame(videogame);
		result.setChildrenCategories(new ArrayList<ForumCategory>());

		
		return result;
	}

	public Collection<ForumCategory> findAll() {
		Collection<ForumCategory> result;

		result = this.forumCategoryRepository.findAll();
		Assert.notNull(result);

		return result;
	}
	
	public ForumCategory findOne(int forumCategoryId) {
		Assert.isTrue(forumCategoryId != 0);
		
		ForumCategory result;

		result = this.forumCategoryRepository.findOne(forumCategoryId);

		return result;
	}

	public ForumCategory save(ForumCategory forumCategory) {
		Assert.notNull(forumCategory);
		
		ForumCategory result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();

		Assert.isTrue(forumCategory.getVideogame().getManager().getUserAccount().equals(principal));

		if(this.forumCategoryRepository.forumCategoriesSize(forumCategory.getVideogame().getId())>0){
			Assert.isTrue(forumCategory.getParent()!=null);
		}
		
		Assert.isTrue(!forumCategory.getName().toUpperCase().equals("FORUM"));
		
		if(forumCategory.getParent().getChildrenCategories().contains(forumCategory)){ 
			for(ForumCategory fc : forumCategory.getParent().getChildrenCategories()){ //No se pueden renombrar categor�as con el mismo nombre y el mismo padre
				Assert.isTrue(forumCategory.equals(fc)||!fc.getName().equals(forumCategory.getName()));
			}
		}
		
		this.forumCategoryRepository.flush();
		result = this.forumCategoryRepository.save(forumCategory);

		if(!result.getParent().getChildrenCategories().contains(result)){ //Actualizar la lista de carpetas del padre{
			addChildren(result);
		}
		
		
		return result;
	}

	public void delete(ForumCategory forumCategory){		
		Actor principal;
		
		principal = actorService.findPrincipal();
		
		Assert.isTrue(forumCategory.getVideogame().getManager().equals(principal));

		Set<ForumCategory> categoriesToDelete;

		categoriesToDelete = new HashSet<>();
		
		if(forumCategoryRepository.parentCategory(forumCategory.getId()) != null){ //Actualizar hijos del padre
			ForumCategory parent;
			
			parent = forumCategoryRepository.parentCategory(forumCategory.getId());
			parent.getChildrenCategories().remove(forumCategory);
			
			parent = forumCategoryRepository.saveAndFlush(parent);
		}
		
		deleteRec(forumCategory, categoriesToDelete); //A�adir todas las categor�as a borrar
		
		forumCategoryRepository.flush();
		
		forumCategoryRepository.delete(categoriesToDelete);
	}
	

	//Other business methods -----------------------------------
	public void addChildren(ForumCategory child){
		ForumCategory parent;
		
		parent = child.getParent();

		Assert.notNull(parent);
		Assert.notNull(child);
				
		for(ForumCategory fc : parent.getChildrenCategories()){ //No se pueden crear categor�as con el mismo nombre y el mismo padre
			Assert.isTrue(!fc.getName().equals(child.getName()));
		}
		
		parent.getChildrenCategories().add(child);
		forumCategoryRepository.save(parent);
	}
	
	public ForumCategory findRootForumCategory(Videogame videogame) {
		ForumCategory result;

		result = forumCategoryRepository.findRootForumCategory(videogame);

		return result;
	}

	public Collection<ForumCategory> findForumCategoriesByVideogame(Videogame videogame) {
		Collection<ForumCategory> result;

		result = forumCategoryRepository.findForumCategoriesByVideogame(videogame);

		return result;
	}
	
	public ForumCategory findParentForumCategory(ForumCategory forumCategory) {
		ForumCategory result;

		result = forumCategoryRepository.parentCategory(forumCategory.getId());

		return result;
	}
	
	void deleteRec(ForumCategory category, Set<ForumCategory> categoriesToDelete){
				
		for(ForumCategory c : category.getChildrenCategories()){
			deleteRec(c, categoriesToDelete);
		}
		
		forumService.setParentCategoryFromServices(category); //Quitar la categor�a de todos los servicios
				
		category.setChildrenCategories(new ArrayList<ForumCategory>());
		
		category = forumCategoryRepository.save(category);
		
		categoriesToDelete.add(category);
	}
	

	public ForumCategory reconstruct(ForumCategory forumCategory,ForumCategory parent,  BindingResult binding){
		ForumCategory result;
		ForumCategory oldForumCategory;
		
		result = forumCategory;
		
		if(forumCategory.getId() == 0){
			result.setVideogame(parent.getVideogame());
			result.setChildrenCategories(new ArrayList<ForumCategory>());
			result.setParent(parent);

			
		}else{
			oldForumCategory = forumCategoryRepository.findOne(forumCategory.getId());	
			
			result.setVersion(oldForumCategory.getVersion());
			result.setParent(oldForumCategory.getParent());
			result.setVideogame(oldForumCategory.getVideogame());
			result.setChildrenCategories(oldForumCategory.getChildrenCategories());
		}
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public ForumCategory saveFORUM(Videogame videogame) {
		ForumCategory result;

		result = (ForumCategory) videogame.getForumCategories().toArray()[0];
		result.setVideogame(videogame);
		result.setChildrenCategories(new ArrayList<ForumCategory>());
		result.setName("FORUM");
		
		result = this.forumCategoryRepository.save(result);

		
		return result;
	}
	
	public ForumCategory createFORUM(Videogame videogame) {
		ForumCategory result;

		result = new ForumCategory();
		if(videogame!=null)
			result.setVideogame(videogame);
			result.setName("FORUM");
		result.setChildrenCategories(new ArrayList<ForumCategory>());

		
		return result;
	}

	public void flush() {
		forumCategoryRepository.flush();
	}
	
	
	
}
