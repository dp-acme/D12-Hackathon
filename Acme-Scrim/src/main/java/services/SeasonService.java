package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.SeasonRepository;
import domain.Actor;
import domain.Manager;
import domain.Priority;
import domain.Scrim;
import domain.Season;
import domain.Videogame;

@Service
@Transactional
public class SeasonService {

	// Managed repository ----------------------------------------

	@Autowired
	private SeasonRepository seasonRepository;

	// Supporting services ---------------------------------------

	@Autowired
	private VideogameService videogameService;

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private MessageService messageService;
	
	// Constructor -----------------------------------------------

	public SeasonService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------

	public Season create(int videogameId) {
		// Comprobamos que el objeto que le pasamos por parametro no es nulo
		Assert.isTrue(videogameId != 0);

		// Comprobamos que el usuario logeado es un manager
		this.actorService.checkIsManager();

		// Creamos el ojeto a devolver
		Season result;
		result = new Season();

		// Cogemos el videogame que hemos pasado por parametros
		Videogame videogame;
		videogame = this.videogameService.findOne(videogameId);
		Date startDate;
		startDate = new Date(System.currentTimeMillis() - 1);

		// Actualizamos los valores de el objeto que se esta creado
		result.setFinished(false);
		result.setVideogame(videogame);
		result.setStartDate(startDate);
		// Devolvemos el resultado
		return result;
	}

	public Collection<Season> findAll() {
		// Creamos el objeto a devolver
		Collection<Season> result;
		// Inicializamos el objeto a devolver con los datos del repositorio
		result = this.seasonRepository.findAll();
		// Devolvemos el resultado
		return result;
	}

	public Season findOne(int seasonId) {
		// Comprobamos que el objeto que le pasamos por parametro no es nulo
		Assert.isTrue(seasonId != 0);
		// Creamos el objeto a devolver
		Season result;
		// Inicializamo el valor con el dato del repositorio
		result = this.seasonRepository.findOne(seasonId);
		// Devolvemos el resultado
		return result;
	}

	public Season save(Season season) {
		// Comprobamos que el objeto que queremos guardar no es nulo
		Assert.notNull(season);

		// Comprobamos que el ususario logeado es un manager
		Manager manager;
		manager = this.actorService.checkIsManager();

		// Comprobamos que la season no este terminada
		Assert.isTrue(!season.isFinished());

		// Comprobamos que a la hora de editar es el manager logeado el mismo
		// que el que ha creado la season
		Assert.isTrue(season.getVideogame().getManager().equals(manager));

		// Creamos la variable a devolver
		Season result;

		// Comprobamos si el objeto se esta creado o por el contrario se esta
		// Creando
		if (season.getId() == 0) {
			// Actualizamos referencias
			Date startDate;
			// Actualizar la hora de inicio
			startDate = new Date(System.currentTimeMillis() - 1);
			season.setStartDate(startDate);

			Season temporadaActual = this.seasonRepository.getLastSeasonOfVideogame(season.getVideogame().getId());
			Assert.isTrue(temporadaActual == null || !temporadaActual.isFinished());
			if(temporadaActual != null) {
				this.finished(temporadaActual);
				
				// Notificacion a todos los players con gameAccount en este videojuego de que se ha cambiado la season
				String subject;
				String body;
				Collection<Actor> recipients;
				
				subject = "Season finished in '" + season.getVideogame().getName() + "'";
				subject += " -- ";
				subject += "Temporada finalizada en '" + season.getVideogame().getName() + "'";
				
				body = "The last season of '" + season.getVideogame().getName() + "' has finished. A new one has started then.";
				body += "\n -- \n";
				body += "La �ltima temporada de '" + season.getVideogame().getName() + "' ha finalizado. Una nueva acaba de comenzar.";
				
				recipients = new ArrayList<Actor>();
				recipients.addAll(this.videogameService.listOfPlayersOfVideogame(season.getVideogame().getId()));
				
				messageService.sendNotification(subject, body, recipients, Priority.HIGH);
			}
		}

		result = this.seasonRepository.save(season);
		
		return result;
	}

	// Other methods ---------------------------------------------

	// Scrims asociados a una temporada pasa por parametros
	public Collection<Scrim> findScrimsPassedOfSeason(Season season) {
		// Comprobamos que el objeto que le pasamos no es nulo
		// Assert.notNull(season);

		// Creamos el objeto a devolver
		Collection<Scrim> result = new ArrayList<Scrim>();
		result = this.seasonRepository.findScrimsPassedOfSeason(season.getId());

		// Devolvemos el objeto
		return result;
	}

	public Collection<Scrim> findScrimsNoPassedOfSeason(Season season) {
		// Comprobamos que el objeto que le pasamos no es nulo
		// Assert.notNull(season);

		// Creamos el objeto a devolver
		Collection<Scrim> result = new ArrayList<Scrim>();
		result = this.seasonRepository.findScrimsNoPassedOfSeason(season
				.getId());

		// Devolvemos el objeto
		return result;
	}

	// Metodo para finalizar una season
	public void finished(Season season) {
		// Comprobamos que la season no es nula
		Assert.notNull(season);

		// Comprobamos que la season no esta terminada
		Assert.isTrue(!season.isFinished());

		// Comprobamos que el usuario logeado es un manager
		Manager manager;
		manager = this.actorService.checkIsManager();

		// Comprobamos que el usuario que quiere editar es el mismo que la creo
		Assert.isTrue(season.getVideogame().getManager().equals(manager));

		// Cambiamos el atributo finish a true para decir que est� finalizada la
		// season
		season.setFinished(true);
		this.seasonRepository.save(season);
	}

	// Obtener la �ltima season de un videgame
	public Season getLastSeasonOfVideogame(int videogameId) {
		// Comprobamos que el objeo que le pasamos no es nulo
		Assert.isTrue(videogameId != 0);

		// Creamos el resultado
		Season result = this.seasonRepository
				.getLastSeasonOfVideogame(videogameId);
		// Devolvemos el resultado
		return result;
	}

	// Seasons de un videogame dado
	public Collection<Season> seasonsOfVideoGame(int videogameId) {
		// Comprobamos que el videogame que le pasamos no es nulo
		Assert.isTrue(videogameId != 0);
		// Creamos el objeto a devolver
		Collection<Season> result = new ArrayList<Season>(
				this.seasonRepository.seasonsOfVideoGame(videogameId));
		// Devolvemos el resultado
		return result;
	}

	@Autowired
	private Validator validator;

	public Season reconstruct(Season result,
			BindingResult binding) {
		Season season;

		season = create(result.getVideogame().getId());
		
		result.setFinished(season.isFinished());
		result.setStartDate(season.getStartDate());

		validator.validate(result, binding);

		return result;
	}

	public void flush() {
		seasonRepository.flush();
	}

}
