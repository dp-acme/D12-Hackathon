<%--
 * textbox.tag
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@ tag language="java" body-content="empty"%>

<%-- Taglibs --%>

<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%-- Attributes --%>

<%@ attribute name="path" required="true"%>
<%@ attribute name="code" required="true"%>

<%@ attribute name="required" required="false"%>
<%@ attribute name="readonly" required="false"%>
<%@ attribute name="placeholderCode" required="false"%>
<%@ attribute name="type" required="false" %>
<%@ attribute name="onchange" required="false" %>


<jstl:if test="${readonly == null}">
	<jstl:set var="readonly" value="false" />
</jstl:if>

<jstl:set var="placeholderMessage" value="" />
<jstl:if test="${placeholderCode != null}">
	<spring:message code="${placeholderCode}" var="placeholderMessage" />
</jstl:if>

<%-- Definition --%>
<jstl:if test="${required ==null}">
	<div>
		<form:label path="${path}">
			<spring:message code="${code}" />
		</form:label>
		<form:input path="${path}" readonly="${readonly}" placeholder="${placeholderMessage}" type="${type}" onchange="${onchange}" />
		<form:errors path="${path}" cssClass="error" />
	</div>
</jstl:if>

<jstl:if test="${required !=null}">
	<div>
		<form:label path="${path}">
			<spring:message code="${code}" />
		</form:label>
		<form:input path="${path}" readonly="${readonly}" placeholder="${placeholderMessage}" required ="required" type="${type}" onchange="${onchange}" />
		<form:errors path="${path}" cssClass="error" />
	</div>
</jstl:if>