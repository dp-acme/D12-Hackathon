<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<a href=""><img class="banner" style="max-width: 45%" src="${banner}" alt="Acme Scrim" /></a>
</div>

<hr />

<jstl:if test="${empty isPremium}">
	<spring:url value="${adURL}" var="advertisementURL" />
	<spring:url value="${adImage}" var="advertisementImage" />
	<jstl:if test="${not empty isSystemAd}">
		<security:authorize access="hasRole('SPONSOR')">
			<spring:url value="advertisement/sponsor/create.do" var="advertisementURL" />
		</security:authorize>
	</jstl:if>
	<div style="height: 120px;">
		<a href="${advertisementURL}"><img style="max-height: 100%; max-width: 100%;" class="banner" src="${advertisementImage}" alt="Ad" /></a>
	</div>

	<hr />
</jstl:if>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv"><spring:message	code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="player/create.do"><spring:message code="master.page.player" /></a></li>
					<li><a href="sponsor/create.do"><spring:message code="master.page.sponsor" /></a></li>
				</ul>
			</li>
		</security:authorize>	
		
		<security:authorize access="hasRole('PLAYER')">
			<li><a class="fNiv"><spring:message	code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="referee/create.do"><spring:message code="master.page.referee" /></a></li>
					<li><a href="manager/create.do"><spring:message code="master.page.manager" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<li><a class="fNiv" href="player/list.do"><spring:message	code="master.page.players" /></a>
		<li><a class="fNiv" href="gameCategory/list.do"><spring:message	code="master.page.gameCategory" /></a>
		
		<security:authorize access="hasAnyRole('ADMIN', 'MANAGER')">
			<li><a class="fNiv" href="referee/list.do"><spring:message	code="master.page.referees" /></a>
		</security:authorize>
		
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv" href="manager/list.do"><spring:message	code="master.page.managers" /></a>
			<li><a class="fNiv" href="sponsor/list.do"><spring:message	code="master.page.sponsors" /></a>
			<li><a class="fNiv" href="permission/create.do"><spring:message	code="master.page.permissions" /></a>
			<li><a class="fNiv" href="socialNetwork/list.do"><spring:message	code="master.page.socialNetworks" /></a>
		</security:authorize>
	
		<li><a href="videogame/list.do"><spring:message code="master.page.videogames" /></a></li>
	
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
		</security:authorize>
		

		
		<security:authorize access="isAuthenticated()">
			<li><a class="fNiv" href="actor/listTeamsFollowed.do"><spring:message code="master.page.teamsFollowed" /></a>
			
			<li>
				<a class="fNiv"> 
					  
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>	

					<li><a href="folder/list.do"><spring:message code="master.page.messages" /></a></li>
					<li><a href="scrim/list.do"><spring:message code="master.page.finder" /></a></li>

					<security:authorize access="hasRole('PLAYER')">
						<jstl:if test="${empty isPremium}">
							<li><a href="premiumSubscription/create.do"><spring:message code="master.page.premium" /></a></li>
						</jstl:if>		
						
						<li><a href="player/display.do"><spring:message code="master.page.profile" /></a></li>
					</security:authorize>
					
					<security:authorize access="hasRole('MANAGER')">
						<li><a href="manager/display.do"><spring:message code="master.page.profile" /></a></li>
						<li><a href="videogame/manager/list.do"><spring:message code="master.page.myVideogames" /></a></li>
					</security:authorize>

					<security:authorize access="hasRole('ADMIN')">
						<li><a href="dashboard/display.do"><spring:message code="master.page.dashboard" /></a></li>
						<li><a href="configuration/display.do"><spring:message code="master.page.configuration" /></a></li>
					</security:authorize>
					
					<security:authorize access="hasRole('REFEREE')">
						<li><a href="referee/display.do"><spring:message code="master.page.profile" /></a></li>
						<li><a href="referee/dashboard.do"><spring:message code="master.page.dashboard" /></a></li>
					</security:authorize>
					
					<security:authorize access="hasRole('SPONSOR')">
						<li><a href="sponsor/display.do"><spring:message code="master.page.profile" /></a></li>
					
					
					<li><a href="advertisement/sponsor/list.do"><spring:message code="master.page.myAdvertisement" /></a></li>
					</security:authorize>
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
			
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

<!-- <br /> -->
<!-- <div> -->
<%-- 	<a href="${advertisement.getWeb()}"> <img class="image" src="${advertisement.getImage()}" --%>
<!-- 		alt="Random image" /> -->
<!-- 	</a> -->
<!-- </div> -->

