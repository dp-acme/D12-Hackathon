<%--
 * footer.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:useBean id="date" class="java.util.Date" />

<br />
<hr />

<a href="welcome/faq.do"><spring:message code="master.page.faq" /></a> | 
<a href="welcome/aboutUs.do"><spring:message code="master.page.about" /></a> | 
<a href="welcome/termsAndConditions.do"><spring:message code="master.page.terms" /></a> | 
<a href="welcome/advertise.do"><spring:message code="master.page.advertise" /></a>

<br />

<b>Copyright &copy; <fmt:formatDate value="${date}" pattern="yyyy" /> Sample Co., Inc.</b>

<input type="hidden" id="cookieAlertMessage" value="<spring:message code="master.page.cookieAlertMessage"/>" />

<script>
	function setCookie(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+ d.toUTCString();
	    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}
	
	function getCookie(cname) {
	    var name = cname + "=";
	    var decodedCookie = decodeURIComponent(document.cookie);
	    var ca = decodedCookie.split(';');
	    for(var i = 0; i <ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "true";
	}
	
	function isAlertSet(){
		return getCookie("showCookieAlert") == "true";
	}
	
	function turnAlertOff(){
		setCookie("showCookieAlert", "false", 365);
	}
</script>

<script>	
	if(isAlertSet()){
		if(confirm(document.getElementById("cookieAlertMessage").value)){
			turnAlertOff();
		} else{
			window.location.replace("http://www.google.com");
		}
	}
</script>