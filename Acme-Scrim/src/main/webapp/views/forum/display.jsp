<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%@page import="org.springframework.web.util.HtmlUtils" %> 
<%@page import="java.util.Collection" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="domain.ForumMessage" %>

<jstl:set value="${parentMessages}" var="f" />

<spring:message code="forumCategory.delete.message" var="deleteForumMessageMessage" />
<spring:message code="forum.dateFormatCode" var="dateFormat"/>
<jstl:set value="${isAuthenticated}" var="isAuthenticated" />
<jstl:set value="${isOwner}" var="isOwner" />



<% 
	@SuppressWarnings("unchecked")
	Collection<ForumMessage> rootForumMessage = (Collection<ForumMessage>)pageContext.getAttribute("f"); 
	HashMap<String, String> messages = new HashMap<String, String>();
	Boolean isAuthenticated = (Boolean)pageContext.getAttribute("isAuthenticated"); 
	Boolean isOwner = (Boolean)pageContext.getAttribute("isOwner"); 

	messages.put("dateFormat", (String)pageContext.getAttribute("dateFormat"));
%>

<%! 
	String safeHTML(String str){ //Funci�n para hacer HTML seguro
		return HtmlUtils.htmlEscape(str);
	}
%>

<%! 
	String displayForumMessages(Collection<ForumMessage> forumMessages, String dateFormat, Boolean isAuthenticated, Boolean isOwner){
		String res = "";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		res += "<div style='margin-left: 0px; margin-right: 0px;'>";
		
		Iterator<ForumMessage> it = forumMessages.iterator();
		while(it.hasNext()){
			ForumMessage f = it.next();
			res += "<fieldset style='border-radius: 5px 5px; display: inline-block;margin-top: 10px;'>";
			res += "<img id='arrow_forumMessage_" + f.getId() + "' src='images/flechaDerecha.png' " + 
					   "width='12px' height='12px' style='visibility: " +
					   (f.getChildrenMessages().size() > 0? "visible" : "hidden") + 
					   "; cursor: pointer;'" +
					   "onClick='switchChildrenVisibility(" + f.getId() + ")' />&nbsp;";
					   
			res += "<div id='forumMessage_" + f.getId() + "_data' style='display: inline-block;'>";
			res += "<span id='forumMessage_" + f.getId() + "_body'>";
			res += safeHTML(f.getBody());
			res += "</span>";
			res += "</div><br>";
			res += "<div style='float:left; color:gray; margin-right: 5em'>";
			res += safeHTML(f.getActor().getUserAccount().getUsername()+"	");
			res += "</span>";
			res += "</div>";
			if(isAuthenticated){
				res += "<div style='float:right'>";
				res += "&nbsp;<img id='addChild_forumMessage_" + f.getId() + "' src='images/reply.png' " + 
						   "width='12px' height='12px'" + 
						   "style='cursor: pointer;'"+
						   "onClick='showForumMessageFormP(" + f.getId() + ")'/>";			
			if(isOwner){
				res += "&nbsp;<img id='delete_forumCategory_" + f.getId() + "' src='images/borrarCarpeta.png' " + 
						   "width='12px' height='12px'" + 
						   "style='cursor: pointer;'"+
						   "onClick='deleteForumMessage(" + f.getId() + ")'/>";
			}
			res += "</div>";
			}
			res += "<div style='float:right; color:	#C0C0C0'>";
			
			res += sdf.format(f.getMoment());
			res += "</div>";
	
			res += "</fieldset>";
			
			 if(f.getChildrenMessages().size() > 0){ //Poner los hijos
				res += "<div id='forumMessage_" + f.getId() + "_children' style='display: none; margin-left: 20px; margin-bottom: 5px;'>";
				res += displayForumMessages(f.getChildrenMessages(), dateFormat, isAuthenticated, isOwner);
				res += "</div>";
			} 
			
			if(it.hasNext()){				
				res += "<br id='imgPadding" + f.getId() + "' style='display: inherit;'>";
			}
		}
			
		res += "</div>";

		return res;
	}
%>

<!-- BARRA DE CARPETAS -->
<script>
		function deleteForumMessage(id){
				relativeRedir("forum/deleteMessage.do?forumMessageId=" + id);
		}
	function switchChildrenVisibility(id){
		element = document.getElementById('forumMessage_' + id + "_children");	
		
		if(element.style.display == 'none'){
			element.style.display = 'inherit';
			element = document.getElementById('arrow_forumMessage_' + id).src = "images/flechaAbajo.png";	
			document.getElementById('imgPadding' + id).style.display = 'none';
	
		} else{
			element.style.display = 'none';			
			element = document.getElementById('arrow_forumMessage_' + id).src = "images/flechaDerecha.png";	
			document.getElementById('imgPadding' + id).style.display = 'inherit';
		}
	}
		
	function showForumMessageFormP(id){
		document.getElementById('forumMessageWithParent').action = 'forum/createMessage.do?parentId=' + id;
		document.getElementById('createForumMessageWithParent').style.display = 'inline-block';
		document.getElementById('imgPadding').style.display = 'inherit';
	}
	
	function hideForumMessageFormP(){
		document.getElementById('createForumMessageWithParent').style.display = 'none';
		document.getElementById('imgPadding').style.display = 'none';
	}	

	function hideForumMessageFormWP(){
		document.getElementById('createForumMessageWithoutParent').style.display = 'none';
		document.getElementById('imgPadding').style.display = 'none';
	}
	
	
	function showForumMessageFormWP(){
		hideForumMessageFormP();
		document.getElementById('createForumMessageWithoutParent').style.display = 'inline-block';
		document.getElementById('imgPadding').style.display = 'inherit';
	}
	
	function submitEditionForm(id){
		body = document.getElementById("body_" + id).value;
		bodyHidden = document.getElementById("edition_form_body_" + id).childNodes[0];
		idHidden = document.getElementById("edition_form_id_" + id).childNodes[0];
		
		bodyHidden.value = body; //Copiar los datos al form
		idHidden.value = id;
				
		document.getElementById("editionForm_" + id).submit(); //Hacer submit
	}
	
</script>

<div>
	<a  href="forum/list.do?videogameId=${ videogameId}">
			<spring:message code="forum.message.back" />
	</a><br>
	<jstl:if test="${isAuthenticated}">
	<div id='createForumMessageWithParent' style="display: none;">
		<form:form action="forum/createMessage.do" modelAttribute="forumMessageWithParent">
			<form:hidden path="actor"/>
			<form:hidden path="forum"/>				
			
			<acme:textarea path="body" code="forum.message.body"/>
			
			<div style="margin-top: 5px;">
				<acme:submit name="createWithParent" code="forumCategory.create"/>
				<spring:message code="forumCategory.cancel" var="cancelButton" />
				<input type="button" value="${cancelButton}" onclick="hideForumMessageFormP()">
			</div>
		</form:form>		
	</div>

	<div id='createForumMessageWithoutParent' style="display: none;">
		<form:form action="forum/createMessage.do" modelAttribute="forumMessageWithoutParent">
				<form:hidden path="actor"/>
			<form:hidden path="forum"/>			
			<acme:textarea path="body" code="forum.message.body" required="required"/>
			
			<div style="margin-top: 5px;">
				<acme:submit name="createWithoutParent" code="forumCategory.create"/>
				<spring:message code="forumCategory.cancel" var="cancelButton" />
				<input type="button" value="${cancelButton}" onclick="hideForumMessageFormWP()">
			</div>
		</form:form>
	</div>
	</jstl:if>
				
	<br id='imgPadding' style="display: none;">
	
	<jstl:if test="${ isAuthenticated}">	
	<img src="images/newMessage.png" width="12px" height="12px"
	     style="cursor: pointer; margin-left: 15px; margin-bottom: 3px; margin-top: 10px;"
		 onclick="showForumMessageFormWP()">
	</jstl:if>
		<%
			out.print(displayForumMessages(rootForumMessage, messages.get("dateFormat"),isAuthenticated,isOwner));
		%>
</div>
