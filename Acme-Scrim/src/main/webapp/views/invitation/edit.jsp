<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form modelAttribute="invitation" action="invitation/create.do">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	
	<form:hidden path="team" />
	
	<acme:select items="${gameAccounts}" itemLabel="nickname" code="invitation.edit.teammate" path="teammate" required="required" />
	
	<acme:textarea code="invitation.edit.message" path="message" />

	<br />

	<acme:submit code="invitation.edit.save" name="save" />
	<acme:cancel code="invitation.edit.cancel" url="team/display.do?teamId=${invitation.getTeam().getId()}" />

</form:form>
