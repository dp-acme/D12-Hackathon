<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>



<jsp:useBean id="date" class="java.util.Date" />


<spring:message code="scrim.search.dateIntervalMessage" var="dateIntervalMessage"/>
<input type="hidden" value="${dateIntervalMessage}" id="dateIntervalMessage">

<script>
function checkDateFormat(str){
	var regex = /^[0-9][0-9]?\/[0-9][0-9]?\/[0-9][0-9]?[0-9]?[0-9]?$/;
	
	return regex.test(str);
}

function checkDateInterval(){
	var res = true;
	
	var dateStartStr = document.getElementById('dateStart').value;
	var dateEndStr = document.getElementById('dateEnd').value;
	
	if(checkDateFormat(dateStartStr) && checkDateFormat(dateEndStr)){
		var dateStart = new Date(dateStartStr);
		var dateEnd = new Date(dateEndStr);
		
		res = dateStart <= dateEnd;
		
		if(!res){
			alert(document.getElementById('dateIntervalMessage').value);
		}	
	}
	
	return res;
}

function checkIntervals(){
	return checkPriceInterval() && checkDateInterval();
}
</script>

<security:authorize access="isAuthenticated()">
		<form:form action="${requestURI}" modelAttribute="finder">
			<form:hidden path="id" />
			<form:hidden path="version" />
			
			<form:hidden path="actor" />
			
			<fmt:formatDate value="${date}" pattern="dd/MM/yyyy HH:mm" var="formattedDate" />
			<input type="hidden" name="lastSaved" value="${formattedDate}" />
			
			<acme:textbox code="scrim.search.keyword" path="keyWord"/>
			
			
			<form:label path="dateStart">
				<spring:message code="scrim.search.startDate" />
			</form:label>
			<form:input path="dateStart" placeholder="dd/MM/yyyy" id="dateStart" />
			<form:errors path="dateStart" cssClass="error"/>

			<form:label path="dateEnd">
				<spring:message code="scrim.search.endDate" />
			</form:label>
			<form:input path="dateEnd" placeholder="dd/MM/yyyy" id="dateEnd" />
			<form:errors path="dateEnd" cssClass="error"/>
						
			<spring:message code="scrim.search.videogame" />
			<form:select path="videogame">
				<form:option label="----" value="0" />
				<form:options items="${videogames}" itemLabel="name" itemValue="id" />
			</form:select>

		<jstl:choose>
			<jstl:when test="${cookie.language.value == 'es'}">
				<jstl:set value="spanishName" var="regionLabel" />
			</jstl:when>
			<jstl:otherwise>
				<jstl:set value="englishName" var="regionLabel" />
			</jstl:otherwise>
		</jstl:choose>
			
				
		<spring:message code="scrim.search.region" />
		<form:select path="region">
				<form:option label="----" value="0" />
				<form:options items="${regions}" itemLabel="${regionLabel }" itemValue="id" />
			</form:select>

			<spring:message code="scrim.search.button" var="searchButton" />
			<input type="submit" name="search" value="${searchButton}" 
			onclick="javascript: return checkIntervals();"/>

		</form:form>
</security:authorize>

<display:table name="scrims" id="row" requestURI="${requestURI}" pagesize="5">

	<spring:message code="scrim.list.moment" var="moment" />
	<display:column title="${moment}" sortable="true">
		<spring:message code="scrim.list.formatDate" var="formatDate" />
		<fmt:formatDate value="${row.getMoment()}" pattern="${formatDate}" />
	</display:column>

	<spring:message code="scrim.list.guest" var="guest" />
	<display:column property="guest.name" title="${guest}" sortable="true" />
	
	<spring:message code="scrim.list.creator" var="creator" />
	<display:column property="creator.name" title="${creator}" sortable="true" />
	
	<display:column >
		<a  href="scrim/display.do?scrimId=${row.getId()}">
			<spring:message code="scrim.search.display" />
		</a>
	</display:column>	
	
</display:table>