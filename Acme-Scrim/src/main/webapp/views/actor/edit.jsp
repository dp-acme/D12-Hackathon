<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:if test="${empty hasPermission}">
	<fieldset style="border-radius: 5px 15px; display: inline-block; margin: 0 auto;
				 background-color: #F7F8E0">
	<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="actor.fieldset" /></h3>
	<hr>
		<form:form action="${requestURI}" modelAttribute="registerActor">
			
			<security:authorize access="isAnonymous()">
				
				<acme:textbox code="actor.edit.username" path="userAccount.username" required="required"/>
				<br>
				
				<acme:password code="actor.edit.password" path="userAccount.password" required="required"/>
				<br>
				
				<acme:password code="actor.edit.confirmPassword" path="password" required="required"/>
				<br>
				
			</security:authorize>
			
			<jstl:if test="${requestURI == \"referee/create.do\" || requestURI == \"manager/create.do\"}">
				<acme:textbox code="actor.edit.username" path="userAccount.username" required="required"/>
				<br>
				
				<acme:password code="actor.edit.password" path="userAccount.password" required="required"/>
				<br>
				
				<acme:password code="actor.edit.confirmPassword" path="password" required="required"/>
				<br>
			</jstl:if>
				
			<jstl:if test="${not empty edit}">
				<form:hidden path="id" />
				<form:hidden path="version"/>
			</jstl:if>
			
			<acme:textbox code="actor.edit.name" path="name" required="required"/>
			<br>
			
			<acme:textbox code="actor.edit.surname" path="surname"/>
			<br>
			
			<acme:textbox code="actor.edit.email" path="email" required="required"/>
			<br>
			
		<jstl:if test="${empty edit}">
			<form:label path="termsAccepted">
					<a href ="welcome/termsAndConditions.do"><spring:message code="actor.edit.termsAccepted" /></a>
			</form:label>
			<form:checkbox path="termsAccepted"/>
			<form:errors path="termsAccepted" cssClass="error" />
			<br>
			<br>
			<jstl:if test="${showMessageTerms !=null && showMessageTerms}">
					<p class="error"><spring:message code="actor.edit.termsNotAccepted"/></p>
			</jstl:if>
			
			<jstl:if test="${showMessageNotMatchingPasswords !=null && showMessageNotMatchingPasswords}">
					<p class="error"><spring:message code="actor.edit.notMatchingPasswords"/></p>
			</jstl:if>
			
		</jstl:if>
			<acme:submit name="save" code="actor.edit.save"/>
			<acme:cancel url="${backURI}" code="actor.edit.cancel"/>
			
		</form:form>
	</fieldset>
</jstl:if>
<jstl:if test="${not empty hasPermission}">
	<jstl:if test="${requestURI == \"referee/create.do\"}">
		<p class="error"><spring:message code="actor.edit.noPermissionReferee"/></p>
		<a href="player/askRefereePermission.do"><spring:message code = "actor.edit.askPermission"/></a>
	</jstl:if>
	<jstl:if test="${requestURI == \"manager/create.do\"}">
		<p class="error"><spring:message code="actor.edit.noPermissionManager"/></p>
		<a href="player/askManagerPermission.do"><spring:message code = "actor.edit.askPermission"/></a>
	</jstl:if>
</jstl:if>
