<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<jstl:if test="${not empty profile}">
	<spring:message code="actor.list.edit" var="profileHeader" />
	<spring:url value="${editURI.concat(actor.id)}"
				var="editActor" />
			<a href="${editActor}"> <jstl:out value="${profileHeader}" />
			</a>
</jstl:if>

<security:authorize access="hasRole('ADMIN')">
	<jstl:if test="${isBanned}">
		<acme:cancel url="actor/unban.do?actorId=${actor.getId()}" code="actor.list.unban"/>
	</jstl:if>
	<jstl:if test="${not isBanned}">
		<acme:cancel url="actor/ban.do?actorId=${actor.getId()}" code="actor.list.ban"/>
	</jstl:if>
</security:authorize>

<p>
	<b>
		<spring:message code="actor.list.username"/>:
	</b>
	<jstl:out value="${actor.getUserAccount().getUsername()}" />
</p>

<p>
	<b>
		<spring:message code="actor.list.name"/>:
	</b>
	<jstl:out value="${actor.getName()}" />
</p> 

<p>
	<b>
		<spring:message code="actor.list.surname"/>:
	</b>
	<jstl:out value="${actor.getSurname()}" />
</p> 

<p>
	<b>
		<spring:message code="actor.list.email"/>:
	</b>
	<jstl:out value="${actor.getEmail()}" />
</p> 
 
 <jstl:if test="${not empty socialIdentities}">
	
	<h3><spring:message code="actor.list.socialIdentities" /></h3>
	
	<display:table requestURI="${requestURI}" name="socialIdentities" id="row" pagesize="5">
	
		<display:column titleKey="player.display.socialIdentity.socialNetwork" property="socialNetwork.name"/>
		
		<display:column titleKey="player.display.socialIdentity.link" href="${row.link}">
			<spring:message code="player.display.socialIdentity.link"></spring:message>
		</display:column>
	
		<jstl:if test="${not empty profile}">
			<display:column title="" sortable="false">
				<a href="socialIdentity/create.do?socialIdentityId=${row.getId()}"> <spring:message
						code="actor.list.edit" />
				</a>
			</display:column>
			
			<display:column title="" sortable="false">
				<a href="socialIdentity/delete.do?socialIdentityId=${row.getId()}"> <spring:message
						code="actor.list.delete" />
				</a>
			</display:column>
		</jstl:if>
	</display:table>
 </jstl:if>
 
<jstl:if test="${not empty profile}">
		<a href="socialIdentity/create.do"><spring:message code="actor.list.createSocial" /></a>
</jstl:if>
 
<jstl:if test="${requestURI == '/player/display.do'}">
	
<br />
	
	<h3><spring:message code="actor.player.gameAccounts" /></h3>

	<display:table requestURI="${requestURI}" name="gameAccounts" id="row" pagesize="5">
	
		<display:column titleKey="actor.player.gameAccount.videogame">
			<a href="videogame/display.do?videogameId=${row.getVideogame().getId()}"><jstl:out value="${row.getVideogame().getName()}" /></a>
		</display:column>
	
		<display:column titleKey="actor.player.gameAccount.nickname" property="nickname" />
	
		<display:column titleKey="actor.player.gameAccount.region">
			<acme:displayRegion region="${row.getRegion()}"/>
		</display:column>
		
		<display:column>
		
			<a href="player/display.do?playerId=${actor.getId()}&gameAccountId=${row.getId()}"><spring:message code="actor.player.gameAccount.teams" /></a>
		
		</display:column>
		
		<security:authorize access="hasRole('PLAYER')">
	
		<display:column>
			<a href="gameAccount/edit.do?gameAccountId=${row.getId()}"><spring:message code="actor.player.gameAccount.edit" /></a>
		</display:column>
		
		</security:authorize>
	</display:table>

	<jstl:if test="${not empty gameAccount && teams != null }">
	
	<br />
	
		<h3><spring:message code="actor.player.teams" /> <a href="videogame/display.do?videogameId=${gameAccount.getVideogame().getId()}" >
			<jstl:out value="${gameAccount.getVideogame().getName()}" /></a></h3>
	
		<display:table requestURI="${requestURI}" name="teams" id="row" pagesize="5">
	
			<display:column titleKey="actor.player.team.name">
				<a href="team/display.do?teamId=${row.getId()}"><jstl:out value="${row.getName()}" /></a>
			</display:column>
		
			<display:column titleKey="actor.player.team.region">
				<acme:displayRegion region="${row.getRegion()}"/>
			</display:column>
		
			<display:column titleKey="actor.player.team.web">
				<jstl:if test="${not empty row.getWeb()}">
					<jstl:set var="web" value="${row.getWeb()}" />
					<a href="${web}"><jstl:out value="${web}" /></a>
				</jstl:if>
			</display:column>
			
			<display:column titleKey="actor.player.team.leader.nickname">
				<a href="player/display.do?playerId=${row.getLeader().getPlayer().getId()}&gameAccountId=${row.getLeader().getId()}"><jstl:out value="${row.getLeader().getNickname()}" /></a>
			</display:column>
			
		</display:table>
	
	</jstl:if>


</jstl:if>