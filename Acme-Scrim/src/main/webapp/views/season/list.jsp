<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<h2>
	<spring:message code="season.list.lastSeasons" />
	:
</h2>

<display:table name="seasons" id="row1" requestURI="${requestURI}"
	pagesize="5">

	<jstl:if test="${requestURI == 'season/list.do'}">
		<spring:url var="urlDisplayCategory"
			value="season/list.do?videogameId=${row1.getVideogame().getId()}&seasonId=${row1.getId()}" />
	</jstl:if>

	<jstl:if test="${requestURI == 'season/manager/list.do'}">
		<spring:url var="urlDisplayCategory"
			value="season/manager/list.do?videogameId=${row1.getVideogame().getId()}&seasonId=${row1.getId()}" />
	</jstl:if>

	<display:column titleKey="season.list.name">
		<a href="${urlDisplayCategory}"><jstl:out value="${row1.name}"></jstl:out></a>
	</display:column>

</display:table>

<h2>
	<spring:message code="season.list.currentSeason" />
	:
</h2>

<jstl:if test="${currentSeason == null}">
	<spring:message code="season.list.message" />
	<br />
	<br />
</jstl:if>

<jstl:if test="${currentSeason != null}">
	<fieldset>
		<legend>
			<b><spring:message code="season.list.datas" /></b>
		</legend>

		<jstl:choose>
			<jstl:when test="${currentSeason.isFinished()}">
				<jstl:set value="background-color: #BDECB6" var="style"></jstl:set>
				<span style="color: red"><spring:message
						code="season.display.finished" /></span>
				<br />
				<br />
			</jstl:when>
			<jstl:otherwise>
				<jstl:set value="background-color: #A18BC1" var="style"></jstl:set>
				<span style="color: green"><spring:message
						code="season.display.NotFinished" /></span>
				<br />
				<br />
			</jstl:otherwise>
		</jstl:choose>

		<b><spring:message code="season.list.name" />:</b>
		<jstl:out value="${currentSeason.getName()}" />
		<br /> <br /> <b><spring:message code="season.list.startDate" />:</b>
		<spring:message code="season.list.formatTime" var="formatDate" />
		<fmt:formatDate value="${currentSeason.getStartDate()}"
			pattern="${formatDate}" var="publicationDateFmt" />

		<jstl:out value="${publicationDateFmt}"></jstl:out>
		<br /> <br /> <b><spring:message code="season.list.Videogame" />:</b><a
			href="videogame/display.do?videogameId=${currentSeason.getVideogame().getId()}"><jstl:out
				value="${currentSeason.getVideogame().getName()}" /></a> <br />

		<security:authentication property="principal" var="principal" />

		<security:authorize access="hasRole('MANAGER')">
			<jstl:if
				test="${requestURI == 'season/manager/list.do' && principal == currentSeason.getVideogame().getManager().getUserAccount()}">
				<a href="season/manager/create.do?videogameId=${videogame.getId()}">
					<spring:message code="season.list.create" />
				</a>
			</jstl:if>
		</security:authorize>

		<jstl:if test="${!currentSeason.isFinished()}">
			<br />
			<br />
			<jstl:if test="${scrimsNoPasadas !=null}">
				<h2>
					<spring:message code="season.list.scrimsNoPasadas" />
					:
				</h2>
				<jstl:choose>
					<jstl:when test="${not empty scrimsNoPasadas}">
						<display:table name="scrimsNoPasadas" id="row"
							requestURI="${requestURI}" pagesize="5">

							<spring:message code="season.list.scrim.momentFormat"
								var="formatDate" />
							<display:column titleKey="season.list.scrim.moment"
								class="${row.getStatus().getScrimStatus()}BG" property="moment"
								format="{0,date,${formatDate}}" />

							<display:column property="creator.name"
								titleKey="season.list.scrim.creator"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="guest.name"
								titleKey="season.list.scrim.guest"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="creatorTeamResult.result"
								titleKey="season.list.scrim.creatorTeamResult"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="guestTeamResult.result"
								titleKey="season.list.scrim.guestTeamResult"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="status.scrimStatus"
								titleKey="season.list.scrim.scrimStatus"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column title="" sortable="false"
								class="${row.getStatus().getScrimStatus()}BG">
								<a href="scrim/display.do?scrimId=${row.getId()}"> <spring:message
										code="season.list.display" />
								</a>
							</display:column>
						</display:table>
					</jstl:when>
					<jstl:otherwise>
						<spring:message code="season.list.scrimNoPasadas"></spring:message>
					</jstl:otherwise>

				</jstl:choose>
			</jstl:if>
			<br />
			<br />
			<jstl:if test="${scrimsPasadas !=null}">
				<h2>
					<spring:message code="season.list.scrimsPasadas" />
					:
				</h2>
				<jstl:choose>
					<jstl:when test="${not empty scrimsPasadas}">
						<display:table name="scrimsPasadas" id="row2"
							requestURI="${requestURI}" pagesize="5">


							<spring:message code="season.list.scrim.momentFormat"
								var="formatDate" />
							<display:column titleKey="season.list.scrim.moment"
								class="${row2.getStatus().getScrimStatus()}BG" property="moment"
								format="{0,date,${formatDate}}" />

							<display:column property="creator.name"
								titleKey="season.list.scrim.creator"
								class="${row2.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="guest.name"
								titleKey="season.list.scrim.guest"
								class="${row2.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="creatorTeamResult.result"
								titleKey="season.list.scrim.creatorTeamResult"
								class="${row2.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="guestTeamResult.result"
								titleKey="season.list.scrim.guestTeamResult"
								class="${row2.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="status.scrimStatus"
								titleKey="season.list.scrim.scrimStatus"
								class="${row2.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column title="" sortable="false"
								class="${row2.getStatus().getScrimStatus()}BG">
								<a href="scrim/display.do?scrimId=${row2.getId()}"> <spring:message
										code="season.list.display" />
								</a>
							</display:column>
						</display:table>
					</jstl:when>
					<jstl:otherwise>
						<spring:message code="season.list.scrimPasadas"></spring:message>
					</jstl:otherwise>
				</jstl:choose>
			</jstl:if>
		</jstl:if>

		<jstl:if test="${currentSeason.isFinished()}">
			<jstl:if test="${scrimsPasadas !=null}">
				<h2>
					<spring:message code="season.list.scrimsPasadas" />
					:
				</h2>
				<jstl:choose>
					<jstl:when test="${not empty scrimsPasadas}">
						<display:table name="scrimsPasadas" id="row"
							requestURI="${requestURI}" pagesize="5">

							<spring:message code="season.list.scrim.momentFormat"
								var="formatDate" />
							<display:column titleKey="season.list.scrim.moment"
								class="${row.getStatus().getScrimStatus()}BG" property="moment"
								format="{0,date,${formatDate}}" />

							<display:column property="creator.name"
								titleKey="season.list.scrim.creator"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="guest.name"
								titleKey="season.list.scrim.guest"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="creatorTeamResult.result"
								titleKey="season.list.scrim.creatorTeamResult"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="guestTeamResult.result"
								titleKey="season.list.scrim.guestTeamResult"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column property="status.scrimStatus"
								titleKey="season.list.scrim.scrimStatus"
								class="${row.getStatus().getScrimStatus()}BG">
							</display:column>

							<display:column title="" sortable="false"
								class="${row.getStatus().getScrimStatus()}BG">
								<a href="scrim/display.do?scrimId=${row.getId()}"> <spring:message
										code="season.list.display" />
								</a>
							</display:column>
						</display:table>
					</jstl:when>
					<jstl:otherwise>
						<spring:message code="season.list.scrimPasadas"></spring:message>
					</jstl:otherwise>
				</jstl:choose>

			</jstl:if>
		</jstl:if>
	</fieldset>

</jstl:if>


