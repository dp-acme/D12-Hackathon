<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<security:authorize access="hasRole('MANAGER')">

	<!-- Abrimos el formulario de edicion/ creacion -->

	<form:form action="season/manager/create.do" modelAttribute="season">

		<form:hidden path="videogame" />

		<br />

		<fieldset>
			<legend>
				<h2>
					<b><spring:message code="season.create.seasonDatas" />:</b>
				</h2>
			</legend>

			<spring:message code="season.create.formatTime"
				var="formatPlaceholder" />

			<!-- Indicamos los campos de formulario -->
			<b><spring:message code="season.create.videogame" /></b>
			<jstl:out value="${season.getVideogame().getName()}"></jstl:out>
			<br /> <b><acme:textbox code="season.create.name" path="name"
					required="required" /></b> <br />
		</fieldset>
		<br />

		<!-- Botones del formulario -->
		<acme:submit name="save" code="season.create.save" />

		<acme:cancel
			url="${request}"
			code="season.create.cancel" />
	</form:form>

</security:authorize>
