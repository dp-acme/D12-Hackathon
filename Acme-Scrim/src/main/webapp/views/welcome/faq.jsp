<%--
 * faq.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><b><spring:message code="welcome.question1" /></b></p>
<p><spring:message code="welcome.answer1" /></p> 

<p><b><spring:message code="welcome.question2" /></b></p>

<spring:message code="welcome.answer2" />
<ul>
	<li><spring:message code="welcome.answer2_1" /></li>
	<li><spring:message code="welcome.answer2_2" /></li>
	<li><spring:message code="welcome.answer2_3" /></li>
	<li><spring:message code="welcome.answer2_4" /></li>
</ul>

<p><b><spring:message code="welcome.question3" /></b></p>
<spring:message code="welcome.answer3" />
<ul>
	<li><spring:message code="welcome.answer3_1" /></li>
	<li><spring:message code="welcome.answer3_2" /></li>
	<li><spring:message code="welcome.answer3_3" /></li>
	<li><spring:message code="welcome.answer3_4" /></li>
	<li><spring:message code="welcome.answer3_5" /></li>
	<li><spring:message code="welcome.answer3_6" /></li>
</ul>

<p><b><spring:message code="welcome.question4" /></b></p>
<p><spring:message code="welcome.answer4" /></p> 