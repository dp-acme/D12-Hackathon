<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal" />

<jstl:url value="${team.getWeb()}" var="webpage" />
<jstl:url value="${team.getLogo()}" var="logo" />

<jstl:if test="${not empty logo}">
	<a href="${webpage}"><img src="${logo}" style="max-width: 10%" /></a>
</jstl:if>

<h1>
	<jstl:out value="${team.getName()}" />
	<span style="font-size: 16px;">
		<acme:displayRegion region="${team.getRegion()}" />
	</span>
</h1>

<security:authorize access = "isAuthenticated()">
	<jstl:if test="${!isFollowed}">
		<acme:cancel url="/actor/followTeam.do?teamId=${team.getId()}" code="team.display.follow"/>
		<br/>
	</jstl:if>
	<jstl:if test="${isFollowed}">
		<acme:cancel url="/actor/unfollowTeam.do?teamId=${team.getId()}" code="team.display.unfollow"/>
		<br/>
	</jstl:if>
</security:authorize>

<jstl:if test="${not empty webpage}">
	<a href="${webpage}"><jstl:out value="${webpage}" /></a>
</jstl:if>

<br />

<img src="images/like.png" /> <jstl:out value="${numberLikes}" />
<img src="images/dislike.png" /> <jstl:out value="${numberDislikes}" />

<br />

<security:authorize access="hasRole('PLAYER')">
	<jstl:if test="${principal.equals(team.getLeader().getPlayer().getUserAccount())}">
		<br />
		<acme:cancel url="invitation/create.do?teamId=${team.getId()}" code="team.display.inviteTeammate"/>
		<acme:cancel url="team/edit.do?teamId=${team.getId()}" code="team.display.edit"/>
	</jstl:if>

	<jstl:if test="${not empty myTeams}">
		<acme:cancel url="scrim/invite.do?teamId=${team.getId()}" code="team.display.invite" />
	</jstl:if>
</security:authorize>

<display:table requestURI="/team/display.do" name="invitations" id="row" pagesize="5">

	<jstl:set value="false" var="pendingInvitation" />

	<display:column titleKey="team.display.player.title">
		<jstl:if test="${row.getTeammate().equals(team.getLeader())}">
			<span class="tooltip">
				<a href="player/display.do?playerId=${row.getTeammate().getPlayer().getId()}"><b><jstl:out value="${row.getTeammate().getPlayer().getName()}" /></b></a>
				<span class="tooltiptext tooltip-right">
					<spring:message code="team.display.leader" />
				</span>
			</span>
		</jstl:if>
		<jstl:if test="${!row.getTeammate().equals(team.getLeader())}">
				<jstl:if test="${row.getMoment() == null}">
					<jstl:set value="true" var="pendingInvitation" />
				</jstl:if>
			<a href="player/display.do?playerId=${row.getTeammate().getPlayer().getId()}"><jstl:out value="${row.getTeammate().getPlayer().getName()}" />
			<jstl:if test="${pendingInvitation == 'true'}"><spring:message code="team.display.player.pendingInvitation" /></jstl:if></a>
		</jstl:if>
	</display:column>
	
	<display:column titleKey="team.display.nickname.title">
		<a href="player/display.do?playerId=${row.getTeammate().getPlayer().getId()}&gameAccountId=${row.getTeammate().getId()}"><jstl:out value="${row.getTeammate().getNickname()}" /></a>
	</display:column>
	
	<display:column titleKey="team.display.region.title">
		<acme:displayRegion region="${row.getTeammate().getRegion()}" />
	</display:column>
	
	<jstl:if test="${team.getLeader().getPlayer().getUserAccount().equals(principal)}">
		<display:column>
			<jstl:if test="${!row.getTeammate().equals(team.getLeader()) && pendingInvitation == 'false'}">
				<acme:cancel url="team/kick.do?teamId=${team.getId()}&gameAccountId=${row.getTeammate().getId()}" code="team.display.kick" />
			</jstl:if>
		</display:column>
		
		<display:column>
			<jstl:if test="${!row.getTeammate().equals(team.getLeader()) && pendingInvitation == 'false'}">
				<acme:cancel url="team/changeLeader.do?teamId=${team.getId()}&gameAccountId=${row.getTeammate().getId()}" code="team.display.changeLeader" />
			</jstl:if>
		</display:column>
	</jstl:if>
	
	<jstl:if test="${!team.getLeader().getPlayer().getUserAccount().equals(principal)}">
		<display:column>
			 <jstl:if test="${row.getTeammate().getPlayer().getUserAccount().equals(principal) && pendingInvitation == 'false'}">
				<acme:cancel url="team/exit.do?teamId=${team.getId()}" code="team.display.exit" />
			</jstl:if>
			<jstl:if test="${row.getTeammate().getPlayer().getUserAccount().equals(principal) && pendingInvitation == 'true'}">
				<acme:cancel url="invitation/accept.do?invitationId=${row.getId()}" code="team.display.invitation.accept" />
				<acme:cancel url="invitation/reject.do?invitationId=${row.getId()}" code="team.display.invitation.reject" />
			</jstl:if>
		</display:column>
	</jstl:if>
	
	<jstl:if test="${empty isTeammate || !isTeammate}">
		<jstl:set value="${row.getTeammate().getPlayer().getUserAccount().equals(principal) && pendingInvitation == 'false'}" var="isTeammate" />
	</jstl:if>

</display:table>

<jstl:if test="${isTeammate}">
	<hr />
	
	<button onclick="javascript: toggleMessage();"><spring:message code="team.display.message.teamMessage" /></button>
	
	<script>
		function toggleMessage() {
			var div = document.getElementById("sendMessage");
			if (div.style.display == "none") {
				div.style.display = "";
			} else {
				div.style.display = "none";
			}
		}
	</script>
	
	<div id="sendMessage" style="display: none; margin-left: 5%">
		
		<br />
	
		<form:form action="message/create.do" modelAttribute="newMessage">
			<form:hidden path="recipients" />
			
			<acme:textbox code="team.display.message.subject" path="subject" required="required"/>
			
			<spring:message code="team.display.message.priority.high" var="highPriority" />
			<spring:message code="team.display.message.priority.neutral" var="neutralPriority" />
			<spring:message code="team.display.message.priority.low" var="lowPriority" />
			<spring:message code="team.display.message.priority" var="priorityLabel" />
			<form:label path="priority">
			${priorityLabel}
			</form:label>
			<form:select path="priority">
				<form:option value="HIGH" label="${highPriority}" />
				<form:option value="NEUTRAL" label="${neutralPriority}" />
				<form:option value="LOW" label="${lowPriority}" />
			</form:select>
			
			<acme:textarea code="team.display.message.body" path="body" required="required"/>
	
			<acme:submit name="send" code="team.display.message.send"/>
		</form:form>
	
	</div>
</jstl:if>

<hr />
<br />

<spring:message var="formatDate" code="team.display.dateFormat" />

<h3><spring:message code="team.display.scrims" /></h3>

<jstl:if test="${empty scrims}">
	<spring:message code="team.display.emptyScrims" />
</jstl:if>

<jstl:if test="${not empty scrims}">
	<display:table requestURI="/team/display.do" name="scrims" id="row" pagesize="5">
	
		<jstl:set var="result" value="none" />
		<jstl:if test="${row.getStatus().getScrimStatus().equals('CLOSED')}">
			<jstl:set var="result" value="${row.getCreatorTeamResult().getResult()}" />
			<jstl:if test="${row.getGuest().equals(team)}">
				<jstl:set var="result" value="${row.getGuestTeamResult().getResult()}" />
			</jstl:if>
		</jstl:if>
		
		<display:column class="${result}BG" titleKey="team.display.scrim.moment" property="moment" format="{0,date,${formatDate}}" />
		
		<display:column class="${result}BG" titleKey="team.display.scrim.creator" property="creator.name" />
		
		<display:column class="${result}BG" titleKey="team.display.scrim.guest" property="guest.name" />
		
		<display:column class="${result}BG" titleKey="team.display.scrim.status" property="status.scrimStatus" />
	
		<display:column class="${result}BG">
			<a href ="scrim/display.do?scrimId=${row.getId()}"><spring:message code="team.display.scrim.display" /></a>
		</display:column>
	
	</display:table>
</jstl:if>

