<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<h3><spring:message code="team.list.likesRanking" /></h3>

<display:table requestURI="/team/list.do" name="teamsLikesList" pagesize="5" uid="teamsLikesUID">

<jstl:set var="teamsLikesNumber" value="${teamsLikesNumber}" />

	<jstl:if test="${not empty teamsLikesList}">
		<%
		Integer rowLikeNumber = (Integer) pageContext.getAttribute("teamsLikesUID_rowNum") - 1;
		@SuppressWarnings("unchecked")
		List<Integer> teamLikeNumberList = ((ArrayList<Integer>) pageContext.getAttribute("teamsLikesNumber"));
		Integer teamLikeNumber = teamLikeNumberList.get(rowLikeNumber);
		pageContext.setAttribute("teamLikeNumber", teamLikeNumber);
		%>
	</jstl:if>

	<display:column titleKey="team.list.likes.title" sortable="true">
		<jstl:out value="${teamLikeNumber}" />
	</display:column>

	<display:column titleKey="team.list.name.title" property="name" />
	
	<display:column titleKey="team.list.leader.title" property="leader.nickname" 
		href="player/display.do?playerId=${teamsLikesUID.getLeader().getPlayer().getId()}&gameAccountId=${teamsLikesUID.getLeader().getId()}" />
	
	<display:column titleKey="team.list.region.title">
		<acme:displayRegion region="${teamsLikesUID.getRegion()}" />
	</display:column>
	
	<display:column>
		<acme:cancel url="team/display.do?teamId=${teamsLikesUID.getId()}" code="team.list.display" />
	</display:column>

</display:table>

<hr />
<br />

<h3><spring:message code="team.list.winsRanking" /></h3>

<display:table requestURI="/team/list.do" name="teamsWinsList" pagesize="5" uid="teamsWinsUID">

	<jstl:set var="teamsWinsNumber" value="${teamsWinsNumber}" />
	
	<jstl:if test="${not empty teamsWinsList}">
		<%
		Integer rowWinNumber = (Integer) pageContext.getAttribute("teamsWinsUID_rowNum") - 1;
		@SuppressWarnings("unchecked")
		List<Integer> teamWinNumberList = ((ArrayList<Integer>) pageContext.getAttribute("teamsWinsNumber"));
		Integer teamWinNumber = teamWinNumberList.get(rowWinNumber);
		pageContext.setAttribute("teamWinNumber", teamWinNumber);
		%>
	</jstl:if>

	<display:column titleKey="team.list.wins.title" sortable="true">
		<jstl:out value="${teamWinNumber}" />
	</display:column>

	<display:column titleKey="team.list.name.title" property="name" />
	
	<display:column titleKey="team.list.leader.title" property="leader.nickname" 
		href="player/display.do?playerId=${teamsWinsUID.getLeader().getPlayer().getId()}&gameAccountId=${teamsWinsUID.getLeader().getId()}" />
	
	<display:column titleKey="team.list.region.title">
		<acme:displayRegion region="${teamsWinsUID.getRegion()}" />
	</display:column>
	
	<display:column>
		<acme:cancel url="team/display.do?teamId=${teamsWinsUID.getId()}" code="team.list.display" />
	</display:column>

</display:table>
