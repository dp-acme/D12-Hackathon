<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form modelAttribute="team" action="team/edit.do">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	
	<jstl:if test="${team.getId() == 0}">
		<acme:select items="${videogames}" itemLabel="name" code="team.edit.videogame" path="videogame" required="required" />
	</jstl:if>
	
	<br />
	
	<jstl:choose>
		<jstl:when test="${cookie.language.value == 'es'}">
			<jstl:set value="spanishName" var="regionLabel" />
		</jstl:when>
		<jstl:otherwise>
			<jstl:set value="englishName" var="regionLabel" />
		</jstl:otherwise>
	</jstl:choose>
	<acme:select items="${regions}" itemLabel="${regionLabel}" code="team.edit.region" path="region" required="true" />
	
	<acme:textbox code="team.edit.name" path="name" required="required" />
	<acme:textbox code="team.edit.web" path="web" />
	<div id="logoPreview">
		<br />
		<img id="logoImage" alt="Preview" src="${team.getLogo()}">
		<br />
	</div>
	<acme:textbox code="team.edit.logo" path="logo" onchange="javascript: refreshPreview();" />
	
	<br />

	<acme:submit code="team.edit.save" name="save" />
	
	<jstl:if test="${team.getId() != 0}">
		<spring:message code="team.edit.delete.confirm" var="confirmDeleteMessage" />
		<acme:submit code="team.edit.delete" name="delete" onClickCallback="javascript: return confirm('${confirmDeleteMessage}');" />
		<acme:cancel url="team/display.do?teamId=${team.getId()}" code="team.edit.cancel"/>
	</jstl:if>
	<jstl:if test="${team.getId() == 0 && team.getVideogame() != null}">
		<acme:cancel url="videogame/display.do?videogameId=${team.getVideogame().getId()}" code="team.edit.cancel"/>
	</jstl:if>
	
</form:form>

<script>

function refreshPreview() {
	var logoPreview = document.getElementById("logoPreview");
	var logoImage = document.getElementById("logoImage");
	var logoInput = document.getElementById("logo");
	
	if (logoInput.value != null && logoInput.value != "") {
		logoImage.src = logoInput.value;
		logoPreview.style = "display: block;";
	} else {
		logoPreview.style = "display: none;";
	}
}

refreshPreview();

</script>
