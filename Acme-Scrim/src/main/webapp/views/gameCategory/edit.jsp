<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<security:authorize access="hasRole('ADMIN')">

	<!-- Abrimos el formulario de edicion/ creacion -->

	<form:form action="gameCategory/admin/edit.do"
		modelAttribute="gameCategory">
		<br />
	<form:hidden path="children" />
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="videogames" />
		<fieldset>
			<legend>
				<h2>
					<b><spring:message code="gameCategory.edit.gameCategoryDatas" />:</b>
				</h2>
			</legend>
			<!-- Indicamos los campos de formulario -->
			<br /> <b><acme:textbox code="gameCategory.edit.name"
					path="name" required="required" /></b> <br /> <b><acme:select
					itemLabel="name" code="gameCategory.create.selectParent"
					path="parent" items="${parentCategoryToSelect}"
					required="required" /></b>
					
<%-- 					<b><acme:select --%>
<%-- 					itemLabel="name" code="gameCategory.create.selectChildren" --%>
<%-- 					path="children" items="${childrenCategoryToSelect}" --%>
<%-- 					required="required" /></b> --%>
					 
		</fieldset>
		<br />

		<jstl:if test="${nameMessage != null && nameMessage != ''}">
			<p class="error"><spring:message code="${nameMessage}"/></p>
		</jstl:if>

		<!-- Botones del formulario -->
		<acme:submit name="save" code="gameCategory.edit.save" />

		<acme:cancel url="${cancel}"
			code="gameCategory.edit.cancel" />
	</form:form>

</security:authorize>
