<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<display:table name="gameCategories" id="row" requestURI="${requestURI}"
	pagesize="5">

<%-- 	<jstl:if test="${requestURI == 'gameCategory/list.do'}">  --%>
	<spring:url var="urlDisplayCategory"
		value="gameCategory/list.do?gameCategoryId=${row.getId()}" />
<%-- 	</jstl:if> --%>
	
<%-- 	<jstl:if test="${requestURI == 'gameCategory/admin/list.do'}">  --%>
<%-- 	<spring:url var="urlDisplayCategory" --%>
<%-- 		value="gameCategory/admin/list.do?gameCategoryId=${row.getId()}" /> --%>
<%-- 	</jstl:if> --%>
	<display:column titleKey="gameCategory.list.name">
		<a href="${urlDisplayCategory}"><jstl:out value="${row.name}"></jstl:out></a>
	</display:column>
	
	<security:authorize access="hasRole('ADMIN')">
		<jstl:if test="${row.getParent() != null}">
			<display:column title="" sortable="false">
				<a
					href="gameCategory/admin/delete.do?gameCategoryId=${row.getId()}">
					<spring:message code="gameCategory.list.delete" />
				</a>

			</display:column>
			
			<display:column title="" sortable="false">
				<a
					href="gameCategory/admin/edit.do?gameCategoryId=${row.getId()}">
					<spring:message code="gameCategory.list.edit" />
				</a>

			</display:column>
		</jstl:if>
	</security:authorize>
	
</display:table>

<%-- <jstl:if --%>
<%-- 	test="${requestURI == 'gameCategory/admin/list.do'}"> --%>
	<security:authorize access="hasRole('ADMIN')">
		<a href="gameCategory/admin/create.do?parentId=${categ.getId()}">
			<spring:message code="gameCategory.list.create" />
		</a>
	</security:authorize>
<%-- </jstl:if> --%>
<br />
<fieldset>
	<legend>
		<b><spring:message code="gameCategory.list.data" /></b>
	</legend>
	<b><spring:message code="gameCategory.list.name" />:</b>
	<jstl:out value="${categ.getName()}" />
	 <br /> <b><spring:message
			code="gameCategory.list.videogames" />:</b> <a href="videogame/list.do?gameCategoryId=${categ.getId()}"><spring:message
			code="gameCategory.list.videogames" /></a>

</fieldset>
