<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<display:table name="advertisements" id="row" requestURI="${requestURI}"
	pagesize="5">
	
	<display:column titleKey="advertisement.list.web" >
	<a href = "${row.web}"><jstl:out value="${row.web}"></jstl:out></a>
	</display:column>

	<display:column property="sponsor.name"
		titleKey="advertisement.list.sponsor" />

	<spring:url var="urlDisplayAdvertisement"
		value="advertisement/sponsor/display.do?advertisementId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false">
		<a href="${urlDisplayAdvertisement}"> <spring:message
				code="advertisement.list.display" />
		</a>
	</display:column>

	<security:authorize access="hasRole('SPONSOR')">
		<jstl:if test="${requestURI == 'advertisement/sponsor/list.do'}">
			<display:column title="" sortable="false">
				<a
					href="advertisement/sponsor/delete.do?advertisementId=${row.getId()}">
					<spring:message code="advertisement.list.delete" />
				</a>

			</display:column>
		</jstl:if>
	</security:authorize>

</display:table>

<jstl:if test="${requestURI == 'advertisement/sponsor/list.do'}">
	<security:authorize access="hasRole('SPONSOR')">
		<a href="advertisement/sponsor/create.do"> <spring:message
				code="advertisement.list.create" /></a>
	</security:authorize>
</jstl:if>