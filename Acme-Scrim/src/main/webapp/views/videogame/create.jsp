<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<security:authorize access="hasRole('MANAGER')">

	<!-- Abrimos el formulario de edicion/ creacion -->

	<form:form action="videogame/manager/create.do"
		modelAttribute="videogameForm">
		
		<fieldset>
			<legend>
				<h2>
					<b><spring:message code="videogame.create.videogameDatas" />:</b>
				</h2>
			</legend>

			<!-- Indicamos los campos de formulario -->
			<br /> 	
			
				<b><acme:select multiple="multiple"
					itemLabel="name" code="videogame.create.selectCategory"
					path="gameCategories" items="${categoryToSelect2}"
					required="required" /></b>
			
				<b><acme:textbox code="videogame.create.name" path="name"
					required="required" /></b> <br />
					
					<b><acme:textarea
					code="videogame.create.description" path="description"
					required="required" /></b> <br />
					
					<b><acme:textbox
					code="videogame.create.image" path="image" /></b> 
					
					<b><acme:textbox code="videogame.create.seasonName" path="seasonName"
					required="required" /></b>
					
				
		</fieldset>
		<br />

		<!-- Botones del formulario -->
		<acme:submit name="save" code="videogame.create.save" />
		<acme:cancel url="videogame/manager/list.do"
			code="videogame.edit.cancel" />

	</form:form>

</security:authorize>
