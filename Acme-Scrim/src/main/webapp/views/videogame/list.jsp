<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<display:table name="videogames" id="row" requestURI="${requestURI}"
	pagesize="5">

	<display:column titleKey="videogame.list.name">
		<a href="videogame/display.do?videogameId=${row.getId()}"><jstl:out
				value="${row.getName()}"></jstl:out></a>
	</display:column>

	<jstl:if test="${requestURI == 'videogame/manager/list.do'}">
		<security:authorize access="hasRole('MANAGER')">
			<br />
			<spring:url var="urlListSeason"
				value="season/manager/list.do?videogameId=${row.getId()}" />

		</security:authorize>
	</jstl:if>
	<jstl:if test="${requestURI != 'videogame/manager/list.do'}">
		<spring:url var="urlListSeason"
			value="season/list.do?videogameId=${row.getId()}" />
	</jstl:if>

	<display:column titleKey="videogame.list.seasons">
		<a href="${urlListSeason}"><spring:message code="videogame.list.showSeasons"/></a>

	</display:column>

</display:table>

<jstl:if test="${requestURI == 'videogame/manager/list.do'}">
	<security:authorize access="hasRole('MANAGER')">
		<a href="videogame/manager/create.do"> <spring:message
				code="videogame.list.create" /></a>
	</security:authorize>
</jstl:if>