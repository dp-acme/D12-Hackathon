<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<h2><jstl:out value="${videogame.getName()}" /></h2>

<display:table requestURI="/gameAccount/list.do" name="gameAccounts" pagesize="5" id="row">

	<display:column titleKey="gameAccount.list.nickname" property="nickname" />

	<display:column titleKey="gameAccount.list.region">
		<acme:displayRegion region="${row.getRegion()}"/>
	</display:column>
	
	<display:column titleKey="gameAccount.list.player" property="player.name" />
	
	<display:column>
		<a href="player/display.do?playerId=${row.getPlayer().getId()}&gameAccountId=${row.getId()}"><spring:message code="gameAccount.list.display" /></a>
	</display:column>

</display:table>
