package services;

import java.util.ArrayList;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Referee;
import domain.Resolution;
import domain.Result;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class RefereeServiceTest extends AbstractTest {
	
	@Autowired
	private RefereeService refereeService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ResolutionService resolutionService;
	
/*
1.	The system must support some kinds of actors, namely: administrators, managers, referees, sponsors, and players. 
	It must store the following information regarding them: their username and password for their accounts, their names, 
	their surnames (optional), their email addresses, and their social identities (optional) (only one for each social network defined by the administrator).
*/
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player2", "name", "surname", "email@mail.mail", "username2", "pass", null},

				{"player3", "name", "surname", "email@mail.mail", "username2", "pass", IllegalArgumentException.class},
				{"player2", null, null, null, "username2", "pass", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3], (String)d[4], (String)d[5], (Class<?>)d[6]);
		}
	}
	
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"referee1", "name2", "surname2", "email@mail2.mail", null},

				{"referee1", "", "surname2", "email@mail2.mail", ConstraintViolationException.class},
				{"referee1", "name2", "surname2", "email.mail", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (String)d[3], (Class<?>)d[4]);
		}
	}
	
/*
21. An actor who is authenticated as a referee must be able to:
	c. Ban players who have more than the minimum reports to be banned (configurable parameter by the admin). 
	   If a player is banned they won't be able to enter to the system anymore.
*/
	
	@Test
	public void driverBan() {
		Object[][] data = {
				{"referee1", "player3", null},

				{"player1", "player3", IllegalArgumentException.class},
				{"referee1", "player2", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateBan((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
/*
19. The leader of a team must be able to:
	i. If any player of his/her team is premium and a played scrim has an illogical result, then he/she can ask for a referee resolution.
*/
	
	@Test
	public void driverResolve() {
		Object[][] data = {
				{"referee1", "resolution1", null},

				{"player1", "resolution1", IllegalArgumentException.class},
				{"admin", "resolution1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateResolve((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String name, String surname, String email, String username, String pass, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Referee referee;
			Authority auth;
			
			auth = new Authority();
			auth.setAuthority(Authority.REFEREE);
			
			referee = refereeService.create();
			referee.setName(name);
			referee.setSurname(surname);
			referee.setEmail(email);
			referee.setUserAccount(new UserAccount());
			referee.getUserAccount().setAccountNonLocked(true);
			referee.getUserAccount().setPassword(pass);
			referee.getUserAccount().setUsername(username);
			referee.getUserAccount().setAuthorities(new ArrayList<Authority>());
			referee.getUserAccount().getAuthorities().add(auth);
			
			refereeService.save(referee);
			refereeService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String name, String surname, String email, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Referee referee;
			
			referee = refereeService.findOne(super.getEntityId(user));
			Assert.notNull(referee);
		
			referee.setName(name);
			referee.setSurname(surname);
			referee.setEmail(email);
			
			refereeService.save(referee);
			refereeService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateBan(String user, String playerBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
						
			actorService.banActor(super.getEntityId(playerBean));
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateResolve(String user, String resolutionBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Resolution resolution;
			Referee referee;
			Result result1, result2;
			
			referee = actorService.checkIsReferee();
			Assert.notNull(referee);
			
			resolution = resolutionService.findOne(super.getEntityId(resolutionBean));
			Assert.notNull(resolution);
			
			result1 = new Result();
			result2 = new Result();
			result1.setResult(Result.WIN);
			result2.setResult(Result.LOSE);
						
			resolutionService.addReferee(resolution, referee);
			resolutionService.setScrimResult(resolution.getId(), result1, result2);
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
