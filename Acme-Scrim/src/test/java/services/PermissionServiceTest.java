package services;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Permission;
import domain.Player;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class PermissionServiceTest extends AbstractTest {

	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private PlayerService playerService;
	
/*
18. An actor who is authenticated as a player must be able to:
	a. Ask the administrator for a referee or manager account. The administrator will then give them a personal permission 
	   to register the new account. That permission will have an expiration date.	
*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"admin", "player1", new Date(System.currentTimeMillis()*2), true, null},

				{"sponsor1", "player1", new Date(System.currentTimeMillis()*2), true, IllegalArgumentException.class},
				{"admin", "player1", null, true, ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (Date)d[2], (Boolean)d[3], (Class<?>)d[4]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"player2", "permission1", null},

				{"admin", "permission1", IllegalArgumentException.class},
				{"player1", "permission1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String playerBean, Date date, Boolean isReferee, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Player player;
			Permission permission;
			
			player = playerService.findOne(super.getEntityId(playerBean));
			Assert.notNull(player);
			
			permission = permissionService.create();
			permission.setPlayer(player);
			permission.setIsReferee(isReferee);
			permission.setExpirationDate(date);
			
			permissionService.save(permission);
			permissionService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String permissionBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Permission permission;
			
			permission = permissionService.findOne(super.getEntityId(permissionBean));
			Assert.notNull(permission);
			
			permissionService.delete(permission.getId());
			permissionService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
