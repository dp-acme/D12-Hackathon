package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Forum;
import domain.ForumMessage;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ForumMessageServiceTest extends AbstractTest {

	@Autowired
	private ForumMessageService forumMessageService;
	
	@Autowired
	private ForumService forumService;
	
	
/*20. An actor who is authenticated as a manager must be able to:
		e. Moderate the comments of his/her videogame forums, which includes answer or
			delete them.*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"manager1", "body", "forum1", null},
				
				{"admin", "", "forum10",NumberFormatException.class},
				{"player", "body", "forum10",IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2],(Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"manager1", "forumMessage1", null},

				{"admin", "forumMessage1", IllegalArgumentException.class},
				{"player1", "forumMessage1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String body,String forumId, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Forum forum;
			ForumMessage forumMessage;
			
			forum = forumService.findOne(super.getEntityId(forumId));
			
			forumMessage = forumMessageService.create(forum,null);
			Assert.notNull(forum);

			forumMessage.setBody(body);
			
			forumMessage = forumMessageService.reconstruct(forumMessage, null, null);
			
			forumMessageService.save(forumMessage);
			forumMessageService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String forumBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			ForumMessage forumMessage;
			forumMessage = forumMessageService.findOne(super.getEntityId(forumBean));
			Assert.notNull(forumMessage);
			
			forumMessageService.delete(forumMessage);
			forumMessageService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
	
