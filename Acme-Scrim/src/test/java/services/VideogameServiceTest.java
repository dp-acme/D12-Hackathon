package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.ForumCategory;
import domain.Videogame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class VideogameServiceTest extends AbstractTest {

	
	@Autowired
	private VideogameService videogameService;

	@Autowired
	private ForumCategoryService forumCategoryService;
	
	@Autowired
	private ManagerService managerService;
	
	
/*	20. An actor who is authenticated as a manager must be able to:
		a. Create and edit videogames.*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"manager1", "videogame", null},
				
				{"manager1", "",ConstraintViolationException.class},
				{"player1", "videogame",IllegalArgumentException.class}

		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"manager1", "videogame1","newname", null},
				{"admin", "videogame1","newname",IllegalArgumentException.class},
				{"manager1", "videogame1","", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}

/*	16. An actor who is not authenticated must be able to:
		e. List the different videogames and their forums with their messages.*/
	@Test
	public void listAndDisplay() {
		Class<?> caught;

		caught = null;
		try {
			videogameService.findAll();
		} catch (Throwable e) {
			caught = e.getClass();

		}
		checkExceptions(null, caught);
	}
	
	private void templateCreate(String user,String name, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Videogame videogame;
			
			videogame = videogameService.create();
			
			videogame.setName(name);
			Collection<ForumCategory> forumCategories;

			forumCategories = new ArrayList<ForumCategory>();
			forumCategories.add(forumCategoryService.createFORUM(null));
			videogame.setForumCategories(forumCategories);
			videogame.setManager(managerService.findOne(super.getEntityId(user)));
			
			videogame.setDescription("asdf");

			videogameService.save(videogame);
			videogameService.flush();
			
			super.unauthenticate();
			
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String videogameBean, String newName, Class<?> expected) {
		Class<?> caught;

		Videogame videogame;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			videogame = videogameService.findOne(super.getEntityId(videogameBean));
			Assert.notNull(videogame);

			videogame.setName(newName);
			
			videogameService.save(videogame);
			videogameService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
	
