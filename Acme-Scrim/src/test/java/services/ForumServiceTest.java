package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Forum;
import domain.ForumCategory;
import domain.Videogame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ForumServiceTest extends AbstractTest {

	@Autowired
	private ForumService forumService;
	
	@Autowired
	private ForumCategoryService forumCategoryService;
	
	@Autowired
	private VideogameService videogameService;
	
/*20. An actor who is authenticated as a manager must be able to:
		d. Manage the forums of his/her videogames. That includes creating, modifying, or
			deleting them.*/
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"manager1", "forum1", "newName", null},

				{"manager2", "forum1", "newName", IllegalArgumentException.class},
				{"admin", "forum1", "newName", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"manager1", "testForum", "description","forumCategoryRoot1","videogame1", null},
				
				{"manager1", "", "", "forumCategoryRoot1","videogame1",ConstraintViolationException.class},
				{"player", "testForum", "description","forumCategoryRoot1","videogame1",IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3], (String)d[4],(Class<?>)d[5]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"manager1", "forum1", null},

				{"admin", "forum1", IllegalArgumentException.class},
				{"player1", "forum1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}

/*	16. An actor who is not authenticated must be able to:
	e. List the different videogames and their forums with their messages.*/
	@Test
	public void listAndDisplay() {
		Class<?> caught;

		caught = null;
		try {
			forumService.findForumsByCategory(super.getEntityId("forumCategoryRoot1"));
		} catch (Throwable e) {
			caught = e.getClass();

		}
		checkExceptions(null, caught);
	}
	
	private void templateEdit(String user, String forumBean, String newName, Class<?> expected) {
		Class<?> caught;

		Forum forum, result;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			forum = forumService.findOne(super.getEntityId(forumBean));
			Assert.notNull(forum);

			forum.setName(newName);
			
			result=	forumService.reconstruct(forum,null, null);
			
			forumService.save(result);
			forumService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateCreate(String user, String name, String description, String forumCategory,String videogame, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Forum forum;
			Videogame vg;
			Collection<ForumCategory> fc=new ArrayList<ForumCategory>();
			
			
			vg = videogameService.findOne(super.getEntityId(videogame));
			fc.add(forumCategoryService.findOne(super.getEntityId(forumCategory)));
			
			forum = forumService.create(vg);
			Assert.notNull(forum);

			forum.setName(name);
			forum.setDescription(description);
			forum.setForumCategories(fc);
			
			forumService.save(forum);
			forumService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String forumBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Forum forum;
			forum = forumService.findOne(super.getEntityId(forumBean));
			Assert.notNull(forum);
			
			forumService.delete(forum);
			forumService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
	
