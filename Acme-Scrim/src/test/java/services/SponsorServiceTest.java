package services;

import java.util.ArrayList;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.LoginService;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Sponsor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class SponsorServiceTest extends AbstractTest {
	
	@Autowired
	private SponsorService sponsorService;
	
/*
1.	The system must support some kinds of actors, namely: administrators, managers, referees, sponsors, and players. 
	It must store the following information regarding them: their username and password for their accounts, their names, 
	their surnames (optional), their email addresses, and their social identities (optional) (only one for each social network defined by the administrator).
*/
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{null, "name", "surname", "email@mail.mail", "username", "pass", null},

				{"player2", "name", "surname", "email@mail.mail", "username", "pass", IllegalArgumentException.class},
				{null, null, null, null, "username", "pass", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3], (String)d[4], (String)d[5], (Class<?>)d[6]);
		}
	}
	
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"sponsor1", "name2", "surname2", "email@mail2.mail", null},

				{"sponsor1", "", "surname2", "email@mail2.mail", ConstraintViolationException.class},
				{"sponsor1", "name2", "surname2", "email.mail", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (String)d[3], (Class<?>)d[4]);
		}
	}
	
	private void templateCreate(String user, String name, String surname, String email, String username, String pass, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			if(LoginService.isAuthenticated()){
				super.unauthenticate();
			}
			super.startTransaction();
			
			if(user != null){
				super.authenticate(user);				
			}
			
			Sponsor sponsor;
			Authority auth;
			
			auth = new Authority();
			auth.setAuthority(Authority.SPONSOR);
			
			sponsor = sponsorService.create();
			sponsor.setName(name);
			sponsor.setSurname(surname);
			sponsor.setEmail(email);
			sponsor.setUserAccount(new UserAccount());
			sponsor.getUserAccount().setAccountNonLocked(true);
			sponsor.getUserAccount().setPassword(pass);
			sponsor.getUserAccount().setUsername(username);
			sponsor.getUserAccount().setAuthorities(new ArrayList<Authority>());
			sponsor.getUserAccount().getAuthorities().add(auth);
			
			sponsorService.save(sponsor);
			sponsorService.flush();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			if(user != null){
				super.unauthenticate();				
			}
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String name, String surname, String email, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Sponsor sponsor;
			
			sponsor = sponsorService.findOne(super.getEntityId(user));
			Assert.notNull(sponsor);
		
			sponsor.setName(name);
			sponsor.setSurname(surname);
			sponsor.setEmail(email);
			
			sponsorService.save(sponsor);
			sponsorService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
