package services;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Videogame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class AdministratorServiceTest extends AbstractTest{

	@Autowired
	private AdministratorService administratorService;
	
	
/*	22. An actor who is authenticated as an admin must be able to:
		a. See a dashboard:
			i. The average, minimum, maximum, and the standard deviation of the number of videogames per manager.*/
	@Test
	public void getAvgMaxMinSdVideogamesPerManager(){
		testAvgMaxMinSdVideogamesPerManager("admin",null);
		testAvgMaxMinSdVideogamesPerManager(null,IllegalArgumentException.class); 		//	El usuario no est� logueado
		testAvgMaxMinSdVideogamesPerManager("manager1",IllegalArgumentException.class);		// 	El usuario es un player
		testAvgMaxMinSdVideogamesPerManager("player1",IllegalArgumentException.class); 	//	El usuario es un manager
	}

//	ii. The average, minimum, maximum, and the standard deviation of the number of seasons per videogame.
	@Test
	public void getAvgMaxMinSdSeasonsPerVideogame(){
		testAvgMaxMinSdSeasonsPerVideogame("admin",null);
		testAvgMaxMinSdSeasonsPerVideogame(null,IllegalArgumentException.class); 		//	El usuario no est� logueado
		testAvgMaxMinSdSeasonsPerVideogame("manager1",IllegalArgumentException.class);		// 	El usuario es un player
		testAvgMaxMinSdSeasonsPerVideogame("player1",IllegalArgumentException.class); 	//	El usuario es un manager
	}

//	iii. The average, minimum, maximum, and the standard deviation of the number of scrims per team.
	@Test
	public void getAvgMaxMinSdScrimsPerTeam(){
		testAvgMaxMinSdScrimsPerTeam("admin",null);
		testAvgMaxMinSdScrimsPerTeam(null,IllegalArgumentException.class); 		//	El usuario no est� logueado
		testAvgMaxMinSdScrimsPerTeam("manager1",IllegalArgumentException.class);		// 	El usuario es un player
		testAvgMaxMinSdScrimsPerTeam("player1",IllegalArgumentException.class); 	//	El usuario es un manager
	}
	
//	iv. The average, minimum, maximum, and the standard deviation of the number of teams per player.
	@Test
	public void getAvgMaxMinSdTeamsPerPlayer(){
		testAvgMaxMinSdTeamsPerPlayer("admin",null);
		testAvgMaxMinSdTeamsPerPlayer(null,IllegalArgumentException.class); 		//	El usuario no est� logueado
		testAvgMaxMinSdTeamsPerPlayer("manager1",IllegalArgumentException.class);		// 	El usuario es un player
		testAvgMaxMinSdTeamsPerPlayer("player1",IllegalArgumentException.class); 	//	El usuario es un manager
	}
	
//	v. The average, minimum, maximum, and the standard deviation of the number of players per team.
	@Test
	public void getAvgMaxMinSdPlayersPerTeam(){
		testAvgMaxMinSdPlayersPerTeam("admin",null);
		testAvgMaxMinSdPlayersPerTeam(null,IllegalArgumentException.class); 		//	El usuario no est� logueado
		testAvgMaxMinSdPlayersPerTeam("manager1",IllegalArgumentException.class);		// 	El usuario es un player
		testAvgMaxMinSdPlayersPerTeam("player1",IllegalArgumentException.class); 	//	El usuario es un manager
	}
	
//	vi. Top 5 teams with more scrims.
	@Test
	public void getTeamsWithMoreScrims(){
		getTeamsWithMoreScrims("admin",null);
		getTeamsWithMoreScrims(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		getTeamsWithMoreScrims("player1",IllegalArgumentException.class);				// 	El usuario es un player
		getTeamsWithMoreScrims("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}

//	ix. Top 5 players with more messages.	
	@Test
	public void getPlayersWithMoreForumMessages(){
		getPlayersWithMoreForumMessages("admin",null);
		getPlayersWithMoreForumMessages(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		getPlayersWithMoreForumMessages("player1",IllegalArgumentException.class);				// 	El usuario es un player
		getPlayersWithMoreForumMessages("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}

//	vii. Top 5 players with more teams.
	@Test
	public void playersWithMoreTeams(){
		playersWithMoreTeams("admin",null);
		playersWithMoreTeams(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		playersWithMoreTeams("player1",IllegalArgumentException.class);				// 	El usuario es un player
		playersWithMoreTeams("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}

//	viii. Top 5 players with more teams per videogame.
	@Test
	public void playersWithMoreTeamsPerVideogame(){
		playersWithMoreTeamsPerVideogame("admin",null);
		playersWithMoreTeamsPerVideogame(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		playersWithMoreTeamsPerVideogame("player1",IllegalArgumentException.class);				// 	El usuario es un player
		playersWithMoreTeamsPerVideogame("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}

//	x. The ratio of players who have commented at any videogame forum.
	@Test
	public void ratioOfPlayersWithForumMessages(){
		ratioOfPlayersWithForumMessages("admin",null);
		ratioOfPlayersWithForumMessages(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		ratioOfPlayersWithForumMessages("player1",IllegalArgumentException.class);			// 	El usuario es un player
		ratioOfPlayersWithForumMessages("manager1",IllegalArgumentException.class); 		//	El usuario es un manager
	}

//	xi. The ratio of cancelled scrims.
	@Test
	public void ratioOfCancelledScrims(){
		ratioOfCancelledScrims("admin",null);
		ratioOfCancelledScrims(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		ratioOfCancelledScrims("player1",IllegalArgumentException.class);			// 	El usuario es un player
		ratioOfCancelledScrims("manager1",IllegalArgumentException.class); 		//	El usuario es un manager
	}
	
	protected void testAvgMaxMinSdScrimsPerTeam(String username, Class<?> expected){
		Class<?> caught;
		
		caught = null;
		try{
			super.authenticate(username);
			
			administratorService.getAvgMaxMinSdScrimsPerTeam();
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void testAvgMaxMinSdSeasonsPerVideogame(String username, Class<?> expected){
		Class<?> caught;
		
		caught = null;
		try{
			super.authenticate(username);
			
			administratorService.getAvgMaxMinSdSeasonsPerVideogame();
			
			
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void testAvgMaxMinSdTeamsPerPlayer(String username, Class<?> expected){
		Class<?> caught;
		
		caught = null;
		try{
			super.authenticate(username);
			
			administratorService.getAvgMaxMinSdTeamsPerPlayer();
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void testAvgMaxMinSdPlayersPerTeam(String username, Class<?> expected){
		Class<?> caught;
		
		caught = null;
		try{
			super.authenticate(username);
			
			administratorService.getAvgMaxMinSdPlayersPerTeam();
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void testAvgMaxMinSdVideogamesPerManager(String username, Class<?> expected){
		Class<?> caught;
		
		caught = null;
		try{
			super.authenticate(username);
			
			administratorService.getAvgMaxMinSdVideogamesPerManager();
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void ratioOfPlayersWithForumMessages(String username, Class<?> expected){
		Class<?> caught;
		
		caught = null;
		try{
			super.authenticate(username);
			
			administratorService.ratioOfPlayersWithForumMessages();
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void ratioOfCancelledScrims(String username, Class<?> expected){
		Class<?> caught;
		
		caught = null;
		try{
			super.authenticate(username);
			
			administratorService.ratioOfCancelledScrims();
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	
	
	protected void getTeamsWithMoreScrims(String username, Class<?> expected){

		Class<?> caught;
		Collection<?> testingData; 
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getTeamsWithMoreScrims();

			Assert.notNull(testingData);
			Assert.isTrue(testingData.size()>0);
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void getPlayersWithMoreForumMessages(String username, Class<?> expected){

		Class<?> caught;
		Collection<?> testingData; 
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getPlayersWithMoreForumMessages();

			Assert.notNull(testingData);
			Assert.isTrue(testingData.size()>0);
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void playersWithMoreTeams(String username, Class<?> expected){

		Class<?> caught;
		Collection<?> testingData; 
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.playersWithMoreTeams();

			Assert.notNull(testingData);
			Assert.isTrue(testingData.size()>0);
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void playersWithMoreTeamsPerVideogame(String username, Class<?> expected){

		Class<?> caught;
		Collection<?> testingData; 
		
		caught = null;
		try{
			super.authenticate(username);
			
			Pageable pageable = new PageRequest(0,5);
			Collection<Videogame> videogames;
			videogames = administratorService.getVideogames(pageable);
			for(Videogame v:videogames){
				testingData = administratorService.playersWithMoreTeamsPerVideogame(v);
				Assert.notNull(testingData);
			}

			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
}
	
	
