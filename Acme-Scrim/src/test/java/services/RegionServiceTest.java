package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Region;

import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class RegionServiceTest extends AbstractTest {
	
	@Autowired
	private RegionService regionService;

/*
28. There should be some configurable parameters for the admin to be changed:
	d. A list of regions for players and teams. It should start containing some default values: 
		("International", "INT", gray), ("Europe", "EU", blue), ("North America", "NA", "red"), ("China", "CN", yellow), ("Korea", "KR", green).
*/
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"admin", "test1", "test2", "T", "cyan", null},

				{"player1", "test1", "test2", "T", "cyan", IllegalArgumentException.class},
				{"admin", "test1", "test2", null, "cyan", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3], (String)d[4], (Class<?>)d[5]);
		}
	}

	@Test
	public void driverEdit() {
		Object[][] data = {
				{"admin", "region2", "wea", "weaeningles", "W", "cyan", null},

				{"player1", "region2", "wea", "weaeningles", "W", "cyan", IllegalArgumentException.class},
				{"admin", "region2", "wea", "", "W", "cyan", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (String)d[3], (String)d[4], (String)d[5], (Class<?>)d[6]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"admin", "region2", null},

				{"admin", "region1", IllegalArgumentException.class},
				{"player1", "region2", IllegalArgumentException.class},				
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String nameSpanish, String nameEnglish, String abbreviation, String color, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Region region;
			
			region = regionService.create();
			region.setSpanishName(nameSpanish);
			region.setEnglishName(nameEnglish);
			region.setAbbreviation(abbreviation);
			region.setColorDisplay(color);
			
			regionService.save(region);
			regionService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String regionBean, String nameSpanish, String nameEnglish, String abbreviation, String color, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Region region;
			
			region = regionService.findOne(super.getEntityId(regionBean));
			Assert.notNull(region);
			
			region.setSpanishName(nameSpanish);
			region.setEnglishName(nameEnglish);
			region.setAbbreviation(abbreviation);
			region.setColorDisplay(color);
			
			regionService.save(region);
			regionService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String regionBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Region region;
			
			region = regionService.findOne(super.getEntityId(regionBean));
			Assert.notNull(region);
			
			regionService.delete(region);
			regionService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
