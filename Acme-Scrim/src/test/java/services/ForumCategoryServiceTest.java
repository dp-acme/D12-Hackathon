package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.ForumCategory;
import domain.Videogame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ForumCategoryServiceTest extends AbstractTest {

	@Autowired
	private ForumCategoryService forumCategoryService;
	
	@Autowired
	private VideogameService videogameService;
	
/*20. An actor who is authenticated as a manager must be able to:
		c. Manage the forum categories of his/her videogames. That includes creating,
			modifying, or deleting.*/
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"manager1", "forumCategoryChildren1", "forumCategoryChildren123", null},

				{"manager2", "forumCategoryChildren1", "forumCategoryChildren123", IllegalArgumentException.class},
				{"manager1", "forumCategoryChildren1", "Hijo de categor�a FORUM del videogame1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"manager1", "testForumCategory", null,"videogame1", null},
				
				{"manager1", "", null, "videogame1",ConstraintViolationException.class},
				{"player", null, null, "videogame1",IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3], (Class<?>)d[4]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"manager1", "forumCategoryChildren1", null},

				{"admin", "forumCategoryChildren1", IllegalArgumentException.class},
				{"player1", "forumCategoryChildren1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateEdit(String user, String forumCategoryBean, String newName, Class<?> expected) {
		Class<?> caught;

		ForumCategory forumCategory, result;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			forumCategory = forumCategoryService.findOne(super.getEntityId(forumCategoryBean));
			Assert.notNull(forumCategory);

			forumCategory.setName(newName);
			
			result=	forumCategoryService.reconstruct(forumCategory,null, null);

			
			forumCategoryService.save(result);
			forumCategoryService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateCreate(String user, String name, String parent,String videogame, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			ForumCategory forumCategory;
			Videogame vg;
			
			if(parent != null){
				forumCategory = forumCategoryService.findOne(super.getEntityId(parent));
				Assert.notNull(forumCategory);	
				
			} else{
				forumCategory = null;
			}
			
			vg = videogameService.findOne(super.getEntityId(videogame));
			
			forumCategory = forumCategoryService.create(vg);
			Assert.notNull(forumCategory);

			forumCategory.setName(name);
			forumCategory.setParent(forumCategory);
			
			forumCategoryService.save(forumCategory);
			forumCategoryService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String forumCategoryBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			ForumCategory forumCategory;
			forumCategory = forumCategoryService.findOne(super.getEntityId(forumCategoryBean));
			Assert.notNull(forumCategory);
			
			forumCategoryService.delete(forumCategory);
			forumCategoryService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
	
