package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.SocialIdentity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class SocialIdentityServiceTest extends AbstractTest {

	
	@Autowired
	private SocialIdentityService socialIdentityService;
	
	@Autowired
	private SocialNetworkService socialNetworkService;
	
	@Autowired
	private ActorService	actorService;
	
	
/*	1. The system must support some kinds of actors, namely: administrators, managers,
		referees, sponsors, and players. It must store the following information regarding them:
		their username and password for their accounts, their names, their surnames (optional),
		their email addresses, and their social identities (optional) (only one for each social
		network defined by the administrator).*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player1", "socialNetwork3", "https://twitter.com/sktelecomfaker?lang=es", null},
				
				{"player6", "socialNetwork1", "",ConstraintViolationException.class},
				{"player1", "socialNetwork1", "https://twitter.com/sktelecomfaker?lang=es",IllegalArgumentException.class},

		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"player1", "socialIdentity1", null} //Que un usuario solo pueda borrar sus Social Identities se comprueba en el controlador

		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String socialNetwork,  String link, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			SocialIdentity socialId;
			
			socialId = socialIdentityService.create();
			
			socialId.setSocialNetwork(socialNetworkService.findOne(super.getEntityId(socialNetwork)));
			socialId.setLink(link);
			socialId.setActor(actorService.findOne(super.getEntityId(user)));

			socialIdentityService.save(socialId);
			socialIdentityService.flush();
			
			super.unauthenticate();
			
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String socialId, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			SocialIdentity socialIdentity;
			socialIdentity = socialIdentityService.findOne(super.getEntityId(socialId));
			
			socialIdentityService.delete(socialIdentity.getId());
			socialIdentityService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
	
