package services;

import java.util.ArrayList;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.LoginService;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Player;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class PlayerServiceTest extends AbstractTest {
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private ActorService actorService;
	
/*
1.	The system must support some kinds of actors, namely: administrators, managers, referees, sponsors, and players. 
	It must store the following information regarding them: their username and password for their accounts, their names, 
	their surnames (optional), their email addresses, and their social identities (optional) (only one for each social network defined by the administrator).
*/
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{null, "name2", "surname2", "email2@mail.mail", "username2", "pass2", null},
				{null, "name3", null, "email3@mail.mail", "username3", "pass3", null},


				{null, null, null, null, "username", "pass", ConstraintViolationException.class},
				{null, null, "surname2","email@mail.mail", "username", "pass", ConstraintViolationException.class},
				{null, "name2", "surname2", null, "username2", "pass2", ConstraintViolationException.class},
				{null, "name2", "surname2", "email2@mail.mail", "user", null, ConstraintViolationException.class},
				{null, "", "", "", "username", "pass", ConstraintViolationException.class},
				{null, "", "surname2","email@mail.mail", "username", "pass", ConstraintViolationException.class},
				{null, "name2", "surname2", "email2@mail.mail", "", "pass", ConstraintViolationException.class},
				{null, "name2", "surname2", "email2@mail.mail", "user", "", ConstraintViolationException.class},
				{"manager1", "name2", "surname2","email@mail.mail", "username", "pass", IllegalArgumentException.class},
				{"player1", "name2", "surname2","email@mail.mail", "username", "pass", IllegalArgumentException.class},


		
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3], (String)d[4], (String)d[5], (Class<?>)d[6]);
		}
	}
	
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"player1", "name2", "surname2", "email@mail2.mail", null},

				{"player1", "", "surname2", "email@mail2.mail", ConstraintViolationException.class},
				{"player1", "name2", "surname2", "email.mail", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (String)d[3], (Class<?>)d[4]);
		}
	}
	
/*
17. An actor who is authenticated must be able to:
	e. Follow teams so he/she will be notified with a message when they play or publish the results in a public scrim.
*/
	@Test
	public void driverFollow() {
		Object[][] data = {
				{"player1", "team1", null},

				{null, "team1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateFollow((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String name, String surname, String email, String username, String pass, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			if(LoginService.isAuthenticated()){
				super.unauthenticate();
			}
			super.startTransaction();
			
			if(user != null){
				super.authenticate(user);				
			}
			
			Player player;
			Authority auth;
			
			auth = new Authority();
			auth.setAuthority(Authority.PLAYER);
			
			player = playerService.create();
			player.setName(name);
			player.setSurname(surname);
			player.setEmail(email);
			player.setUserAccount(new UserAccount());
			player.getUserAccount().setAccountNonLocked(true);
			player.getUserAccount().setPassword(pass);
			player.getUserAccount().setUsername(username);
			player.getUserAccount().setAuthorities(new ArrayList<Authority>());
			player.getUserAccount().getAuthorities().add(auth);
			
			playerService.save(player);
			playerService.flush();
			
			

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			if(user != null){
				super.unauthenticate();				
			}
			super.rollbackTransaction();
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String name, String surname, String email, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Player player;
			
			player = playerService.findOne(super.getEntityId(user));
			Assert.notNull(player);
		
			player.setName(name);
			player.setSurname(surname);
			player.setEmail(email);
			
			playerService.save(player);
			playerService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateFollow(String user, String teamBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			actorService.followTeam(super.getEntityId(teamBean));
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
