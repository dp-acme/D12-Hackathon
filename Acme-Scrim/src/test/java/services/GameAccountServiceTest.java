package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.GameAccount;
import domain.Videogame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class GameAccountServiceTest extends AbstractTest {
	
	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private VideogameService videogameService;
	
	@Autowired
	private ActorService actorService;

/*
8. Players may create a game account for each videogame. The game accounts will be identified by the players' nicknames in the videogames. 
*/
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player1", "videogame1", "XxX_YUGIOH-MAST3R_XxX", null},

				{"sponsor1", "videogame1", "test", IllegalArgumentException.class},
				{"player1", "videogame1", "", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}

	@Test
	public void driverEdit() {
		Object[][] data = {
				{"player1", "gameAccount1", "Eltinroquedespegadenuevo", null},

				{"sponsor1", "gameAccount1", "Eltinroquedespegadenuevo", IllegalArgumentException.class},
				{"player2", "gameAccount1", "Eltinroquedespegadenuevo", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"player1", "gameAccount1", null},

				{"player2", "gameAccount1", IllegalArgumentException.class},
				{"sponsor1", "gameAccount1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String videogameBean, String nickname, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			GameAccount gameAccount;
			Videogame videogame;
			
			videogame = videogameService.findOne(super.getEntityId(videogameBean));
			Assert.notNull(videogame);
			
			gameAccount = gameAccountService.create(actorService.checkIsPlayer());
			gameAccount.setNickname(nickname);
			gameAccount.setVideogame(videogame);
			
			gameAccountService.save(gameAccount);
			gameAccountService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String gameAccountBean, String nickname, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			GameAccount gameAccount;
			
			gameAccount = gameAccountService.findOne(super.getEntityId(gameAccountBean));
			gameAccount.setNickname(nickname);
			
			gameAccountService.save(gameAccount);
			gameAccountService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String gameAccountBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			GameAccount gameAccount;
			
			gameAccount = gameAccountService.findOne(super.getEntityId(gameAccountBean));
			Assert.notNull(gameAccount);
			
			gameAccountService.delete(gameAccount);
			gameAccountService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
