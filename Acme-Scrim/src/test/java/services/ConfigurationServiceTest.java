package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ConfigurationServiceTest extends AbstractTest {

	@Autowired
	private ConfigurationService configurationService;

/*
28. There should be some configurable parameters for the admin to be changed:
	a. The banner of the webpage.
	b. The minimum number of reports to ban a player.
	c. A list of taboo words so a message that contains any of them should be sent to the spambox. 
		It should start containing some default values: "polla", "sex", "sexo", "penis", "cialis", "hack", "script", "eloboost".
	d. A list of regions for players and teams. It should start containing some default values: 
		("International", "INT", gray), ("Europe", "EU", blue), ("North America", "NA", "red"), ("China", "CN", yellow), ("Korea", "KR", green).
	e. A ratio for the probability of the ad banner to show an internal ad to show people 
		they can purchase the possibility to advertise themselves depending on the number of ads that already exists in the system.
*/
	
	@Test
	public void driverFind() {
		templateFind();
	}
	
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"admin", "http://www.cosa.com/banner.png", 10, 20.0, "http://www.cosa.com/banner2.png", 2, 20, null},
				
				{"player1", "http://www.cosa.com/banner.png", 10, 20.0, "http://www.cosa.com/banner2.png", 2, 20, IllegalArgumentException.class},
				{"admin", null, 10, -5.7, null, 2, 20, ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (Integer)d[2], (Double)d[3], (String)d[4], (Integer)d[5], (Integer)d[6], (Class<?>)d[7]);
		}
	}

	public void templateFind() {
		Configuration configuration;
		
		configuration = configurationService.find();
		
		Assert.notNull(configuration);
	}
	
	private void templateEdit(String user, String banner, Integer mReports, Double ratio, String adBanner, 
							  Integer finderCacheTime, Integer finderResults, Class<?> expected) {
		Class<?> caught;

		Configuration configuration;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			configuration = configurationService.find();
			
			configuration.setBanner(banner);
			configuration.setMinimumReports(mReports);
			configuration.setAdRatio(ratio);
			configuration.setAdImage(adBanner);
			configuration.setFinderCacheTime(finderCacheTime);
			configuration.setFinderResults(finderResults);
			
			configurationService.save(configuration);
			configurationService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
