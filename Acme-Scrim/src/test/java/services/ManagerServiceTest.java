package services;

import java.util.ArrayList;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Manager;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ManagerServiceTest extends AbstractTest {
	
	@Autowired
	private ManagerService managerService;
	
/*
1.	The system must support some kinds of actors, namely: administrators, managers, referees, sponsors, and players. 
	It must store the following information regarding them: their username and password for their accounts, their names, 
	their surnames (optional), their email addresses, and their social identities (optional) (only one for each social network defined by the administrator).
*/
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player3", "name", "surname", "email@mail.mail", "username", "pass", null},

				{"player2", "name", "surname", "email@mail.mail", "username", "pass", IllegalArgumentException.class},
				{"player3", null, null, null, "username", "pass", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3], (String)d[4], (String)d[5], (Class<?>)d[6]);
		}
	}
	
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"manager1", "name2", "surname2", "email@mail2.mail", null},

				{"manager1", "", "surname2", "email@mail2.mail", ConstraintViolationException.class},
				{"manager1", "name2", "surname2", "email.mail", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (String)d[3], (Class<?>)d[4]);
		}
	}
	
	private void templateCreate(String user, String name, String surname, String email, String username, String pass, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Manager manager;
			Authority auth;
			
			auth = new Authority();
			auth.setAuthority(Authority.MANAGER);
			
			manager = managerService.create();
			manager.setName(name);
			manager.setSurname(surname);
			manager.setEmail(email);
			manager.setUserAccount(new UserAccount());
			manager.getUserAccount().setAccountNonLocked(true);
			manager.getUserAccount().setPassword(pass);
			manager.getUserAccount().setUsername(username);
			manager.getUserAccount().setAuthorities(new ArrayList<Authority>());
			manager.getUserAccount().getAuthorities().add(auth);
			
			managerService.save(manager);
			managerService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String name, String surname, String email, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Manager manager;
			
			manager = managerService.findOne(super.getEntityId(user));
			Assert.notNull(manager);
		
			manager.setName(name);
			manager.setSurname(surname);
			manager.setEmail(email);
			
			managerService.save(manager);
			managerService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
