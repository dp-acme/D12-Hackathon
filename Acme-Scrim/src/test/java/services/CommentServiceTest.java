package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class CommentServiceTest extends AbstractTest {
	
	@Autowired
	private CommentService commentService;
	
/*
11.	The leader of a team can ask for a scrim publicly or directly to a specific team. 
	The system should store the following information regarding these scrims: 
	the moment when it is going to be played, the videogame's season when it is played, the privacy, 
	a link to a video streaming website (optional), the team that asked for it, the desired region of the opponent team (optional), 
	and some comments written by any of the players of the two teams in the scrim (optional) 
*/
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player2", "Comment", "scrim1", null},

				{"player5", "Comment", "scrim1", IllegalArgumentException.class},
				{"player2", null, "scrim1", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	private void templateCreate(String user, String comments, String scrimBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Comment comment;
			
			comment = commentService.create(super.getEntityId(scrimBean));
			comment.setComments(comments);
			
			commentService.save(comment);
			commentService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
