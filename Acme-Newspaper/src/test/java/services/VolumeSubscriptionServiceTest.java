package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.CreditCard;
import domain.Customer;
import domain.Volume;
import domain.VolumeSubscription;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class VolumeSubscriptionServiceTest extends AbstractTest {

	@Autowired
	private VolumeService volumeService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private VolumeSubscriptionService  volumeSubscriptionService;

	
/*	9. An actor who is authenticated as a customer must be able to:
		1. Subscribe to a volume by providing a credit card. 
		Note that subscribing to a volume implies subscribing 
		automatically to all of the newspapers of which it is composed, 
		including newspapers that might be published after the subscription takes place.
*/
	@Test
	public void driverCreate() {
		Object[][] testingData = {
				{"customer3","customer3","volume1","volumeSubscription1",null}, 
		
				{null,"customer3",  "volume3", "volumeSubscription1",IllegalArgumentException.class}, 
				{"user1", "customer3", "volume3", "volumeSubscription1",IllegalArgumentException.class}, 

		};

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3],
					(Class<?>) testingData[i][4]);
		}
	}
	

	protected void templateCreate(String username, String customerBean, String volumeBean, String creditCardBean,Class<?> expected) {
		Class<?> caught;

		Actor actor;
		Volume volume;
		VolumeSubscription volumeSubscription;
		CreditCard creditCard;

		caught = null;

		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			actor = actorService.findOne(super.getEntityId(customerBean));
			volume = volumeService.findOne(super.getEntityId(volumeBean));
			creditCard = volumeSubscriptionService.findOne(super.getEntityId(creditCardBean)).getCreditCard();
			
			volumeSubscription = volumeSubscriptionService.create((Customer) actor, volume);
			Assert.notNull(volumeSubscription);

			volumeSubscription.setCreditCard(creditCard);

			volumeSubscription = volumeSubscriptionService.save(volumeSubscription);
			volumeSubscriptionService.flush();
			
			Assert.isTrue(volumeSubscriptionService.findAll().contains(volumeSubscription));
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	


}
