package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Agent;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class AgentServiceTest extends AbstractTest{

	@Autowired
	private AgentService agentService;
	
/*
	3. An actor who is not authenticated must be able to:
		1. Register to the system as an agent.
*/
	@Test
	public void driverCreate(){
		Calendar cal;
		Calendar futureCal;
		
		cal = Calendar.getInstance();
		futureCal = Calendar.getInstance();
		cal.set(1998, 11, 19);
		futureCal.set(2030, 11, 12);
		Object[][] testingData = {
				
				//Positivos
				{null, "agentTest", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", null},
				{null, "agentTest1", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", null},
				
				//Negativos

				{null, "aget", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class}, 								// Nombre de usuario (agentname) demasiado corto (menos de 4 caracteres)
				{null, "agent1234123412341234123412341234", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},		// Nombre de usuario (agentname) demasiado largo (más de 32 caracteres)
				{null, "agentTest2", "pass", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},									// Contraseña (password) demasiado corta (menos de 4 caracteres)
				{null, "agentTest2", "pass1234123412341234123412341234", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},		// Contraseña (password) demasiado larga (más de 32 caracteres)
				{null, "agentTest2", "passwordTest", "", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},									// Nombre(name) en blanco
				{null, "agentTest2", "passwordTest", "nameTest", "", "mail@test.test", "", "", ConstraintViolationException.class},										// Apellido/s (surname) en blanco
				{null, "agentTest2", "passwordTest", "nameTest", "surnameTest", "", "", "", ConstraintViolationException.class},											// Correo electrónico (email) en blanco
				{null, "agentTest2", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},						// Fecha de nacimiento (birthday) futura
				{null, "agent1", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},								// Nombre de usuario (agentname) ya registrado
				{"admin", "agentTest3", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", ConstraintViolationException.class},		// Intento de registro ya logueado como admin
				{"manager1", "agentTest3", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", ConstraintViolationException.class}	// Intento de registro ya logueado como manager
				
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
						 (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5],
						 (String) testingData[i][6], (String) testingData[i][7], (Class<?>) testingData[i][8]);
		}
	}
	
	protected void templateCreate(String username, String usernameToBeSetted, String password, String name,String surname, String email, String phone, String address, Class<?> expected){
		Class<?>  caught;
		
		Agent agent;
		UserAccount userAccount;
		Collection<Authority> auths;
		Authority auth;
	
		auth = new Authority();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.AGENT);
		auths.add(auth);
		userAccount = new UserAccount();
		caught = null;
		
		try{			
			super.authenticate(username);
			
			agent = agentService.create();
			Assert.notNull(agent);

			agent.setName(name);
			agent.setSurname(surname);
			agent.setEmail(email);
			agent.setPhone(phone);
			agent.setAddress(address);
			userAccount.setUsername(usernameToBeSetted);
			userAccount.setPassword(password);
			userAccount.setAuthorities(auths);
			agent.setUserAccount(userAccount);
				
			agentService.save(agent);
			agentService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}	
}
