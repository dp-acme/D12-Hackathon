package services;

import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Chirp;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ChirpServiceTest extends AbstractTest {

	@Autowired
	private ChirpService chirpService;

	/*
	 * 16. An actor who is authenticated as a user must be able to: 1. Post a
	 * chirp. Chirps may not be changed or deleted once they are posted.
	 */

	@Test
	public void driverCreate() {
		Object[][] testingData = {
				// Positivo
				{ "user1", "Chirp 10", "Descripción para Chirp 10", null },
				// Negativos
				{ "admin", "Chirp Sex", "Descripcion para Chirp Sex",
						IllegalArgumentException.class },
				{ "user2", null, "Descripcion para Chirp Sex",
						ConstraintViolationException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(Class<?>) testingData[i][3]);
		}
	}

	protected void templateCreate(String username, String title,
			String description, Class<?> expected) {
		Class<?> caught;

		Chirp chirp;

		caught = null;

		try {
			super.authenticate(username);

			chirp = chirpService.create();
			Assert.notNull(chirp);

			// Le seteamos los atributos que no le hemos pasado a la hora de
			// crear
			chirp.setTitle(title);
			chirp.setDescription(description);

			chirpService.save(chirp);
			chirpService.flush();

			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 16. An actor who is authenticated as a user must be able to: 5. Display a
	 * stream with the chirps posted by all of the users that he or she follows.
	 */

	@Test
	public void driverChirpsOfTheUsersHeFollows() {
		Object[][] testingData = {
				// Positivo
				{ "user2", "chirp1", null },
				// Negativos
				{ "admin", "chirp1", IllegalArgumentException.class },
				{ null, "chirp1", IllegalArgumentException.class }, 
				};

		for (int i = 0; i < testingData.length; i++) {
			templateChirpsOfTheUsersHeFollows((String) testingData[i][0],
					(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateChirpsOfTheUsersHeFollows(String username,
			String chirpBean, Class<?> expected) {
		Class<?> caught;

		Collection<Chirp> chirps;
		Chirp chirp;
		caught = null;

		try {
			super.authenticate(username);

			chirp = this.chirpService.findOne(this.getEntityId(chirpBean));

			chirps = chirpService.chirpsPostedByAllUsersFollows();
			// Comprobamos que el chirp que le pasamos esta contenido en el
			// conjunto
			Assert.isTrue(chirps.contains(chirp));

			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 17. An actor who is authenticated as an administrator must be able to: 4.
	 * List the chirps that contain taboo words.
	 */

	@Test
	public void driverChirpsWithTabooWords() {
		Object[][] testingData = {
				// Positivo
				{ "admin", "chirp6", null },
				// Negativos
				{ "user1", "chirp1", IllegalArgumentException.class },
				{ null, "chirp1", IllegalArgumentException.class }, 
				};

		for (int i = 0; i < testingData.length; i++) {
			templateChirpsWithTabooWords((String) testingData[i][0],
					(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateChirpsWithTabooWords(String username,
			String chirpBean, Class<?> expected) {
		Class<?> caught;

		Collection<Chirp> chirps;
		Chirp chirp;

		caught = null;

		try {
			super.authenticate(username);

			chirp = this.chirpService.findOne(this.getEntityId(chirpBean));
			chirps = chirpService.chirpsContainsTabooWords();
			// Comprobamos que el chirp que le pasamos esta contenido en el
			// conjunto
			Assert.isTrue(chirps.contains(chirp));

			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 17. An actor who is authenticated as an administrator must be able to: 5.
	 * Remove a chirp that he or she thinks is inappropriate.
	 */

	@Test
	public void driverDelete() {
		Object[][] testingData = {
				// Positivo
				{ "admin", "chirp1", null },
				// Negativos
				{ "user1", "chirp1", IllegalArgumentException.class },
				{ null, "chirp1", IllegalArgumentException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateDelete((String) testingData[i][0],(String) testingData[i][1],
					(Class<?>) testingData[i][2]);
		}
	}

	protected void templateDelete(String username, String chirpBean, Class<?> expected) {
		Class<?> caught;

		Chirp chirp;

		caught = null;

		try {
			super.authenticate(username);
			
			// Cogemos el chirp
			chirp = this.chirpService.findOne(this.getEntityId(chirpBean));
			this.chirpService.delete(chirp);

			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}
}
