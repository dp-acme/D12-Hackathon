package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Article;
import domain.Newspaper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ArticleServiceTest extends AbstractTest {

	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private NewspaperService newspaperService;

	
/* 6. An actor who is authenticated as a user must be able to:
  		3. Write an article and attach it to any newspaper that has not been published, yet.
	 		Note that articles may be saved in draft mode, which allows to modify them later, or
	 		final model, which freezes them forever.*/
	@Test
	public void driverCreate() {
		Object[][] testingData = {
				{"user1", "title", "summary", "body", null, "newspaper3", null}, //Artículo
				{"user1", "title", "summary", "body", "article4", null, null}, //Follow-up

				{"customer1", "title", "summary", "body", "article4", null, IllegalArgumentException.class}, //Falta de credenciales (Customer)
				{"admin", "title", "summary", "body", "article4", null, IllegalArgumentException.class}, //Falta de credenciales (Admin)
				{"user1", "title", "summary", "body", null, "newspaper2", IllegalArgumentException.class}, //Newspaper incorrecto
				{"user1", "title", "summary", "body", "article6", null, IllegalArgumentException.class}, //Artículo de otro newspaper
				{"user1", "title", "summary", "body", null, null, IllegalArgumentException.class}, //Ni artículo ni follow-up
				
				{"user1", "", "summary", "body", "article4", null, ConstraintViolationException.class}, //Restricciones de dominio
				{"user1", null, "summary", "body", "article4", null, ConstraintViolationException.class},
				{"user1", "title1", "", "body", "article4", null, ConstraintViolationException.class},
				{"user1", "title1", null, "body", "article4", null, ConstraintViolationException.class},
		};

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], 
						   (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], 
						   (Class<?>) testingData[i][6]);
		}
	}
	
/* 6. An actor who is authenticated as a user must be able to:
	3. Write an article and attach it to any newspaper that has not been published, yet.
	Note that articles may be saved in draft mode, which allows to modify them later, or
	final model, which freezes them forever.*/
	
	@Test
	public void driverEdit() {
		Object[][] testingData = {
				{"user2", "article6", "title", "summary", "body", null}, //Artículo

				{"customer1", "article6", "title", "summary", "body", IllegalArgumentException.class}, //Falta de credenciales (Customer)
				{"user1", "article1", "title", "summary", "body", IllegalArgumentException.class}, //Edición sin draft
		};

		for (int i = 0; i < testingData.length; i++) {
			templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], 
						 (String) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);
		}
	}
/*7. An actor who is authenticated as an administrator must be able to:
		1. Remove an article that he or she thinks is inappropriate.*/	
	@Test
	public void driverDelete() {
		Object[][] testingData = {
				{"admin", "article1", null}, //Artículo

				{"user2", "article6", IllegalArgumentException.class}, //Falta de credenciales (User)
				{"admin", "article7", IllegalArgumentException.class}, //Borrado de follow-up
		};

		for (int i = 0; i < testingData.length; i++) {
			templateDelete((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}
	
/* 6. An actor who is authenticated as a user must be able to:
	3. Write an article and attach it to any newspaper that has not been published, yet.
	Note that articles may be saved in draft mode, which allows to modify them later, or
	final model, which freezes them forever.*/	
	
	@Test
	public void driverSetAsFinal() {
		Object[][] testingData = {
				{"user2", "article6", null}, //Publicación

				{"user2", "article1", IllegalArgumentException.class}, //Artículo ya publicado
				{"user2", "article7", IllegalArgumentException.class}, //Publicar un follow-up
		};

		for (int i = 0; i < testingData.length; i++) {
			templateSetAsFinal((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateCreate(String username, String title, String summary, String body, String articleBean, String newspaperBean, Class<?> expected) {
		Class<?> caught;

		Article article;
		Article parentArticle;
		Newspaper newspaper;

		caught = null;

		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			if(newspaperBean != null){
				newspaper = newspaperService.findOne(super.getEntityId(newspaperBean));				
			
			} else{
				newspaper = null;
			}
			
			if(articleBean != null){
				parentArticle = articleService.findOne(super.getEntityId(articleBean));				
			
			} else{
				parentArticle = null;
			}
			
			article = articleService.create(parentArticle, newspaper);
			Assert.notNull(article);

			article.setTitle(title);
			article.setSummary(summary);
			article.setBody(body);

			article = articleService.save(article);
			articleService.flush();
			
			Assert.isTrue(articleService.findAll().contains(article));
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	protected void templateEdit(String username, String articleBean, String title, String summary, String body, Class<?> expected) {
		Class<?> caught;

		Article article;

		caught = null;

		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			article = articleService.findOne(super.getEntityId(articleBean));
			Assert.notNull(article);

			article.setTitle(title);
			article.setSummary(summary);
			article.setBody(body);

			article = articleService.save(article);
			articleService.flush();
			
			Assert.isTrue(article.getTitle().equals(title));
			Assert.isTrue(article.getSummary().equals(summary));
			Assert.isTrue(article.getBody().equals(body));
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}

	protected void templateDelete(String username, String articleBean, Class<?> expected) {
		Class<?> caught;
	
		Article article;
	
		caught = null;
	
		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			article = articleService.findOne(super.getEntityId(articleBean));
			Assert.notNull(article);
	
			articleService.delete(article);
			articleService.flush();
			
			Assert.isTrue(!articleService.findAll().contains(article));
			
			super.unauthenticate();
	
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}
	
		checkExceptions(expected, caught);
	}

	protected void templateSetAsFinal(String username, String articleBean, Class<?> expected) {
		Class<?> caught;
	
		Article article;
	
		caught = null;
	
		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			article = articleService.findOne(super.getEntityId(articleBean));
			Assert.notNull(article);
	
			articleService.putArticleInFinalMode(article);
			articleService.flush();
			
			article = articleService.findOne(super.getEntityId(articleBean));
			Assert.isTrue(!article.getDraft());
			
			super.unauthenticate();
	
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}
	
		checkExceptions(expected, caught);
	}
}
