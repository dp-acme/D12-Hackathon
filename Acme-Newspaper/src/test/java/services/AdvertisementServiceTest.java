package services;

import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;

import com.mchange.v1.util.UnexpectedException;

import domain.Actor;
import domain.Advertisement;
import domain.Newspaper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class AdvertisementServiceTest extends AbstractTest {

	@Autowired
	private AdvertisementService advertisementService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private ActorService actorService;

	
/*	4. An actor who is authenticated as an agent must be able to:
		2. Register an advertisement and place it in a newspaper.*/
	@Test
	public void driverCreate() {
		Object[][] testingData = {
				{"agent1", "title", "https://www.jackripper.com/coke/ProdImages/700.jpg", "https://ccardgenerator.com/generat-visa-card-numbers.php", "advertisement1", "newspaper1", "agent1", null}, 
		
				{null, "title", "https://www.jackripper.com/coke/ProdImages/700.jpg", "https://ccardgenerator.com/generat-visa-card-numbers.php", "advertisement1", "newspaper1", "agent1", IllegalArgumentException.class}, 
				{"agent1", "", "https://www.jackripper.com/coke/ProdImages/700.jpg", "https://ccardgenerator.com/generat-visa-card-numbers.php", "advertisement1", "newspaper1", "agent1", ConstraintViolationException.class}, 

		};

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], 
						   (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], 
						   (String) testingData[i][6], (Class<?>) testingData[i][7]);
		}
	}
	
/*	5. An actor who is authenticated as an administrator must be able to:
		2. Remove an advertisement that he or she thinks is inappropriate.*/	
	@Test
	public void driverDelete() {
		Object[][] testingData = {
				{"admin", "advertisement1", null}, 

				{"user2", "advertisement1", IllegalArgumentException.class}, 
				{"agent1", "advertisement1", IllegalArgumentException.class}, 
		};

		for (int i = 0; i < testingData.length; i++) {
			templateDelete((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}
	
/*	1. List the advertisements that contain taboo words in its title.
 */
	@Test
	public void listDrive(){
		Object[][] testingData = {
				{ "admin",  null },
				
				{ null, IllegalArgumentException.class },
				{ "user1", IllegalArgumentException.class },

				};

		for (int i = 0; i < testingData.length; i++) {
			templateList((String) testingData[i][0],(Class<?>) testingData[i][1]);
		}
	}
	
	/*	3. List the newspapers in which they have placed an advertisement.
	 */
		@Test
		public void listAdvertisedNewspaperDrive(){
			Object[][] testingData = {
					{ "agent1",  null },
					
					{ "admin", IllegalArgumentException.class },
					{ "user1", IllegalArgumentException.class },

					};

			for (int i = 0; i < testingData.length; i++) {
				templateListAdvertisedNewspaper((String) testingData[i][0],(Class<?>) testingData[i][1]);
			}
		}
		
	/*	4. List the newspapers in which they have not placed any advertisements.
	 */
		@Test
		public void listNotAdvertisedNewspaperDrive(){
			Object[][] testingData = {
					{ "agent1",  null },
					
					{ "admin", IllegalArgumentException.class },
					{ "user1", IllegalArgumentException.class },

					};

			for (int i = 0; i < testingData.length; i++) {
				templateListNotAdvertisedNewspaper((String) testingData[i][0],(Class<?>) testingData[i][1]);
			}
		}
	

	protected void templateCreate(String username, String title, String banner, String target, String advertisementBeanForCreditCard, String newspaperBean, String agentBean, Class<?> expected) {
		Class<?> caught;

		Advertisement advertisementForCreditCard;
		Advertisement advertisement;
		Actor agent;
		Newspaper newspaper;

		caught = null;

		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			newspaper = newspaperService.findOne(super.getEntityId(newspaperBean));				
			advertisementForCreditCard = advertisementService.findOne(super.getEntityId(advertisementBeanForCreditCard));
			agent = actorService.findOne(super.getEntityId(agentBean));
			
			advertisement = advertisementService.create(agent.getId(), newspaper.getId());
			Assert.notNull(advertisement);

			advertisement.setTitle(title);
			advertisement.setBanner(banner);
			advertisement.setTarget(target);
			advertisement.setCreditCard(advertisementForCreditCard.getCreditCard());

			advertisement = advertisementService.save(advertisement);
			advertisementService.flush();
			
			Assert.isTrue(advertisementService.findAll().contains(advertisement));
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	protected void templateDelete(String username, String advertisementBean, Class<?> expected) {
		Class<?> caught;
	
		Advertisement advertisement;
	
		caught = null;
	
		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			advertisement = advertisementService.findOne(super.getEntityId(advertisementBean));
			Assert.notNull(advertisement);
	
			advertisementService.delete(advertisement.getId());
			advertisementService.flush();
			
			Assert.isTrue(!advertisementService.findAll().contains(advertisement));
			
			super.unauthenticate();
	
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}
	
		checkExceptions(expected, caught);
	}
	
	protected void templateList(String username, Class<?> expected){
		
		Class<?> caught;

		Collection<Advertisement> adverts;
		
		caught = null;

		try {
			super.authenticate(username);
			
			adverts = advertisementService.advertisementsContainsTabooWords();
			if(adverts==null) throw new UnexpectedException();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	
	protected void templateListAdvertisedNewspaper(String username, Class<?> expected){
		
		Class<?> caught;

		Collection<Newspaper> newspapers;
		
		caught = null;

		try {
			super.authenticate(username);
			Actor actor;
			actor = actorService.findOne(super.getEntityId(username));
			newspapers = advertisementService.getAdvertisedNewspapers(actor.getId());
			if(newspapers==null||newspapers.size()<0) throw new UnexpectedException();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}

	protected void templateListNotAdvertisedNewspaper(String username, Class<?> expected){
		
		Class<?> caught;

		Collection<Newspaper> newspapers;
		
		caught = null;

		try {
			super.authenticate(username);
			Actor actor;
			actor = actorService.findOne(super.getEntityId(username));
			newspapers = advertisementService.getNotAdvertisedNewspapers(actor.getId());
			if(newspapers==null||newspapers.size()<0) throw new UnexpectedException();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
}
