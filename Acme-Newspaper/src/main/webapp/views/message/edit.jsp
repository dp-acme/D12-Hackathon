<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form method="POST" action="message/edit.do" modelAttribute="myMessage">
	
	<input type="hidden" name="isNotification" value="${isNotification}" />
	
	<jstl:if test="${!isNotification}">
		<form:label path="recipients">
			<spring:message code="message.edit.recipients"/>
		</form:label>
		<form:select path="recipients" multiple="multiple">
			<jstl:forEach items="${actors}" var="actor">
				<jstl:set var="actorSelected" value="" />
				<jstl:if test="${myMessage.getRecipients().contains(actor)}">
					<jstl:set var="actorSelected" value="selected" />
				</jstl:if>
				<form:option label="${actor.getUserAccount().getUsername()} | ${actor.getName()} ${actor.getSurname()}" 
				value="${actor.getId()}" selected="${actorSelected}" />
			</jstl:forEach>
		</form:select>
		<form:errors path="recipients" cssClass="error" />
	</jstl:if>
	
	<br />
	
	<form:label path="priority.priority">
		<spring:message code="message.edit.priority"/>
	</form:label>
	<form:select path="priority.priority">
		<form:option value="NEUTRAL"><spring:message code="message.edit.priority.neutral" /></form:option>
		<form:option value="LOW"><spring:message code="message.edit.priority.low" /></form:option>
		<form:option value="HIGH"><spring:message code="message.edit.priority.high" /></form:option>
	</form:select>
	<form:errors path="priority.priority" cssClass="error" />
	
	<br />
	
	<acme:textbox required="required" code="message.edit.subject" path="subject"/>
	
	<br />
	
	<acme:textarea required="required" code="message.edit.body" path="body"/>
	
	<br />
	
	<acme:submit name="send" code="message.edit.send"/>
	<acme:cancel url="folder/list.do" code="message.edit.cancel"/>
	
</form:form>