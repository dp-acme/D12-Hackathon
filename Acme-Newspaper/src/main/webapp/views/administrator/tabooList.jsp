<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<spring:message var="newspapersList" code="taboo.newspaper.list"/>
<h2><jstl:out value="${newspapersList}"/></h2>	

<display:table name="newspapers" id="row" requestURI="${requestURI}"
	pagesize="5">
	<display:column property="title" titleKey="taboo.newspaper.list.title" />

	<display:column property="description"
		titleKey="taboo.newspaper.list.description" />

	<spring:url var="urlDisplayNewspaper"
		value="newspaper/display.do?newspaperId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false">
		<a href="${urlDisplayNewspaper}"> <spring:message
				code="taboo.newspaper.list.display" />
		</a>
	</display:column>

</display:table>
<br />

<spring:message var="articlesList" code="taboo.article.list"/>
<h2><jstl:out value="${articlesList}"/></h2>	

<display:table name="articles" id="row" requestURI="${requestURI}"
	pagesize="5">
	<display:column property="title" titleKey="taboo.article.list.title" />

	<display:column property="summary"
		titleKey="taboo.article.list.description" />

	<spring:url var="urlDisplayarticle"
		value="article/display.do?articleId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false">
		<a href="${urlDisplayarticle}"> <spring:message
				code="taboo.article.list.display" />
		</a>
	</display:column>

</display:table>
<br />


<spring:message var="chirpsList" code="taboo.chirp.list"/>
<h2><jstl:out value="${chirpsList}"/></h2>	

<display:table name="chirps" id="row" requestURI="${requestURI}"
	pagesize="5">
	<display:column property="title" titleKey="taboo.chirp.list.title" />

	<display:column property="description"
		titleKey="taboo.chirp.list.description" />

	<display:column titleKey="taboo.chirp.list.user">
		<jstl:out value="${row.user.name}" />
	</display:column>

	<spring:url var="urlDisplayChirp"
		value="chirp/display.do?chirpId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false">
		<a href="${urlDisplayChirp}"> <spring:message
				code="taboo.chirp.list.display" />
		</a>
	</display:column>

</display:table>

<spring:message var="advertList" code="taboo.advert.list"/>
<h2><jstl:out value="${advertList}"/></h2>	

<display:table name="advertisements" id="row" requestURI="${requestURI}"
	pagesize="5">
	<display:column property="title" titleKey="taboo.advert.list.title" />

	<display:column titleKey="taboo.advertisement.list.newspaper">
		<jstl:out value="${row.newspaper.title}" />
	</display:column>
	
	<spring:url var="urlDisplayAdvert"
		value="advertisement/admin/delete.do?advertisementId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false">
		<a href="${urlDisplayAdvert}"> <spring:message
				code="taboo.advert.list.delete" />
		</a>
	</display:column>

</display:table>
<br />



