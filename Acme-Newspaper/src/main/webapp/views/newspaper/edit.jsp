
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

</head>
<body>

<form:form action="newspaper/user/edit.do?newspaperId=${request.getId()}" modelAttribute="newspaper" >

	<form:hidden path="id" />

	<acme:textbox code="newspaper.display.title" path="title" required="required" />
	<br />
	
	<acme:textbox code="newspaper.display.description" path="description" required="required" />
	<br />
	
	<acme:textbox code="edit.picture" path="picture"/>
	<br />
				
	<form:label path="isPrivate">
		<spring:message code="newspaper.display.private" />
	</form:label>
	<form:checkbox path="isPrivate" /><br/><br/>
	
	<!-- Botones del formulario -->
	<acme:submit name="save" code="edit.save" />
	<acme:cancel url="/newspaper/list.do" code="edit.cancel" />
	

</form:form>

</body>
</html>