
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
</head>
<body>

<script>
	function search(){
		var keyword = document.getElementById("keywordInput").value;
		
		relativeRedir("newspaper/list.do?keyword=" + keyword);
	}
</script>

<jstl:if test="${requestURI != \"advertisement/agent/listAdvertised\"}">
	<jstl:if test="${requestURI != \"advertisement/agent/listNotAdvertised\"}">	
		<input type="text" id="keywordInput"/>
		<spring:message var="searchButton" code="article.button.search" />
		<input type="button" value="${searchButton}" onclick="javascript: search();"/>
	</jstl:if>
</jstl:if>

<p>
	<b>
		<spring:message code="newspaper.list.published"/>:
	</b>
</p>

<display:table name="newspaper" requestURI="newspaper/list.do" id="row" pagesize="5">

	<display:column property="title" titleKey="newspaper.list.title" style="${style}" />

	<display:column title="" sortable="false">
		<a href="newspaper/display.do?newspaperId=${row.id}"> <spring:message code="newspaper.list.display" /> </a>
	</display:column>

</display:table>

<security:authorize access="hasRole('USER')">
	<p>
		<b>
			<spring:message code="newspaper.list.myNews"/>:
		</b>
	</p>
	
	<display:table name="myNewspaper" requestURI="newspaper/list.do" id="row" pagesize="5">
	
		<display:column property="title" titleKey="newspaper.list.title" style="${style}" />
	
		<display:column title="" sortable="false">
			<a href="newspaper/display.do?newspaperId=${row.id}"> <spring:message code="newspaper.list.display" /> </a>
		</display:column>
	
	</display:table>
	
	<br/>
	<a href="newspaper/user/create.do"> <spring:message code="newspaper.list.create" /> </a>
</security:authorize>
</body>
</html>