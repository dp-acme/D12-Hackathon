
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

</head>
<body>
<spring:message var="formatDatePattern" code="newspaper.display.dateFormat" />

<jstl:if test="${not empty banner}">
	<br />
	<jstl:url value="${target}" var="target"></jstl:url>
	<a href="${target}"><img src="${banner}" style="width:300px"/></a>
	<br />
</jstl:if>

<security:authorize access="hasRole('AGENT')">
	<a href="advertisement/agent/edit.do?newspaperId=${newspaper.id}"> <spring:message code="create.advertisement" /> </a><br/>
</security:authorize>

<jstl:if test="${newspaper.draft and creator}">
		<a href="newspaper/user/edit.do?newspaperId=${newspaper.id}"> <spring:message code="edit.edit" /> </a><br/>
		<jstl:if test="${allFinal == null}">
			<a href="newspaper/user/publish.do?newspaperId=${newspaper.id}"> <spring:message code="edit.publish" /> </a><br/>
		</jstl:if>
		<a href="article/user/create.do?newspaperId=${newspaper.id}"> <spring:message code="edit.createArticle" /> </a><br/>
</jstl:if>

<security:authorize access="hasRole('ADMIN')">
	<a href="newspaper/admin/delete.do?newspaperId=${newspaper.id}"> <spring:message code="edit.delete" /> </a><br/>
</security:authorize>


<h2>
	<jstl:out value="${newspaper.title}" />
</h2>

<!-- Newspaper -->

<jstl:if test="${not empty newspaper.picture}" >
	<img src="${newspaper.picture}" width="300" /><br/><br/>
</jstl:if>

<b><spring:message code="newspaper.display.description" />:</b>
	<jstl:out value="${newspaper.description}" /> <br/><br/>
	
<b><spring:message code="newspaper.display.date" />:</b>
	<jstl:choose>
		<jstl:when test="${newspaper.publicationDate != null}">
			<fmt:formatDate value="${newspaper.publicationDate}" pattern="${formatDatePattern}" />
		</jstl:when>
		<jstl:otherwise>
				<spring:message code="newspaper.display.notpublished" />
		</jstl:otherwise>
	</jstl:choose>
 <br/><br/>
 
<jstl:choose>
	<jstl:when test="${newspaper.draft}">
		<b>*<spring:message code="newspaper.display.draft" />*</b><br/><br/>
	</jstl:when>
</jstl:choose>

<jstl:choose>
	<jstl:when test="${newspaper.isPrivate}">
		<b style="color: red"> *<spring:message code="newspaper.display.private" />*</b> <br/><br/>
	</jstl:when>
</jstl:choose>

<security:authorize access="hasRole('CUSTOMER')">
	<jstl:if test="${!suscribed and newspaper.isPrivate}">
		<a href="newspaperSubscription/customer/create.do?newspaperId=${newspaper.id}"> <spring:message code="newspaper.display.subscribe" /> </a>
	</jstl:if>
</security:authorize>

<!-- Articles -->

<h3><b><spring:message code="newspaper.display.articles" /></b><br/></h3>

	<display:table name="articles" id="row" requestURI="${requestURI}" pagesize="5">
		
		<display:column titleKey="newspaper.display.title">
			<jstl:if test="${!newspaper.isPrivate or creator or suscribed}">
				<a href="article/display.do?articleId=${row.id}"><jstl:out value="${row.title}"/></a>	
			</jstl:if>
			<jstl:if test="${!(!newspaper.isPrivate or creator or suscribed)}">
				<jstl:out value="${row.title}"/>
			</jstl:if>
		</display:column>

		<display:column property="summary" titleKey="newspaper.display.summary" />

		<display:column titleKey="newspaper.display.writer"><a href="/user/display.do?userId=${row.newspaper.creator.id}">
			<jstl:out value="${row.newspaper.creator.name}"/></a>
		</display:column>
	
	</display:table>
	<br/>
	<acme:cancel url="newspaper/list.do" code="newspaper.display.back"/>
</body>
</html>