<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<fieldset>
	<!-- Title -->
	<h2>
		<jstl:out value="${chirp.getTitle()}" />
	</h2>


	<!-- Description -->
	<b><spring:message code="chirp.display.description" />:</b>
	<jstl:out value="${chirp.getDescription()}" />

	<br/> <br/> 
	
	<!-- Moment -->
	<b><spring:message code="chirp.display.moment" />:</b>
	<spring:message code="chirp.display.formatTime" var="formatDate" />
	<fmt:formatDate value="${chirp.getMoment()}" pattern="${formatDate}"
		var="publicationDateFmt" />

	<jstl:out value="${publicationDateFmt}"></jstl:out>
	
	<br/> <br/> 
	
	<!-- User -->
	<b><spring:message code="chirp.display.user" />:</b>
	<jstl:out value="${chirp.getUser().getName()}" />
</fieldset>