<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<security:authorize access="hasRole('USER')">

	<!-- Abrimos el formulario -->

	<form:form action="chirp/user/create.do" modelAttribute="chirpForm">

		<!-- Indicamos los campos de los formularios -->

		<acme:textbox code="chirp.create.title" path="editedTitle"
			required="required" />
		<br />
		<br />

		<acme:textarea code="chirp.create.description" path="editedDescription"
			required="required" />
		<br />
		<br />


		<!-- Botones del formulario -->
		<acme:submit name="save" code="chirp.create.save" />

		<acme:cancel url="chirp/user/list.do" code="chirp.create.cancel" />


	</form:form>
</security:authorize>