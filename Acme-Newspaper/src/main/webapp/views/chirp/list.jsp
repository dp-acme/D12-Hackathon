<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<display:table name="chirps" id="row" requestURI="${requestURI}"
	pagesize="5">


	<display:column property="title" titleKey="chirp.list.title" />

	<display:column property="description"
		titleKey="chirp.list.description" />

	<display:column titleKey="chirp.list.user">
		<jstl:out value="${row.user.name}" />
	</display:column>

	<spring:url var="urlDisplayChirp"
		value="chirp/display.do?chirpId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false">
		<a href="${urlDisplayChirp}"> <spring:message
				code="chirp.list.display" />
		</a>
	</display:column>

	<security:authorize access="hasRole('ADMIN')">
		<jstl:if test="${requestURI == 'chirp/admin/list.do'}">
			<display:column title="" sortable="false">
				<a href="chirp/admin/delete.do?chirpId=${row.getId()}"> <spring:message
						code="chirp.list.delete" />
				</a>

			</display:column>
		</jstl:if>
	</security:authorize>

</display:table>
<br />

<jstl:if test="${requestURI == 'chirp/user/list.do'}">
	<security:authorize access="hasRole('USER')">
		<a href="chirp/user/create.do"> <spring:message
				code="chirp.list.create" /></a>
	</security:authorize>
</jstl:if>


