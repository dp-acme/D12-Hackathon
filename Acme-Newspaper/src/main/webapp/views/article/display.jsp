<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<spring:message code="article.dateFormat" var="datePattern"/>

<fieldset>
	<jstl:if test="${isEditable}">
		<i><spring:message code="article.draft"/></i>
			
		(<a href="article/user/publish.do?articleId=${article.id}"><spring:message code="article.button.publish"/></a>)		

		<br>

		<h2><jstl:out value="${article.title}" /></h2>	
	</jstl:if>

	<jstl:if test="${!isEditable}">
		<h2 style="margin-top: 0px;"><jstl:out value="${article.title}" /></h2>	
	</jstl:if>
	
	<h3><jstl:out value="${article.summary}" /></h3>

	<p>
		<b>
			<spring:message code="article.moment" />: <fmt:formatDate value="${article.moment}" pattern="${datePattern}" />
		</b>
	</p>
	
	<p>
		<jstl:forEach items="${article.pictures}" var="image">
			<img src="${image}" height="200" width="200"/>
		</jstl:forEach>
	</p>
		
	<p>
	 <jstl:out value="${article.body}" />
	</p>
	
	<jstl:if test="${isFollowable}">
		<input type="button" value="<spring:message code="article.button.addFollowUp"/>" 
			   onclick="javascript: relativeRedir('article/user/create.do?articleId=${article.id}');"/>
	</jstl:if>
	
	<jstl:if test="${isEditable}">
		<input type="button" value="<spring:message code="article.button.editArticle"/>" 
			   onclick="javascript: relativeRedir('article/user/edit.do?articleId=${article.id}');"/>
	</jstl:if>
	
	<jstl:if test="${isDeleteable}">
		<spring:message code="article.button.deleteMessage" var="deleteMessage" />
		<jstl:set value="${article.id}" var="articleId" />
		
		<script>
			function conditionalDelete(){
				var message = "<%= pageContext.getAttribute("deleteMessage") %>";
				
				if(confirm(message)){
					relativeRedir("article/administrator/delete.do?articleId=<%= pageContext.getAttribute("articleId") %>");
				}
			}
		</script>
	
		<input type="button" value="<spring:message code="article.button.delete"/>" 
			   onclick="javascript: conditionalDelete();"/>
	</jstl:if>
</fieldset>

<h1><spring:message code="article.followUps" /></h1>

<jstl:forEach items="${followUps}" var="followUp">	
	<fieldset>
		<h2 style="margin-top: 0px;"><jstl:out value="${followUp.title}" /></h2>
		<h3><jstl:out value="${followUp.summary}" /></h3>
	
		<p>
			<b>
				<spring:message code="article.moment" />: <fmt:formatDate value="${followUp.moment}" pattern="${datePattern}" />
			</b>
		</p>
		
		<p>
			<jstl:forEach items="${followUp.pictures}" var="image">
				<img src="${image}" height="200" width="200"/>
			</jstl:forEach>
		</p>
	
		<p>
		 <jstl:out value="${followUp.body}" />
		</p>
	</fieldset>
</jstl:forEach>