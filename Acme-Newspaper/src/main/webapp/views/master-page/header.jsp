<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Sample Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv"><spring:message	code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="dashboard/display.do"><spring:message code="master.page.administrator.dashboard" /></a></li>
					<li><a href="configuration/admin/display.do"><spring:message code="master.page.administrator.configuration" /></a></li>
					<li><a href="administrator/tabooList.do"><spring:message code="master.page.administrator.tabooList" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv" href="chirp/admin/list.do"><spring:message code="master.page.chirps" /></a>
		</security:authorize>
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv"><spring:message	code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="user/create.do"><spring:message code="master.page.user" /></a></li>
					<li><a href="customer/create.do"><spring:message code="master.page.customer" /></a></li>
					<li><a href="agent/create.do"><spring:message code="master.page.agent" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		
		
		<li><a class="fNiv" href="user/list.do"><spring:message	code="master.page.users" /></a>
			<security:authorize access="hasRole('USER')">	
				<ul>
					<li><a href="user/listFollowed.do"><spring:message code="master.page.listFollowed" /></a></li>
					<li><a href="user/listFollowers.do"><spring:message code="master.page.listFollowers" /></a></li>
				</ul>
			</security:authorize>
		<li><a class="fNiv" href="volume/list.do"><spring:message code="master.page.volumes" /></a>
			<security:authorize access="hasRole('USER')">	
				<ul>
					<li><a href="volume/list.do?userId=${principalActor.id}"><spring:message code="master.page.myVolumes" /></a></li>
				</ul>
			</security:authorize>
			<security:authorize access="hasRole('CUSTOMER')">	
				<ul>
					<li><a href="volumeSubscription/customer/list.do?customerId=${principalActor.id}"><spring:message code="master.page.myVolumeSubscriptions" /></a></li>
				</ul>
			</security:authorize>
		<li><a class="fNiv" href="newspaper/list.do"><spring:message code="master.page.newspapers" /></a>
			<security:authorize access="hasRole('USER')">	
				<ul>
					<li class="arrow"></li>
					<li><a href="newspaper/user/create.do"><spring:message code="master.page.newspaper.create" /></a></li>
				</ul>
			</security:authorize>
			<security:authorize access="hasRole('CUSTOMER')">	
				<ul>
					<li class="arrow"></li>
					<li><a href="newspaperSubscription/customer/list.do"><spring:message code="master.page.newspaper.subscription" /></a></li>
				</ul>
			</security:authorize>
			
			<security:authorize access="hasRole('AGENT')">	
				<ul>
					<li class="arrow"></li>
					<li><a href="advertisement/agent/listAdvertised.do"><spring:message code="master.page.newspaper.listAdvertised" /></a></li>
					<li><a href="advertisement/agent/listNotAdvertised.do"><spring:message code="master.page.newspaper.listNotAdvertised" /></a></li>
				</ul>
			</security:authorize>			
		</li>
		<li><a class="fNiv" href="article/list.do"><spring:message code="master.page.articles" /></a></li>
		
			<security:authorize access="hasRole('USER')">	
				<li><a class="fNiv" href="chirp/user/chirpsUsersFollows.do"><spring:message code="master.page.chirps" /></a>
				<ul>
					<li><a href="chirp/user/list.do"><spring:message code="master.page.myChirps" /></a></li>
					<li><a href="chirp/user/chirpsUsersFollows.do"><spring:message code="master.page.followingChirps" /></a></li>
				</ul>
			</security:authorize>
	
	<security:authorize access="isAuthenticated()">
		<li>
			<a class="fNiv" href="folder/list.do">
		        <spring:message	code="master.page.messages" />
			</a>
		</li>
		
		<li>
			<a class="fNiv">
				  
		        (<security:authentication property="principal.username" />)
			</a>
			<ul>
				<li class="arrow"></li>
				<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				<security:authorize access="hasRole('USER')">	
					<li><a href="user/display.do"><spring:message code="master.page.profile" /></a></li>
				</security:authorize>
			</ul>
		</li>
	</security:authorize>
		
	<security:authorize access="isAnonymous()">
		<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
	</security:authorize>
	
</ul>
	
		
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

