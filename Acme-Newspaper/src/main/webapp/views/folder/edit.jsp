<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form method="POST" action="folder/edit.do" modelAttribute="folder">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="parentFolder" />
	
	<acme:textbox code="folder.edit.name" path="name"/>
	
	<br />
	
	<jstl:if test="${folder.getId() != 0}" >
		<acme:select items="${parentFolders}" itemLabel="name" code="folder.edit.parentFolder" path="parentFolder"/>
	</jstl:if>
	
	<br />
	<br />
	
	<acme:submit name="save" code="folder.edit.save"/>
	
	<jstl:if test="${folder.getId() != 0}">
		<spring:message var="deleteConfirm" code="folder.edit.deleteConfirm" />
		<acme:submit name="delete" code="folder.edit.delete" onClickCallback="javascript: return confirm('${deleteConfirm}');"/>
	</jstl:if>
	
	<jstl:set value="" var="backFolderIdParam" />
	<jstl:if test="${folder.getId() != 0}">
		<jstl:set value="?folderId=${folder.getId()}" var="backFolderIdParam" />
	</jstl:if>
	<jstl:if test="${folder.getId() == 0 && folder.getParentFolder() != null}">
		<jstl:set value="?folderId=${folder.getParentFolder().getId()}" var="backFolderIdParam" />
	</jstl:if>
	
	<acme:cancel url="folder/list.do${backFolderIdParam}" code="folder.edit.cancel"/>
	
</form:form>