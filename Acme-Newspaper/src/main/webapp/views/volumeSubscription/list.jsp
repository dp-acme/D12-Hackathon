<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="subscriptions" requestURI="volumeSubscription/customer/list.do?customerId=${param.customerId}" id="row">
	<spring:message code="volume.table.title" var="titleHeader" />
	<display:column title="${titleHeader}">
		<a href="volume/display.do?volumeId=${row.volume.id}">
			<jstl:out value="${row.volume.title}"/>
		</a>
	</display:column>
	
	<spring:message code="volume.table.description" var="descriptionHeader" />
	<display:column title="${descriptionHeader}">
		<jstl:out value="${row.volume.description}"/>
	</display:column>

	<spring:message code="volume.table.year" var="yearHeader" />	
	<display:column title="${yearHeader}">
		<jstl:out value="${row.volume.year}"/>
	</display:column>
</display:table>