<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<display:table name="newspaperSubscriptions" id="row" requestURI="newspaperSubscription/list.do" pagesize="5">
	
	<display:column property="newspaper.title" titleKey="newspaperSubscription.list.title" sortable="false" href="newspaper/display.do?newspaperId=${row.getNewspaper().getId()}" />
	
	<display:column property="newspaper.description" titleKey="newspaperSubscription.list.description" sortable="false" />
	
	<spring:message var="formatDate" code="newspaperSubscription.list.formatDate" />
	<display:column property="newspaper.publicationDate" titleKey="newspaperSubscription.list.publicationDate" sortable="true" format="{0,date,${formatDate}}" />
	
</display:table>
