<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<b> <spring:message code="volume.creator" />: </b> <jstl:out value="${volume.creator.name}" />
<br><br>

<b><spring:message code="volume.table.title" />: </b> <jstl:out value="${volume.title}" />
<br>
<b><spring:message code="volume.table.description" />: </b> <jstl:out value="${volume.description}" />
<br>
<b><spring:message code="volume.table.year" />: </b> <jstl:out value="${volume.year}" />
<br>

<security:authorize access="hasRole('CUSTOMER')">	
	<br>
	<jstl:if test="${!subscribed}">
		<spring:message code="volume.button.subscription" var="subscriptionButton" />
		<input type="button" value="${subscriptionButton}" onclick="javascript: relativeRedir('volumeSubscription/customer/create.do?volumeId=${volume.id}')">
	</jstl:if>
</security:authorize>

<h2> <spring:message code="volume.newspapers"/> </h2>

<display:table name="volume.newspapers" requestURI="volume/display.do?volumeId=${volume.id}" id="row">
	<spring:message code="newspaper.display.title"  var="titleHeader"/>
	<display:column title="${titleHeader}">
		<a href="newspaper/display.do?newspaperId=${row.id}">
			<jstl:out value="${row.title}" />
		</a>
	</display:column>
	
	<spring:message code="newspaper.display.description"  var="descriptionHeader"/>
	<display:column title="${descriptionHeader}" property="description"/>
	
	<spring:message code="newspaper.display.date"  var="publicationDateHeader"/>
	<spring:message code="newspaper.display.dateFormat"  var="dateFormat"/>
	<display:column title="${publicationDateHeader}">
		<fmt:formatDate value="${row.publicationDate}" pattern="${dateFormat}" />
	</display:column>
	
	<jstl:if test="${creator}">
		<spring:message code="volume.button.delete"  var="deleteButton"/>
		<display:column>
			<a href="javascript: relativeRedir('volume/user/delete.do?volumeId=${volume.id}&newspaperId=${row.id}')">
				<jstl:out value="${deleteButton}" />
			</a>
		</display:column>
	</jstl:if>
</display:table>

<jstl:if test="${creator && freeNewspapers.size() > 0}">
	<jstl:if test="${volume.newspapers.size() == 0}">
		<br>
	</jstl:if>
	
	<select id = "newspaperSelect">
		<jstl:forEach items="${freeNewspapers}" var="n">
			<option id = "${n.id}">
				<jstl:out value="${n.title}"/>
			</option>
		</jstl:forEach>
	</select>
	
	<script>
		function addNewspaper(volumeId){
			var select = document.getElementById("newspaperSelect");
			var newspaperId = select.options[select.selectedIndex].id;
			
			relativeRedir("volume/user/add.do?volumeId=" + volumeId + "&newspaperId=" + newspaperId);
		}
	</script>
	
	<spring:message code="volume.button.addNewspaper" var="addButton"/>
	<input type="button" value="${addButton}" onclick="javascript: addNewspaper('${volume.id}')" />
</jstl:if>