<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form modelAttribute="volume" action="volume/user/edit.do?userId=${param.userId}"> 
	<form:hidden path="id" />
	<form:hidden path="version" />
	
	<acme:textbox code="volume.table.title" path="title" required="required"/>
	<acme:textarea code="volume.table.description" path="description" required="required"/>
	<acme:textbox code="volume.table.year" path="year" type="number" required="required"/>
	
	<br>
	
	<acme:submit code="volume.button.submit" name="save"/>
	<acme:cancel code="volume.button.cancel" url="volume/list.do?userId=${param.userId}"/>
</form:form>