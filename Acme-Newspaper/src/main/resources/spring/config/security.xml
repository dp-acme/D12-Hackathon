<?xml version="1.0" encoding="UTF-8"?>

<!-- 
 * security.xml
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 -->

<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:security="http://www.springframework.org/schema/security"	
	xsi:schemaLocation="
		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd		
        http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security-3.2.xsd
    ">

	<!-- Security infrastructure -->

	<bean id="loginService" class="security.LoginService" />

	<bean id="passwordEncoder"
		class="org.springframework.security.authentication.encoding.Md5PasswordEncoder" />

	<!-- Access control -->

	<security:http auto-config="true" use-expressions="true">
		<security:intercept-url pattern="/" access="permitAll" /> 

		<security:intercept-url pattern="/favicon.ico" access="permitAll" /> 
		<security:intercept-url pattern="/images/**" access="permitAll" /> 
		<security:intercept-url pattern="/scripts/**" access="permitAll" /> 
		<security:intercept-url pattern="/styles/**" access="permitAll" /> 

		<security:intercept-url pattern="/views/misc/index.jsp" access="permitAll" /> 

		<security:intercept-url pattern="/security/login.do" access="permitAll" /> 
		<security:intercept-url pattern="/security/loginFailure.do" access="permitAll" /> 

		<security:intercept-url pattern="/welcome/index.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/faq.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/aboutUs.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/termsAndConditions.do" access="permitAll" />
		
		<security:intercept-url pattern="/user/create.do" access="isAnonymous()" />
		<security:intercept-url pattern="/customer/create.do" access="isAnonymous()" />
		<security:intercept-url pattern="/agent/create.do" access="isAnonymous()" />
		<security:intercept-url pattern="/user/list.do" access="permitAll" />
		<security:intercept-url pattern="/user/display.do" access="permitAll" />
		<security:intercept-url pattern="/user/listFollowed.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/user/listFollowers.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/user/follow.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/user/unfollow.do" access="hasRole('USER')" />
		
		<security:intercept-url pattern="/folder/list.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/folder/edit.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/folder/create.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/folder/delete.do" access="isAuthenticated()" />
		
		<security:intercept-url pattern="/message/display.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/message/edit.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/message/delete.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/message/create.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/message/reply.do" access="isAuthenticated()" />
		
		<security:intercept-url pattern="/configuration/admin/display.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/configuration/admin/edit.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/newspaper/list.do" access="permitAll" />
		<security:intercept-url pattern="/newspaper/display.do" access="permitAll" />
		<security:intercept-url pattern="/newspaper/user/create.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/newspaper/user/edit.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/newspaper/user/publish.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/newspaper/admin/delete.do" access="hasRole('ADMIN')" />

		<security:intercept-url pattern="/volume/list.do" access="permitAll" />
		<security:intercept-url pattern="/volume/display.do" access="permitAll" />
		<security:intercept-url pattern="/volume/user/create.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/volume/user/edit.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/volume/user/add.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/volume/user/delete.do" access="hasRole('USER')" />

		<security:intercept-url pattern="/volumeSubscription/customer/create.do" access="hasRole('CUSTOMER')" />
		<security:intercept-url pattern="/volumeSubscription/customer/edit.do" access="hasRole('CUSTOMER')" />
		<security:intercept-url pattern="/volumeSubscription/customer/list.do" access="hasRole('CUSTOMER')" />
		
		<security:intercept-url pattern="/advertisement/agent/edit.do" access="hasRole('AGENT')" />
		<security:intercept-url pattern="/advertisement/agent/listAdvertised.do" access="hasRole('AGENT')" />
		<security:intercept-url pattern="/advertisement/agent/listNotAdvertised.do" access="hasRole('AGENT')" />
		<security:intercept-url pattern="/advertisement/admin/list.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/advertisement/admin/delete.do" access="hasRole('ADMIN')" />
<!-- 		<security:intercept-url pattern="/advertisement/admin/advertisementsWithTabooWords.do" access="hasRole('ADMIN')" /> -->

		<security:intercept-url pattern="/article/display.do" access="permitAll" />
		<security:intercept-url pattern="/article/list.do" access="permitAll" />
		<security:intercept-url pattern="/article/user/create.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/article/user/edit.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/article/user/save.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/article/user/publish.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/article/administrator/delete.do" access="hasRole('ADMIN')" />
				
		<security:intercept-url pattern="/newspaperSubscription/customer/list.do" access="hasRole('CUSTOMER')" />
		<security:intercept-url pattern="/newspaperSubscription/customer/create.do" access="hasRole('CUSTOMER')" />
		
				
		<security:intercept-url pattern="/chirp/user/list.do" access="hasRole('USER')" />	
		<security:intercept-url pattern="/chirp/user/create.do" access="hasRole('USER')" />	
		<security:intercept-url pattern="/chirp/user/chirpsUsersFollows.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/chirp/admin/list.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/chirp/admin/delete.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/chirp/display.do" access="hasAnyRole('USER', 'ADMIN')" />
		<security:intercept-url pattern="/chirp/admin/chirpsWithTabooWords.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/dashboard/display.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/administrator/tabooList.do" access="hasRole('ADMIN')" />
		
		
		
		<security:intercept-url pattern="/**" access="hasRole('NONE')" />

		<security:form-login 
			login-page="/security/login.do"
			password-parameter="password" 
			username-parameter="username"
			authentication-failure-url="/security/loginFailure.do" />

		<security:logout 
			logout-success-url="/" 
			invalidate-session="true" />
	</security:http>

</beans>