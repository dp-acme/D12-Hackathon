package services;

import java.text.DecimalFormat;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Message;
import domain.Newspaper;
import domain.Priority;

@Service
@Transactional
public class AdministratorService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private AdministratorRepository administratorRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor ---------------------------------------------------
	
	public AdministratorService() {
		super();
	}
	
	public Message broadcastNotification(String subject, String body, Priority priority, Actor actor) {
		Assert.hasLength(subject);
		Assert.hasLength(body);
		Assert.notNull(actor);
		Assert.notNull(priority);

		Authority aut;
		Message result;

		aut = new Authority();
		aut.setAuthority(Authority.ADMIN);

		Assert.isTrue(actor.getUserAccount().getAuthorities().contains(aut));

		result = this.messageService.create(actor);
		result.setRecipients(this.actorService.findAll());
		result.setSubject(subject);
		result.setBody(body);
		result.setPriority(priority);

		result = this.messageService.sendMessage(result, true);

		return result;
	}
	
	public 	Double[] getAvgSdNewspapersPerUser(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdNewspapersPerUser();
		
		return res;
	}	
	
	public 	Double[] getAvgSdArticlesPerUser(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdArticlesPerUser();
		
		return res;
	}	
	
	public 	Double[] getAvgSdArticlesPerNewspaper(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdArticlesPerNewspaper();
		
		return res;
	}	
	
	public Collection<Newspaper> getNewspaperWith10PercentMoreArticlesThanAvg(){
		Authority auth;
		UserAccount userAccount;
		Collection<Newspaper> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getNewspaperWith10PercentMoreArticlesThanAvg();
		
		return res;
	}
	
	public Collection<Newspaper> getNewspaperWith10PercentLessArticlesThanAvg(){
		Authority auth;
		UserAccount userAccount;
		Collection<Newspaper> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getNewspaperWith10PercentLessArticlesThanAvg();
		
		return res;
	}
	
	public Double ratioOfCreators(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioOfCreators();
		
		return res;
	}
	
	public Double ratioOfArticleCreators(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioOfArticleCreators();
		
		return res;
	}
	
	public Double getAvgFollowUpsPerArticle(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgFollowUpsPerArticle();
		
		return res;
	}
	
	public 	Double[] getAvgSdChirpsPerUser(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdChirpsPerUser();
		
		return res;
	}	
	
	public Double ratioUsersWithOver75PercentAvgChirps(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioUsersWithOver75PercentAvgChirps();
		
		return res;
	}
	
	public Double ratioPublicOverPrivateNewspapers(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioPublicOverPrivateNewspapers();
		
		return res;
	}
	
	public Double getAvgArticlesPerPrivateNewspaper(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgArticlesPerPrivateNewspaper();
		
		return res;
	}
	
	public Double getAvgArticlesPerPublicNewspaper(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgArticlesPerPublicNewspaper();
		
		return res;
	}
	
	public Double getAvgRatioPrivateOverPublicNewspaperPerUser(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgRatioPrivateOverPublicNewspaperPerUser();
		
		return res;
	}
	
	public Collection<Object[]> ratioOfSubscribersPerNewspaper(){
		Authority auth;
		UserAccount userAccount;
		Collection<Object[]> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioOfSubscribersPerNewspaper();
		
		return res;
		
	}
	
	public Double getAvgFollowupsPerArticlePlus1Week(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgFollowupsPerArticlePlus1Week();
		
		return res;
	}
	
	public Double getAvgFollowupsPerArticlePlus2Weeks(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgFollowupsPerArticlePlus2Weeks();
		
		return res;
	}
	
	
	public String roundTo(Double n, Integer decimals){
		DecimalFormat format;
		String result;
		if(n==null){
			result = "No hay datos suficientes";
		}else{
		
			format = new DecimalFormat();
			format.setMaximumFractionDigits(decimals);
			result = format.format(n);
		}
		
		return result;
	}
	public String[] roundTo(Double[] ns, Integer decimals){
		String[] result;
		
		
		result = new String[ns.length];
		
		for(int i = 0; i < result.length; i ++){
			result[i] = roundTo(ns[i], decimals);
		}
		
		return result;
	}
	
	
	public Double ratioOfNewspaperWithAdvertisementsOverOthers(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioOfNewspaperWithAdvertisementsOverOthers();
		
		return res;
	}
	
	public Double ratioOfTabooAdvertisements(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioOfTabooAdvertisements();
		
		return res;
	}
	
	public Double avgNewspaperPerVolume(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.avgNewspaperPerVolume();
		
		return res;
	}
	
	public Double ratioVolumeSubscriptionsVersusNewspaperSubscription(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioVolumeSubscriptionsVersusNewspaperSubscription();
		
		return res;
	}

}
