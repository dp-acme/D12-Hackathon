package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.VolumeRepository;
import domain.Actor;
import domain.Newspaper;
import domain.User;
import domain.Volume;

@Service
@Transactional
public class VolumeService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private VolumeRepository volumeRepository;

	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private UserService userService;

	
	@Autowired
	private NewspaperService newspaperService;
	
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Constructor ---------------------------------------------------

	public VolumeService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Volume create(User user) {
		Volume result;
		
		result = new Volume();
		result.setCreator(user);
		result.setNewspapers(new ArrayList<Newspaper>());
		
		return result;
	}

	public Volume findOne(int volumeId) {
		Volume result;
		
		result = volumeRepository.findOne(volumeId);

		return result;
	}

	public Collection<Volume> findAll() {
		Collection<Volume> result;

		result = this.volumeRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}

	public Volume save(Volume volume) {
		Actor principal;
		Volume result;
		
		principal = actorService.findPrincipal();
		
		Assert.isTrue(principal instanceof User && principal.equals(volume.getCreator())); //Se est� logueado como el creador del volumen
		Assert.isTrue(volume.getId() != 0 || volume.getNewspapers().size() == 0); //Se debe crear sin newspapers asignados

		if(volume.getNewspapers().size() != 0){ //Comprobar que todos los newspapers son del creador
			for(Newspaper n : volume.getNewspapers()){
				Assert.isTrue(volume.getCreator().equals(n.getCreator()));
			}
		}
		
		result = volumeRepository.save(volume);
		
		return result;
	}

	public void delete(Volume volume) {
		Actor principal;

		principal = actorService.findPrincipal();

		Assert.isTrue(principal instanceof User && principal.equals(volume.getCreator())); //Se est� logueado como el creador del volumen
		
		volumeRepository.delete(volume);
	}

	public Collection<Volume> getVolumesByUserId(Integer userId) {
		Collection<Volume> result;
		
		result = volumeRepository.getVolumesByUserId(userId);
		Assert.notNull(result);
		
		return result;
	}

	// Other methods----------------------------------------------------------

	public Volume reconstruct(Volume volume, Integer userId, BindingResult binding) {
		Volume result;
		
		result = volume;
		
		if(volume.getId() == 0){ //Caso de creaci�n
			User user;
			user = userService.findOne(userId);
			Assert.notNull(user);
			
			result.setCreator((User) user);
			result.setNewspapers(new ArrayList<Newspaper>());	
		}
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public void addNewspaperToVolume(Volume volume, Newspaper newspaper){
		Actor principal;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof User && principal.equals(volume.getCreator())); //Se est� logueado como el creador del volumen
		Assert.isTrue(!volume.getNewspapers().contains(newspaper)); //No se admiten repetidos
		
		volume.getNewspapers().add(newspaper);
		
		this.save(volume);
	}

	public void deleteNewspaperFromVolume(Volume volume, Newspaper newspaper) {
		Actor principal;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof User && principal.equals(volume.getCreator())); //Se est� logueado como el creador del volumen
		Assert.isTrue(volume.getNewspapers().contains(newspaper)); //Debe estar contenido para borrarlo
		
		volume.getNewspapers().remove(newspaper);
		
		this.save(volume);		
	}

	public void refreshNewspaperVolumes(int id) {
		Newspaper np;
		Collection<Volume> volumes;
		
		np = newspaperService.findOne(id);
		volumes = volumeRepository.findVolumesByNewspaperId(id);
		for(Volume v:(volumes)){
			v.getNewspapers().remove(np);
		}
		volumeRepository.save(volumes);
	}

	public void flush() {
		volumeRepository.flush();
	}
}
