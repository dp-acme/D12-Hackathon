package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.AgentRepository;
import security.Authority;
import domain.Advertisement;
import domain.Agent;
import domain.Folder;
import forms.RegisterCustomer;

@org.springframework.stereotype.Service
@Transactional
public class AgentService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private AgentRepository agentRepository;
		
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	// Constructor ---------------------------------------------------
		
	public AgentService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------
	
	public Agent create() {
		Agent result;
		
		result = new Agent();
		result.setMyAdvertisements(new ArrayList<Advertisement>());
		result.setFolders(new ArrayList<Folder> ());
		
		return result;
	}
	
	public Agent findOne(int agentId) {
		Assert.isTrue(agentId != 0);
		
		Agent result;
		
		result = agentRepository.findOne(agentId);
		
		return result;
	}
	
	public Collection<Agent> findAll() {		
		Collection<Agent> result;
		
		result = agentRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Agent save(Agent agent) {
		Assert.notNull(agent);
		Agent result;
		
		if (agent.getId() == 0){
			String role;
			Md5PasswordEncoder encoder;

			if(SecurityContextHolder.getContext().getAuthentication()!=null
				&&SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray().length>0){
				role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0].toString();
				Assert.isTrue(role.equals("ROLE_ANONYMOUS"));
			}
			encoder = new Md5PasswordEncoder();
			agent.getUserAccount().setPassword(encoder.encodePassword(agent.getUserAccount().getPassword(),null));
		}
		
		result = agentRepository.save(agent);
		
		if (agent.getId() == 0) {
			result = (Agent)actorService.addSystemFolders(result); //Cuando se persiste por primera vez se a�aden los folders del sistema
		}
		
		result = agentRepository.save(result);

		return result;
	}
	
	// Other business methods -------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	public Agent reconstruct(RegisterCustomer formObject, BindingResult binding) {
		Agent result;
		Authority authority;
		
		result = create();
		authority = new Authority();
		result.setAddress(formObject.getAddress());
		result.setEmail(formObject.getEmail());
		result.setName(formObject.getName());
		result.setPhone(formObject.getPhone());
		result.setSurname(formObject.getSurname());
		authority.setAuthority(Authority.AGENT);
		formObject.getUserAccount().addAuthority(authority);
		result.setUserAccount(formObject.getUserAccount());
		
		validator.validate(formObject, binding);
		
		return result;
	}
	
	public void flush() {
		agentRepository.flush();
	}
}
