package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.MessageRepository;
import domain.Actor;
import domain.Folder;
import domain.FolderType;
import domain.Message;

@Service
@Transactional
public class MessageService {

	// Managed repository --------------------------------------

	@Autowired
	private MessageRepository		messageRepository;

	// Supporting services -------------------------------------

	@Autowired
	private FolderService			folderService;

	@Autowired
	private ConfigurationService	configurationService;
	
	@Autowired
	private ActorService			actorService;

	// Constructors --------------------------------------------

	public MessageService() {
		super();
	}

	// Simple CRUD methods -------------------------------------

	public Message create(final Actor sender) {
		Message result;

		result = new Message();

		result.setMoment(new Date(System.currentTimeMillis() - 1));
		result.setRecipients(new ArrayList<Actor>());
		result.setSender(sender);
		result.setIsSpam(false);

		return result;
	}

	public Collection<Message> findAll() {
		Collection<Message> result;

		result = this.messageRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Message findOne(final int messageId) {
		Message result;

		result = this.messageRepository.findOne(messageId);

		return result;
	}

	public void delete(Message message, Actor actor) {
		this.delete(message, actor, false);
	}

	public void delete(Message message, Actor actor, boolean deletingFolder) {
		Assert.notNull(message);
		Assert.isTrue(this.messageRepository.exists(message.getId()));

		Folder trashBox;
		Folder folder;

		folder = this.folderService.getFolderByActorAndMessage(actor.getId(), message.getId());
		Assert.notNull(folder);

		if (!folder.getType().getFolderType().equals(FolderType.TRASHBOX)) {
			trashBox = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.TRASHBOX);

			this.changeFolder(message, trashBox, actor, deletingFolder);

		} else {
			if (!deletingFolder) {
				folder.getMessages().remove(message);
				this.folderService.save(folder);
			}

			if (this.messageRepository.getMessageReferences(message.getId()) == 0)
				this.messageRepository.delete(message);
		}
	}

	// Other business methods -----------------------------------

	@Autowired
	private Validator validator;
	
	public Message reconstruct(Message message, BindingResult binding, boolean isNotification) {
		Actor sender;
		Message newMessage;
		
		sender = actorService.findPrincipal();
		Assert.notNull(sender);
		
		newMessage = create(sender);
		Assert.notNull(newMessage);
		
		message.setSender(newMessage.getSender());
		message.setMoment(newMessage.getMoment());
		
		if (isNotification) {
			Collection<Actor> recipients;
			
			recipients = actorService.findAll();
			recipients.remove(actorService.findPrincipal());
			
			message.setRecipients(recipients);
		}
		
		validator.validate(message, binding);
		
		return message;
	}
	
	public Message sendMessage(Message message, boolean isNotification) {
		Assert.notNull(message);

		Assert.notNull(message.getSender());
		Assert.notEmpty(message.getRecipients());
		Assert.hasLength(message.getSubject());
		Assert.hasLength(message.getBody());
		Assert.notNull(message.getPriority());
		if (!isNotification)
			Assert.isTrue(!message.getRecipients().contains(message.getSender()));

		Message result;
		Folder folder;
		String folderType;

		message.setIsSpam(this.configurationService.hasTabooWords(message.getSubject() + message.getBody())); //Comprobación de spam

		result = this.messageRepository.save(message);

		if (!isNotification) {
			folder = this.folderService.getFolderByActorIdAndType(result.getSender().getId(), FolderType.OUTBOX);
			folder.getMessages().add(result);
			this.folderService.save(folder);
		}

		folderType = result.getIsSpam() ? FolderType.SPAMBOX : (isNotification ? FolderType.NOTIFICATIONBOX : FolderType.INBOX);

		for (final Actor recipient : result.getRecipients()) {
			folder = this.folderService.getFolderByActorIdAndType(recipient.getId(), folderType);
			folder.getMessages().add(result);
			this.folderService.save(folder);
		}
		
		return result;
	}

	public void changeFolder(Message message, Folder folder, Actor actor) {
		this.changeFolder(message, folder, actor, false);
	}

	public void changeFolder(Message message, Folder folder, Actor actor, boolean deletingFolderOld) {
		Assert.notNull(message);
		Assert.notNull(folder);
		Assert.notNull(actor);
		Assert.isTrue(this.messageRepository.exists(message.getId()));
		Assert.notNull(this.folderService.findOne(folder.getId()));

		Folder folderOld;

		Assert.isTrue(message.getSender().equals(actor) || message.getRecipients().contains(actor));
		Assert.isTrue(actor.getFolders().contains(folder));

		if (!deletingFolderOld) {
			folderOld = this.folderService.getFolderByActorAndMessage(actor.getId(), message.getId());

			folderOld.getMessages().remove(message);
			this.folderService.save(folderOld);
		}

		folder.getMessages().add(message);
		this.folderService.save(folder);
	}

	public Collection<Message> getMessagesBySenderId(int senderId) {
		Collection<Message> result;

		result = this.messageRepository.getMessagesBySenderId(senderId);

		return result;
	}

	public void flush() {
		messageRepository.flush();
	}
}
