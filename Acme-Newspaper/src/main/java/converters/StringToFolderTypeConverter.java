package converters;

import java.net.URLDecoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.FolderType;

@Component
@Transactional
public class StringToFolderTypeConverter implements Converter<String, FolderType>{

	@Override
	public FolderType convert(String source) {
		FolderType result;
		String parts[];
		
		if(source==null)
			result = null;
		else{
			try{
				parts = source.split("\\|");
				result = new FolderType();
				result.setFolderType(URLDecoder.decode(parts[0], "UTF-8"));
			}catch(Throwable oops){
				throw new IllegalArgumentException(oops);
				}
		}
		return result;
	}

}
