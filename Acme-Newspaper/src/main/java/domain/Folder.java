package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Folder extends DomainEntity {

	// Constructor

	public Folder() {
		super();
	}

	// Attributes

	private String		name;
	private FolderType	type;

	@NotBlank
	public String getName() {
		return this.name;
	}
	public void setName(final String name) {
		this.name = name;
	}

	@NotNull
	@Valid
	public FolderType getType() {
		return this.type;
	}
	public void setType(final FolderType type) {
		this.type = type;
	}

	// Relationships

	private Actor				actor;
	private Collection<Message>	messages;
	private Folder				parentFolder;

	@Valid
	@ManyToOne(optional = true)
	public Actor getActor() {
		return this.actor;
	}
	public void setActor(final Actor actor) {
		this.actor = actor;
	}

	@NotNull
	@Valid
	@ManyToMany
	public Collection<Message> getMessages() {
		return this.messages;
	}
	public void setMessages(final Collection<Message> messages) {
		this.messages = messages;
	}

	@Valid
	@ManyToOne(optional = true)
	public Folder getParentFolder() {
		return this.parentFolder;
	}
	public void setParentFolder(final Folder parentFolder) {
		this.parentFolder = parentFolder;
	}
}
