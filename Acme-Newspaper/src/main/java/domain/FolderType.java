package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Embeddable
@Access(AccessType.PROPERTY)
public class FolderType {

	// Constructors -----------------------------------------------------------

	public FolderType() {
		super();
	}

	// Values -----------------------------------------------------------------

	public static final String	INBOX = "INBOX";
	public static final String	OUTBOX = "OUTBOX";
	public static final String	NOTIFICATIONBOX = "NOTIFICATIONBOX";
	public static final String	TRASHBOX = "TRASHBOX";
	public static final String	SPAMBOX = "SPAMBOX";
	public static final String	USERBOX = "USERBOX";

	// Attributes -------------------------------------------------------------

	private String				folderType;

	@NotBlank
	@Pattern(regexp = "^" + FolderType.INBOX + "|" + FolderType.OUTBOX + "|" + FolderType.NOTIFICATIONBOX + "|" + FolderType.TRASHBOX + "|" + FolderType.SPAMBOX + "|" + FolderType.USERBOX + "$")
	public String getFolderType() {
		return this.folderType;
	}
	public void setFolderType(final String folderType) {
		this.folderType = folderType;
	}
}