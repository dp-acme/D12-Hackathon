package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor {

	// Constructor
	
	public User() {
		super();
	}

	// Attributes
	
	// Relationships
	
	private Collection<User> myFollowings;
	private Collection<Newspaper> myNewspapers;
	
	@Valid
	@NotNull
	@ManyToMany
	public Collection<User> getMyFollowings() {
		return myFollowings;
	}
	public void setMyFollowings(Collection<User> myFollowings) {
		this.myFollowings = myFollowings;
	}
	
	@Valid
	@NotNull
	@OneToMany(mappedBy = "creator")
	public Collection<Newspaper> getMyNewspapers() {
		return myNewspapers;
	}
	public void setMyNewspapers(Collection<Newspaper> myNewspapers) {
		this.myNewspapers = myNewspapers;
	}
	
}
