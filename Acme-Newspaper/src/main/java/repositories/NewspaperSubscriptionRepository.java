package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.NewspaperSubscription;

@Repository
public interface NewspaperSubscriptionRepository extends JpaRepository<NewspaperSubscription, Integer>{

	@Query("select s from NewspaperSubscription s where s.customer.id = ?1 AND s.newspaper.id = ?2")
	NewspaperSubscription getSubscriptionFromCustomerAndNewspaper(int customerId, int newspaperId);
	
	@Query("select s from NewspaperSubscription s where s.customer.id = ?1")
	Collection<NewspaperSubscription> getSubscriptionsFromCustomer(int customerId);
	
	@Query("select s from NewspaperSubscription s where s.newspaper.id = ?1")
	Collection<NewspaperSubscription> getSubscriptionsFromNewspaper(int newspaperId);
	
}