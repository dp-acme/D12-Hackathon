package repositories;


import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Administrator;
import domain.Newspaper;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer>{
	
	/*The average and the standard deviation of newspapers created per user.*/
	@Query("select avg(u.myNewspapers.size),sqrt(sum(u.myNewspapers.size*u.myNewspapers.size)/count(u)-avg(u.myNewspapers.size)*avg(u.myNewspapers.size)) from User u")
	Double[] getAvgSdNewspapersPerUser();

	/*The average and the standard deviation of articles created per user.*/
	@Query("select avg(coalesce(cast((select count(a) from Article a where a.newspaper.creator=u)as float),0)),sqrt(sum(coalesce(cast((select count(a) from Article a where a.newspaper.creator=u)as float),0)*coalesce(cast((select count(a) from Article a where a.newspaper.creator=u)as float),0))/count(u)-avg(coalesce(cast((select count(a) from Article a where a.newspaper.creator=u)as float),0))*avg(coalesce(cast((select count(a) from Article a where a.newspaper.creator=u)as float),0))) from User u")
	Double[] getAvgSdArticlesPerUser();
	
	/*The average and the standard deviation of articles per newspaper.*/
	@Query("select avg(coalesce(cast((select count(a) from Article a where a.newspaper=n)as float),0)),sqrt(sum(coalesce(cast((select count(a) from Article a where a.newspaper=n)as float),0)*coalesce(cast((select count(a) from Article a where a.newspaper=n)as float),0))/count(n)-avg(coalesce(cast((select count(a) from Article a where a.newspaper=n)as float),0))*avg(coalesce(cast((select count(a) from Article a where a.newspaper=n)as float),0))) from Newspaper n")
	Double[] getAvgSdArticlesPerNewspaper();
	
	/*The newspapers that have at least 10% more articles than the average.*/
	@Query("select n from Newspaper n where (select count(a) from Article a where a.newspaper=n)>1.1*cast((select avg(coalesce(cast((select count(a) from Article a where a.newspaper=np)as float),0)) from Newspaper np)as float)")
	Collection<Newspaper> getNewspaperWith10PercentMoreArticlesThanAvg();
	
	/*The newspapers that have at least 10% more articles than the average.*/
	@Query("select n from Newspaper n where (select count(a) from Article a where a.newspaper=n)<1.1*cast((select avg(coalesce(cast((select count(a) from Article a where a.newspaper=np)as float),0)) from Newspaper np)as float)")
	Collection<Newspaper> getNewspaperWith10PercentLessArticlesThanAvg();
	
	/* The ratio of users who have ever created a newspaper.*/
	@Query("select cast((select count(us) from User us where us.myNewspapers.size>0)as float)/count(u) from User u")
	Double ratioOfCreators();
	
	/*The ratio of users who have ever written an article.*/
	@Query("select coalesce(count(distinct a.newspaper.creator),0)/cast((select count(ar) from Article ar)as float) from Article a")
	Double ratioOfArticleCreators();

	
	/*The average number of follow-ups per article*/
	@Query("select avg(coalesce(cast((select count(ac) from Article ac where ac.article=a)as float),0)) from Article a where a.article = null")
	Double getAvgFollowUpsPerArticle();
	
	
	/*The average number of follow-ups per article up to one week after the corresponding newspaper's been published.*/
	@Query("select avg(coalesce(cast((select count(a) from Article a where a.article=ac AND (DATEDIFF(a.moment, a.article.moment)<7 OR (DATEDIFF(a.moment, a.article.moment)=7 AND (hour(a.moment)*60*60+minute(a.moment)*60+second(a.moment)<=hour(a.article.moment)*60*60+minute(a.article.moment)*60+second(a.article.moment)))))as float),0)) from Article ac where ac.article = null")
	Double getAvgFollowupsPerArticlePlus1Week();
	
	/*The average number of follow-ups per article up to two weeks after the corresponding newspaper's been published.*/	
	@Query("select avg(coalesce(cast((select count(a) from Article a where a.article=ac AND (DATEDIFF(a.moment, a.article.moment)<14 OR (DATEDIFF(a.moment, a.article.moment)=7 AND (hour(a.moment)*60*60+minute(a.moment)*60+second(a.moment)<=hour(a.article.moment)*60*60+minute(a.article.moment)*60+second(a.article.moment)))))as float),0)) from Article ac where ac.article = null")
	Double getAvgFollowupsPerArticlePlus2Weeks();
	
	/*The average and the standard deviation of the number of chirps per user.*/
	@Query("select avg(coalesce(cast((select count(c) from Chirp c where c.user=u)as float),0)), sqrt(sum(coalesce(cast((select count(c) from Chirp c where c.user=u)as float),0)*coalesce(cast((select count(c) from Chirp c where c.user=u)as float),0))/count(u)-avg(coalesce(cast((select count(c) from Chirp c where c.user=u)as float),0))*avg(coalesce(cast((select count(c) from Chirp c where c.user=u)as float),0))) from User u")
	Double[] getAvgSdChirpsPerUser();
	
	/*The ratio of users who have posted above 75% the average number of chirps per user.*/
	@Query("select count(u)/cast((select count(us) from User us)as float) from User u where (select count(c) from Chirp c where c.user=u)>0.75*cast((select avg(coalesce(cast((select count(c) from Chirp c where c.user=usr)as float),0)) from User usr)as float)")
	Double ratioUsersWithOver75PercentAvgChirps();
	
	/*The ratio of public versus private newspapers.*/
	@Query("select count(n)/cast((select count(np) from Newspaper np where np.isPrivate=true)as float) from Newspaper n where n.isPrivate=false")
	Double ratioPublicOverPrivateNewspapers();
	
	/*The average number of articles per private newspapers. .*/
	@Query("select avg(cast((select count(a) from Article a where a.newspaper=n)as float)) from Newspaper n where n.isPrivate = true")
	Double getAvgArticlesPerPrivateNewspaper();
	
	/*The average number of articles per private newspapers.*/
	@Query("select avg(cast((select count(a) from Article a where a.newspaper=n)as float)) from Newspaper n where n.isPrivate = false")
	Double getAvgArticlesPerPublicNewspaper();
	
	/*The ratio of subscribers per private newspaper versus the total number of customers.*/
	@Query("select n, (select count(s) from NewspaperSubscription s where s.newspaper=n)/cast((select count(c) from Customer c)as float) from Newspaper n where n.isPrivate=true AND n.draft=false")
	Collection<Object[]> ratioOfSubscribersPerNewspaper();
	
	/*The average ratio of private versus public newspapers per publisher.*/
	@Query("select avg(cast((select count(n)/cast((select count(np) from Newspaper np where np.isPrivate=false AND np.creator=u)as float) from Newspaper n where n.isPrivate=true AND n.creator=u)as float)) from User u")
	Double getAvgRatioPrivateOverPublicNewspaperPerUser();
	
	/*The ratio of newspapers that have at least one advertisement versus the newspapers that haven't any.*/
	@Query("select cast((count(distinct a.newspaper))as float)/((select count(n) from Newspaper n)-count(distinct a.newspaper)) from Advertisement a")
	Double ratioOfNewspaperWithAdvertisementsOverOthers();
	
	/*The ratio of advertisements that have taboo words. */
	@Query("select cast((count(a))as float)/(select count(ad) from Advertisement ad) from Advertisement a where a.containsTabooWord=true")
	Double ratioOfTabooAdvertisements();
	
	/*The average number of newspapers per volume.*/
	@Query("select avg(v.newspapers.size) from Volume v")
	Double avgNewspaperPerVolume();
	
	/*The ratio of subscriptions to volumes versus subscriptions to newspapers.*/
	@Query("select cast((count(vs))as float)/(select count(ns) from NewspaperSubscription ns)  from VolumeSubscription vs")
	Double ratioVolumeSubscriptionsVersusNewspaperSubscription();

}