package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Advertisement;
import domain.Newspaper;

@Repository
public interface NewspaperRepository extends JpaRepository<Newspaper, Integer>{
	
	@Query("select n from Newspaper n where n.draft=false and n.isPrivate=false")
	Collection<Newspaper> getFinalPublicNewspapers();
	
	@Query("select n from Newspaper n where n.draft=false and n.isPrivate=true")
	Collection<Newspaper> getFinalPrivateNewspapers();
	
	@Query("select s.newspaper from NewspaperSubscription s where s.customer.id = ?1")
	Collection<Newspaper> getNewspapersSubscribed(int customerId);
	
	@Query("select n from Newspaper n where n.draft = false and n.isPrivate = false and (n.title like ?1 or n.description like ?1)")	
	Collection<Newspaper> searchNewspaperbyKeyword(String keyword);

	@Query("select n from Newspaper n where n.containsTabooWord = true and n.draft = false")	
	Collection<Newspaper> newspapersContainingTabooWords();

	@Query("select n from Newspaper n, Volume v where v.id = ?1 AND n in elements(v.newspapers) and n.draft = false")
	Collection<Newspaper> getPublishedNewspapersByVolumeId(Integer volumeId);

	@Query("select n from Newspaper n, Volume v where v.id = ?1 AND n not in elements(v.newspapers) and n.draft = false and n.creator.id = ?2")
	Collection<Newspaper> getNewspapersOutsideVolume(Integer volumeId, Integer userId);	
	
	@Query("select a from Advertisement a where a.newspaper.id = ?1")
	Collection<Advertisement> getAdvertisements(int id);
}