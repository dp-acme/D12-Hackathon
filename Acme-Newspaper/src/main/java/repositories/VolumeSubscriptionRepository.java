package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.VolumeSubscription;

@Repository
public interface VolumeSubscriptionRepository extends JpaRepository<VolumeSubscription, Integer>{

	@Query("select s from VolumeSubscription s where s.customer.id = ?2 and s.volume.id = ?1")
	VolumeSubscription getSuscriptionByVolumeAndCustomer(Integer volumeId, Integer customerId);
	
	@Query("select s from VolumeSubscription s where s.customer.id = ?2 and ?1 in elements(s.volume.newspapers)")
	Collection<VolumeSubscription> getVolumeSubscriptionsForNewspaperId(Integer newspaperId, Integer customerId);

	@Query("select s from VolumeSubscription s where s.customer.id = ?1")	
	Collection<VolumeSubscription> getByCustomerId(Integer customerId);
	
}