package controllers.volumeSubscription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdvertisementService;
import services.VolumeSubscriptionService;
import controllers.AbstractController;
import domain.Actor;
import domain.Customer;
import domain.VolumeSubscription;

@Controller
@RequestMapping("/volumeSubscription/customer")
public class VolumeSubscriptionCustomerController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private VolumeSubscriptionService volumeSubscriptionService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private AdvertisementService advertisementService;
	
	// Constructor -----------------------------------------------------------

	public VolumeSubscriptionCustomerController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/list")
	public ModelAndView list(@RequestParam(value = "customerId", required = true) Integer customerId) {
		ModelAndView result;
		Actor principal;
		
		principal = actorService.findPrincipal();
		
		Assert.isTrue(principal instanceof Customer && principal.getId() == customerId); //Comprobar credenciales
		
		result = new ModelAndView("volumeSubscription/customer/list");
		result.addObject("subscriptions", volumeSubscriptionService.getByCustomerId(customerId));

		return result;
	}
	
	@RequestMapping(value = "/create")
	public ModelAndView create(@RequestParam(value = "volumeId", required = true) Integer volumeId) {
		ModelAndView result;
		
		Assert.isTrue(actorService.findPrincipal() instanceof Customer); //Comprobar credenciales
		
		result = new ModelAndView("volumeSubscription/customer/edit");
		result.addObject("volumeSubscription", volumeSubscriptionService.create(null, null));

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ModelAndView save(@RequestParam(value = "volumeId", required = true) Integer volumeId,
							 VolumeSubscription volumeSubscription, BindingResult binding) {
		ModelAndView result;
		
		volumeSubscription = volumeSubscriptionService.reconstruct(volumeSubscription, volumeId, binding);
		
		if(!advertisementService.checkCreditCard(volumeSubscription.getCreditCard().getExpirationMonth(), volumeSubscription.getCreditCard().getExpirationYear())){
			result = new ModelAndView("volumeSubscription/customer/edit");
			result.addObject("volumesubscription", volumeSubscription);
			result.addObject("creditCardMessage", "volumesubscription.commit.error.creditCardDate");
			
		}else if(binding.hasErrors()){
			result = new ModelAndView("volumeSubscription/customer/edit");
			result.addObject("volumesubscription", volumeSubscription);
			
		} else{
			try{
				volumeSubscriptionService.save(volumeSubscription);
				
				result = new ModelAndView("redirect:/volume/display.do?volumeId=" + volumeId);
		
			} catch (Throwable oops) {
				result = new ModelAndView("volumeSubscription/customer/edit");
				result.addObject("volumesubscription", volumeSubscription);
				result.addObject("message", "volumesubscription.commit.error");
			}
		}
				
		return result;
	}
}
