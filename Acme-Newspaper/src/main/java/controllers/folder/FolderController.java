/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.folder;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.FolderService;
import controllers.AbstractController;
import domain.Actor;
import domain.Folder;
import domain.FolderType;
import domain.Message;

@Controller
@RequestMapping("/folder")
public class FolderController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private FolderService	folderService;

	@Autowired
	private ActorService	actorService;

	// Constructors -----------------------------------------------------------

	public FolderController() {
		super();
	}

	// Listing -----------------------------------------------------------
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(required = false, value = "folderId") Integer folderId) {
		ModelAndView result;
		Folder folder;
		Collection<Folder> systemFolders;
		Folder parentFolder;
		Collection<Folder> subFolders;
		Collection<Message> messages;

		UserAccount principal;
		Actor actor;

		principal = LoginService.getPrincipal();
		Assert.notNull(principal);
		actor = this.actorService.findByUserAccountId(principal.getId());
		Assert.notNull(actor);
		
		final Folder inboxFolder = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.INBOX);
		systemFolders = new ArrayList<Folder>();

		systemFolders.add(inboxFolder);
		systemFolders.add(this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.OUTBOX));
		systemFolders.add(this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.NOTIFICATIONBOX));
		systemFolders.add(this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.SPAMBOX));
		systemFolders.add(this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.TRASHBOX));
		
		if (folderId == null)
			folder = inboxFolder;
		else
			folder = this.folderService.findOne(folderId);
		Assert.notNull(folder);
		Assert.isTrue(actor.equals(folder.getActor()));

		if (folder.getType().getFolderType().equals(FolderType.USERBOX))
			subFolders = this.folderService.getChildrenFolders(folder.getId());
		else
			subFolders = this.folderService.getUserParentFolders(actor.getId());

		parentFolder = folder.getParentFolder();
		messages = folder.getMessages();

		Assert.notNull(messages);

		result = new ModelAndView("folder/list");
		result.addObject("folder", folder);
		result.addObject("systemFolders", systemFolders);
		result.addObject("parentFolder", parentFolder);
		result.addObject("subFolders", subFolders);
		result.addObject("messages", messages);

		return result;
	}

	// Creation -----------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = false, defaultValue = "-1", value = "parentFolderId") int parentFolderId) {
		ModelAndView result;
		Folder folder;
		Actor actor;
		UserAccount principal;

		principal = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(principal.getId());

		folder = this.folderService.create(actor);

		if (parentFolderId != -1) {
			Folder parentFolder;

			parentFolder = this.folderService.findOne(parentFolderId);
			Assert.notNull(parentFolder);

			folder.setParentFolder(parentFolder);
		}

		result = this.createEditModelAndView(folder);

		return result;
	}

	// Edition -----------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int folderId) {
		ModelAndView result;
		Folder folder;

		folder = this.folderService.findOne(folderId);
		Assert.notNull(folder);

		result = this.createEditModelAndView(folder);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(Folder folder, BindingResult binding) {
		ModelAndView result;

		folder = folderService.reconstruct(folder, binding);
		
		if (binding.hasErrors())
			result = this.createEditModelAndView(folder);
		else
			try {
				Folder folderRes;
				folderRes = this.folderService.save(folder);
				result = new ModelAndView("redirect:list.do?folderId=" + folderRes.getId());
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(folder, "folder.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(Folder folder, BindingResult binding) {
		ModelAndView result;
		
		folder = folderService.findOne(folder.getId());

		try {
			this.folderService.delete(folder);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(folder, "folder.commit.error");
		}

		return result;
	}

	// Ancillary methods -------------------------------------------------

	protected ModelAndView createEditModelAndView(Folder folder) {
		ModelAndView result;

		result = this.createEditModelAndView(folder, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Folder folder, String messageCode) {
		ModelAndView result;
		Collection<Folder> parentFolders;
		Actor actor;
		UserAccount principal;

		principal = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(principal.getId());

		parentFolders = this.removeChildrenFolders(this.folderService.getUserFolders(actor.getId()), folder, actor);

		result = new ModelAndView("folder/edit");
		result.addObject("folder", folder);
		result.addObject("parentFolders", parentFolders);

		result.addObject("message", messageCode);

		return result;
	}

	// Auxiliary methods --------------------------------------------------

	private Collection<Folder> removeChildrenFolders(Collection<Folder> folders, Folder folder, Actor actor) {

		folders.remove(folder);
		for (final Folder childFolder : this.folderService.getChildrenFolders(folder.getId()))
			folders = this.removeChildrenFolders(folders, childFolder, actor);

		return folders;
	}

}
