package controllers.newspaperSubscription;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdvertisementService;
import services.NewspaperService;
import services.NewspaperSubscriptionService;
import controllers.AbstractController;
import domain.Actor;
import domain.Customer;
import domain.Newspaper;
import domain.NewspaperSubscription;

@Controller
@RequestMapping("/newspaperSubscription/customer")
public class NewspaperSubscriptionCustomerController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private NewspaperSubscriptionService newspaperSubscriptionService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private AdvertisementService advertisementService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	// Constructor -----------------------------------------------------------

	public NewspaperSubscriptionCustomerController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		Actor actor;
		Customer customer;
		Collection<NewspaperSubscription> newspaperSubscriptions;
		
		actor = actorService.findPrincipal();
		Assert.isInstanceOf(Customer.class, actor);
		customer = (Customer) actor;
		
		newspaperSubscriptions = newspaperSubscriptionService.getSubscriptionsFromCustomer(customer.getId());
		
		result = new ModelAndView("newspaperSubscription/list");
		result.addObject("newspaperSubscriptions", newspaperSubscriptions);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) int newspaperId){
		ModelAndView result;
		NewspaperSubscription newspaperSubscription;
		Newspaper newspaper;
		
		newspaper = newspaperService.findOne(newspaperId);
		Assert.notNull(newspaper);
		
		newspaperSubscription = newspaperSubscriptionService.create(newspaper);
		
		result = this.createEditModelAndView(newspaperSubscription);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@RequestParam(required = true, value = "newspaperId") int newspaperId , 
			@ModelAttribute(value="newspaperSubscription") NewspaperSubscription newspaperSubscription, BindingResult binding){
		ModelAndView result;
		
		newspaperSubscription = newspaperSubscriptionService.reconstruct(newspaperSubscription, binding, newspaperId);
		
		if (!advertisementService.checkCreditCard(newspaperSubscription.getCreditCard().getExpirationMonth(), newspaperSubscription.getCreditCard().getExpirationYear())){
			result = createEditModelAndView(newspaperSubscription, null, "newspapersubscription.commit.error.creditCardDate");
		} else if (binding.hasErrors()) {
			result = createEditModelAndView(newspaperSubscription, null);
		} else { 
			try {
				newspaperSubscription = newspaperSubscriptionService.save(newspaperSubscription);
				
				result = new ModelAndView("redirect:/newspaper/display.do?newspaperId=" + newspaperSubscription.getNewspaper().getId());
				
			} catch (Throwable oops) {
				result = createEditModelAndView(newspaperSubscription, "newspaperSubscription.commit.error");
			}
		}
		return result;
	}
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(NewspaperSubscription newspaperSubscription) {
		ModelAndView result;
		
		result = createEditModelAndView(newspaperSubscription, null);
		
		return result;
	}
	
	protected ModelAndView createEditModelAndView(NewspaperSubscription newspaperSubscription, String messageCode) {
		ModelAndView result;
		
		result = createEditModelAndView(newspaperSubscription, messageCode, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(NewspaperSubscription newspaperSubscription, String messageCode, String creditCardMessage) {
		ModelAndView result;
		
		result = new ModelAndView("newspaperSubscription/create");
		result.addObject("newspaperSubscription", newspaperSubscription);
		result.addObject("message", messageCode);
		result.addObject("creditCardMessage", creditCardMessage);
		
		return result;
	}

}
