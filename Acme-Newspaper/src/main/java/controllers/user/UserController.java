package controllers.user;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.UserService;
import domain.Actor;
import domain.Article;
import domain.User;
import forms.RegisterCustomer;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ActorService actorService;
	
//	@Autowired
//	private RendezvousService rendezvousService;
	
	// Constructor -----------------------------------------------------------

	public UserController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		RegisterCustomer formObject;
		
		formObject = new RegisterCustomer();
		result = this.createEditModelAndView(formObject);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("userEditForm") RegisterCustomer userEditForm, BindingResult binding){
		ModelAndView result;
		User user;
		
		if(!userEditForm.getTermsAccepted()){
			result = createEditModelAndView(userEditForm);
			result.addObject("showMessageTerms", true);
			return result;
		}
		
		user = userService.reconstruct(userEditForm, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(userEditForm);
			
		} else { 
			try {
				userService.save(user);
				result = new ModelAndView("redirect:/");
				
			} catch (Throwable oops) {
				result = createEditModelAndView(userEditForm, "user.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<User> users;

		users = userService.findAll();	
			
		result = new ModelAndView("user/list");
		result.addObject("users", users);
		result.addObject("requestURI", "user/list.do");

		return result;
	}
	
	@RequestMapping(value = "/listFollowers", method = RequestMethod.GET)
	public ModelAndView listFollowers() {
		ModelAndView result;
		Collection<User> users;
		Actor actor;
		User user;
		
		UserAccount principal;
		principal = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(principal.getId());

		Assert.isInstanceOf(User.class, actor);
		user = (User)actor ;
		
		users = userService.getFollowers(user);	
			
		result = new ModelAndView("user/list");
		result.addObject("users", users);
		result.addObject("requestURI", "user/list.do");

		return result;
	}
	
	@RequestMapping(value = "/listFollowed", method = RequestMethod.GET)
	public ModelAndView listFollowed() {
		ModelAndView result;
		Collection<User> users;
		Actor actor;
		User user;
		
		UserAccount principal;
		principal = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(principal.getId());

		Assert.isInstanceOf(User.class, actor);
		user = (User)actor ;
		
		users = user.getMyFollowings();	
			
		result = new ModelAndView("user/list");
		result.addObject("users", users);
		result.addObject("requestURI", "user/list.do");

		return result;
	}
	
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required=false) Integer userId) {
		ModelAndView result;
		Boolean isFollowed = false;
		Boolean show = false;
		Actor actor;
		User user;
		User principalUser;
		Collection<Article> articles;
		UserAccount principal;
		
			
		
		if(userId!=null){
			user = userService.findOne(userId);
			if(LoginService.isAuthenticated()){
				principal = LoginService.getPrincipal();
				actor = actorService.findByUserAccountId(principal.getId());
				if(actor instanceof User && !user.equals((User)actor)){	
					principalUser = (User)actor;
					show = true;
					if(principalUser.getMyFollowings().contains(user)){
						isFollowed = true;
					}
				}
			}
		}else{
			principal = LoginService.getPrincipal();
			actor = actorService.findByUserAccountId(principal.getId());
			Assert.isInstanceOf(User.class, actor);
			user = (User)actor;
			userId = user.getId();
		}
		
		articles = userService.getArticles(userId);
		
		result = new ModelAndView("user/display");
		
		result.addObject("isFollowed", isFollowed);
		result.addObject("show", show);
		result.addObject("user", user);
		result.addObject("articles", articles);
		result.addObject("requestURI", "user/display.do");

		return result;
	}
	
	@RequestMapping("/follow")
	public ModelAndView follow(@RequestParam(required = true) Integer userId) {
		User user;
		User principalUser;
		UserAccount principal;
		Actor actor;
		ModelAndView result;
		
		user = userService.findOne(userId);
		Assert.notNull(user);
		
		principal = LoginService.getPrincipal();
		actor = actorService.findByUserAccountId(principal.getId());
		Assert.isInstanceOf(User.class, actor);
		principalUser = (User)actor;
		
		Assert.isTrue(!principalUser.getMyFollowings().contains(user));
		principalUser.getMyFollowings().add(user);
		result = new ModelAndView("redirect:/user/display.do?userId=" + user.getId());
		
		try{
			userService.save(principalUser);
			
		} catch (Throwable oops) {
			result.addObject("message", "user.commit.error");
		}

		return result;
	}
	
	@RequestMapping("/unfollow")
	public ModelAndView unfollow(@RequestParam(required = true) Integer userId) {
		User user;
		User principalUser;
		UserAccount principal;
		Actor actor;
		ModelAndView result;
		
		user = userService.findOne(userId);
		Assert.notNull(user);
		
		principal = LoginService.getPrincipal();
		actor = actorService.findByUserAccountId(principal.getId());
		Assert.isInstanceOf(User.class, actor);
		principalUser = (User)actor;
		
		Assert.isTrue(principalUser.getMyFollowings().contains(user));
		principalUser.getMyFollowings().remove(user);
		result = new ModelAndView("redirect:/user/display.do?userId=" + user.getId());
		
		try{
			userService.save(principalUser);
			
		} catch (Throwable oops) {
			result.addObject("message", "user.commit.error");
		}

		return result;
	}
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(RegisterCustomer userEditForm) {
		ModelAndView result; 
		
		result = createEditModelAndView(userEditForm, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(RegisterCustomer userEditForm, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/user/create");
		result.addObject("userEditForm", userEditForm);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "user/create.do");
		
		return result;
	}

}
