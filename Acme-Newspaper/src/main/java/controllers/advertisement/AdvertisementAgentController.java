package controllers.advertisement;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdvertisementService;
import services.NewspaperService;
import controllers.AbstractController;
import domain.Actor;
import domain.Advertisement;
import domain.Agent;
import domain.Newspaper;

@Controller
@RequestMapping("/advertisement/agent")
public class AdvertisementAgentController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private AdvertisementService advertisementService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	// Constructor -----------------------------------------------------------

	public AdvertisementAgentController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/listAdvertised", method=RequestMethod.GET)
	public ModelAndView listNewspaperAdvertised(){
		ModelAndView result;
		Set<Newspaper> newspapers = new HashSet<Newspaper>();
		Actor principal;
		
		principal = actorService.findPrincipal();
		Assert.isInstanceOf(Agent.class, principal);

		newspapers.addAll(advertisementService.getAdvertisedNewspapers(principal.getId()));

		result = new ModelAndView("advertisement/agent/listAdvertised");
		result.addObject("newspaper", new ArrayList<Newspaper>(newspapers));
		
		return result;
	}
	
	@RequestMapping(value = "/listNotAdvertised", method=RequestMethod.GET)
	public ModelAndView listNewspaperNotAdvertised(){
		ModelAndView result;
		Set<Newspaper> newspapers = new HashSet<Newspaper>();
		Actor principal;
		
		principal = actorService.findPrincipal();
		Assert.isInstanceOf(Agent.class, principal);
		
		newspapers.addAll(advertisementService.getNotAdvertisedNewspapers(principal.getId()));

		result = new ModelAndView("advertisement/agent/listNotAdvertised");
		result.addObject("newspaper", new ArrayList<Newspaper>(newspapers));
		
		return result;
	}
	
	@RequestMapping(value = "/edit")
	public ModelAndView create(@RequestParam(value = "newspaperId", required = true) int newspaperId) {
		ModelAndView result;
		Newspaper newspaper;
		
		Assert.isTrue(actorService.findPrincipal() instanceof Agent); //Comprobar credenciale
		newspaper = newspaperService.findOne(newspaperId);
		Assert.notNull(newspaper);
		Assert.isTrue(!newspaper.isDraft());
		
		result = new ModelAndView("advertisement/agent/edit");
		result.addObject("advertisement", advertisementService.create(actorService.findPrincipal().getId(), newspaperId));

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ModelAndView save(@RequestParam(value = "newspaperId", required = true) Integer newspaperId,
							 Advertisement advertisement, BindingResult binding) {
		ModelAndView result;
		Newspaper newspaper;
		
		Assert.isTrue(actorService.findPrincipal() instanceof Agent); //Comprobar credenciale
		newspaper = newspaperService.findOne(newspaperId);
		Assert.notNull(newspaper);
		Assert.isTrue(!newspaper.isDraft());
		
		if(!advertisementService.checkCreditCard(advertisement.getCreditCard().getExpirationMonth(), advertisement.getCreditCard().getExpirationYear())){
			result = new ModelAndView("advertisement/agent/edit");
			result.addObject("advertisement",advertisement);
			result.addObject("showMessageCreditCard", true);
			return result;
		}
		
		advertisement = advertisementService.reconstruct(advertisement, newspaperId, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("advertisement/agent/edit");
			result.addObject("advertisement", advertisement);
			
		} else{
			try{
				advertisementService.save(advertisement);
				
				result = new ModelAndView("redirect:/newspaper/display.do?newspaperId=" + newspaperId);
		
			} catch (Throwable oops) {
				result = new ModelAndView("advertisement/agent/edit");
				result.addObject("advertisement", advertisement);
				result.addObject("message", "advertisement.commit.error");
			}
		}
				
		return result;
	}
}
