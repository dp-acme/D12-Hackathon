/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.rendezvous;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.RendezvousService;
import controllers.AbstractController;
import domain.Rendezvous;

@Controller
@RequestMapping("/rendezvous/admin")
public class RendezvousAdminController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	private RendezvousService	rendezvousService;


	// Constructors -----------------------------------------------------------

	public RendezvousAdminController() {
		super();
	}


	//Delete--------------------------------------------------------
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam int rendezvousId) {
		ModelAndView result;
		
		Rendezvous rendezvous = rendezvousService.findOne(rendezvousId);
		
		rendezvousService.delete(rendezvous);
		
		result = new ModelAndView("redirect:/rendezvous/list.do");

		return result;
	}
}
