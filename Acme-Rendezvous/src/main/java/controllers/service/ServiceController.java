package controllers.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.RendezvousService;
import services.ServiceService;
import controllers.AbstractController;
import domain.Actor;
import domain.Rendezvous;
import domain.Service;
import domain.User;

@Controller
@RequestMapping("/service")
public class ServiceController extends AbstractController {

	// Services------------------------------------------------------------

	@Autowired
	private ServiceService serviceService;

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RendezvousService rendezvousService;

	// Constructor--------------------------------------------------------------------

	public ServiceController() {
		super();
	}

	// Display ----------------------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam int serviceId) {
		ModelAndView result;
		Service service;
		boolean canRequest;
		
		canRequest = false;

		service = this.serviceService.findOne(serviceId);
		Assert.notNull(service);
		Assert.isTrue(!service.isCancelled());
		
		Actor actor;
		actor = actorService.findPrincipal();
		
		if (actor instanceof User) {
			User user;
			Collection<Rendezvous> rendezvouses;
			
			user = (User) actor;
			
			rendezvouses = rendezvousService.getMyRendezvousNoDraftNoDeletedNoService(user, service);
			
			canRequest = rendezvouses.size() > 0;
		}
		
		result = new ModelAndView("service/display");
		result.addObject("service", service);
		result.addObject("canRequest", canRequest);
		
		return result;
	}

	// List
	// -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		// Creamos el objeto a devolver
		ModelAndView result;
		// Creamos una coleccion de services para almacenar todos
		Collection<Service> allServices;

		allServices = this.serviceService.findAll();

		result = new ModelAndView("service/list");
		// Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("services", allServices);
		result.addObject("requestURI", "service/list.do");

		return result;
	}

	// Cancelar
	@RequestMapping(value = "/cancel", method = RequestMethod.GET)
	public ModelAndView cancel(@RequestParam int serviceId) {
		ModelAndView result;
		Service service;

		service = this.serviceService.findOne(serviceId);

		this.serviceService.cancel(service);

		result = new ModelAndView("redirect:list.do");

		return result;
	}

}
