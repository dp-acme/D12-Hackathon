/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CommentService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Actor;
import domain.Comment;

@Controller
@RequestMapping("/comment/user")
public class CommentUserController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	RendezvousService rendezvousService;
	
	@Autowired
	CommentService commentService;
	
	@Autowired
	ActorService actorService;
	
	// Constructors -----------------------------------------------------------

	public CommentUserController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	@RequestMapping(method = RequestMethod.POST, value = "/edit", params = "save")
	public ModelAndView save(@RequestParam(value = "rendezvousId", required = true) Integer rendezvousId,
							 @ModelAttribute("newComment") Comment comment, BindingResult binding) {
		ModelAndView result;
		Actor principal;

		comment = commentService.reconstruct(comment, rendezvousId, null, binding);
				
		result = new ModelAndView("rendezvous/display");
		
		principal = actorService.findPrincipal();
		
		if(binding.hasErrors()){			
			result.addObject("newComment", comment);	

		} else{
			try{
				commentService.save(comment);	
								
				result.addObject("newComment", commentService.create(null, null, null));	

			} catch (Throwable oops) {
				result.addObject("newComment", comment);	
				result.addObject("message", "rendezvous.commit.error");	
			}
		}
		
		
		result.addObject("rendezvous", comment.getRendezvous());
		result.addObject("comments", commentService.getParentComments(comment.getRendezvous().getId()));
		result.addObject("newReply", commentService.create(null, null, null));	
		result.addObject("joined", comment.getRendezvous().getUsers().contains(principal) || comment.getRendezvous().getCreator().equals(principal));
		
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/edit", params = "reply")
	public ModelAndView reply(@RequestParam(value = "rendezvousId", required = true) Integer rendezvousId,
							  @RequestParam(value = "parentId", required = true) Integer parentId,
							  @ModelAttribute("newReply") Comment comment, BindingResult binding) {
		ModelAndView result;
		Actor principal;
		principal = actorService.findPrincipal();
		
		comment = commentService.reconstruct(comment, rendezvousId, parentId, binding);
				
		result = new ModelAndView("rendezvous/display");
		
		if(binding.hasErrors()){			
			result.addObject("newReply", comment);	
		} else{
			try{				
				commentService.save(comment);	
								
				result.addObject("newReply", commentService.create(null, null, null));	

			} catch (Throwable oops) {				
				result.addObject("newReply", comment);	
				result.addObject("message", "rendezvous.commit.error");	
			}
		}
	
		result.addObject("rendezvous", comment.getRendezvous());
		result.addObject("comments", commentService.getParentComments(comment.getRendezvous().getId()));
		result.addObject("newComment", commentService.create(null, null, null));	
		result.addObject("joined", comment.getRendezvous().getUsers().contains(principal) || comment.getRendezvous().getCreator().equals(principal));		
		
		return result;
	}
}
