/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ConfigurationService;
import domain.Configuration;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public WelcomeController() {
		super();
	}
	
	// Services
	
	@Autowired
	private ConfigurationService configurationService;

	// Index ------------------------------------------------------------------		
	
	@RequestMapping(value = "/index")
	public ModelAndView index(@RequestParam(required = false, defaultValue = "John Doe") final String name) {
		ModelAndView result;
		Configuration config;
		String[] welcomeMessage;

		config = configurationService.findConfiguration();

		welcomeMessage = new String[2];
		welcomeMessage[0] = config.getWelcomeMessageEnglish();
		welcomeMessage[1] = config.getWelcomeMessageSpanish();
		
		result = new ModelAndView("welcome/index");
		result.addObject("name", name);
		result.addObject("moment", new Date());
		result.addObject("welcomeMessage", welcomeMessage);

		return result;
	}
	
	@RequestMapping(value = "/aboutUs")
	public ModelAndView aboutUs() {
		ModelAndView result;

		result = new ModelAndView("welcome/aboutUs");

		return result;
	}
	
	@RequestMapping(value = "faq")
	public ModelAndView faq() {
		ModelAndView result;

		result = new ModelAndView("welcome/faq");

		return result;
	}
	
	@RequestMapping(value = "termsAndConditions")
	public ModelAndView termsAndConditions() {
		ModelAndView result;

		result = new ModelAndView("welcome/termsAndConditions");

		return result;
	}
}
