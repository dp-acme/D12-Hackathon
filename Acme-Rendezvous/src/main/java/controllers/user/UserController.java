package controllers.user;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.Authority;
import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.RendezvousService;
import services.UserService;
import controllers.AbstractController;
import domain.Actor;
import domain.Rendezvous;
import domain.User;
import forms.TermsAndConditionsFormObject;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
	// Constructor -----------------------------------------------------------

	public UserController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		TermsAndConditionsFormObject formObject;
		
		formObject = new TermsAndConditionsFormObject();
		result = this.createEditModelAndView(formObject);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("userEditForm") TermsAndConditionsFormObject userEditForm, BindingResult binding){
		ModelAndView result;
		User user;
		
		if(!userEditForm.getTermsAccepted()){
			result = createEditModelAndView(userEditForm);
			result.addObject("showMessageTerms", true);
			return result;
		}
		
		user = userService.reconstruct(userEditForm, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(userEditForm);
			
		} else { 
			try {
				userService.save(user);
				result = new ModelAndView("redirect:/");
				
			} catch (Throwable oops) {
				result = createEditModelAndView(userEditForm, "user.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<User> users;

		users = userService.findAll();	
			
		result = new ModelAndView("user/list");
		result.addObject("users", users);
		result.addObject("requestURI", "user/list.do");

		return result;
	}
	
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required=false) Integer userId) {
		ModelAndView result;
		User user;
		Collection<Rendezvous> rsvps;
		
		if (userId == null) {
			UserAccount principal;
			
			principal = LoginService.getPrincipal();
			
			user = (User) actorService.findByUserAccountId(principal.getId());
		} else
			user = userService.findOne(userId);
			
		if (SecurityContextHolder.getContext().getAuthentication() != null && 
			SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
			!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
			UserAccount principal;
			Authority adminAuth;
			
			adminAuth = new Authority();
			adminAuth.setAuthority(Authority.ADMIN);
			
			principal = LoginService.getPrincipal();
			
			if (principal.getAuthorities().contains(adminAuth)) {
				rsvps = rendezvousService.getRSVPSWithoutDraft(user);
			} else {
				Actor principalActor;
				
				principalActor = actorService.findByUserAccountId(principal.getId());
				
				if (principalActor instanceof User && user.equals(principalActor)) {
					rsvps = rendezvousService.getRSVPSAndMyRendezvous(user);
				} else if (actorService.isAdult(principalActor)) {
					rsvps = rendezvousService.getRSVPSWithoutDeleted(user);
				} else {
					rsvps = rendezvousService.getRSVPSWithoutDeletedOrAdultsOnly(user);
				}
			}
			
		} else {
			rsvps = rendezvousService.getRSVPSWithoutDeletedOrAdultsOnly(user);
		}
		
		result = new ModelAndView("user/display");
		
		result.addObject("user", user);
		result.addObject("rsvp", rsvps);
		result.addObject("requestURI", "user/display.do");

		return result;
	}
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(TermsAndConditionsFormObject userEditForm) {
		ModelAndView result; 
		
		result = createEditModelAndView(userEditForm, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(TermsAndConditionsFormObject userEditForm, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/user/create");
		result.addObject("userEditForm", userEditForm);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "user/create.do");
		
		return result;
	}

}
