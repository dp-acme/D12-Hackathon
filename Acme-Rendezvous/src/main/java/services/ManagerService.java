package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Manager;
import domain.Service;
import forms.TermsAndConditionsManagerFormObject;

@org.springframework.stereotype.Service
@Transactional
public class ManagerService {
	// Managed repository ---------------------------------------------------
		@Autowired
		private ManagerRepository managerRepository;
		
	// Supporting services ---------------------------------------------------
		
	// Constructor ---------------------------------------------------
		
		public ManagerService() {
			super();
		}
	
		// Simple CRUD methods ---------------------------------------------------
		public Manager create() {
			
			Manager result;
			
			result = new Manager();
			result.setServices(new ArrayList<Service>());
			
			return result;
		}
		
		public Manager findOne(int managerId) {
			Assert.isTrue(managerId != 0);
			
			Manager result;
			
			result = managerRepository.findOne(managerId);
			
			return result;
		}
		
		public Collection<Manager> findAll() {		
			Collection<Manager> result;
			
			result = managerRepository.findAll();
			Assert.notNull(result);
			
			return result;
		}
		
		public Manager save(Manager manager) {
			Assert.notNull(manager);
			Manager result;
			UserAccount userAccount;
			
			if(manager.getId()!=0){
				userAccount = LoginService.getPrincipal();
				Assert.isTrue(manager.getUserAccount().equals(userAccount));
			}else{
				String role;
				Md5PasswordEncoder encoder;

				if(SecurityContextHolder.getContext().getAuthentication()!=null
			&&SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray().length>0){
					role = SecurityContextHolder.getContext()
						.getAuthentication().getAuthorities().toArray()[0].toString();
				Assert.isTrue(role.equals("ROLE_ANONYMOUS"));
				}
				encoder = new Md5PasswordEncoder();
				manager.getUserAccount().setPassword(encoder.encodePassword(manager.getUserAccount().getPassword(),null));
			}
			
			result = managerRepository.save(manager);

			return result;
		}
		
		// Other business methods -------------------------------------------------
		
		@Autowired
		private Validator validator;
		
		public Manager reconstruct(TermsAndConditionsManagerFormObject formObject, BindingResult binding) {
			Manager result;
			Authority authority;
			
			result = create();
			authority = new Authority();
			result.setAddress(formObject.getAddress());
			result.setBirthday(formObject.getBirthday());
			result.setEmail(formObject.getEmail());
			result.setName(formObject.getName());
			result.setPhone(formObject.getPhone());
			result.setSurname(formObject.getSurname());
			result.setVat(formObject.getVat());
			authority.setAuthority(Authority.MANAGER);
			formObject.getUserAccount().addAuthority(authority);
			result.setUserAccount(formObject.getUserAccount());
			
			validator.validate(formObject, binding);
			
			return result;
		}
		
		public void flush() {
			managerRepository.flush();
		}
}
