package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.User;

@Component
@Transactional
public class UserToStringConverter implements Converter<User, String> {

	@Override
	public String convert(User source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}

}
