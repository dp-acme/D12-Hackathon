package converters;

import java.net.URLDecoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import security.Authority;

@Component
@Transactional
public class StringToAuthorityConverter implements Converter<String, Authority>{

	@Override
	public Authority convert(String source) {
		Authority result;
		String parts[];
		
		if(source==null)
			result = null;
		else{
			try{
				parts = source.split("\\|");
				result = new Authority();
				result.setAuthority(URLDecoder.decode(parts[0], "UTF-8"));
			}catch(Throwable oops){
				throw new IllegalArgumentException(oops);
				}
		}
		return result;
	}

}
