package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="draft,deleted,adultsOnly,creator_id")})
public class Rendezvous extends DomainEntity{

	// Constructor

	public Rendezvous(){
		super();
	}
	
	// Attributes
	
	private String name;
	private String description;
	private Date moment;
	private String picture;
	private Double latitude;
	private Double longitude;
	private boolean adultsOnly;
	private boolean draft;
	private boolean deleted;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return moment;
	}
	public void setMoment(Date moment) {
		this.moment = moment;
	}
	
	@URL
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public boolean isAdultsOnly() {
		return adultsOnly;
	}
	public void setAdultsOnly(boolean adultsOnly) {
		this.adultsOnly = adultsOnly;
	}
	
	public boolean isDraft() {
		return draft;
	}
	public void setDraft(boolean draft) {
		this.draft = draft;
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	// Relationships
	
	private Collection<Question> questions;
	private Collection<Announcement> announcements;
	private Collection<User> users;
	private User creator;
	private Collection<Rendezvous> links;

	@Valid
	@NotNull
	@OneToMany(mappedBy = "rendezvous")
	public Collection<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(Collection<Question> questions2) {
		this.questions = questions2;
	}
	
	@Valid
	@NotNull
	@OneToMany(mappedBy = "rendezvous")
	public Collection<Announcement> getAnnouncements() {
		return announcements;
	}
	public void setAnnouncements(Collection<Announcement> announcements) {
		this.announcements = announcements;
	}
	
	@Valid
	@NotNull
	@ManyToMany(mappedBy = "myRSVPs")
	public Collection<User> getUsers() {
		return users;
	}
	public void setUsers(Collection<User> users) {
		this.users = users;
	}
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	
	@Valid
	@NotNull
	@ManyToMany
	public Collection<Rendezvous> getLinks() {
		return links;
	}
	public void setLinks(Collection<Rendezvous> links) {
		this.links = links;
	}
	
}
