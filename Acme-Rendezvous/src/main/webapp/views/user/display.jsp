<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<p>
	<b>
		<spring:message code="user.list.name"/>:
	</b>
	<jstl:out value="${user.getName()}" />
</p>

<p>
	<b>
		<spring:message code="user.list.surname"/>:
	</b>
	<jstl:out value="${user.getSurname()}" />
</p>

<p>
	<b>
		<spring:message code="user.list.email"/>:
	</b>
	<jstl:out value="${user.getEmail()}" />
</p>

<p>
	<b>
		<spring:message code="user.list.birthday"/>:
	</b>
	<spring:message var="formatDatePattern" code="user.list.formatDatePattern" />
	<fmt:formatDate value="${user.getBirthday()}" pattern="${formatDatePattern}" />
</p>

<p>
	<jstl:if test="${not empty user.getAddress()}">
		<b>
			<spring:message code="user.list.address" />:
		</b>
		<jstl:out value="${user.getAddress()}" />
	</jstl:if>
</p>

<p>
	<jstl:if test="${not empty user.getPhone()}">
		<b>
			<spring:message code="user.list.phone" />:
		</b>
		<jstl:out value="${user.getPhone()}" />
	</jstl:if>
</p>

<jsp:useBean id="currentDate" class="java.util.Date" />

<display:table name="rsvp" id="row" requestURI="${requestUri}" pagesize="5">
	
	<jstl:if test="${(row.deleted == false) && !(row.moment<currentDate)}">
		<jstl:set value="background-color: #BDECB6" var="style"></jstl:set>
	</jstl:if>

	<jstl:if test="${row.moment < currentDate}">
		<jstl:set value="background-color: #A18BC1" var="style"></jstl:set>
	</jstl:if>
	
	<jstl:if test="${row.deleted == true}">
		<jstl:set value="background-color: #D36E70" var="style"></jstl:set>
	</jstl:if>
	
	<display:column property="name" titleKey="rendezvous.list.name" style="${style}" />

	<display:column property="description" titleKey="rendezvous.list.description" style="${style}" />

	<display:column titleKey="rendezvous.list.users" style="${style}">
		<jstl:forEach var="i" items="${row.users}">
			 	<a href="user/display.do?userId=${i.id}"><jstl:out value="${i.name}"/></a>
		</jstl:forEach>
		<jstl:if test="${empty row.users}">
			<spring:message var="noUser" code="rendezvous.list.messageToEmptyUsers" />
			<jstl:out value="${noUser}"/>
	 	</jstl:if>
	</display:column>

	<display:column titleKey="rendezvous.list.creator" style="${style}">
		<a href="user/display.do?userId=${row.creator.id}"><jstl:out value="${row.creator.name}"/></a>
	</display:column>

	<spring:url var="urlDisplayRendezvous" value="rendezvous/display.do?rendezvousId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false" style="${style}">
		<jstl:if test="${row.deleted == false}">
			<a href="${urlDisplayRendezvous}">
				<spring:message code="rendezvous.list.display" />
			</a>
		</jstl:if>
	</display:column>
	
</display:table>
