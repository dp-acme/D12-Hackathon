<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.GrantedAuthority"%>
<%@page import="security.Authority"%>

<%@page import="forms.CategoryEditionForm"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="domain.Category"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.springframework.web.util.HtmlUtils" %> 
<%@page import="java.util.Date" %>
<%@page import="java.text.SimpleDateFormat" %>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:set value="${categories}" var="c" />
<jstl:set value="${categoryEditionForm}" var="editionForm" />

<!-- FORMULARIO DE CREACI�N -->

<security:authorize access="hasRole('ADMIN')"> 
	<fieldset style="display: inline; border-radius: 8px;"> 
		<br>
	
		<form:form modelAttribute="newCategory" action="category/admin/save.do"> 
			<form:hidden path="id"/>
			<form:hidden path="version"/>
			
			<acme:textbox path="name" code="category.create.name" required="true"/>
			<acme:textarea path="description" code="category.create.description"  required="true"/>
		
			<br>
	
			<acme:submit name="create" code="category.create.createButton"/>
		</form:form>
	</fieldset> 

	<br><br>

</security:authorize>

<!-- FORMULARIO DE CREACI�N CON PADRE -->

<div style="display: none;" id="parentCreation">
	<br>
	
	<p id = "creationFormIndentation" style="float: left;"></p>

	<fieldset style="display: inline; border-radius: 8px; margin-top: 25px; margin-bottom: 0px;"> 
		<br>
	
		<form:form modelAttribute="newCategoryWithParent" action="category/admin/save.do"> 
			<form:hidden path="id"/>
			<form:hidden path="version"/>
			
			<acme:textbox path="name" code="category.create.name" required="true"/>
			<acme:textarea path="description" code="category.create.description"  required="true"/>
		
			<br>
	
			<acme:submit name="createWithParent" code="category.create.createButton"/>
		</form:form>
	</fieldset> 
</div>


<!-- FORMULARIO DE EDICI�N -->

<div style="display: none;" id="editionForm">		
		<form:form modelAttribute="categoryEditionForm" action="category/admin/save.do"> 
			
			<form:input path="editedName" required="true"/>
			<form:errors path="editedName" cssClass="error"/>
			
			<hr>
			
			<form:textarea path="editedDescription" required="true"/>
			<form:errors path="editedDescription" cssClass="error"/>
			
			<br>
	
			<acme:submit name="edit" code="category.create.saveButton"/>
			
			<spring:message code="category.create.cancelButton" var="cancelButton"/>
			<input type="button" onclick="javascript: hideEditionForm();" value="${cancelButton}"/>
		</form:form>
</div>

<!-- MOSTRAR CATEGOR�AS -->

<jstl:if test="${deleteError != null}">
	<spring:message code="category.commit.error" var="errorMessage"/>

	<p class="error">
		<jstl:out value="${errorMessage}"/> 
	</p>
</jstl:if>

<spring:message code="category.create.addButton" var="addButton"/>
<spring:message code="category.create.deleteButton" var="deleteButton"/>
<spring:message code="category.create.editButton" var="editButton"/>
<spring:message code="category.create.moveHereButton" var="moveHereButton"/>
<spring:message code="category.create.moveButton" var="moveButton"/>

<spring:message code="category.create.deleteMessage" var="deleteMessage"/>

<% 
	@SuppressWarnings("unchecked")
	List<Category> categories = (ArrayList<Category>)pageContext.getAttribute("c");
	Map<Integer, Integer> indentationMap = new HashMap<Integer, Integer>();
	
	Map<String, String> internationalizedMessages = new HashMap<String, String>(); //A�adir mensajes internacionalizados al map
	internationalizedMessages.put("addButton", (String)pageContext.getAttribute("addButton"));
	internationalizedMessages.put("deleteButton", (String)pageContext.getAttribute("deleteButton"));
	internationalizedMessages.put("editButton", (String)pageContext.getAttribute("editButton"));
	internationalizedMessages.put("moveButton", (String)pageContext.getAttribute("moveButton"));
	internationalizedMessages.put("moveHereButton", (String)pageContext.getAttribute("moveHereButton"));
	
	internationalizedMessages.put("deleteMessage", (String)pageContext.getAttribute("deleteMessage"));
%>

<%! 
	String getMovementButtons(Collection<Category> categories){ //Funci�n auxiliar para obtener la cadena
		String res = "";
	
		for(Category c : categories){
			res += ", " + c.getId() + "";
			
			if(c.getChildren().size() > 0){
				res += getMovementButtons(c.getChildren());				
			}
		}
		
		return res;
	}
%>

<%
	String movementButtons = "['Root'" + getMovementButtons(categories) + "]";
%>

<script>
Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}

function hideCreationForm(){
	document.getElementById('parentCreation').style.display = 'none';	
}

function makeDataVisible(){
	var idList = <%= movementButtons %>;
	
	for(var i = 0; i < idList.length; i ++){
		if(idList[i] != 'Root'){
			document.getElementById('categoryData' + idList[i]).style.display = 'inline';			
		}
	}
}

function hideEditionForm(){
	makeDataVisible();
	
	document.getElementById('editionForm').style.display = 'none';
}

function showMovementButtons(exclusionList, callerId){
	hideCreationForm();
	hideEditionForm();
	
	var idList = <%= movementButtons %>;
	
	function indexClosure(index){
		return function(){
			relativeRedir('category/admin/move.do?categoryId=' + callerId + (idList[index] == 'Root'? "" : "&newParentId=" + idList[index]));
		};
	}
	
	for(var i = 0; i < idList.length; i ++){
		if(!exclusionList.contains(idList[i])){			
			document.getElementById('moveTo' + idList[i]).style.display = 'inline';	
			document.getElementById('moveTo' + idList[i]).onclick = indexClosure(i);
		
		} else{
			document.getElementById('moveTo' + idList[i]).style.display = 'none';						
		}
	}
}

function hideMovementButtons(){
	var idList = <%= movementButtons %>;
	var i;
	
	for(i = 0; i < idList.length; i ++){
		document.getElementById('moveTo' + idList[i]).style.display = 'none';			
	}
}

function creationFormIn(parentId, indentation){
	hideEditionForm();
	hideMovementButtons();
	
	document.getElementById('newCategoryWithParent').action = "category/admin/save.do?parentId=" + parentId;
	document.getElementById('creationFormIndentation').innerHTML = indentation;
	document.getElementById('category' + parentId).appendChild(document.getElementById('parentCreation'));
	document.getElementById('parentCreation').style.display = 'inline';
}

function editionFormIn(categoryId){
	makeDataVisible();
	hideMovementButtons();
	hideCreationForm();
		
	document.getElementById('categoryEditionForm').action = "category/admin/save.do?categoryId=" + categoryId;
	document.getElementById('categoryEdition' + categoryId).appendChild(document.getElementById('editionForm'));
	document.getElementById('categoryData' + categoryId).style.display = 'none';
		
	document.getElementById('editedName').value = document.getElementById('categoryName' + categoryId).innerHTML;
	document.getElementById('editedDescription').value = document.getElementById('categoryDescription' + categoryId).innerHTML;
	document.getElementById('editionForm').style.display = 'inline';
}

function confirmAndDelete(categoryId, message){
	if(confirm(message)){
		relativeRedir('category/admin/delete.do?categoryId=' + categoryId);	
	}
}
</script>

<%! 
	String safeHTML(String str){ //Funci�n para hacer HTML seguro
		return HtmlUtils.htmlEscape(str);
	}
%>

<%! 
	String formatDate(Date date, String format){ //Funci�n para formatear fechas
		SimpleDateFormat dt1 = new SimpleDateFormat(format);
		
		return dt1.format(date);
	}
%>

<%! 
	String indent(Integer tabs){ //Funci�n para indentar niveles
		String res = "";
		
		for(int i = 0; i < tabs; i ++){
			res += "&emsp;&emsp;&emsp;";
		}
		
		return res;
	}
%>

<%! 
	String printCategory(Category category, Integer indentation, Map<Integer, Integer> indentationMap, Map<String, String> internationalizedMessages, String auth){
		indentationMap.put(category.getId(), indentation);
	
		String res = "";
		
		res += "<div id = category" + category.getId() + ">";		
		
		res += "<p style = 'float: left;'>" + indent(indentation) + "</p>";
		
		res += "<fieldset style=\"display: inline; border-radius: 8px; margin-top: 5px; text-align: center; background-color: #f8ffa5;\">";
		
		res += "<div id = categoryEdition" + category.getId() + ">"; //Div para el formulario de edici�n
		res += "</div>";

		//Texto
		res += "<div id = categoryData" + category.getId() + ">"; //Div interno	con los datos

		res += "<span><a href='rendezvous/listRendezvousCategory.do?categoryId=" + category.getId() + "'><b id='categoryName" + category.getId() +  "'>" + safeHTML(category.getName()) + "</b></a></span>" + "<br>";
		res += "<hr style=\"margin-top: 2px;\">";
		res += "<span id='categoryDescription" + category.getId() +  "'>" + safeHTML(category.getDescription()) + "</span>";

		res += "</div>";
				
		//Botones
		if(auth.equals("ADMIN")){
			res += "<hr>";		

			res += "<input type='button' value='" + safeHTML(internationalizedMessages.get("addButton")) + "' style='margin-right: 10px;'" + 
					"onclick=\"javascript: creationFormIn(" + category.getId() + ", '" + indent(indentation) + "');\"" +
					"/>";

			res += "<input type='button' value='" + safeHTML(internationalizedMessages.get("editButton")) + "' style='margin-right: 10px;'" + 
					"onclick=\"javascript: editionFormIn(" + category.getId() + ");\"" +				
					"/>";
					
			res += "<input type='button' value='" + safeHTML(internationalizedMessages.get("moveButton")) + "'" + 
					"onclick=\"javascript: showMovementButtons([" + "" + category.getId() + ", " + 
							(category.getParent() == null? "'Root'" : category.getParent().getId()) + 
							getMovementButtons(category.getChildren()) +
							"], " + category.getId() + ");\"" +				
					"style='margin-right: 10px;'/>";
					
			res += "<input type='button' value='" + safeHTML(internationalizedMessages.get("deleteButton")) + "'" + 
					"onclick=\"javascript: confirmAndDelete(" + category.getId() + ", '" + safeHTML(internationalizedMessages.get("deleteMessage")) + "');\"" +				
					"/>";	
		}
		
		res += "</fieldset>";
		
		res += "<input id='moveTo" + category.getId() + "' type='button' value='" + safeHTML(internationalizedMessages.get("moveHereButton")) + "' style='margin-left: 10px; display: none;'/>";
		
		res += "</div>";
		
		res += "<br>";
		
		return res;
	}

	String printCategoryList(Collection<Category> categories, Integer indentation, Map<Integer, Integer> indentationMap, 
							 Map<String, String> internationalizedMessages, String auth){
		String res = "";
		
		for(Category c : categories){
			res += printCategory(c, indentation, indentationMap, internationalizedMessages, auth);
			
			if(c.getChildren().size() > 0){
				res += printCategoryList(c.getChildren(), indentation + 1, indentationMap, internationalizedMessages, auth);
			}
		}
		
		return res;
	}
%>

<% 
	@SuppressWarnings("unchecked")
	String auth = ((List<GrantedAuthority>)(SecurityContextHolder.getContext().getAuthentication().getAuthorities())).get(0).toString();

	out.print(printCategoryList(categories, 0, indentationMap, internationalizedMessages, auth)); 
	out.print("<input id='moveToRoot' type='button' value='" + safeHTML(internationalizedMessages.get("moveHereButton")) + "' style='margin-left: 10px; display: none;'/>");
%>

<% 
	if(request.getParameter("parentId") != null){ //Si el par�metro parentId est� presente se muestra el formulario de creaci�n con los errores
		Integer indentation = indentationMap.get(Integer.valueOf(request.getParameter("parentId")));
		
		out.print("<script> creationFormIn(" + Integer.valueOf(request.getParameter("parentId")) + ", '" + indent(indentation) + "');" + "</script>");
	}

	if(request.getParameter("categoryId") != null){ //Si el par�metro categoryId est� presente se muestra el formulario de edici�n con los errores y los datos introducidos
		out.print("<script> editionFormIn(" + Integer.valueOf(request.getParameter("categoryId")) + ");" + "</script>");
%>
		<script>
			document.getElementById('editedName').value = '<%= safeHTML(((CategoryEditionForm)pageContext.getAttribute("editionForm")).getEditedName()) %>';
			document.getElementById('editedDescription').value = '<%= safeHTML(((CategoryEditionForm)pageContext.getAttribute("editionForm")).getEditedDescription()) %>';
		</script>
<%
	}
%>