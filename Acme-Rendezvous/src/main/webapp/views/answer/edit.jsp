<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<spring:message code="answer.join" var="join"/>
<h2><jstl:out value="${join}"/><br/></h2>

<jstl:forEach items="${questions}" var="question">
    <b>- ${question.text}</b><br>
</jstl:forEach>

<form action="answer/user/edit.do" id="usrform" method="post">
	<input value="${rendezvous.id}" name="rendezvousId" hidden="true" />
	<spring:message code="answer.edit.placeholder" var="placeholder" />
	<br/><textarea name="answers" form="usrform" style="width: 500px; height: 200px" placeholder="${placeholder}"></textarea><br/>
	<spring:message code="answer.constraint" var="constraint"/>
	*<jstl:out value="${constraint}"/><br/>
	<br/><input type="submit" value="<spring:message code="answer.joinRvps"/>" name="save"/>
</form>

<jstl:if test="${error == true}">
	<br/>
	<spring:message code="answer.error" var="error" />	
	<b style = "color: red"><jstl:out value="${error}"/></b><br/>
</jstl:if>



