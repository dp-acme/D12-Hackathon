<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<a href="/Acme-Rendezvous/"> <img class="banner" src="${banner}"
		alt="Acme Rendezvous" />
	</a>
</div>

<div>
	<h1><jstl:out value="${businessName}" /></h1>
</div>


<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv" href="dashboard/display.do"><spring:message code="master.page.administrator.dashboard" /></a></li>	
			<li><a class="fNiv" href="configuration/display.do"><spring:message code="master.page.administrator.config" /></a></li>	
			
			<li><a class="fNiv"><spring:message	code="master.page.services" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="service/list.do"><spring:message code="master.page.administrator.listService" /></a></li>			
				</ul>
			</li>
			
		</security:authorize>
		
		<li><a class="fNiv"><spring:message	code="master.page.rendezvous" /></a>
			<ul>
				<li class="arrow"></li>
				<li><a href="rendezvous/list.do"><spring:message code="master.page.rendezvous.listOfRendezvouses" /></a></li>
				<security:authorize access="hasRole('USER')">
					<li><a href="rendezvous/user/list.do"><spring:message code="master.page.rendezvous.myRendezvouses" /></a></li>
					<li><a href="rendezvous/user/myRsvp.do"><spring:message code="master.page.rendezvous.myRsvps" /></a></li>
				</security:authorize>
				
			</ul>
		</li>
		
		<li><a class="fNiv" href="category/list.do"><spring:message code="master.page.categories" /></a></li>	
		
		<security:authorize access="hasRole('MANAGER')">
		
		<li><a class="fNiv"><spring:message	code="master.page.services" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="service/list.do"><spring:message code="master.page.administrator.listService" /></a></li>
					<li><a href="service/manager/list.do"><spring:message code="master.page.manager.myServices" /></a></li>
					<li><a href="request/list.do"><spring:message code="master.page.manager.myRequests" /></a></li>
				</ul>
			</li>
		
		</security:authorize>
		
		
		<li><a class="fNiv"><spring:message	code="master.page.users" /></a>
			<ul>
				<li class="arrow"></li>
				<li><a href="user/list.do"><spring:message code="master.page.users.listOfUsers" /></a></li>
			</ul>
		</li>
		
		<security:authorize access="hasRole('USER')">
	
			<li><a class="fNiv"><spring:message	code="master.page.services" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="service/list.do"><spring:message code="master.page.administrator.listService" /></a></li>			
				</ul>
			</li>
			
			
			<li><a class="fNiv"><spring:message	code="master.page.announcements" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="announcement/list.do"><spring:message code="master.page.announcements.myAnnouncements" /></a></li>			
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
			<li>
				<a class="fNiv"> 
					(<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasRole('USER')">
						<li><a href="user/display.do"><spring:message code="master.page.profile.user.profile" /></a></li>
					</security:authorize>					
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li><a class="fNiv"><spring:message	code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="user/create.do"><spring:message code="master.page.register.user" /></a></li>
					<li><a href="manager/create.do"><spring:message code="master.page.register.manager" /></a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

