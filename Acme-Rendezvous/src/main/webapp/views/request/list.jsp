<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<display:table name="requests" id="row" requestURI="request/list.do" pagesize="5">
	
	<jstl:if test="${row.getService().isCancelled()}">
		<jstl:set value="background-color: #D36E70" var="style"></jstl:set>
	</jstl:if>

	<jstl:if test="${!row.getService().isCancelled()}">
		<jstl:set value="background-color: #BDECB6" var="style"></jstl:set>
	</jstl:if>
	
	<display:column titleKey="request.list.service" sortable="true" style="${style}">
		<jstl:if test="${!row.getService().isCancelled()}">
			<a href="service/display.do?serviceId=${row.getService().getId()}"><jstl:out value="${row.getService().getName()}" /></a>
		</jstl:if>
		<jstl:if test="${row.getService().isCancelled()}">
			<jstl:out value="${row.getService().getName()}" />
		</jstl:if>
	</display:column>
	
	<display:column titleKey="request.list.rendezvous" sortable="true" style="${style}">
		<jstl:if test="${!row.getRendezvous().isDeleted() && (!row.getRendezvous().isAdultsOnly() || isAdult)}">
			<a href="rendezvous/display.do?rendezvousId=${row.getRendezvous().getId()}"><jstl:out value="${row.getRendezvous().getName()}" /></a>
		</jstl:if>
		<jstl:if test="${row.getRendezvous().isDeleted() || (!isAdult && row.getRendezvous().isAdultsOnly())}">
			<jstl:out value="${row.getRendezvous().getName()}" />
		</jstl:if>
	</display:column>
	
	<display:column property="comments" titleKey="request.list.comments" sortable="false" style="${style}" />
	
</display:table>
