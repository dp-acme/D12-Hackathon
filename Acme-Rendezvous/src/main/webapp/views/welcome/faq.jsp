<%--
 * faq.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><b><spring:message code="welcome.question1" /></b></p>

<p><spring:message code="welcome.answer1" /></p>

<p><b><spring:message code="welcome.question2" /></b></p>

<p><spring:message code="welcome.answer2" /></p>

<p><b><spring:message code="welcome.question3" /></b></p>

<p><spring:message code="welcome.answer3" /></p>

<p><b><spring:message code="welcome.question4" /></b></p>

<p><spring:message code="welcome.answer4" /></p>

<p><b><spring:message code="welcome.question5" /></b></p>

<p><spring:message code="welcome.answer5" /></p>