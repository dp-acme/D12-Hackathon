<%--
 * index.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<security:authorize access="isAuthenticated()">
	<security:authentication property="principal.username" var="principal" />
</security:authorize>

<security:authorize access="isAnonymous()">
	<jstl:set var="principal" value="Anonymous" />
</security:authorize>

<b>
	<spring:message code="welcome.languageIndex" var="lIndex" />
  	<jstl:out value="${welcomeMessage[lIndex]}" />
</b>

<p><spring:message code="welcome.greeting.prefix" /> ${principal} <spring:message code="welcome.greeting.suffix" /></p>

<spring:message var="formatDate" code="welcome.formatDate" />
<p><spring:message code="welcome.greeting.current.time" /> <fmt:formatDate value="${moment}" pattern="${formatDate}" /> </p> 
