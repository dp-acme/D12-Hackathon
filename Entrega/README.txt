Para la configuraci�n del protocolo HTTPS
se debe de seguir la gu�a descrita en nuestro entregable D08 - Lessons Learnt.



Las modificaciones necesarias para implementar dicho protocolo
se realizan sobre los archivos de configuraci�n del servidor, 

debido a esto en el proyecto no se refleja ning�n cambio con respecto a la versi�n sin HTTPS.


-----------------

Toda la documentaci�n adicional del Item 2 se encuentra en los sistemas de administraci�n de proyecto que hemos utilizado. Sus urls 
estan disponibles en el Responsability Statements.

-----------------

Los Items 4 y 5 van incluidos en la misma carpeta ya que los tests est�n contenidos dentro del proyecto. 

-----------------

Acme-Scrim presenta una URL denominada www.acme.com/manager (localhost/manager). 

Al ser esta URL id�ntica a la del manager de Tomcat se genera un conflicto entre ambas
por lo que la soluci�n propuesta ser�a cambiar el nombre a la URL del manager de Tomcat.

Para ello necesitamos cambiar el nombre de la carpeta que aloja los archivos del manager
de Tomcat, ubicado en C:\Program Files\Apache Software Foundation\Tomcat 7.0\webapps.

En primer lugar, paramos el servidor �Apache Tomcat 7.0 Tomcat7 Properties� que se
encuentra en ejecuci�n, para ello accediendo a la siguiente pesta�a desde el men� de
Windows XP y presionando Stop paramos el servidor.

Accedemos a la ruta donde se encuentra la carpeta de Tomcat manager y procedemos
a cambiarle el nombre por otro que no resulte en un conflicto con Acme Explorer, por ejemplo,
tomcatManager

Tras reiniciar el servidor podremos acceder al manager por la nueva url, /tomcatManager.

-----------------