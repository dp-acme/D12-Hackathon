package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.TagKeyRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.TagKey;
import domain.TagValue;

@Service
@Transactional
public class TagKeyService {

	// MANAGED REPOSITORY ------------------------------------
	
	@Autowired
	private TagKeyRepository tagKeyRepository;

	// SUPPORTING SERVICES -----------------------------------

	@Autowired
	private TagValueService tagValueService;
	
	// Constructor ---------------------------------------------------

	public TagKeyService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public TagKey create(){
		TagKey result;
		
		result = new TagKey();
		
		return result;
	}

	public TagKey findOne(int tagKeyId) {
		TagKey result;

		result = tagKeyRepository.findOne(tagKeyId);

		return result;
	}

	public Collection<TagKey> findAll() {
		Collection<TagKey> result;

		result = tagKeyRepository.findAll();
		
		Assert.notNull(result);

		return result;
	}

	public TagKey save(TagKey tagKey) {
		Assert.notNull(tagKey);
		
		UserAccount userAccount;
		Authority aut;
		TagKey result;
		Collection<TagKey> tags;

		userAccount = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);
		Assert.isTrue(userAccount.getAuthorities().contains(aut));

		if(tagKey.getId() == 0){ //No se pueden crear TagKeys con nombres iguales
			tags = this.findAll();
			
			for(TagKey t : tags){
				Assert.isTrue(!t.getName().equals(tagKey.getName()));
			}
		}
		
		result = tagKeyRepository.save(tagKey);

		return result;
	}

	public void delete(TagKey tagKey) {
		Assert.notNull(tagKey);
		
		Collection<TagValue> tagValues;
		UserAccount userAccount;
		Authority aut;

		userAccount = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);
		Assert.isTrue(userAccount.getAuthorities().contains(aut));
		
		Assert.isTrue(tagKeyRepository.exists(tagKey.getId()));
				
		tagValues = tagValueService.filterByTagKey(tagKey);
		
		for(TagValue tagValue : tagValues) {
			tagValueService.delete(tagValue);
		}

		tagKeyRepository.delete(tagKey);
	}
}
