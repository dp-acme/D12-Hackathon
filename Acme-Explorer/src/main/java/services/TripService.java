
package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.TripRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.ApplyFor;
import domain.Configuration;
import domain.Finder;
import domain.LegalText;
import domain.Manager;
import domain.Sponsorship;
import domain.Stage;
import domain.Status;
import domain.SurvivalClass;
import domain.TagValue;
import domain.Trip;

@Service
@Transactional
public class TripService {

	// Managed repository --------------------------------------

	@Autowired
	private TripRepository			tripRepository;

	// Supporting services -------------------------------------

	@Autowired
	private LegalTextService		legalTextService;

	@Autowired
	private ManagerService			managerService;

	@Autowired
	private ConfigurationService	configurationService;

	@Autowired
	private SurvivalClassService	survivalClassService;

	@Autowired
	private ApplyForService			applyForService;

	@Autowired
	private TagValueService			tagValueService;

	@Autowired
	private SponsorshipService		sponsorshipService;

	@Autowired
	private ActorService			actorService;


	// Constructors --------------------------------------------

	public TripService() {
		super();
	}

	// Simple CRUD methods -------------------------------------

	public void delete(final Trip trip) {
		Assert.notNull(trip);
		Manager manager;
		LegalText legalText;
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Collection<SurvivalClass> survivalClasses;

		Assert.isTrue(trip.getManager().getUserAccount().equals(userAccount));
		Assert.isTrue(trip.getPublicationDate().after(new Date()));
		Assert.isTrue(this.tripRepository.exists(trip.getId()));

		if (!trip.getTags().isEmpty())
			this.tagValueService.deleteInBatch(trip.getTags());

		manager = trip.getManager();
		legalText = trip.getLegalText();

		manager.getTrips().remove(trip);

		this.managerService.save(manager);

		survivalClasses = this.survivalClassService.getSurvivalClassesbyTripId(trip.getId());

		if (!survivalClasses.isEmpty())
			this.survivalClassService.deleteInBatch(survivalClasses, trip.getManager());

		this.legalTextService.save(legalText);

		this.tripRepository.delete(trip);
	}

	public Trip create() {
		UserAccount userAccount;
		Actor actor;

		Trip result;

		userAccount = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(userAccount.getId());
		Assert.isInstanceOf(Manager.class, actor);

		result = new Trip();
		result.setManager((Manager) actor);
		result.setStages(new ArrayList<Stage>());
		result.setTags(new ArrayList<TagValue>());

		return result;
	}

	public Trip save(final Trip trip) {
		Assert.notNull(trip);

		LegalText legalText;
		Trip result;
		UserAccount userAccount;
		Configuration configuration;

		configuration = this.configurationService.findConfiguration();
		userAccount = LoginService.getPrincipal();
		legalText = trip.getLegalText();

		Assert.isTrue(trip.getEndDate().after(trip.getStartDate()) || trip.getEndDate().equals(trip.getStartDate()));
		Assert.isTrue(trip.getStartDate().after(trip.getPublicationDate()));
		Assert.isTrue(trip.getPublicationDate().after(new Date()));
		Assert.isTrue(trip.getManager().getUserAccount().equals(userAccount));
		Assert.isNull(trip.getCancellationReason());
		Assert.isTrue(legalText.isFinalMode());

		trip.setPrice(0);

		for (final Stage s : trip.getStages())
			trip.setPrice(trip.getPrice() + s.getPrice());

		trip.setPrice(trip.getPrice() * (configuration.getVat() + 1));

		if (trip.getId() == 0) {
			result = this.tripRepository.save(trip); // Primer save para obtener un
			// id

			result.setTicker(TripService.generateTicker(result.getId())); // Generar ticker
			// con id
			// generado por
			// el primer
			// save

			result = this.tripRepository.save(result); // Actualizar objeto con
														// ticker

		} else
			result = this.tripRepository.save(trip);

		return result;
	}

	public Trip findOne(final int tripId) {
		Assert.isTrue(tripId != 0);
		Trip result;

		result = this.tripRepository.findOne(tripId);

		return result;
	}

	public Collection<Trip> findAll() {
		Collection<Trip> result;

		result = this.tripRepository.findAll();

		return result;
	}

	// Other business methods -----------------------------------

	// Solo se tienen en cuenta las referencias del viaje (No se tienen en
	// cuenta restricciones)

	public Trip cancel(final int tripId, final String cancellationReason) {
		Assert.isTrue(this.tripRepository.exists(tripId));
		Assert.notNull(cancellationReason);
		Assert.isTrue(cancellationReason != "");

		Trip result;
		UserAccount userAccount;
		Date currentDate;
		Status status;
		Collection<ApplyFor> applications;

		userAccount = LoginService.getPrincipal();
		result = this.findOne(tripId);

		Assert.isTrue(result.getCancellationReason() == null);
		Assert.isTrue(result.getManager().getUserAccount().equals(userAccount));

		status = new Status();
		status.setStatus(Status.CANCELLED);
		currentDate = new Date();

		Assert.isTrue(result.getPublicationDate().before(currentDate) && result.getStartDate().after(currentDate));

		result.setCancellationReason(cancellationReason);

		applications = this.applyForService.getApplicationsByTrip(result.getId());

		for (final ApplyFor a : applications) {
			a.setStatus(status);
			this.applyForService.save(a);
		}

		result = this.tripRepository.save(result);

		return result;
	}

	public void eraseTagValueFromTrip(final Trip trip, final TagValue tagValue) {
		trip.getTags().remove(tagValue);

		this.tripRepository.save(trip);
	}

	public Collection<Trip> findByManager() {
		Collection<Trip> result;
		UserAccount userAccount;
		Actor actor;

		userAccount = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(userAccount.getId());
		Assert.isInstanceOf(Manager.class, actor);

		result = this.tripRepository.findByManager(actor.getId());

		return result;
	}

	public Collection<Trip> filterTripsByCriteriaManager(String keyWord, Date dateStart, Date dateEnd, Double minPrice, Double maxPrice) {

		UserAccount userAccount;
		Actor actor;

		userAccount = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(userAccount.getId());
		Assert.isInstanceOf(Manager.class, actor);

		Collection<Trip> result;

		if (keyWord == null)
			keyWord = "";
		if (dateStart == null)
			dateStart = new Date(0);
		if (dateEnd == null)
			dateEnd = new Date(Long.MAX_VALUE);
		if (minPrice == null)
			minPrice = Double.MIN_VALUE;
		if (maxPrice == null)
			maxPrice = Double.MAX_VALUE;

		result = this.tripRepository.filterTripsByCriteriaManager('%' + keyWord + '%', dateStart, dateEnd, minPrice, maxPrice, new Date(), actor.getId());

		Assert.notNull(result);

		// for(Trip t:result){
		// if(t.getPublicationDate().after(new Date()) &&
		// !t.getManager().equals((Manager)actor)){
		// result.remove(t);
		// }
		// }

		return result;
	}

	public Collection<Trip> filterTripsByCriteria(String keyWord, Date dateStart, Date dateEnd, Double minPrice, Double maxPrice) {

		Collection<Trip> result;

		if (keyWord == null)
			keyWord = "";
		if (dateStart == null)
			dateStart = new Date(0);
		if (dateEnd == null)
			dateEnd = new Date(Long.MAX_VALUE);
		if (minPrice == null)
			minPrice = Double.MIN_VALUE;
		if (maxPrice == null)
			maxPrice = Double.MAX_VALUE;

		result = this.tripRepository.filterTripsByCriteria('%' + keyWord + '%', dateStart, dateEnd, minPrice, maxPrice, new Date());
		Assert.notNull(result);

		return result;
	}

	public Collection<Trip> filterTripsByCriteria(final Finder finder) {
		String keyWord;
		Date dateStart, dateEnd;
		Double minPrice, maxPrice;

		Collection<Trip> result;

		keyWord = finder.getKeyWord();
		dateStart = finder.getDateStart();
		dateEnd = finder.getDateEnd();
		minPrice = finder.getPriceStart();
		maxPrice = finder.getPriceEnd();

		if (keyWord == null)
			keyWord = "";
		if (dateStart == null)
			dateStart = new Date(0);
		if (dateEnd == null)
			dateEnd = new Date(Long.MAX_VALUE);
		if (minPrice == null)
			minPrice = Double.MIN_VALUE;
		if (maxPrice == null)
			maxPrice = Double.MAX_VALUE;

		result = this.tripRepository.filterTripsByCriteriaFinder('%' + keyWord + '%', dateStart, dateEnd, minPrice, maxPrice, new Date(), new PageRequest(0, this.configurationService.findConfiguration().getFinderMaxResults())).getContent();
		Assert.notNull(result);

		return result;
	}

	public Sponsorship getRandomSponsorship(final int tripId) {
		List<Sponsorship> sponsorships;
		Sponsorship result;
		result = null;

		sponsorships = new ArrayList<Sponsorship>(this.sponsorshipService.getSponsorshipsByTrip(tripId)); // Obtener la
		// lista de
		// Sponsorships
		if (!sponsorships.isEmpty())
			result = sponsorships.get(ThreadLocalRandom.current().nextInt(0, sponsorships.size())); // Obtener una Sponsorship aleatoria
		return result;
	}

	public Collection<Trip> getTripsPublishedAfter(final Date date) {
		Assert.notNull(date);

		Collection<Trip> result;

		result = this.tripRepository.getTripsPublishedAfter(date);

		return result;
	}

	public Collection<Trip> getTripsPublished() {

		Collection<Trip> result;

		result = this.tripRepository.getTripsPublished(new Date());

		return result;
	}

	public Trip getTripByTicker(final String ticker) {
		Assert.notNull(ticker);

		Trip result;

		result = this.tripRepository.getTripByTicker(ticker);

		return result;
	}

	public Collection<Trip> getTripsByCategory(final int categoryId) {
		Collection<Trip> result;
		Date date;

		date = new Date();
		result = this.tripRepository.getTripsByCategory(categoryId, date);

		return result;
	}

	/*---------------------------------------------------------------METODOS TICKER UNICOS-----------------------------------------------------------------*/
	private static String getDate() {
		String year, month, day;

		year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2); // Suponiendo que el a�o tiene 4 d�gitos (Cambiar
																						// implementaci�n para el a�o 10.000)
		month = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);

		if (month.length() == 1)
			month = '0' + month;

		day = String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

		if (day.length() == 1)
			day = '0' + day;

		return year + month + day;
	}

	public static String generateTicker(int id) {
		String res;
		List<Character> alphabeth;

		res = TripService.getDate() + "-";

		alphabeth = Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

		id %= Math.pow(alphabeth.size(), 4);

		for (int i = 0; i < 4; i++) {
			res += alphabeth.get(id % alphabeth.size());
			id = (id - id % alphabeth.size()) / alphabeth.size();
		}

		return res;
	}

	public Trip deleteCategory(final Integer tripId) {
		Trip result;

		result = this.findOne(tripId);
		Assert.notNull(result);

		result.setCategory(null);

		this.tripRepository.save(result);

		return result;
	}
}
