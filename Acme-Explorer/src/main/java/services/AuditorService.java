package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AuditorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.AuditRecord;
import domain.Auditor;
import domain.Folder;
import domain.Note;
import domain.SocialIdentity;

@Service
@Transactional
public class AuditorService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private AuditorRepository auditorRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ActorService actorService;

	// Constructor ---------------------------------------------------
	
	public AuditorService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Auditor create() {
		
		Auditor result;
		
		result = new Auditor();
		result.setFolders(new ArrayList<Folder>());
		result.setNotes(new ArrayList<Note>());
		result.setSocialIdentities(new ArrayList<SocialIdentity>());
		result.setAuditRecords(new ArrayList<AuditRecord>());
		result.setSuspicious(false);
		result.setBanned(false);
		
		return result;
	}
	
	public Auditor findOne(int auditorId) {
		Assert.isTrue(auditorId != 0);
		
		Auditor result;
		
		result = auditorRepository.findOne(auditorId);
		
		return result;
	}
	
	public Collection<Auditor> findAll() {		
		Collection<Auditor> result;
		
		result = auditorRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Auditor save(Auditor auditor) {
		Assert.notNull(auditor);
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		
		if (auditor.getId() != 0) {
			Assert.isTrue(auditor.getUserAccount().equals(principal));
		} else {
			Authority auth;
			Md5PasswordEncoder encoder;
			
			auth = new Authority();
			auth.setAuthority(Authority.ADMINISTRATOR);
			Assert.isTrue(principal.getAuthorities().contains(auth));
			encoder = new Md5PasswordEncoder();
			auditor.getUserAccount().setPassword(encoder.encodePassword(auditor.getUserAccount().getPassword(),null));
			auditor.getUserAccount().setAccountNonLocked(true);

		}
		
		Auditor result;
		
		result = auditorRepository.save(auditor);
		
		if (auditor.getId() == 0) {
			result = (Auditor)actorService.addSystemFolders(result); //Cuando se persiste por primera vez se a�aden los folders del sistema
		
		}else{
			result.setSuspicious(configurationService.hasSpam((Actor) result));
		}
		
		result = auditorRepository.save(result);

		return result;
	}

	
	// Other business methods ---------------------------------------------------


}
