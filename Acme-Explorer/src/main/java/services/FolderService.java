
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.FolderRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Folder;
import domain.FolderType;
import domain.Message;

@Service
@Transactional
public class FolderService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private FolderRepository	folderRepository;

	// Supporting Services ---------------------------------------------------

	@Autowired
	private ActorService		actorService;

	@Autowired
	private MessageService		messageService;


	// Constructor ---------------------------------------------------

	public FolderService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Folder create(final Actor actor) {
		Assert.notNull(actor);

		Folder result;
		FolderType folderType;

		result = new Folder();
		folderType = new FolderType();
		folderType.setFolderType(FolderType.USERBOX);

		result.setMessages(new ArrayList<Message>());
		result.setType(folderType);
		result.setActor(actor);

		return result;
	}

	public Folder findOne(final int folderId) {
		Assert.isTrue(folderId != 0);

		Folder result;

		result = this.folderRepository.findOne(folderId);

		return result;
	}

	public Collection<Folder> findAll() {
		Collection<Folder> result;

		result = this.folderRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Folder save(Folder folder) {
		Assert.notNull(folder);
		Folder folderOld, result;
		Actor actor;

		actor = folder.getActor();
		folderOld = null;

		if (folder.getId() == 0) {
			if (!folder.getType().getFolderType().equals(FolderType.USERBOX)) {
				folderOld = this.getFolderByActorIdAndType(actor.getId(), folder.getType().getFolderType());
				Assert.isNull(folderOld);
			}
		} else {
			folderOld = this.findOne(folder.getId());
			UserAccount principal;
			principal = LoginService.getPrincipal();

			if (!folder.getActor().getUserAccount().equals(principal)) {
				Assert.isTrue(folder.getName() == folderOld.getName());
				Assert.isTrue(folder.getParentFolder() == folderOld.getParentFolder());
			}

			if (!folder.getType().getFolderType().equals(FolderType.USERBOX)) {
				Assert.isTrue(folder.getName().equals(folderOld.getName()));
				Assert.isNull(folder.getParentFolder());
			} else
				Assert.isTrue(this.checkFolderTreeIntegrity(folder));

			Assert.isTrue(folder.getActor() == folderOld.getActor());
			Assert.isTrue(folderOld.getType().getFolderType().equals(folder.getType().getFolderType()));
		}
		
		Assert.isTrue(checkFolderNames(folder));

		result = this.folderRepository.saveAndFlush(folder);

		if (folder.getId() == 0) {
			actor.getFolders().add(result);
			this.actorService.save(actor);
		}

		return result;
	}

	public void delete(final Folder folder) {
		Assert.notNull(folder);
		Assert.isTrue(folder.getType().getFolderType().equals(FolderType.USERBOX));

		Collection<Folder> childrenFolders;
		Collection<Message> messages;
		UserAccount userAccount;
		Actor actor;

		userAccount = LoginService.getPrincipal();
		actor = folder.getActor();

		Assert.notNull(folder);
		Assert.isTrue(folder.getActor().getUserAccount().equals(userAccount));
		Assert.isTrue(this.folderRepository.exists(folder.getId()));

		childrenFolders = new ArrayList<Folder>(this.folderRepository.getChildrenFolders(folder.getId()));
		for (final Folder f : childrenFolders)
			this.delete(f);

		messages = new ArrayList<Message>(folder.getMessages());
		for (final Message m : messages)
			this.messageService.delete(m, actor, true);

		actor.getFolders().remove(folder);

		this.folderRepository.flush();

		this.actorService.save(actor);
		this.folderRepository.delete(folder);
	}

	// Other business methods ---------------------------------------------------

	// Para buscar folders de sistema de un actor, no las USERBOX
	public Folder getFolderByActorIdAndType(final int actorId, final String type) {
		Assert.isTrue(actorId != 0);
		Assert.notNull(type);

		Folder result;

		result = this.folderRepository.getFolderByActorIdAndType(actorId, type);

		return result;
	}

	// Para buscar folders de sistema de un actor, no las USERBOX
	public Folder getFolderByActorAndMessage(final int actorId, final int messageId) {
		Assert.isTrue(actorId != 0);
		Assert.isTrue(messageId != 0);

		Folder result;

		result = this.folderRepository.getFolderByActorAndMessage(actorId, messageId);

		return result;
	}

	public Collection<Folder> getAllNotificationFolders() {
		Collection<Folder> result;

		result = this.folderRepository.getAllNotificationFolders();
		Assert.notNull(result);

		return result;
	}

	public Collection<Folder> getUserParentFolders(final int actorId) {
		Collection<Folder> result;

		result = this.folderRepository.getUserParentFolders(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Folder> getUserFolders(final int actorId) {
		Collection<Folder> result;

		result = this.folderRepository.getUserFolders(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Folder> getChildrenFolders(final int folderId) {
		Collection<Folder> result;

		result = this.folderRepository.getChildrenFolders(folderId);
		Assert.notNull(result);

		return result;
	}

	// Auxiliary method

	private boolean checkFolderTreeIntegrity(final Folder folder) {
		return this.checkFolderTreeIntegrity(folder, folder);
	}

	private boolean checkFolderTreeIntegrity(final Folder folder, final Folder originalFolder) {
		boolean result;
		Collection<Folder> childrenFolders;

		result = true;

		childrenFolders = this.getChildrenFolders(folder.getId());
		for (final Folder f : childrenFolders)
			if (result == false || originalFolder.getParentFolder() == f) {
				result = false;
				break;
			} else
				result = this.checkFolderTreeIntegrity(f, originalFolder);

		return result;
	}
	
	private boolean checkFolderNames(Folder folder) {
		Assert.notNull(folder);
		
		boolean result;
		Collection<Folder> siblingFolders;
		Folder parentFolder;
		
		parentFolder = folder.getParentFolder();
		if (parentFolder == null) {
			siblingFolders = getUserParentFolders(folder.getActor().getId());
		} else {
			siblingFolders = getChildrenFolders(parentFolder.getId());
		}
		siblingFolders.remove(folder);
		
		result = true;
		for (Folder f : siblingFolders) {
			if (f.getName().equals(folder.getName())) {
				result = false;
				break;
			}
		}
		
		return result;
	}

}
