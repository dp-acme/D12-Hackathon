package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
	@Query("select count(m) from Folder f join f.messages m where m.id = ?1")
	Integer getMessageReferences(int messageId);
	
	@Query("select m from Message m where m.sender.id = ?1")
	Collection<Message> getMessagesBySenderId(int senderId);
}
