package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.ApplyFor;
import domain.Explorer;

@Repository
public interface ExplorerRepository extends JpaRepository<Explorer, Integer>{
	
	@Query("select a from ApplyFor a where a.explorer.id = ?1 AND a.trip.id = ?2")
	ApplyFor getApplyForOfExplorerByTrip(int explorerId, int tripId);

	@Query("select e from Explorer e join e.applications ap where ap.trip.id = ?1")
	Collection<Explorer> findByTrip(int tripId);
}
