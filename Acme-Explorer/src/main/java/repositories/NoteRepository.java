package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Integer>{
	
	@Query ("select n from Note n where n.auditor.id = ?1 AND n.trip.manager.id = ?2")
	Collection<Note> getNotesByAuditorAndManager(int auditorId, int managerId);
	
	@Query ("select n from Note n where n.trip.id = ?1")
	Collection<Note> getNotesByTrip(int tripId);
	
	@Query ("select n from Note n where n.trip.manager.userAccount.id = ?1")
	Collection<Note> getNotesByTripManager(int ManagerId);
	
	@Query ("select n from Note n where n.auditor.userAccount.id = ?1")
	Collection<Note> getNotesByAuditor(int id);
}