package controllers.note;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.NoteService;
import domain.Note;

@Controller
@RequestMapping("/note/auditor")
public class NoteAuditorController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private NoteService noteService;

	// Constructors -----------------------------------------------------------

	public NoteAuditorController() {
		super();
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int tripId) {
		ModelAndView result;
		Note note;

		note = this.noteService.create(tripId);
		result = this.createEditModelAndView(note);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Note> notes;

		notes = noteService.getNotesByAuditor();

		result = new ModelAndView("note/auditor/list");
		result.addObject("notes", notes);
		result.addObject("requestURI", "note/auditor/list.do");

		return result;

	}
		
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Note note, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(note);
		} else {
			try {
				noteService.save(note);
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(note, "note.commit.error");
			}
		}
		return result;
	}

	protected ModelAndView createEditModelAndView(Note note) {
		ModelAndView result;

		result = createEditModelAndView(note, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Note note, String messageCode) {
		ModelAndView result;

		if(note.getId()==0){
			result = new ModelAndView("note/auditor/create");
		}else{
			result = new ModelAndView("note/manager/edit");
		}
		result.addObject("note", note);

		result.addObject("message", messageCode);

		return result;
	}
}
