package controllers.sponsorship;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.SponsorshipService;
import services.TripService;
import controllers.AbstractController;
import domain.Sponsor;
import domain.Sponsorship;
import domain.Trip;

@Controller
@RequestMapping("/sponsorship")
public class SponsorshipSponsorController extends AbstractController {

	@Autowired
	private SponsorshipService sponsorshipService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private TripService tripService;

	@RequestMapping("/sponsor/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Sponsorship> sponshorships;
		Sponsor sponsor;

		sponsor = (Sponsor) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		sponshorships = sponsor.getSponsorships();

		result = new ModelAndView("sponsorship/sponsor/list");
		result.addObject("sponsorships", sponshorships);

		return result;
	}

	@RequestMapping("/sponsor/edit")
	public ModelAndView edit(@RequestParam int sponsorshipId) {
		ModelAndView result;
		Sponsorship sponsorship;

		sponsorship = sponsorshipService.findOne(sponsorshipId);

		Assert.notNull(sponsorship);

		result = createEditModelAndView(sponsorship);

		return result;
	}
	
	@RequestMapping(value = "/sponsor/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Sponsorship sponsorship, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()){
			result = createEditModelAndView(sponsorship);
		}else{
			try{
				sponsorshipService.save(sponsorship);
				result = new ModelAndView("redirect:list.do");
			}catch(Throwable oops){
				result = createEditModelAndView(sponsorship, "sponsorship.commit.error");
			}
		}

		return result;
	}
	
	@RequestMapping("/sponsor/create")
	public ModelAndView create(@RequestParam int tripId) {
		ModelAndView result;
		Sponsorship sponsorship;
		Sponsor sponsor;
		Trip trip;

		trip = tripService.findOne(tripId);
		sponsor = (Sponsor) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		sponsorship = sponsorshipService.create(sponsor, trip);
		
		result = createEditModelAndView(sponsorship);
		
		return result;
	}
	
	

	private ModelAndView createEditModelAndView(Sponsorship sponsorship) {
		ModelAndView result;

		result = createEditModelAndView(sponsorship, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Sponsorship sponsorship, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("sponsorship/sponsor/edit");
		
		result.addObject("sponsorship", sponsorship);
		result.addObject("message", messageCode);

		return result;
	}

}
