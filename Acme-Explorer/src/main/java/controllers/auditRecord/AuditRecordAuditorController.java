/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.auditRecord;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AuditRecordService;
import controllers.AbstractController;
import domain.AuditRecord;

@Controller
@RequestMapping("/auditrecord/auditor")
public class AuditRecordAuditorController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private AuditRecordService	auditRecordService;


	// Constructors -----------------------------------------------------------

	public AuditRecordAuditorController() {
		super();
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int tripId) {
		ModelAndView result;
		AuditRecord auditRecord;

		auditRecord = this.auditRecordService.create(tripId);
		result = this.createEditModelAndView(auditRecord);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listByAuditor(@RequestParam(required = false) final Integer tripId) {
		ModelAndView result;
		Collection<AuditRecord> auditRecords;

		if (tripId == null)
			auditRecords = this.auditRecordService.findAllByPrincipal();
		else
			auditRecords = this.auditRecordService.findAllByTripAuditor(tripId);

		result = new ModelAndView("auditrecord/auditor/list");
		result.addObject("auditRecords", auditRecords);
		result.addObject("requestURI", "auditrecord/auditor/list.do");

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int auditRecordId) {
		ModelAndView result;
		AuditRecord auditRecord;

		auditRecord = this.auditRecordService.findOne(auditRecordId);
		Assert.notNull(auditRecord);
		Assert.isTrue(auditRecord.isDraft());
		result = this.createEditModelAndView(auditRecord);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final AuditRecord auditRecord, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(auditRecord);
		else
			try {
				this.auditRecordService.save(auditRecord);
				result = new ModelAndView("redirect:/auditrecord/auditor/list.do?tripId=" + auditRecord.getTrip().getId());
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(auditRecord, "auditRecord.commit.error");
			}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final AuditRecord auditRecord, final BindingResult binding) {
		ModelAndView result;

		try {
			this.auditRecordService.delete(auditRecord);
			result = new ModelAndView("redirect:/auditrecord/auditor/list.do?tripId=" + auditRecord.getTrip().getId());
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(auditRecord, "auditRecord.commit.error");
		}
		return result;
	}

	protected ModelAndView createEditModelAndView(final AuditRecord auditRecord) {
		ModelAndView result;

		result = this.createEditModelAndView(auditRecord, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final AuditRecord auditRecord, final String messageCode) {
		ModelAndView result;
		if (auditRecord.getId() == 0)
			result = new ModelAndView("auditrecord/auditor/create");
		else
			result = new ModelAndView("auditrecord/auditor/edit");
		result.addObject("auditRecord", auditRecord);

		result.addObject("message", messageCode);

		return result;
	}

}
