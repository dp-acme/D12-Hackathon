/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.manager;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ConfigurationService;
import services.ManagerService;
import controllers.AbstractController;
import domain.Manager;

@Controller
@RequestMapping("/manager/administrator")
public class ManagerAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private ManagerService managerService;

	@Autowired
	private ConfigurationService configurationService;
	
	
	// Constructors -----------------------------------------------------------

	public ManagerAdministratorController() {
		super();
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		Manager manager;
		
		manager = this.managerService.create();
		result = this.createEditModelAndView(manager);

		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView saveCreate(@Valid Manager manager, BindingResult binding){
		ModelAndView result;
		System.out.println(binding.hasErrors());
		if(binding.hasErrors()){
			result = createEditModelAndView(manager);
		} else { 
			try {
				managerService.save(manager);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(manager, "manager.commit.error");
			}
		}
		return result;
	}
	
	
	protected ModelAndView createEditModelAndView(Manager manager) {
		ModelAndView result; 
		
		result = createEditModelAndView(manager, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Manager manager,
			String messageCode) {
		ModelAndView result;
		result = new ModelAndView("manager/administrator/create");
		
		result.addObject("manager", manager);
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());

		result.addObject("message", messageCode);
		
		return result;
	}

}
