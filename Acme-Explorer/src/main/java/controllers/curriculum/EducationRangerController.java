
package controllers.curriculum;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.EducationService;
import controllers.AbstractController;
import domain.Curriculum;
import domain.Education;
import domain.Ranger;

@Controller
@RequestMapping("/education")
public class EducationRangerController extends AbstractController {

	//Services------------------------------------------------------------
	@Autowired
	private EducationService	educationService;
	@Autowired
	private ActorService		actorService;



	//Constructors------------------------------------------------------------
	private EducationRangerController() {
		super();
	}

	//Display-------------------------------------------------------------------------------
	@RequestMapping(value = "/ranger/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam final int educationId) {
		ModelAndView result;
		Education education;

		education = this.educationService.findOne(educationId);
		Assert.notNull(education);
		result = new ModelAndView("education/ranger/display");
		result.addObject("education", education);

		return result;
	}

	//List -------------------------------------------------------------------------
	@RequestMapping("/ranger/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Education> educationRecords;
		Ranger ranger;

		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		educationRecords = ranger.getCurriculum().getEducationRecords();

		result = new ModelAndView("education/ranger/list");
		result.addObject("educationRecords", educationRecords);

		return result;
	}

	//Edit-----------------------------------------------------------------------------
	@RequestMapping("/ranger/edit")
	public ModelAndView edit(@RequestParam int educationId) {
		ModelAndView result;
		Education education;

		education = educationService.findOne(educationId);

		Assert.notNull(education);

		result = createEditModelAndView(education);

		return result;
	}
	//Save--------------------------------------------------------------------------
	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Education education, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(education);
		} else {
			try {
				educationService.save(education);
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(education, "education.commit.error");
			}
		}

		return result;
	}
	//Create----------------------------
	@RequestMapping("/ranger/create")
	public ModelAndView create() {
		ModelAndView result;
		Education education;
		Curriculum curriculum;
		Ranger ranger;
		
		
		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		curriculum = ranger.getCurriculum();
		education = educationService.create(curriculum);

		result = createEditModelAndView(education);

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Education education, final BindingResult binding) {
		ModelAndView result;

		try {
			this.educationService.delete(education);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(education, "education.commit.error");
		}

		return result;
	}

	private ModelAndView createEditModelAndView(Education education) {
		ModelAndView result;

		result = createEditModelAndView(education, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Education education, String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("education/ranger/edit");
		
		result.addObject("education", education);
		result.addObject("message", messageCode);

		return result;
	}

}
