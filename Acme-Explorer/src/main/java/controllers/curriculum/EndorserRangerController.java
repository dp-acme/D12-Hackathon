
package controllers.curriculum;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ConfigurationService;
import services.EndorserService;
import controllers.AbstractController;
import domain.Curriculum;
import domain.Endorser;
import domain.Ranger;

@Controller
@RequestMapping("/endorser")
public class EndorserRangerController extends AbstractController {

	//Services------------------------------------------------------------
	@Autowired
	private EndorserService		endorserService;

	@Autowired
	private ActorService		actorService;
	
	@Autowired
	private ConfigurationService	configurationService;


	//Constructors------------------------------------------------------------
	private EndorserRangerController() {
		super();
	}

	//Display-------------------------------------------------------------------------------
	@RequestMapping(value = "/ranger/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam final int endorserId) {
		ModelAndView result;
		Endorser endorser;

		endorser = this.endorserService.findOne(endorserId);
		Assert.notNull(endorser);
		result = new ModelAndView("endorser/ranger/display");
		result.addObject("endorser", endorser);

		return result;
	}

	//List -------------------------------------------------------------------------
	@RequestMapping("/ranger/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Endorser> endorserRecords;
		Ranger ranger;
		ranger = (Ranger) this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		endorserRecords = ranger.getCurriculum().getEndorserRecords();

		result = new ModelAndView("endorser/ranger/list");
		result.addObject("endorserRecords", endorserRecords);

		return result;
	}
	@RequestMapping("/ranger/edit")
	public ModelAndView edit(@RequestParam int endorserId) {
		ModelAndView result;
		Endorser endorser;

		endorser = endorserService.findOne(endorserId);

		Assert.notNull(endorser);

		result = createEditModelAndView(endorser);

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Endorser endorser, final BindingResult binding) {
		ModelAndView result;

		try {
			this.endorserService.delete(endorser);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(endorser, "endorser.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Endorser endorser, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(endorser);
		} else {
			try {
				this.endorserService.save(endorser);
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(endorser, "endorser.commit.error");
			}
		}

		return result;
	}

	@RequestMapping("/ranger/create")
	public ModelAndView create() {
		ModelAndView result;
		Endorser endorser;
		Ranger ranger;
		Curriculum curriculum;

		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		curriculum = ranger.getCurriculum();
		endorser = endorserService.create(curriculum);

		result = createEditModelAndView(endorser);

		return result;
	}

	private ModelAndView createEditModelAndView(Endorser endorser) {
		ModelAndView result;

		result = createEditModelAndView(endorser, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Endorser endorser, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("endorser/ranger/edit");

		result.addObject("endorser", endorser);
		result.addObject("message", messageCode);
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());


		return result;
	}

}
