/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.category;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CategoryService;
import controllers.AbstractController;
import domain.Category;

@Controller
@RequestMapping("/category")
public class CategoryAdministratorController extends AbstractController {
	
	// Sevices
	@Autowired
	CategoryService categoryService;

	// Constructors -----------------------------------------------------------

	public CategoryAdministratorController() {
		super();
	}
	
	@RequestMapping(value = "/administrator/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(value = "parentCategoryId", required = true) Integer parentCategoryId) {
		ModelAndView result;
		Category category, parent;
		
		result = new ModelAndView("category/administrator/create");
		category = categoryService.create();
		parent = categoryService.findOne(parentCategoryId);
		
		Assert.notNull(parent);
		
		result.addObject("category", category);
		result.addObject("parentCategory", parent);
		
		return result;
	}
	
	@RequestMapping(value = "/administrator/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(value = "categoryId", required = true) Integer categoryId) {
		ModelAndView result;
		Category category;
		
		result = new ModelAndView("category/administrator/edit");
		category = categoryService.findOne(categoryId);
		
		Assert.notNull(category);
				
		result.addObject("category", category);
		
		return result;
	}
	
	@RequestMapping(value = "/administrator/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Category category, final BindingResult binding) {
		ModelAndView result;

		try {
			categoryService.delete(category);
			
			result = new ModelAndView("redirect:/category/list.do");
			
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(category, "category.commit.error");
		}

		return result;
	}
	
	@RequestMapping(value = "/administrator/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@RequestParam(value = "parentCategoryId", required = true) Integer parentCategoryId,
							 @Valid Category category, BindingResult binding) {
		ModelAndView result;
		Category parentCategory;
		
		if(binding.hasErrors()){
			result = createCreateModelAndViewWithParent(category, null, parentCategoryId);
			
		}else{
			try{
				parentCategory = categoryService.findOne(parentCategoryId);
				
				Assert.notNull(parentCategory);
				
				categoryService.addChildCategory(parentCategory, category);
				
				result = new ModelAndView("redirect:/category/list.do?categoryId=" + parentCategory.getId());
				
			}catch(Throwable oops){
				result = createCreateModelAndViewWithParent(category, "category.commit.error", parentCategoryId);
			}
		}

		return result;
	}

	@RequestMapping(value = "/administrator/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Category category, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()){
			result = createEditModelAndView(category, null);
			
		}else{
			try{								
				categoryService.save(category);
				
				result = new ModelAndView("redirect:/category/list.do?categoryId=" + category.getId());
				
			}catch(Throwable oops){
				result = createEditModelAndView(category, "category.commit.error");
			}
		}

		return result;
	}
	
	protected ModelAndView createEditModelAndView(Category category, String message) {
		ModelAndView result;
		
		result = new ModelAndView("category/administrator/edit");
		result.addObject("category", category);
		result.addObject("message", message);
		
		return result;
	}
	
	protected ModelAndView createCreateModelAndView(Category category, String message) {
		ModelAndView result;
		
		result = new ModelAndView("category/administrator/create");
		result.addObject("category", category);
		result.addObject("message", message);
		
		return result;
	}
	
	protected ModelAndView createCreateModelAndViewWithParent(Category category, String message, Integer parentCategoryId) {
		ModelAndView result;
		
		result = new ModelAndView("category/administrator/create");
	
		result.addObject("category", category);
		result.addObject("message", message);
		result.addObject("parentCategory", categoryService.findOne(parentCategoryId));
		
		return result;
	}
}
