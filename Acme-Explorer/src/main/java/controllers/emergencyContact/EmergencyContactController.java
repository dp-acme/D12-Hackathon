
package controllers.emergencyContact;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.Authority;
import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.ConfigurationService;
import services.EmergencyContactService;
import services.ExplorerService;
import controllers.AbstractController;
import domain.EmergencyContact;
import domain.Explorer;

@Controller
@RequestMapping("/emergencyContact")
public class EmergencyContactController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private EmergencyContactService	emergencyContactService;

	@Autowired
	private ExplorerService			explorerService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private ConfigurationService	configurationService;


	// Constructors -----------------------------------------------------------

	public EmergencyContactController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required = false, value = "explorerId", defaultValue = "-1") final int explorerId) {
		ModelAndView result;
		Explorer explorer;
		Collection<EmergencyContact> emergencyContacts;

		if (explorerId == -1) {
			UserAccount principal;
			Authority auth;

			auth = new Authority();
			auth.setAuthority(Authority.EXPLORER);

			principal = LoginService.getPrincipal();
			Assert.isTrue(principal.getAuthorities().contains(auth));

			explorer = (Explorer) this.actorService.findByUserAccountId(principal.getId());
		} else
			explorer = this.explorerService.findOne(explorerId);

		Assert.notNull(explorer);

		emergencyContacts = explorer.getEmergencyContacts();

		result = new ModelAndView("emergencyContact/list");
		result.addObject("explorer", explorer);
		result.addObject("emergencyContacts", emergencyContacts);

		return result;
	}

	// Creation -----------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		EmergencyContact emergencyContact;
		Explorer explorer;
		UserAccount principal;
		Authority auth;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.EXPLORER);
		Assert.isTrue(principal.getAuthorities().contains(auth));

		explorer = (Explorer) this.actorService.findByUserAccountId(principal.getId());

		emergencyContact = this.emergencyContactService.create();

		result = this.createEditModelAndView(explorer, emergencyContact);

		return result;
	}

	// Edition -----------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = true) final int emergencyContactId) {
		ModelAndView result;
		EmergencyContact emergencyContact;
		UserAccount principal;
		Explorer explorer;
		Authority auth;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.EXPLORER);
		Assert.isTrue(principal.getAuthorities().contains(auth));

		explorer = (Explorer) this.actorService.findByUserAccountId(principal.getId());
		Assert.notNull(explorer);

		emergencyContact = this.emergencyContactService.findOne(emergencyContactId);
		Assert.notNull(emergencyContact);

		result = this.createEditModelAndView(explorer, emergencyContact);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(final HttpServletRequest request, @Valid final EmergencyContact emergencyContact, final BindingResult binding) {
		ModelAndView result;
		Explorer explorer;

		explorer = this.explorerService.findOne(Integer.parseInt(request.getParameter("explorerId")));
		Assert.notNull(explorer);

		if (binding.hasErrors())
			result = this.createEditModelAndView(explorer, emergencyContact);
		else
			try {
				this.emergencyContactService.save(emergencyContact);
				result = new ModelAndView("redirect:/emergencyContact/list.do?explorerId=" + explorer.getId());
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(explorer, emergencyContact, "emergencyContact.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final HttpServletRequest request, @Valid final EmergencyContact emergencyContact, final BindingResult binding) {
		ModelAndView result;
		Explorer explorer;

		explorer = this.explorerService.findOne(Integer.parseInt(request.getParameter("explorerId")));
		Assert.notNull(explorer);

		try {
			this.emergencyContactService.delete(emergencyContact);
			result = new ModelAndView("redirect:/emergencyContact/list.do?explorerId=" + explorer.getId());
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(explorer, emergencyContact, "emergencyContact.commit.error");
		}

		return result;
	}

	// Ancillary Methods ------------------------------------------------------

	protected ModelAndView createEditModelAndView(final Explorer explorer, final EmergencyContact emergencyContact) {
		ModelAndView result;

		result = this.createEditModelAndView(explorer, emergencyContact, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Explorer explorer, final EmergencyContact emergencyContact, final String messageCode) {
		ModelAndView result;

		result = new ModelAndView("emergencyContact/edit");
		result.addObject("explorer", explorer);
		result.addObject("emergencyContact", emergencyContact);
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());

		result.addObject("message", messageCode);

		return result;
	}
}
