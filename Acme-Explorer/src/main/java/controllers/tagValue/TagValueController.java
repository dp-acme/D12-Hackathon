package controllers.tagValue;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.TripService;
import controllers.AbstractController;
import domain.TagValue;
import domain.Trip;

@Controller
@RequestMapping("/tagvalue")
public class TagValueController extends AbstractController {
	
	// Services------------------------------
		@Autowired
		private TripService tripService;
	// Constructor---------------------------
		public TagValueController() {
			super();
		}
	//Listing--------------------------------
		@RequestMapping(value = "/list")
		public ModelAndView list(@RequestParam("tripId") Integer tripId){
			ModelAndView result;
			Collection<TagValue> tagValues;
			Trip trip;
			
			trip = tripService.findOne(tripId);
			tagValues = trip.getTags();
			result = new ModelAndView("tagvalue/list");
			
			Assert.notNull(trip);
			Assert.isTrue(!trip.getPublicationDate().after(new Date()));
			
			result.addObject("tagValues", tagValues);
			result.addObject("trip", trip);
			result.addObject("requestUri", "tagvalue/list.do");
			
			return result;
		}
			

}
