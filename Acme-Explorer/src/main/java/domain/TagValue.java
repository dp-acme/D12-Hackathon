package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class TagValue extends DomainEntity {

	private String value;

	@NotBlank
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}	
	
	private TagKey tagKey;

	@Valid
	@ManyToOne(optional=false)
	public TagKey getTagKey() {
		return tagKey;
	}
	public void setTagKey(TagKey tagKey) {
		this.tagKey = tagKey;
	}
	
}


