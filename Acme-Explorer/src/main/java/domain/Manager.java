package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

	// Constructor
	
	public Manager() {
		super();
	}
	
	// Attributes
	
	// Relationships

	private Collection<Trip> trips;
	
	
	@NotNull
	@Valid
	@OneToMany(mappedBy="manager")
	public Collection<Trip> getTrips() {
		return trips;
	}
	public void setTrips(Collection<Trip> trips) {
		this.trips = trips;
	}
}
