<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!-- MOSTRAR ATRIBUTOS -->
<jstl:if test="${legalText.isFinalMode()}">
<spring:message code="legalText.formatDate" var="formatDate" />
<fmt:formatDate value="${legalText.getMoment()}" pattern="${formatDate}" var="moment" />
<b><spring:message code="message.display.moment" /></b> <jstl:out value="${moment}" />
<br />
<br />

<b><spring:message code="legalText.display.title" />:</b>
<jstl:out value="${legalText.title}"></jstl:out>

<br />
<br />
<b><spring:message code="legalText.display.body" />:</b>
<jstl:out value="${legalText.body}"></jstl:out>

<br />
<br />
<b><spring:message code="legalText.display.laws" />:</b>
<jstl:out value="${legalText.laws}"></jstl:out>

<br />
<br />
<!--Button Back-->
</jstl:if>
<input type="button" name="back"
	value="<spring:message code="legalText.display.back" />"
	onclick="javascript: relativeRedir('trip/list.do');" />

<br />