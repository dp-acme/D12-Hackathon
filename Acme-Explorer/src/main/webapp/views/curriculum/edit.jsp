<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<input type="hidden" id="messagePhone"
	value="<spring:message code="curriculum.phone.message"/>" />
<input type="hidden" id="countryCode"
	value="${countryCode}" />

<script type="text/javascript">
	function checkPhone() {
		phone = document.getElementById("phone");
		res = true;
		
		if (!/^(\+[0-9]{1,3}\s)?(\([0-9]{1,3}\))?\s?[0-9]{4,}$/.test(phone.value)&&phone.value) {
			res = confirm(document.getElementById("messagePhone").value);
		}else if (/^[0-9]{4,}$/.test(phone.value)&&phone.value) {
			phoneCountry = document.getElementById("countryCode").value+ ' ' + document.getElementById("phone").value ;
	 		newPhone = document.getElementById("phone");
			newPhone.value = phoneCountry;
			
		} 
		return res;
	}
</script>

<jstl:choose>
	<jstl:when test="${param.curriculumId==null}">
		<h1>
			<spring:message code="curriculum.edit.ranger.create" />
		</h1>
		<jstl:set var="disable" value="false" />
	</jstl:when>
	<jstl:otherwise>
		<h1>
			<spring:message code="curriculum.edit.ranger.edit" />
		</h1>
		<jstl:set var="disable" value="true" />
	</jstl:otherwise>
</jstl:choose>


<form:form action="curriculum/ranger/edit.do"
	modelAttribute="curriculum">

	<!-- ATRIBUTOS QUE NO SE VAN A MODIFICAR -->
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="ranger" />
	<form:hidden path="ticker" />
	<form:hidden path="educationRecords" />
	<form:hidden path="endorserRecords" />
	<form:hidden path="miscellaneousRecords" />
	<form:hidden path="professionalRecords" />

	<!-- ATRIBUTOS QUE VAMOS A MODIFICAR -->
	<b><spring:message code="curriculum.editCurriculum" /></b>
	<br />
	<br />

	<!-- NAME -->
	<form:label path="name">
		<spring:message code="curriculum.edit.name" />
	</form:label>
	<form:input path="name" />
	<form:errors cssClass="error" path="name"></form:errors>

	<br />
	<br />
	<!-- EMAIL -->
	<form:label path="email">
		<spring:message code="curriculum.edit.email" />
	</form:label>
	<form:input path="email" />
	<form:errors cssClass="error" path="email"></form:errors>
	<br />

	<br />

	<!-- PHOTO -->
	<form:label path="photo">
		<spring:message code="curriculum.edit.photo" />
	</form:label>
	<form:input path="photo" />
	<form:errors cssClass="error" path="photo"></form:errors>
	<br />

	<br />
	<!-- PHONE -->
	<form:label path="phone">
		<spring:message code="curriculum.edit.phone" />
	</form:label>
	<form:input path="phone" />
	<form:errors cssClass="error" path="phone"></form:errors>
	<br />

	<br />

	<!-- PROFILE -->
	<form:label path="profile">
		<spring:message code="curriculum.edit.profile" />
	</form:label>
	<form:input path="profile" />
	<form:errors cssClass="error" path="profile"></form:errors>
	<br />

	<br />
	<!-- PARA EL SALTO DE LINEA -->

	<!-- BOTONES -->

	<!-- BOTON DE SAVE -->
	<input type="submit" name="save"
		value="<spring:message code="curriculum.edit.save"/>" 
			onClick="javascript: return checkPhone();" />
	<!-- PREGUNTAMOS POR LOS CASOS QUE TENEMOS -->
	<!-- VAMOS A EDITARLO -->
	<jstl:if test="${curriculum.id !=0}">
		<!-- QUIERE DECIR QUE HAY OBJETOS  -->
		<!-- PODEMOS PONER BOTON DE BORRAR -->
		<input type="submit" name="delete"
			value="<spring:message code="curriculum.edit.delete" />"
			onclick="javascript: return confirm('<spring:message code="curriculum.confirm.delete" />')" />
		&nbsp;<!-- separacion a la derecha -->
	</jstl:if>

	<input type="button" name="cancel"
		value="<spring:message code="curriculum.edit.cancel" />"
		onclick="javascript: relativeRedir('curriculum/ranger/display.do');" />
	<!-- RELATIVEREDIR ES QE ME REDIRIJE A LA URL QUE HAYA PUESTO  -->
	<br />

</form:form>
<!-- FIN DE CREACION DE FORMULARIO -->