<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form modelAttribute="tagKey" action="tagkey/administrator/edit.do">
	<form:hidden path="id" />
	<form:hidden path="version" />

	<form:label path="name"> 
		<spring:message code="tagkey.edit.name"/>:	
	</form:label>
	<form:input path="name" />
	
	<spring:message code="tagkey.edit.save" var="saveButton"/>
	<input type="submit" name="save" value="${saveButton}"/>
	
	<jstl:if test="${tagKey.getId() != 0}"> 
		<spring:message code="tagkey.edit.delete" var="deleteButton"/>	
		<spring:message code="tagkey.edit.deleteMessage" var="deleteMessage"/>	
		<input type="submit" name="delete" value="${deleteButton}"  onclick="return confirm('${deleteMessage}')"/>
	</jstl:if>
	
	<spring:message code="tagkey.edit.cancel" var="backButton"/>
	<input type="button" value="${backButton}" onclick="javascript: relativeRedir('tagkey/administrator/list.do');">

	<br>
	<form:errors path="name" cssClass="error"/>
</form:form>
