<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<div>
	<a href="/Acme-Explorer/"> <img class="banner" src="${banner}"
		alt="Acme Explorer" />
	</a>
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->

		<security:authorize access="hasRole('ADMINISTRATOR')">
			<li><a class="fNiv"><spring:message
						code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="trip/list.do"><spring:message
								code="master.page.trip.list" /></a></li>
					<li><a href="category/list.do"><spring:message
								code="master.page.category.list" /></a></li>
					<li><a href="dashboard/display.do"><spring:message
								code="master.page.administrator.dashboard" /></a></li>
					<li><a href=""><spring:message
								code="master.page.administrator.createAccount" /></a>
						<ul>
							<li><a href="ranger/administrator/create.do"><spring:message
										code="master.page.actor.ranger" /></a></li>
							<li><a href="manager/administrator/create.do"><spring:message
										code="master.page.manager" /></a></li>
							<li><a href="auditor/administrator/create.do"><spring:message
										code="master.page.auditor" /></a></li>
							<li><a href="sponsor/administrator/create.do"><spring:message
										code="master.page.sponsor" /></a></li>
						
						</ul></li>

					<li><a href="legalText/list.do"><spring:message
								code="master.page.administrator.legalText" /></a></li>
					<li><a href="tagkey/administrator/list.do"><spring:message
								code="master.page.administrator.tagkeys" /></a></li>
					<li><a href="administrator/administrator/suspicious.do"><spring:message
								code="master.page.administrator.suspiciousActors" /></a></li>
					<li><a href="configuration/display.do"><spring:message
								code="master.page.administrator.configuration" /></a></li>
				</ul></li>
		</security:authorize>

		<security:authorize access="hasRole('MANAGER')">
			<li><a class="fNiv"><spring:message
						code="master.page.manager" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="trip/manager/list.do"><spring:message
								code="master.page.trip.list" /></a></li>
					<li><a href="category/list.do"><spring:message
								code="master.page.category.list" /></a></li>
					<li><a href="applyfor/manager/list.do"><spring:message
								code="master.page.manager.applications" /></a></li>
					<li><a href="legalText/list.do"><spring:message
								code="master.page.administrator.legalText" /></a></li>
					<li><a href="note/manager/list.do"><spring:message
								code="master.page.manager.notes" /></a></li>
					<li><a href="survivalClass/manager/list.do"><spring:message
								code="master.page.manager.survivalClass" /></a></li>
					
				</ul></li>
		</security:authorize>

		<security:authorize access="hasRole('EXPLORER')">
			<li><a class="fNiv"><spring:message
						code="master.page.explorer" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="trip/explorer/list.do"><spring:message
								code="master.page.trip.list" /></a></li>
					<li><a href="category/list.do"><spring:message
								code="master.page.category.list" /></a></li>
					<li><a href="applyfor/explorer/list.do"><spring:message
								code="master.page.explorer.applications" /></a></li>
					<li><a href="story/explorer/list.do"><spring:message
								code="master.page.explorer.stories" /></a></li>
					<li><a href="survivalClass/explorer/mysurvivals.do"><spring:message
								code="master.page.explorer.mysurvivals" /></a></li>
					<li><a href="emergencyContact/list.do"><spring:message
								code="master.page.explorer.emergencyContact" /></a></li>
					
				</ul>
			</li>
		</security:authorize>

		<security:authorize access="hasRole('SPONSOR')">
			<li><a class="fNiv"><spring:message
						code="master.page.sponsor" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="trip/list.do"><spring:message
								code="master.page.trip.list" /></a></li>
					<li><a href="category/list.do"><spring:message
								code="master.page.category.list" /></a></li>
					<li><a href="sponsorship/sponsor/list.do"><spring:message
								code="master.page.sponsor.sponsorships" /></a></li>
					
				</ul>
			</li>
		</security:authorize>

		<security:authorize access="hasRole('AUDITOR')">
			<li><a class="fNiv"><spring:message
						code="master.page.auditor" /></a>
				<ul>
					<li><a href="trip/list.do"><spring:message
								code="master.page.trip.list" /></a></li>
					<li><a href="category/list.do"><spring:message
								code="master.page.category.list" /></a></li>
					<li><a href="auditrecord/auditor/list.do"><spring:message
								code="master.page.auditor.auditrecords" /></a></li>
					<li><a href="note/auditor/list.do"><spring:message
								code="master.page.auditor.notes" /></a></li>
					
				</ul></li>
		</security:authorize>
		
			<security:authorize access="hasRole('RANGER')">
			<li><a class="fNiv"><spring:message
						code="master.page.ranger" /></a>
				<ul>
					<li><a href="trip/list.do"><spring:message
								code="master.page.trip.list" /></a></li>
					<li><a href="category/list.do"><spring:message
								code="master.page.category.list" /></a></li>
					<li><a href="curriculum/ranger/display.do"><spring:message
								code="master.page.ranger.curriculum" /></a></li>
					
				</ul></li>
		</security:authorize>


		<security:authorize access="isAnonymous()">
			<li><a class="fNiv"><spring:message
						code="master.page.anonymous" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="security/login.do"><spring:message
								code="master.page.login" /></a></li>
					<li><a href=" "><spring:message
								code="master.page.administrator.createAccount" /></a>
						<ul>
							<li><a href="ranger/create.do"><spring:message
										code="master.page.actor.ranger" /></a></li>
							<li><a href="explorer/create.do"><spring:message
										code="master.page.explorer" /></a></li>
						</ul></li>
					<li><a href="trip/list.do"><spring:message
								code="master.page.trip.list" /></a></li>
					<li><a href="category/list.do"><spring:message
								code="master.page.category.list" /></a></li>
				</ul></li>
		</security:authorize>

<security:authorize access="isAuthenticated()">
			<li><a class="fNiv"> <spring:message
						code="master.page.profile" /> (<security:authentication
						property="principal.username" />)
			</a>
				<ul>
					<li class="arrow"></li>

					<security:authorize access="hasRole('ADMINISTRATOR')">
						<spring:url var="URLEdit"
							value="administrator/administrator/edit.do" />
					</security:authorize>

					<security:authorize access="hasRole('RANGER')">
						<spring:url var="URLEdit" value="ranger/ranger/edit.do" />
					</security:authorize>

					<security:authorize access="hasRole('AUDITOR')">
						<spring:url var="URLEdit" value="auditor/auditor/edit.do" />
					</security:authorize>

					<security:authorize access="hasRole('SPONSOR')">
						<spring:url var="URLEdit" value="sponsor/sponsor/edit.do" />
					</security:authorize>

					<security:authorize access="hasRole('MANAGER')">
						<spring:url var="URLEdit" value="manager/manager/edit.do" />
					</security:authorize>

					<security:authorize access="hasRole('EXPLORER')">
						<spring:url var="URLEdit" value="explorer/explorer/edit.do" />
					</security:authorize>


					<li><a href="${URLEdit}"><spring:message
								code="master.page.actor.edit" /></a></li>

					<li><a href="folder/list.do"><spring:message
								code="master.page.folder.list" /></a></li>
					
					<li><a href="j_spring_security_logout"><spring:message
								code="master.page.logout" /> </a></li>
				</ul></li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

