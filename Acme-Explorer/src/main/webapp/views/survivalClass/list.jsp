<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:useBean id="date" class="java.util.Date" />
<security:authorize access="hasRole('MANAGER')">
	<jstl:set var="uri"
		value="survivalClass/manager/list.do?tripId=${tripId}" />
</security:authorize>
<security:authorize access="hasRole('EXPLORER')">
	<jstl:set var="uri"
		value="survivalClass/explorer/list.do?tripId=${tripId}" />
</security:authorize>

<security:authorize access="hasAnyRole('EXPLORER', 'MANAGER')">
	<display:table name="survivalClasses" id="row" requestURI="${uri}"
		pagesize="5">
		<security:authorize access="hasRole('MANAGER')">
			<spring:url value="trip/manager/display.do?tripId=${row.trip.id}"
				var="URI" />
		</security:authorize>

		<security:authorize access="hasRole('EXPLORER')">
			<spring:url value="trip/explorer/display.do?tripId=${row.trip.id}"
				var="URI" />
		</security:authorize>

		<spring:message code="survival.list.tickerHeader" var="tickerHeader" />
		<display:column title="${tickerHeader}" sortable="true">
			<a href="${URI}"> <jstl:out value="${row.getTrip().getTicker()} " />
			</a>
		</display:column>
		<spring:message code="survival.list.titleHeader" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" />

		<spring:message code="survival.list.descriptionHeader"
			var="descriptionHeader" />
		<display:column property="description" title="${descriptionHeader}"
			sortable="false" />

		<spring:message code="survival.list.momentHeader" var="momentHeader" />
		<display:column title="${momentHeader}" sortable="true">
			<spring:message code="survival.create.dateFormat" var="formatDate" />
			<fmt:formatDate value="${row.getMoment()}" pattern="${formatDate}" />
		</display:column>

		<spring:message code="survival.list.latitudeHeader"
			var="locationHeader" />
		<display:column property="location.latitude" title="${locationHeader}"
			sortable="false" />

		<spring:message code="survival.list.logitudeHeader"
			var="longitudeHeader" />
		<display:column property="location.longitude"
			title="${longitudeHeader}" sortable="false" />

		<spring:message code="survival.list.coordinates.name"
			var="coordinatesName" />
		<display:column property="location.name" title="${coordinatesName}"
			sortable="true" />

		<security:authorize access="hasRole('MANAGER')">

			<display:column title="" sortable="false">
				<a
					href="<spring:url value="survivalClass/manager/edit.do?survivalClassId=${row.getId()}"/>">
					<spring:message code="survival.list.edit" />
				</a>
			</display:column>
		</security:authorize>

		<jstl:if test="${not empty param.tripId}">
			<display:column title="" sortable="false">
				<security:authorize access="hasRole('EXPLORER')">
					<jstl:if test="${not row.explorers.contains(principal)}">
						<jstl:forEach var="apply" items="${principal.getApplications()}">
							<jstl:if
								test="${(apply.trip.id == param.tripId) and apply.status.status == 'ACCEPTED'}">
								<a
									href="<spring:url value="/survivalClass/explorer/join.do?survivalClassId=${row.id}" />"><spring:message
										code="survival.list.join" /></a>
							</jstl:if>
						</jstl:forEach>
					</jstl:if>

				</security:authorize>
			</display:column>
		</jstl:if>
		<security:authorize access="hasRole('EXPLORER')">
			<jstl:if test="${empty param.tripId}">
				<display:column title="" sortable="false">
					<a
						href="<spring:url value="/survivalClass/explorer/unjoin.do?survivalClassId=${row.id}" />"><spring:message
							code="survival.list.unjoin" /></a>
				</display:column>
			</jstl:if>
		</security:authorize>
	</display:table>



</security:authorize>
<%-- <security:authorize access="hasRole('MANAGER')"> --%>
<%-- 	<jstl:choose> --%>
<%-- 		<jstl:when test="${empty param.tripId}"> --%>
<%-- 			<jstl:set var="uriBack" value="trip/manager/list.do" /> --%>
<%-- 		</jstl:when> --%>
<%-- 		<jstl:otherwise> --%>
<%-- 			<jstl:set var="uriBack" --%>
<%-- 				value="trip/manager/display.do?tripId=${param.tripId}" /> --%>
<%-- 		</jstl:otherwise> --%>
<%-- 	</jstl:choose> --%>
<%-- </security:authorize> --%>

<%-- <security:authorize access="hasRole('EXPLORER')"> --%>
<%-- 	<jstl:choose> --%>
<%-- 		<jstl:when test="${empty param.tripId}"> --%>
<%-- 			<jstl:set var="uriBack" value="trip/explorer/list.do" /> --%>
<%-- 		</jstl:when> --%>
<%-- 		<jstl:when test="${not empty param.tripId}"> --%>
<%-- 			<jstl:set var="uriBack" --%>
<%-- 				value="trip/explorer/display.do?tripId=${param.tripId}" /> --%>
<%-- 		</jstl:when> --%>
<%-- 	</jstl:choose> --%>
<%-- </security:authorize> --%>

<security:authorize access="hasRole('MANAGER')">
	<jstl:set var="uriBack" value="trip/manager/list.do" />
	<jstl:if test="${empty row.id}">
		<input type="button" name="back"
			value="<spring:message code="survival.list.back" />"
			onclick="javascript: relativeRedir('${uriBack}');" />
	</jstl:if>
</security:authorize>

<security:authorize access="hasRole('EXPLORER')">
	<jstl:set var="uriBack" value="trip/explorer/list.do" />
	<jstl:if test="${empty row.id}">
		<input type="button" name="back"
			value="<spring:message code="survival.list.back" />"
			onclick="javascript: relativeRedir('${uriBack}');" />
	</jstl:if>
</security:authorize>