<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:choose>
	<jstl:when test="${param.miscellaneousId==null}">
		<h1>
			<spring:message code="miscellaneous.ranger.create" />
		</h1>
		<jstl:set var="disable" value="false" />
	</jstl:when>
	<jstl:otherwise>
		<h1>
			<spring:message code="miscellaneous.ranger.edit" />
		</h1>
		<jstl:set var="disable" value="true" />
	</jstl:otherwise>
</jstl:choose>


<form:form action="miscellaneous/ranger/edit.do"
	modelAttribute="miscellaneous">

	<!-- ATRIBUTOS QUE NO SE VAN A MODIFICAR -->
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curriculum" />

	<!-- ATRIBUTOS QUE VAMOS A MODIFICAR -->

	<!-- TITLE -->
	<form:label path="title">
		<spring:message code="miscellaneous.title" />:
	</form:label>
	<form:input path="title" />
	<form:errors cssClass="error" path="title" />

	<br />
	<br />
	<!-- ATTATCHMENT -->
	<form:label path="attachment">
		<spring:message code="miscellaneous.attachment" />:
	</form:label>
	<form:input path="attachment" />
	<form:errors cssClass="error" path="attachment" />

	<br />
	<br />
	<!-- COMMENTS -->
	<form:label path="comments">
		<spring:message code="miscellaneous.comments" />:
	</form:label>
	<form:input path="comments" />
	<form:errors cssClass="error" path="comments" />

	<br />
	<br />
	<!-- PARA EL SALTO DE LINEA -->

	<!-- BOTONES -->

	<!-- BOTON DE SAVE -->
	<input type="submit" name="save"
		value="<spring:message code="miscellaneous.save"/>" />
	<!-- PREGUNTAMOS POR LOS CASOS QUE TENEMOS -->
	<!-- VAMOS A EDITARLO -->
	<jstl:if test="${miscellaneous.id !=0}">
		<!-- QUIERE DECIR QUE HAY OBJETOS  -->
		<!-- PODEMOS PONER BOTON DE BORRAR -->
		<input type="submit" name="delete"
			value="<spring:message code="miscellaneous.delete" />"
			onclick="javascript: return confirm('<spring:message code="miscellaneous.confirm.delete" />')" />
		&nbsp;<!-- separacion a la derecha -->
	</jstl:if>

	<input type="button" name="cancel"
		value="<spring:message code="miscellaneous.cancel" />"
		onclick="javascript: <('miscellaneous/ranger/list.do');" />
	<!-- RELATIVEREDIR ES QE ME REDIRIJE A LA URL QUE HAYA PUESTO  -->
	<br />

</form:form>
<!-- FIN DE CREACION DE FORMULARIO -->