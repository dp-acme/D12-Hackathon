<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<security:authorize access="hasAnyRole('MANAGER','AUDITOR')">

	<jstl:if test="${not empty note.getManagerReply()}">
		<spring:message code="note.edit.replied" />
	</jstl:if>
	<jstl:if test="${empty note.getManagerReply()}">
	
		<security:authorize access="hasRole('AUDITOR')">
			<spring:url value="/note/auditor/create.do" var="formEdit" />
		</security:authorize>
		
		<security:authorize access="hasRole('MANAGER')">
					<spring:url value="/note/manager/edit.do" var="formEdit" />
		</security:authorize>	

			<form:form method="POST" action="${formEdit}" modelAttribute="note">


			<form:hidden path="id" />
			<form:hidden path="version" />
			<form:hidden path="trip" />
			<form:hidden path="auditor" />
			<form:hidden path="timeAuditor" />
			<form:hidden path="timeManager"/>
			
			<security:authorize access="hasRole('MANAGER')">
			
			<form:hidden path="auditorRemark" />
			<form:label path="managerReply">
				<spring:message code="note.edit.managerReply"/>
			</form:label><br>
			<form:textarea path="managerReply" value="${edit.managerReply}"/>
			<form:errors path="managerReply" cssClass="error"/><br>
			
			</security:authorize>
		
			<security:authorize access="hasRole('AUDITOR')">
			
			<form:hidden path="managerReply" />
			<form:label path="auditorRemark">
				<spring:message code="note.edit.auditorRemark"/>
			</form:label>
			<form:textarea path="auditorRemark" value="${edit.auditorRemark}"/><br>
			<form:errors path="auditorRemark" cssClass="error"/><br>
			
			
			</security:authorize>
			
			<spring:message code="note.edit.save" var="editSave" />
			<input type="submit" name="save" value="${editSave}" />
			<security:authorize access="hasRole('AUDITOR')">
				<jstl:set value="auditor" var="role"/>
			</security:authorize>	
			<security:authorize access="hasRole('MANAGER')">
				<jstl:set value="manager" var="role"/>
			</security:authorize>	
			<spring:message code="note.edit.cancel" var="editCancel"/>
			<input type="button" value="${editCancel}" 
			onClick="javascript:relativeRedir('note/${role}/list.do');" />
		</form:form>
	</jstl:if>
</security:authorize>