package services;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import utilities.AbstractTest;
import domain.Curriculum;
import domain.Endorser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class EndorserServiceTest extends AbstractTest{
	
	@Autowired 
	private EndorserService endorserService;
	
	@Autowired 
	private CurriculumService curriculumService;
	


	@SuppressWarnings("unused")
	@Test
	public void saveNewEndorserRecordAuth(){
		List<Curriculum> curriculums;
		Curriculum curriculum;
		Endorser result;
		Date startDate;
		
		startDate = new Date(System.currentTimeMillis() - 1);
		this.authenticate("ranger1");
		curriculums = (List<Curriculum>) curriculumService.findAll();
		curriculum = null;
		for(Curriculum c : curriculums){
			if(c.getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				curriculum = c;
			}
		}
		
		result = endorserService.create(curriculum);
		
		result.setFullName("Fullname");
		result.setEmail("test@test.es");
		result.setPhone("698547859");
		result.setProfile("http://www.test.es");
		
		result = endorserService.save(result);
		
		Assert.isTrue(endorserService.findAll().contains(result));
		Assert.isTrue(curriculumService.findOne(curriculum.getId()).getEndorserRecords().contains(result));
		
		this.authenticate(null);

	}
	
	@SuppressWarnings("unused")
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void saveNewEndorserRecordBadAuth1(){
		List<Curriculum> curriculums;
		Curriculum curriculum;
		Endorser result;
		Date startDate;
		
		startDate = new Date(System.currentTimeMillis() - 1);
		this.authenticate("ranger1");
		curriculums = (List<Curriculum>) curriculumService.findAll();
		curriculum = null;
		
		for(Curriculum c : curriculums){
			if(!c.getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				curriculum = c;
			}
		}
		
		result = endorserService.create(curriculum);
		
		result.setFullName("Fullname");
		result.setEmail("test@test.es");
		result.setPhone("698547859");
		result.setProfile("http://www.test.es");
		
		result = endorserService.save(result);
		
		Assert.isTrue(endorserService.findAll().contains(result));
		Assert.isTrue(curriculumService.findOne(curriculum.getId()).getEndorserRecords().contains(result));
		
		this.authenticate(null);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void editEndorserRecordAuth(){
		List<Curriculum> curriculums;
		List<Endorser> endorsers;
		Curriculum curriculum;
		Endorser result;
	
		this.authenticate("ranger1");
		curriculums = (List<Curriculum>) curriculumService.findAll();
		endorsers = (List<Endorser>) endorserService.findAll();
		
		curriculum = null;
		for(Curriculum c : curriculums){
			if(c.getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				curriculum = c;
			}
		}
		
		result = curriculum.getEndorserRecords().iterator().next(); //Pertenece a ranger1
		
		result.setFullName("Fullname");
		result.setEmail("test@test.es");
		result.setPhone("698547859");
		result.setProfile("http://www.test.es");
		
		result = endorserService.save(result);
		
		Assert.isTrue(endorserService.findAll().contains(result));
		Assert.isTrue(curriculumService.findOne(curriculum.getId()).getEndorserRecords().contains(result));
		
		this.authenticate(null);
	}
	
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void editEndorserRecordBadAuth(){
		List<Endorser> endorsers;
		Endorser result;
	
		this.authenticate("ranger1");
		endorsers = (List<Endorser>) endorserService.findAll();
		
		result  = null;
		for(Endorser e : endorsers){
			if(!e.getCurriculum().getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				result = e;
			}
		}
				
		result = endorserService.save(result);
		
		this.authenticate(null);
	}
	
	
	@Test
	public void deleteEndorserRecordAuth(){
		List<Curriculum> curriculums;
		Curriculum curriculum;
		Endorser result;
		
		this.authenticate("ranger1");
		curriculums = (List<Curriculum>) curriculumService.findAll();
		
		curriculum = null;
		for(Curriculum c : curriculums){
			if(c.getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				curriculum = c;
			}
		}
		
		result = curriculum.getEndorserRecords().iterator().next();
		
		endorserService.delete(result);
		
		Assert.isTrue(!endorserService.findAll().contains(result));
		Assert.isTrue(!curriculumService.findOne(curriculum.getId()).getEndorserRecords().contains(result));
		
		this.authenticate(null);
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void deleteEndorserRecordBadAuth(){
		List<Endorser> endorsers;
		Endorser result;
		
		this.authenticate("ranger1");
		endorsers = (List<Endorser>) endorserService.findAll();
		result  = null;
		
		for(Endorser e : endorsers){
			if(!e.getCurriculum().getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				result = e;
			}
		}
		
		endorserService.delete(result);
		
		this.authenticate(null);
	}
		
	@Test
	public void findAllEndorserRecordTest(){
		List<Endorser> auditRecords;
		
		auditRecords = (List<Endorser>) endorserService.findAll();

		Assert.notNull(auditRecords);
		
	}
	
	@Test
	public void findOneEndorserRecordTest(){
		List<Endorser> endorsers;
		Endorser endorser;
		
		endorsers = (List<Endorser>) endorserService.findAll();
		Assert.notNull(endorsers);
		endorser = endorsers.get(0);

		Assert.isTrue(endorserService.findAll().contains(endorserService.findOne(endorser.getId())));
	}
}