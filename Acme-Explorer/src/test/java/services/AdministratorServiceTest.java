package services;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Administrator;
import domain.Manager;
import domain.Ranger;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class AdministratorServiceTest  extends AbstractTest{

	@Autowired 
	private AdministratorService administratorService;

	
	@Test
	public void testFindAll(){
		this.authenticate("admin");

		Collection<Administrator> result;
		
		result = administratorService.findAll();
		
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testFindOne(){
		this.authenticate("admin");

		List<Administrator> all;
		Administrator administratorFromList, administratorFromRepository;
		
		all = (List<Administrator>) administratorService.findAll();
		Assert.notEmpty(all);
		administratorFromList = all.get(0);
		
		administratorFromRepository = administratorService.findOne(administratorFromList.getId());
		
		Assert.notNull(administratorFromRepository);
		Assert.isTrue(administratorFromList.equals(administratorFromRepository));
		
		this.unauthenticate();
	}
	
	@Test
	public void testAvgMaxMinSdApplicationOfTrips(){
		this.authenticate("admin");
		
		Double[] result;
		
		result = administratorService.getAvgMaxMinSdApplicationOfTrips();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testAvgMaxMinSdManagersOfTrips(){
		this.authenticate("admin");
		
		Double[] result;
		
		result = administratorService.getAvgMaxMinSdManagersOfTrips();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testAvgMaxMinSdPriceOfTrips(){
		this.authenticate("admin");
		
		Double[] result;
		
		result = administratorService.getAvgMaxMinSdPriceOfTrips();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testAvgMaxMinSdRangersOfTrips(){
		this.authenticate("admin");
		
		Double[] result;
		
		result = administratorService.getAvgMaxMinSdRangersOfTrips();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testRatioPendingApplications(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfPendingApplications();
				
		Assert.notNull(result);
		
		this.unauthenticate();		
	}
	
	@Test
	public void testRatioDueApplications(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfDueApplications();
				
		Assert.notNull(result);
		
		this.unauthenticate();		
	}
	
	@Test
	public void testRatioAcceptedApplications(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfAcceptedApplications();
				
		Assert.notNull(result);
		
		this.unauthenticate();		
	}
	
	@Test
	public void testRatioCancelledApplications(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfCancelledApplications();
				
		Assert.notNull(result);
		
		this.unauthenticate();		
	}
	
	@Test
	public void testRatioCancelledTrips(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfCancelledTrips();
				
		Assert.notNull(result);
		
		this.unauthenticate();		
	}
	
	@Test
	public void testTripsOver10PercentOverAvgApplications(){
		this.authenticate("admin");
		
		Collection<Trip> result;
		
		result = administratorService.getTripsOver10PercentOverAvgApplications();
				
		Assert.notNull(result);
		
		this.unauthenticate();		
	}
	
	@Test
	public void testLegalTextNumberOfTrips(){
		this.authenticate("admin");
		
		Collection<Object[]> result;
		
		result = administratorService.getLegalTextNumberOfTrips();
				
		Assert.notNull(result);
		
		this.unauthenticate();		
	}
	
	@Test
	public void testAvgMaxMinSdNotesOfTrips(){
		this.authenticate("admin");
		
		Double[] result;
		
		result = administratorService.getAvgMaxMinSdNotesOfTrips();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testAvgMaxMinSdAuditRecordsOfTrips(){
		this.authenticate("admin");
		
		Double[] result;
		
		result = administratorService.getAvgMaxMinSdAuditRecordsOfTrips();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testRatioOfTripsWithAuditRecords(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfTripsWithAuditRecords();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testRatioOfRangersWithCurriculum(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfRangersWithCurriculum();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testRatioOfRangersWithEndorsedCurriculum(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfRangersWithEndorsedCurriculum();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testRatioOfSuspiciousRangers(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfSuspiciousRangers();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testRatioOfSuspiciousManagers(){
		this.authenticate("admin");
		
		Double result;
		
		result = administratorService.getRatioOfSuspiciousManagers();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testSuspiciousRangers(){
		this.authenticate("admin");
		
		Collection<Ranger> result;
		
		result = administratorService.getSuspiciousRangers();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testSuspiciousManagers(){
		this.authenticate("admin");
		
		Collection<Manager> result;
		
		result = administratorService.getSuspiciousManagers();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testSuspiciousActors(){
		this.authenticate("admin");
		
		Collection<Actor> result;
		
		result = administratorService.getSuspiciousActors();
				
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	//Operaciones específicas
}
