package services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.TagKey;
import domain.TagValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml" })
@Transactional

public class TagKeyServiceTest extends AbstractTest {
	
	@Autowired
	private TagKeyService tagKeyService;
	
	@Autowired
	private TagValueService tagValueService;
	
	@Test
	public void testSaveNewTagKey(){
		TagKey result;
		List <TagKey> tagKeys;
		
		super.authenticate("admin");
		result = tagKeyService.create();
		result.setName("testTagKey");
		result= tagKeyService.save(result);
		tagKeys = new ArrayList<TagKey>(tagKeyService.findAll());
		Assert.isTrue(tagKeys.contains(result));
		super.authenticate(null);

	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void testSaveNewTagKeyBadAuth(){
		TagKey result;
		List <TagKey> tagKeys;
		
		super.authenticate("ranger1");
		result = tagKeyService.create();
		result.setName("testTagKey");
		result= tagKeyService.save(result);
		tagKeys = new ArrayList<TagKey>(tagKeyService.findAll());
		Assert.isTrue(tagKeys.contains(result));
		super.authenticate(null);

	}
	
	@Test
	public void testSaveExistingTagKey(){
		TagKey result, savedTagKey;
		List <TagKey> tagKeys;
		
		super.authenticate("admin");
		tagKeys = new ArrayList<TagKey>(tagKeyService.findAll());
		result = tagKeys.get(0);

		result.setName("testNameExistingTagKey");
		tagKeyService.save(result);
		
		savedTagKey = tagKeyService.findOne(result.getId());
		Assert.isTrue(savedTagKey.getName().equals("testNameExistingTagKey"));
		super.authenticate(null);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void testSaveExistingTagKeyBadAuth(){
		TagKey result, savedTagKey;
		List <TagKey> tagKeys;
		
		super.authenticate("ranger1");
		tagKeys = new ArrayList<TagKey>(tagKeyService.findAll());
		result = tagKeys.get(0);

		result.setName("testNameExistingTagKey");
		tagKeyService.save(result);
		
		savedTagKey = tagKeyService.findOne(result.getId());
		Assert.isTrue(savedTagKey.getName().equals("testNameExistingTagKey"));
		super.authenticate(null);
	}
	
	@Test
	public void testFindAllTagKey(){
		List <TagKey> tagkeys;
		tagkeys = new ArrayList<TagKey>(tagKeyService.findAll());
		Assert.isTrue(tagkeys.size()==3);
	}
	
	@Test
	public void testFindOneTagKey(){
		TagKey tagKey;
		List <TagKey> tagkeys;
		tagkeys = new ArrayList<TagKey>(tagKeyService.findAll());
		tagKey = tagKeyService.findOne(tagkeys.get(0).getId());
		Assert.isTrue(tagKeyService.findAll().contains(tagKey));
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void testDeleteTagKeyBadAuth(){
		
		super.authenticate("ranger1");
		TagKey tagKey;
		TagKey result;
		List <TagValue> tagValues;

		tagValues = new ArrayList<TagValue>(tagValueService.findAll());
		result = null;
		tagKey = tagValues.get(0).getTagKey();
		
		tagKeyService.delete(tagKey);
		Assert.isTrue(!tagValueService.findAll().contains(tagValues));
		Assert.isTrue(!tagKeyService.findAll().contains(result));
		super.authenticate(null);

	}
	@Test
	public void testDeleteTagKey(){
		
		super.authenticate("admin");
		TagKey tagKey;
		TagKey result;
		List <TagValue> tagValues;

		tagValues = new ArrayList<TagValue>(tagValueService.findAll());
		result = null;
		tagKey = tagValues.get(0).getTagKey();
		
		tagKeyService.delete(tagKey);
		Assert.isTrue(!tagValueService.findAll().contains(tagValues));
		Assert.isTrue(!tagKeyService.findAll().contains(result));
		super.authenticate(null);

	}
	
}
