
package services;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Manager;
import domain.Ranger;
import domain.SocialIdentity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class SocialIdentityServiceTest extends AbstractTest {

	//Service under test
	@Autowired
	private SocialIdentityService	socialIdentityService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private ManagerService			managerService;


	@Test
	public void testCreate() { // Probamos con cualquier actor en este caso lo hemos hecho con manager
		SocialIdentity socialIdentity;
		Actor manager;
		List<Actor> listActors = (List<Actor>) this.actorService.findAll();

		manager = listActors.get(0);
		socialIdentity = socialIdentityService.create(manager.getId());

		// Solo inicializamos los atributos 'importantes' 
		socialIdentity.setActor(manager);

		socialIdentity.setLink(null);
		socialIdentity.setNetName(null);
		socialIdentity.setNick(null);
		socialIdentity.setPhoto(null);

	}

	// Hacemos que un actor en este caso manager este logeado.
	private Actor actorLogged() {
		Actor res;
		List<Manager> listManager;
		Manager manager;

		listManager = (List<Manager>) this.managerService.findAll();
		manager = listManager.get(0);
		this.authenticate("manager1");

		Assert.isTrue(manager.getUserAccount().getUsername().equals("manager1"));

		res = manager;

		return res;
	}

	@Test
	public void testSave() {
		SocialIdentity socialIdentity, saved;
		Collection<SocialIdentity> socialIdentities;
		Actor manager;

		manager = this.actorLogged();
		socialIdentity = this.socialIdentityService.create(manager.getId());

		socialIdentity.setActor(manager);
		socialIdentity.setLink("https://www.google.es/");
		socialIdentity.setNetName("Javier");
		socialIdentity.setNick("Javi");
		socialIdentity.setPhoto("https://www.google.es/");

		saved = this.socialIdentityService.save(socialIdentity); // guardamos en el repositorio el socialIdentity creado
		socialIdentities = this.socialIdentityService.findAll(); // cogemos todos los socialIdentities 

		Assert.isTrue(socialIdentities.contains(saved)); // comprobamos que el socialIdentity que hemos creado existe en la BD
	}

	@Test
	public void testDelete() {
		List<SocialIdentity> listSocialIdentities;
		Ranger ranger;
		SocialIdentity socialId;
		
		super.authenticate("ranger1");
		ranger = (Ranger) actorService.findPrincipal();
		if(ranger.getSocialIdentities().size()!=0){
			socialId = (SocialIdentity) ranger.getSocialIdentities().toArray()[0];
			this.socialIdentityService.delete(socialId);
			
			listSocialIdentities = (List<SocialIdentity>) this.socialIdentityService.findAll();
			Assert.isTrue(!listSocialIdentities.contains(socialId));
		}
	}

	@Test
	public void testFindOne() {// Lo cogemos del populate gracias al id asi que puede cambiar.
		List<SocialIdentity> socialIdentities;
		SocialIdentity socialIdentity;

		socialIdentities = (List<SocialIdentity>) this.socialIdentityService.findAll();
		socialIdentity = socialIdentities.get(0);
		this.socialIdentityService.findOne(socialIdentity.getId());

		Assert.notNull(socialIdentity);

		;
	}

	@Test
	public void testFindAll() {

		Collection<SocialIdentity> result;

		result = this.socialIdentityService.findAll();

		Assert.notNull(result);

	}
}
