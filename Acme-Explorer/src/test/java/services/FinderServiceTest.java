package services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import utilities.AbstractTest;
import domain.Explorer;
import domain.Finder;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class FinderServiceTest extends AbstractTest{

	@Autowired 
	private FinderService finderService;
	
	@Autowired 
	private ActorService actorService;
	
	@Autowired 
	private ExplorerService explorerService;
	
	@Autowired 
	private TripService tripService;
	
	@Test
	public void saveNewFinderTest(){
		Finder result;
		Trip trip;
		double startPrice, endPrice;
		
		this.authenticate("explorer1");
		
		result = finderService.create();
		
		trip = tripService.findAll().iterator().next();
		startPrice = trip.getPrice() - 1;
		endPrice = trip.getPrice() + 100;

		result.setKeyWord("Title");
		result.setPriceStart(startPrice);
		result.setPriceEnd(endPrice);

		result = finderService.save(result);
		
		Assert.notNull(finderService.findOne(result.getId()));
	}
	
	@Test
	public void editFinderTestAuth(){
		Finder result;
		Explorer explorer;

		this.authenticate("explorer1");
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		explorerService.createExplorerFinder(explorer);
		
		result   = explorer.getFinder();
				
		result.setKeyWord("Title");

		result = finderService.save(result);
	}
	
	@Test
	public void findAllFinderTest(){
		List<Finder> finders;
		
		finders = (List<Finder>) finderService.findAll();

		Assert.notNull(finders);	
	}
	
	@Test
	public void findOneFinderTest(){
		Explorer explorer;
		List<Finder> finders;
		Finder record;
		
		explorer = ((ArrayList<Explorer>)explorerService.findAll()).get(0);
		
		explorerService.createExplorerFinder(explorer);
		
		finders = (List<Finder>) finderService.findAll();
		Assert.notNull(finders);
		record = finders.get(0);

		Assert.isTrue(finderService.findAll().contains(finderService.findOne(record.getId())));
	}
	
	
}
