package sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class GenericTest {
	private static String getDate(){
		String year, month, day;
		
		year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2); //Suponiendo que el a�o tiene 4 d�gitos (Cambiar implementaci�n para el a�o 10.000)
		month = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);
		
		if(month.length() == 1){ //Asegurar que el mes tiene longitud 2
			month = '0' + month;
		}
		
		day = String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

		if(day.length() == 1){ //Asegurar que el d�a tiene longitud 2
			day = '0' + day;
		}
		
		return year + month + day;
	}
	
	private static String generateTicker(int id){
		String res;
		List<Character> alphabeth;
		
		res = getDate() + "-";
		
		alphabeth = Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
		     'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
		     'U', 'V', 'W', 'X', 'Y', 'Z');
		
		id %= Math.pow(alphabeth.size(), 4);
		
		for(int i = 0; i < 4; i ++){
			res += alphabeth.get(id%alphabeth.size());
			id = (id - id%alphabeth.size())/alphabeth.size();
		}
		
		return res;
	}
	
	public static void main(String[] args) {
		List<String> tickers = new ArrayList<>();
		
		for(int i = 0; i < 100000; i ++){
			tickers.add("ID: " + i + " -> Ticker: " + generateTicker(i));
		}
				
		System.out.println(tickers);
		
		System.out.println("Distinct tickers: " + tickers.size());
		System.out.println(generateTicker(0));
	}

}
