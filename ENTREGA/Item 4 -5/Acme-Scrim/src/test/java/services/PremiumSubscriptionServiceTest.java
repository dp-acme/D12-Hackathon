package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.CreditCard;
import domain.PremiumSubscription;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class PremiumSubscriptionServiceTest extends AbstractTest {
	
	@Autowired
	private PremiumSubscriptionService premiumSubscriptionService;
	
	@Autowired
	private ActorService actorService;
	
/*
18. An actor who is authenticated as a player must be able to:
	a. Ask the administrator for a referee or manager account. 
	   The administrator will then give them a personal permission to register the new account. That permission will have an expiration date.
	b. Pay with a credit card in order to get a premium service and don't see any of the webpage ads.
*/
	
	@Test
	public void driverCreate() {
		CreditCard cc, cc2;
		
		cc = new CreditCard();
		cc.setBrandName("brand");
		cc.setCvv(450);
		cc.setExpirationMonth(3);
		cc.setExpirationYear(20);
		cc.setHolderName("Bob Esponja");
		cc.setNumber("4581662445129279");
		
		cc2 = new CreditCard();
		cc2.setBrandName("brand2");
		cc2.setCvv(450);
		cc2.setExpirationMonth(5);
		cc2.setExpirationYear(25);
		cc2.setHolderName("Patricio");
		cc2.setNumber("4892291315932770");
		
		Object[][] data = {
				{"player1", cc, null},

				{"player2", cc2, IllegalArgumentException.class},
				{"admin", cc, IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (CreditCard)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, CreditCard cc, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			PremiumSubscription premiumSubscription;
			
			premiumSubscription = premiumSubscriptionService.create();
			premiumSubscription.setCreditCard(cc);
			premiumSubscription.setPlayer(actorService.checkIsPlayer());
			
			premiumSubscriptionService.save(premiumSubscription);
			premiumSubscriptionService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
