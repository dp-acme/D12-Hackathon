package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.GameAccount;
import domain.Invitation;
import domain.Team;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class InvitationServiceTest extends AbstractTest {
	
	@Autowired
	private GameAccountService gameAccountService;
	
	@Autowired
	private InvitationService invitationService;
	
	@Autowired
	private TeamService teamService;

/*
10. The leader of a team may invite players that have a game account created in the videogame to it. 
	The system should store the following information regarding these invitations: a message, if it is accepted or rejected, and the moment when it is answered.
*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player1", "team1", "gameAccount7", "Comments", null},

				{"player2", "team1", "gameAccount7", "Comments", IllegalArgumentException.class},
				{"player1", "team1", "gameAccount7", null, ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3], (Class<?>)d[4]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"player1", "invitation1", null},

				//TODO: Poner casos negativos
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String teamBean, String accountBean, String comments, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Invitation invitation;
			GameAccount gameAccount;
			Team team;
			
			gameAccount = gameAccountService.findOne(super.getEntityId(accountBean));
			Assert.notNull(gameAccount);
			
			team = teamService.findOne(super.getEntityId(teamBean));
			Assert.notNull(team);
			
			invitation = invitationService.create(team);
			invitation.setTeammate(gameAccount);
			
			invitation.setMessage(comments);
			
			invitationService.save(invitation);
			invitationService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String invitationBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Invitation invitation;
			
			invitation = invitationService.findOne(super.getEntityId(invitationBean));
			Assert.notNull(invitation);
			
			teamService.kickTeammate(invitation.getTeam(), invitation.getTeammate());
			invitationService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
