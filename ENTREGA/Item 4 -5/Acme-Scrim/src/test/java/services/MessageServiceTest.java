package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Message;
import domain.Priority;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class MessageServiceTest extends AbstractTest {

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private MessageService messageService;
	
/*
15. A message system will be provided for all the users. Users will be able to send messages to other users containing the following 
	information: the sender, recipients, the priority (Low, Neutral, High), a title and the body. 
	Messages will be stored in folders. There are different types of folders: system folders 
	(Inbox, Outbox, Notificationbox, Spambox, Trashbox), and user folders, which may be arbitrarily nested by the user and have their own names.	
*/
	
	@Test
	public void driverSend() {
		Object[][] data = {
				{"player1", "subject", "body", Priority.NEUTRAL, null},

				{"player2", "subject", null, Priority.HIGH, IllegalArgumentException.class},
				{"player3", "", "", Priority.LOW, ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateSend((String)d[0], (String)d[1], (String)d[2], (String)d[3], (Class<?>)d[4]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"player1", "message1", null},

				{"player1", "message2", IllegalArgumentException.class},
				{"admin", "message1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	@Test
	public void driverMove() {
		Object[][] data = {
				{"player1", "message1", "folder2", null},

				{"player1", "message1", "folder6", IllegalArgumentException.class},
				{"player1", "message2", "folder2", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateMove((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	private void templateSend(String user, String subject, String body, String priority, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			messageService.sendMessage(subject, body, actorService.findAll(), priority);
			messageService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String messageBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Message message;
			message = messageService.findOne(super.getEntityId(messageBean));
			Assert.notNull(message);
			
			messageService.delete(message);
			messageService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateMove(String user, String messageBean, String newParentBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			messageService.moveMessage(super.getEntityId(messageBean), super.getEntityId(newParentBean));
			messageService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
