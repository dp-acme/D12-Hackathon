package services;

import java.util.ArrayList;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Invitation;
import domain.Team;
import domain.Videogame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class TeamServiceTest extends AbstractTest {

	
	@Autowired
	private TeamService teamService;

	@Autowired
	private VideogameService videogameService;
	
	@Autowired
	private RegionService regionService;
	
	@Autowired
	private GameAccountService gameAccountService;
	
	
	
/*	18. An actor who is authenticated as a player must be able to:
		d. Create a team and become the leader.*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player1","videogame2", "team","region3","gameAccount1", null},
				
				{"manager1","videogame2", "team","region3","gameAccount1",IllegalArgumentException.class},
				{"player2","videogame2", "","region3","gameAccount1",IllegalArgumentException.class}

		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (String)d[3],(String)d[4], (Class<?>)d[5]);
		}
	}
/*	19. The leader of a team must be able to:
		a. Modify the team data.*/
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"player1","team1", "teamName",null},
				{"player1", "team1","",ConstraintViolationException.class},
				{"manager1", "team1","teamName", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"player1", "team1", null} //Que solo lo pueda borrar el lider se comprueba en el controlador
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	
/*	16. An actor who is not authenticated must be able to:
			b. List the public teams and see their profiles.
			g. See the wins ranking of teams for each videogame and season.
			h. See the likes/dislikes ranking of teams for each videogame.
	 */
	@Test
	public void listAndDisplay(){
		Class<?> caught;

		caught = null;
		try{
			super.authenticate("manager1");
			Videogame videogame;
			Team	team;
			
			videogame = videogameService.findOne(super.getEntityId("videogame2"));
			
			team = (Team)teamService.getTeamsOfVideogame(videogame).toArray()[0];
			teamService.findOne(team.getId());
			teamService.getTeamsOrderedByLikes(videogame);
			teamService.getTeamsOrderedByWonGames(videogame);
			
		super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		
		}
		checkExceptions(null, caught);
	}


	
	private void templateCreate(String user,String videogameBean,String name,String region, String gameAccount,Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Team team;
			
			team = teamService.create();
			
			team.setName(name);
			team.setVideogame(videogameService.findOne(super.getEntityId(videogameBean)));
			team.setRegion(regionService.findOne(super.getEntityId(region)));
			team.setLeader(gameAccountService.findOne(super.getEntityId(gameAccount)));
			team.setInvitations(new ArrayList<Invitation>());

			teamService.save(team);
			
			super.unauthenticate();
			
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String teamBean, String newName, Class<?> expected) {
		Class<?> caught;

		Team team;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			team = teamService.findOne(super.getEntityId(teamBean));
			Assert.notNull(team);

			team.setName(newName);
			
			teamService.save(team);
			teamService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String teamBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Team team;
			team = teamService.findOne(super.getEntityId(teamBean));
			Assert.notNull(team);
			
			teamService.delete(team);
			teamService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
	
