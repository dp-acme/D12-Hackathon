package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Advertisement;
import domain.CreditCard;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class AdvertisementServiceTest extends AbstractTest {
	
	@Autowired
	private AdvertisementService advertisementService;
	
/*
14. Sponsors must provide a valid credit card in order to post a banner to advertise their webpages. 
	For each ad, it should include, as mentioned before, a valid credit card, an image, and a link to the desired webpage.
*/
	@Test
	public void driverCreate() {
		CreditCard cc, cc2;
		
		cc = new CreditCard();
		cc.setBrandName("brand");
		cc.setCvv(450);
		cc.setExpirationMonth(3);
		cc.setExpirationYear(20);
		cc.setHolderName("Bandolero");
		cc.setNumber("4581662445129279");
		
		cc2 = new CreditCard();
		cc2.setBrandName("brand2");
		cc2.setCvv(450);
		cc2.setExpirationMonth(5);
		cc2.setExpirationYear(25);
		cc2.setHolderName("Tragabuche");
		cc2.setNumber("4892291315932770");
		
		Object[][] data = {
				{"sponsor1", "http://www.web.com/image.png", cc, "http://www.web.com", null},

				{"player1", "http://www.web.com/image.png", cc2, "http://www.web.com", IllegalArgumentException.class},
				{"sponsor1", "", cc, null, ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (CreditCard)d[2], (String)d[3], (Class<?>)d[4]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"sponsor1", "advertisement1", null},

				{"sponsor1", "advertisement4", IllegalArgumentException.class},
				{"admin", "advertisement1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String image, CreditCard cc, String web, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Advertisement advertisement;
			
			advertisement = advertisementService.create();
			advertisement.setCreditCard(cc);
			advertisement.setImage(image);
			advertisement.setWeb(web);
			
			advertisementService.save(advertisement);
			advertisementService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String advertisementBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Advertisement advertisement;
			advertisement = advertisementService.findOne(super.getEntityId(advertisementBean));
			Assert.notNull(advertisement);
			
			advertisementService.delete(advertisement);
			advertisementService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
