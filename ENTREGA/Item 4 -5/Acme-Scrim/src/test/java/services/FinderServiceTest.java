package services;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Finder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class FinderServiceTest extends AbstractTest {

	
	@Autowired
	private VideogameService videogameService;

	@Autowired
	private FinderService finderService;
	
	@Autowired
	private RegionService regionService;
	
	
/*	19. The leader of a team must be able to:
		h. Enter the results of a finished scrim.*/
	@Test
	public void driverCreateAndSearch() {
		Object[][] data = {
				{"manager1", "videogame2","team", false, null},
				
				{"manager1", "videogame2","team",true,IllegalArgumentException.class},

		};
		
		for(Object[] d : data){
			templateCreateAndSearch((String)d[0], (String)d[1],(String)d[2],(Boolean)d[3], (Class<?>)d[4]);
		}
	}
	
	private void templateCreateAndSearch(String user,String videogame,String keyword,Boolean dates, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Finder finder;
			
			finder = finderService.create();
			
			finder.setVideogame(videogameService.findOne(super.getEntityId(videogame)));
			finder.setRegion(regionService.findOne(super.getEntityId("region1")));
			finder.setKeyWord(keyword);
			
			if(dates){
				finder.setDateEnd(new Date(System.currentTimeMillis()-50000));
				finder.setDateStart(new Date(System.currentTimeMillis()+50000));

			}
			
			finderService.save(finder);
			finderService.flush();
			finderService.filterScrimsByCriteria(finder);
			super.unauthenticate();
			
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
	
