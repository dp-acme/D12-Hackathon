package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Review;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ReviewServiceTest extends AbstractTest {

	
	@Autowired
	private ReviewService reviewService;
	
	
/*	19. The leader of a team must be able to:
		k. When a scrim is finished he/she can like or dislike players of the opponent team.
	16. An actor who is not authenticated must be able to:
		h. See the likes/dislikes ranking of teams for each videogame.*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player1", "scrim3", "team2", null},
				
				{"player6", "scrim3", "team2",IllegalArgumentException.class},
				{"manager1", "scrim3", "team2",IllegalArgumentException.class},

		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	private void templateCreate(String user, String scrimId,  String teamId, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Review review;
			
			review = reviewService.create(super.getEntityId(scrimId), super.getEntityId(teamId));
			Assert.notNull(review);

			review.setPositive(true);
			
			review= reviewService.save(review);
			reviewService.flush();
			
			super.unauthenticate();
			
			Assert.isTrue(reviewService.getLikesByTeam(super.getEntityId(teamId)).size()>0);


		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
}
	
