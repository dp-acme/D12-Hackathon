package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.GameCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class GameCategoryServiceTest extends AbstractTest {
	
	@Autowired
	private GameCategoryService gameCategoryService;

/*
4.	The taxonomy of game categories can be managed by the administrator. 
	The system should store the following information regarding these categories: their names, and their children categories. 
	There should be a default category at the top of the taxonomy that cannot be deleted or modified called "Videogame". 
	If a category is deleted, the videogames that had that category or one of the descendant will change it to its parent one.	
*/
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"admin", "gameCategory4", "Clones de Fortnite", null},

				{"player1", "gameCategory4", "Clones de Fortnite", IllegalArgumentException.class},
				{"admin", "gameCategory4", "", ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}

	@Test
	public void driverEdit() {
		Object[][] data = {
				{"admin","gameCategory2","NBA", null},
//				{"player1", "gameCategory2", "VIDEOGAME", IllegalArgumentException.class},
//				{"admin", "gameCategory4", "", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"admin", "gameCategory4", null},

				{"player1", "gameCategory4", IllegalArgumentException.class},
				{"sponsor1", "gameCategory4", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user, String parentBean, String name, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			GameCategory parent, gameCategory;
			
			if(parentBean != null){
				parent = gameCategoryService.findOne(super.getEntityId(parentBean));
				Assert.notNull(parent);
				
			} else{
				parent = null;
			}
			
			gameCategory = gameCategoryService.create(parent);
			gameCategory.setName(name);
			
			gameCategoryService.save(gameCategory);
			gameCategoryService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String gameCategoryBean, String name, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			GameCategory gameCategory;
			
			gameCategory = gameCategoryService.findOne(super.getEntityId(gameCategoryBean));
			gameCategory.setName(name);
			
			gameCategoryService.save(gameCategory);
			gameCategoryService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String gameCategoryBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			GameCategory gameCategory;
			
			gameCategory = gameCategoryService.findOne(super.getEntityId(gameCategoryBean));
			Assert.notNull(gameCategory);
			
			gameCategoryService.delete(gameCategory);
			gameCategoryService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
