package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Report;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ReportServiceTest extends AbstractTest {

	@Autowired
	private ReportService reportService;
	
/*	19. The leader of a team must be able to:
	 	j. When a scrim is finished he/she can report players of the opponent team.*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player1", "player3","scrim3","comments",null},
				
				{"player1", "player3","scrim2","",IllegalArgumentException.class},
				{"manager1", "player3","scrim3","comments",IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2],(String)d[3],(Class<?>)d[4]);
		}
	}
	
	
	private void templateCreate(String user, String playerId, String scrimId,String comments,Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Report report;
			
			report = reportService.create(super.getEntityId(playerId), super.getEntityId(scrimId));

			report.setComments("comments");
			
			reportService.save(report);
			reportService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
}
	
