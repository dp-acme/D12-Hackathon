package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Season;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class SeasonServiceTest extends AbstractTest {

	
	@Autowired
	private SeasonService seasonService;
	
	
	
/*	20. An actor who is authenticated as a manager must be able to:
		b. Finish a videogame season to start another one.*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"manager1", "videogame1", "newSeason", null},
				
				{"player6", "videogame3", "newSeason",IllegalArgumentException.class},
				{"manager2", "videogame1", "newSeason",IllegalArgumentException.class},

		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	private void templateCreate(String user, String videogameId,  String seasonName, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Season season;
			seasonService.finished((Season)seasonService.getLastSeasonOfVideogame(super.getEntityId(videogameId)));
			season = seasonService.create(super.getEntityId(videogameId));

			season.setName(seasonName);
			
			
			seasonService.save(season);
			seasonService.flush();
			
			super.unauthenticate();
			
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
}
	
