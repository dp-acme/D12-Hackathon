package services;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Result;
import domain.Scrim;
import domain.Season;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ScrimServiceTest extends AbstractTest {

	@Autowired
	private ScrimService scrimService;
	@Autowired
	private TeamService teamService;

	@Autowired
	private VideogameService videogameService;

	@Autowired
	private SeasonService seasonService;

	/*
	 * 19. The leader of a team must be able to: 
	 * d. Join a public scrim with the team. 
	 * e. Create a public scrim with the team.
	 */
	@Test
	public void driverCreateAndJoin() {
		Object[][] data = {
				{ "player1", "player3", "team1", "team2", "videogame2", null },
				{ "player2", "player3", "team1", "team2", "videogame2",
						IllegalArgumentException.class },
				{ "player1", "player1", "team1", "team2", "videogame2",
						IllegalArgumentException.class },

		};

		for (Object[] d : data) {
			templateCreateAndJoin((String) d[0], (String) d[1], (String) d[2],
					(String) d[3], (String) d[4], (Class<?>) d[5]);
		}
	}

	/*
	 * f. Create a scrim invitation directly to an opponent team. 
	 * g. Accept or reject a scrim invitation from other team
	 */
	@Test
	public void driverInviteAndJoin() {
		Object[][] data = {
				{ "player1", "player3", "team1", "team2", "videogame2", null },
				{ "player2", "player3", "team1", "team2", "videogame2",
						IllegalArgumentException.class },
				{ "player1", "player1", "team1", "team2", "videogame2",
						IllegalArgumentException.class },

		};

		for (Object[] d : data) {
			templateInviteAndJoin((String) d[0], (String) d[1], (String) d[2],
					(String) d[3], (String) d[4], (Class<?>) d[5]);
		}
	}
	
	/*
	 * 19. The leader of a team must be able to: 
	 * h. Enter the results of a finished scrim.
	 */
	@Test
	public void addResults() {
		Object[][] data = {
				{ "player1", "scrim3", "team1",null },
				{ "player2", "scrim3", "team1",
						IllegalArgumentException.class },
				{ "admin", "scrim3", "team1",
						IllegalArgumentException.class },

		};

		for (Object[] d : data) {
			templateAddResults((String) d[0], (String) d[1], (String) d[2],
					 (Class<?>) d[3]);
		}
	}
	

	private void templateCreateAndJoin(String userCreator, String userJoin,
			String teamId, String guestTeamId, String videogameId,
			Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(userCreator);

			Scrim scrim;

			scrim = scrimService.create(videogameService.findOne(super
					.getEntityId(videogameId)));
			Assert.notNull(scrim);

			scrim.setCreator(teamService.findOne(super.getEntityId(teamId)));
			scrim.setSeason(seasonService.getLastSeasonOfVideogame(super
					.getEntityId(videogameId)));
			scrim.setMoment(new Date(System.currentTimeMillis() + 5000000));
			scrimService.reconstructCreateEdit(scrim, null);

			scrim = scrimService.createScrimAnnouncement(scrim.getCreator(),
					scrim.getMoment(), scrim.getDesiredGuestRegion());
			scrimService.flush();

			scrim.setSeason((Season) seasonService.getLastSeasonOfVideogame(
					super.getEntityId(videogameId)));
			scrim.setMoment(new Date(System.currentTimeMillis() + 5000000));
			scrimService.reconstructCreateEdit(scrim, null);

			scrim = scrimService.createScrimAnnouncement(scrim.getCreator(),
					scrim.getMoment(), scrim.getDesiredGuestRegion());

			super.unauthenticate();
			super.authenticate(userJoin);
			scrim.setGuest(teamService.findOne(super.getEntityId(guestTeamId)));
			scrimService.acceptScrim(scrim, scrim.getGuest());

			scrim = scrimService.save(scrim);
			scrimService.flush();

			super.unauthenticate();

			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();

		} finally {
			super.rollbackTransaction();
		}

		checkExceptions(expected, caught);
	}

	
	private void templateInviteAndJoin(String userCreator, String userJoin,
			String teamId, String guestTeamId, String videogameId,
			Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(userCreator);

			Scrim scrim;

			scrim = scrimService.create(videogameService.findOne(super
					.getEntityId(videogameId)));
			Assert.notNull(scrim);

			scrim.setCreator(teamService.findOne(super.getEntityId(teamId)));
			scrim.setSeason(seasonService.getLastSeasonOfVideogame(super
					.getEntityId(videogameId)));
			scrim.setMoment(new Date(System.currentTimeMillis() + 5000000));
			scrimService.reconstructCreateEdit(scrim, null);
			scrim.setSeason((Season) seasonService
					.getLastSeasonOfVideogame(super.getEntityId(videogameId)));
			scrim.setMoment(new Date(System.currentTimeMillis() + 5000000));
			scrimService.reconstructCreateEdit(scrim, null);
			scrim.setGuest(teamService.findOne(super.getEntityId(guestTeamId)));

			scrim = scrimService.createScrimInvitation(scrim.getCreator(),
					scrim.getGuest(), scrim.getMoment());
			scrimService.flush();

			super.unauthenticate();
			super.authenticate(userJoin);
			scrim.setGuest(teamService.findOne(super.getEntityId(guestTeamId)));
			scrimService.acceptScrim(scrim, scrim.getGuest());

			scrim = scrimService.save(scrim);
			scrimService.flush();

			super.unauthenticate();

			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();

		} finally {
			super.rollbackTransaction();
		}

		checkExceptions(expected, caught);
	}
	
	private void templateAddResults(String user, String scrimBean,
			String teamBean,
			Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			Scrim scrim;
			Result result;
			
			result = new Result();
			
			result.setResult(Result.WIN);
			
			scrim = scrimService.findOne(super.getEntityId(scrimBean));
			scrimService.insertResult(scrim, teamService.findOne(super.getEntityId(teamBean)), result);
			scrimService.flush();

			super.unauthenticate();

			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();

		} finally {
			super.rollbackTransaction();
		}
		checkExceptions(expected, caught);

	}
}
