package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Folder;
import domain.FolderType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class FolderServiceTest extends AbstractTest {

	@Autowired
	private FolderService folderService;
	
	@Autowired
	private ActorService actorService;
	
/*
15. A message system will be provided for all the users. Users will be able to send messages to other users containing the following 
	information: the sender, recipients, the priority (Low, Neutral, High), a title and the body. 
	Messages will be stored in folders. There are different types of folders: system folders 
	(Inbox, Outbox, Notificationbox, Spambox, Trashbox), and user folders, which may be arbitrarily nested by the user and have their own names.	
*/
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"player1", "folder1", "folder2", null},

				{"player2", "folder1", "folder2", IllegalArgumentException.class},
				{"player1", "folder1", "", ConstraintViolationException.class},
				{"player1", "folder1", null, ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player1", "testFolder", null, null},
				
				{"player1", "", null, ConstraintViolationException.class},
				{"player1", null, null, ConstraintViolationException.class},
		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"player10", "folder91", null},

				{"admin", "folder92", IllegalArgumentException.class},
				{"player10", "folder1", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	@Test
	public void driverMove() {
		Object[][] data = {
				{"player10", "folder92", null, null},

				{"admin", "folder92", null, IllegalArgumentException.class},
				{"player10", "folder1", null, IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateMove((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}

	private void templateEdit(String user, String folderBean, String newName, Class<?> expected) {
		Class<?> caught;

		Folder folder;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			folder = folderService.findOne(super.getEntityId(folderBean));
			Assert.notNull(folder);

			folder.setName(newName);
			
			folderService.save(folder);
			folderService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateCreate(String user, String name, String parent, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Actor principal;
			Folder folder;
			
			principal = actorService.findPrincipal();
			
			if(parent != null){
				folder = folderService.findOne(super.getEntityId(parent));
				Assert.notNull(folder);	
				
			} else{
				folder = null;
			}
			
			folder = folderService.create(folder, principal);
			Assert.notNull(folder);

			folder.setName(name);
			folder.setType(new FolderType());
			folder.getType().setFolderType(FolderType.USERBOX);
			
			folderService.save(folder);
			folderService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String folderBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Folder folder;
			folder = folderService.findOne(super.getEntityId(folderBean));
			Assert.notNull(folder);
			
			folderService.delete(folder);
			folderService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateMove(String user, String folderBean, String newParentBean, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			folderService.moveFolder(super.getEntityId(folderBean), 
									 newParentBean == null? null : super.getEntityId(newParentBean));
			folderService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
