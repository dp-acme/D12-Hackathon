package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.SocialNetwork;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class SocialNetworkServiceTest extends AbstractTest {

	
	@Autowired
	private SocialNetworkService socialNetworkService;
	
	
/*	22. An actor who is authenticated as an admin must be able to:
		c. Manage social networks, which includes creating new ones, modifying, or deleting
them.*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"admin", "socialNetwork4", "", null},
				
				{"admin", "", "",ConstraintViolationException.class},
				{"player1", "socialNetwork1", "https://twitter.com/sktelecomfaker?lang=es",IllegalArgumentException.class},

		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	@Test
	public void driverEdit() {
		Object[][] data = {
				{"admin", "socialNetwork1", "newName", null},

				{"admin", "socialNetwork1", "", ConstraintViolationException.class},
				{"manager1", "socialNetwork1", "newName", IllegalArgumentException.class},
		};
		
		for(Object[] d : data){
			templateEdit((String)d[0], (String)d[1], (String)d[2], (Class<?>)d[3]);
		}
	}
	
	
	@Test
	public void driverDelete() {
		Object[][] data = {
				{"admin", "socialNetwork1", null},
				{"player1", "socialNetwork1", IllegalArgumentException.class}
		};
		
		for(Object[] d : data){
			templateDelete((String)d[0], (String)d[1], (Class<?>)d[2]);
		}
	}
	
	private void templateCreate(String user,String name, String link, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			SocialNetwork socialNetwork;
			
			socialNetwork = socialNetworkService.create();
			
			socialNetwork.setName(name);
			socialNetwork.setImage(link);
			
			socialNetworkService.save(socialNetwork);
			socialNetworkService.flush();
			
			super.unauthenticate();
			
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateDelete(String user, String socialNetworkId, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			SocialNetwork socialNetwork;
			socialNetwork = socialNetworkService.findOne(super.getEntityId(socialNetworkId));
			
			socialNetworkService.delete(socialNetwork.getId());
			socialNetworkService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	private void templateEdit(String user, String socialNetworkBean, String newName, Class<?> expected) {
		Class<?> caught;

		SocialNetwork socialNetwork;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			socialNetwork = socialNetworkService.findOne(super.getEntityId(socialNetworkBean));
			Assert.notNull(socialNetwork);

			socialNetwork.setName(newName);

			socialNetworkService.save(socialNetwork);
			socialNetworkService.flush();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
}
	
