package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Resolution;
import domain.Result;
import domain.Scrim;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ResolutionServiceTest extends AbstractTest {

	@Autowired
	private RefereeService refereeService;
	
	@Autowired
	private ResolutionService resolutionService;
	
	@Autowired
	private ScrimService scrimService;
	
/*	19. The leader of a team must be able to:
		i. If any player of his/her team is premium and a played scrim has an illogical result,
			then he/she can ask for a referee resolution.
	21. An actor who is authenticated as a referee must be able to:
		b. Help a leader with a premium teammate to give a resolution for a doubtful scrim.*/
	@Test
	public void driverCreate() {
		Object[][] data = {
				{"player1","referee1", "scrim6", "comments", null},
				
				{"player3","referee1", "scrim1", "",IllegalArgumentException.class},
				{"manager1","referee1", "scrim1", "comments",IllegalArgumentException.class},
				{"player1","manager2", "scrim6", "",IllegalArgumentException.class},

		};
		
		for(Object[] d : data){
			templateCreate((String)d[0], (String)d[1], (String)d[2],(String)d[3], (Class<?>)d[4]);
		}
	}
	
	
	private void templateCreate(String user,String referee, String scrimId, String comments, Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			super.startTransaction();
			super.authenticate(user);
			
			Resolution resolution;
			Scrim scrim;
			Result result1,result2;
			
			
			scrim = scrimService.findOne(super.getEntityId(scrimId));
			
			resolution = resolutionService.create(scrim.getId());
			Assert.notNull(resolution);

			resolution.setComments(comments);
			
			resolution= resolutionService.save(resolution);
			resolutionService.flush();
			
			super.unauthenticate();
			super.authenticate(referee);
			resolution  = resolutionService.addReferee(resolution, refereeService.findOne(super.getEntityId(referee)));
			
			result1 = new Result();
			result2 = new Result();
			
			result1.setResult(Result.DRAW);
			result2.setResult(Result.DRAW);
			
			resolutionService.setScrimResult(resolution.getId(),result1, result2);
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
}
	
