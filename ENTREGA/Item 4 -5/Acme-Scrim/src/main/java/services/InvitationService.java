package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.InvitationRepository;
import domain.Actor;
import domain.GameAccount;
import domain.Invitation;
import domain.Player;
import domain.Priority;
import domain.Team;

@Service
@Transactional
public class InvitationService {

	// Managed repository ----------------------------------------

	@Autowired
	private InvitationRepository invitationRepository;

	// Supporting services ---------------------------------------

	@Autowired
	private TeamService teamService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private MessageService messageService;
	
	// Constructor -----------------------------------------------
	
	public InvitationService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------

	public Invitation create(Team team) {
		Invitation result;
		
		result = new Invitation ();
		
		result.setTeam(team);
		
		return result;
	}

	public Collection<Invitation> findAll() {
		Collection<Invitation> result;

		result = this.invitationRepository.findAll();

		return result;
	}

	public Invitation findOne(int invitationId) {
		Assert.isTrue(invitationId != 0);

		Invitation result;

		result = this.invitationRepository.findOne(invitationId);

		return result;
	}
	
	public Invitation save(Invitation invitation) {
		return save(invitation, true);
	}

	public Invitation save(Invitation invitation, boolean checkPrincipalLeader) {
		Assert.notNull(invitation);
		
		Actor principal;
		principal = actorService.findPrincipal();
		
		if (invitation.getId() == 0) {
			if (checkPrincipalLeader) Assert.isTrue(principal.equals(invitation.getTeam().getLeader().getPlayer()));
			
			Assert.isNull(invitation.getMoment());
			Assert.isTrue(!invitation.getTeam().getLeader().equals(invitation.getTeammate()));
			Assert.isNull(this.getGameAccountTeamInvitation(invitation.getTeam(), invitation.getTeammate(), false));
		} // Invitation accepted
		else {
			if (checkPrincipalLeader) Assert.isTrue(principal.equals(invitation.getTeammate().getPlayer()));
			
			Assert.isTrue(!invitation.getTeam().getLeader().equals(invitation.getTeammate()));
			Assert.isTrue(this.getGameAccountTeamInvitation(invitation.getTeam(), invitation.getTeammate(), false).equals(invitation));
		}
		
		Invitation result;
		
		result = invitationRepository.save(invitation);
		Assert.notNull(result);
		
		if (invitation.getId() == 0) {
			Team team;
			team = result.getTeam();
			
			team.getInvitations().add(result);
			
			teamService.save(team);
		}

		return result;
	}

	public void delete(Invitation invitation) {
		Assert.notNull(invitation);
		
		Team team;
		team = invitation.getTeam();
		
		team.getInvitations().remove(invitation);
		teamService.save(team);

		this.invitationRepository.delete(invitation);
	}

	// Other methods ---------------------------------------------

	public Invitation getGameAccountTeamInvitation(Team team, GameAccount gameAccount, boolean checkNotNull) {
		Assert.notNull(team);
		Assert.notNull(gameAccount);
		
		Invitation result;

		result = this.invitationRepository.getGameAccountTeamInvitation(team.getId(), gameAccount.getId());
		if (checkNotNull)
			Assert.notNull(result);
		
		return result;
	}
	
	public void acceptInvitation(Invitation invitation) {
		Assert.notNull(invitation);
		
		Invitation result;
		result = this.findOne(invitation.getId());
		Assert.notNull(result);
		
		Player principal;
		principal = actorService.checkIsPlayer();
		
		Assert.isTrue(principal.equals(result.getTeammate().getPlayer()));
		Assert.isNull(result.getMoment());
		
		result.setMoment(new Date(System.currentTimeMillis() - 1));
		
		result = this.save(result);
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Invitation Accepted: " + result.getTeammate().getNickname() + " in '" + result.getTeam() + "'";
		subject += "--";
		subject += "Invitación Aceptada: " + result.getTeammate().getNickname() + " en '" + result.getTeam() + "'";
		
		body = "Your invitation to play in team '" + result.getTeam() + "' has been accepted by " + result.getTeammate().getNickname() + ".";
		body += "\n--\n";
		body += "Tu invitación para jugar en el equipo '" + result.getTeam() + "' ha sido aceptada por " + result.getTeammate().getNickname() + ".";;
		
		recipients = new ArrayList<Actor>();
		recipients.add(result.getTeam().getLeader().getPlayer());
		
		messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
	}
	
	public void rejectInvitation(Invitation invitation) {
		Assert.notNull(invitation);
		
		Invitation result;
		
		Player principal;
		principal = actorService.checkIsPlayer();
		
		Assert.isTrue(principal.equals(invitation.getTeammate().getPlayer()));
		Assert.isNull(invitation.getMoment());
		
		result = this.findOne(invitation.getId());
		Assert.notNull(result);
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Invitation Rejected: " + result.getTeammate().getNickname() + " in '" + result.getTeam() + "'";
		subject += "--";
		subject += "Invitación Rechazada: " + result.getTeammate().getNickname() + " en '" + result.getTeam() + "'";
		
		body = "Your invitation to play in team '" + result.getTeam() + "' has been rejected by " + result.getTeammate().getNickname() + ".";
		body += "\n--\n";
		body += "Tu invitación para jugar en el equipo '" + result.getTeam() + "' ha sido rechazada por " + result.getTeammate().getNickname() + ".";;
		
		recipients = new ArrayList<Actor>();
		recipients.add(result.getTeam().getLeader().getPlayer());
		
		messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
		
		this.delete(invitation);
	}

	public void flush() {
		invitationRepository.flush();
	}
	
}
