package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CommentRepository;
import domain.Comment;
import domain.Player;
import domain.Scrim;

@Service
@Transactional
public class CommentService {
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ScrimService scrimService;
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private Validator validator;
	
	public Comment findOne(int id){
		Assert.isTrue(id != 0);
		
		Comment result;
		
		result = commentRepository.findOne(id);
		
		return result;
	}
	
	public Collection<Comment> findAll(){
		Collection<Comment> result; 
		
		result = commentRepository.findAll();
		
		Assert.notNull(result);
		
		return result;
	}
	
	public Comment create(int scrimId){
		Comment result;
		Player principal;
		Scrim scrim;
		
		principal = actorService.checkIsPlayer();
		scrim = scrimService.findOne(scrimId);
		
		result = new Comment();
		
		result.setScrim(scrim);
		result.setCreator(principal);		
		
		return result;
	}
	
	public Comment save(Comment comment){
		Comment result;
		Player principal;
		
		Assert.notNull(comment);
		
		principal = actorService.checkIsPlayer();
		
		Assert.isTrue(comment.getCreator().equals(principal));
		Assert.isTrue(playerService.getPlayersByScrim(comment.getScrim().getId()).contains(principal));
		
		result = commentRepository.save(comment);
		
		return result;
	}
	
	public void delete(Comment comment) {
		Assert.notNull(comment);
		
		this.commentRepository.delete(comment);
	}
	
	public Collection<Comment> getCommentsByPlayerAndScrim(int playerId, int scrimId){
		return commentRepository.getCommentsByPlayerAndScrim(playerId, scrimId);
	}
	
	public Collection<Comment> getCommentsByScrim(int scrimId){
		return commentRepository.getCommentsByScrim(scrimId);
	}
	
	public Comment reconstruct (Comment result, int scrimId, BindingResult br) {
		Comment comment;
		
		comment = create(scrimId);
		
		result.setScrim(comment.getScrim());
		result.setCreator(comment.getCreator());
				
		validator.validate(result, br);
		
		return result;	
	}

	public void flush() {
		commentRepository.flush();
	}
}
