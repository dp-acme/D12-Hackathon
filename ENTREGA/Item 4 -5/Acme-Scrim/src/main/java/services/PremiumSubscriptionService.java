package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.PremiumSubscriptionRepository;
import domain.Player;
import domain.PremiumSubscription;

@Service
@Transactional
public class PremiumSubscriptionService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private PremiumSubscriptionRepository premiumSubscriptionRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PlayerService playerService;
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Constructor ---------------------------------------------------
	
	public PremiumSubscriptionService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public PremiumSubscription create(){
		PremiumSubscription result;
		
		result = new PremiumSubscription();
		
		return result;
	}
	
	public PremiumSubscription findOne(int premiumSubscriptionId) {
		Assert.isTrue(premiumSubscriptionId != 0);
		
		PremiumSubscription result;
		
		result = premiumSubscriptionRepository.findOne(premiumSubscriptionId);
		
		return result;
	}
	
	public Collection<PremiumSubscription> findAll() {		
		Collection<PremiumSubscription> result;
		
		try{
			result = premiumSubscriptionRepository.findAll();
		}
		catch(Throwable e){
			result = null;
		}
		
		Assert.notNull(result);
		
		return result;
	}
	
	public PremiumSubscription save(PremiumSubscription premiumSubscription) {
		Assert.notNull(premiumSubscription);
		PremiumSubscription result;
		Player player;
		
		actorService.checkIsPlayer();//Debes estar autenticado como admin
		Assert.isTrue(!this.hasAnyPremiumSubscription(premiumSubscription.getPlayer().getId()));
		Assert.isTrue(this.checkCreditCard(premiumSubscription.getCreditCard().getExpirationMonth(), premiumSubscription.getCreditCard().getExpirationYear()));
		
		result = premiumSubscriptionRepository.save(premiumSubscription);
		
		player = (Player)actorService.findPrincipal();
		player.setPremiumSubscription(result);
		playerService.save(player);
		
		return result;
	}
	
	// Other business methods ---------------------------------------------------
	
	public PremiumSubscription reconstruct(PremiumSubscription premiumSubscription, BindingResult binding) {
		PremiumSubscription result;
		
		result = premiumSubscription;
		result.setCreditCard(premiumSubscription.getCreditCard());
		result.setPlayer(playerService.findOne(actorService.findPrincipal().getId()));
		validator.validate(result, binding);
		
		return result;
	}
	
	
	public void flush() {
		premiumSubscriptionRepository.flush();
	}
	
	public boolean hasAnyPremiumSubscription(int playerId){
		boolean result = false;	
		
		if(premiumSubscriptionRepository.findPremiumSubscription(playerId) != null){
			result = true;
		}
	
		return result;
	}
	
	public PremiumSubscription getPremiumSubscription(int playerId){
		PremiumSubscription result;
	
		result = premiumSubscriptionRepository.findPremiumSubscription(playerId);
		
		return result;
	}
	
	@SuppressWarnings("deprecation")
	public boolean checkCreditCard(int month, int year){
		Date date = new Date();
		year += 2000;
		int nowMonth = date.getMonth() + 1;
		int nowYear = date.getYear() + 1900;

		return year > nowYear || (month > nowMonth && year == nowYear);
	}
}
