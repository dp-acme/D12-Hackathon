package services;

import java.text.DecimalFormat;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Administrator;
import domain.Player;
import domain.Team;
import domain.Videogame;

@Service
@Transactional
public class AdministratorService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private AdministratorRepository administratorRepository;
	
	// Supporting services ---------------------------------------------------
	
	// Constructor ---------------------------------------------------
	
	public AdministratorService() {
		super();
	}
	
	public Collection<Administrator> findAll() {
		Collection<Administrator> result;
		
		result = this.administratorRepository.findAll();
		
		return result;
	}
	
	public 	Double[] getAvgMaxMinSdVideogamesPerManager(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgMaxMinSdVideogamesPerManager();
		
		return res;
	}	
	public 	Double[] getAvgMaxMinSdSeasonsPerVideogame(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgMaxMinSdSeasonsPerVideogame();
		
		return res;
	}
	public 	Double[] getAvgMaxMinSdScrimsPerTeam(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgMaxMinSdScrimsPerTeam();
		
		return res;
	}
	public 	Double[] getAvgMaxMinSdTeamsPerPlayer(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgMaxMinSdTeamsPerPlayer();
		
		return res;
	}
	public 	Double[] getAvgMaxMinSdPlayersPerTeam(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgMaxMinSdPlayersPerTeam();
		
		return res;
	}
	
	public Collection<Team> getTeamsWithMoreScrims(){
		Authority auth;
		UserAccount userAccount;
		Collection<Team> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		Pageable pageable = new PageRequest(0,5);
		
		res = administratorRepository.getTeamsWithMoreScrims(pageable).getContent();
		
		return res;
	}
	
	public Collection<Player> getPlayersWithMoreForumMessages(){
		Authority auth;
		UserAccount userAccount;
		Collection<Player> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		Pageable pageable = new PageRequest(0,5);
		
		res = administratorRepository.getPlayersWithMoreForumMessages(pageable).getContent();
		
		return res;
	}
	
	public Collection<Player> playersWithMoreTeams(){
		Authority auth;
		UserAccount userAccount;
		Collection<Player> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		Pageable pageable = new PageRequest(0,5);
		
		res = administratorRepository.playersWithMoreTeams(pageable).getContent();
		
		return res;
	}
	
	public Double ratioOfPlayersWithForumMessages(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioOfPlayersWithForumMessages();
		
		return res;
	}
	
	public Double ratioOfCancelledScrims(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioOfCancelledScrims();
		
		return res;
	}
	
	public String roundTo(Double n, Integer decimals){
		DecimalFormat format;
		String result;
		if(n==null){
			result = "No hay datos suficientes";
		}else{
		
			format = new DecimalFormat();
			format.setMaximumFractionDigits(decimals);
			result = format.format(n);
		}
		
		return result;
	}
	
	public String[] roundTo(Double[] ns, Integer decimals){
		String[] result;
		
		
		result = new String[ns.length];
		
		for(int i = 0; i < result.length; i ++){
			result[i] = roundTo(ns[i], decimals);
		}
		
		return result;
	}

	public Collection<Videogame> getVideogames(Pageable pageable) {
		Authority auth;
		UserAccount userAccount;
		Collection<Videogame> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getVideogames(pageable).getContent();
		
		return res;
	}
	
	public Collection<Player> playersWithMoreTeamsPerVideogame(Videogame videogame) {
		Authority auth;
		UserAccount userAccount;
		Collection<Player> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		
		Pageable pageable = new PageRequest(0,5);
		
		res = administratorRepository.playersWithMoreTeamsPerVideogame(videogame.getId(), pageable).getContent();
		
		return res;
	}
}
