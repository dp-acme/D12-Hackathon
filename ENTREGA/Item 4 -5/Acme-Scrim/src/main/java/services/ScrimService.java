package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ScrimRepository;
import domain.Actor;
import domain.Comment;
import domain.Player;
import domain.Priority;
import domain.Region;
import domain.Report;
import domain.Resolution;
import domain.Result;
import domain.Review;
import domain.Scrim;
import domain.ScrimStatus;
import domain.Season;
import domain.Team;
import domain.Videogame;

@Service
@Transactional
public class ScrimService {

	// Managed repository ----------------------------------------

	@Autowired
	private ScrimRepository scrimRepository;

	// Supporting services ---------------------------------------

	@Autowired
	private RegionService regionService;
	
	@Autowired
	private SeasonService seasonService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private ReviewService reviewService;
	
	@Autowired
	private ResolutionService resolutionService;
	
	@Autowired
	private ReportService reportService;
	
	@Autowired
	private TeamService teamService;
	
	// Constructor -----------------------------------------------
	
	public ScrimService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------

	public Scrim create(Videogame videogame) {
		Assert.notNull(videogame);
		
		Scrim result;
		
		Region international;
		
		international = regionService.getInternational();
		Season lastSeason = seasonService.getLastSeasonOfVideogame(videogame.getId());
		
		
		result = new Scrim ();
		
		result.setDesiredGuestRegion(international);
		result.setSeason(lastSeason);
		
		return result;
	}

	public Collection<Scrim> findAll() {
		Collection<Scrim> result;

		result = this.scrimRepository.findAll();

		return result;
	}

	public Scrim findOne(int scrimId) {
		Assert.isTrue(scrimId != 0);

		Scrim result;

		result = this.scrimRepository.findOne(scrimId);

		return result;
	}

	public Scrim save(Scrim scrim) {
		Assert.notNull(scrim);

		Scrim result;
		
		result = this.scrimRepository.save(scrim);
		Assert.notNull(result);
		
		return result;
	}
	
	public void delete(Scrim scrim) {
		Assert.notNull(scrim);
		
		Collection<Comment> comments;
		
		comments = this.commentService.getCommentsByScrim(scrim.getId());
		
		for (Comment comment : comments) {
			this.commentService.delete(comment);
		}

		Collection<Review> reviews;
		
		reviews = this.reviewService.getLikesByScrim(scrim.getId());
		
		for (Review review : reviews) {
			this.reviewService.delete(review);
		}
		
		Resolution resolution;
		
		resolution = scrim.getResolution();
		
		if (resolution != null) {
			this.resolutionService.delete(resolution);
			this.scrimRepository.flush();
		}
		
		Collection<Report> reports;
		
		reports = this.reportService.getReportsOfScrim(scrim);
		
		for (Report report : reports) {
			this.reportService.delete(report);
		}
		
		this.scrimRepository.delete(scrim);
	}

	// Other methods ---------------------------------------------

	@Autowired
	private Validator validator;
	
	public Scrim reconstructCreateEdit(Scrim result, BindingResult binding) {
		Assert.notNull(result);
		
		Scrim scrim;
		
		if (result.getId() == 0) {
			scrim = this.create(result.getCreator().getVideogame());
		} else {
			scrim = this.findOne(result.getId());
			
			result.setCreator(scrim.getCreator());
			result.setDesiredGuestRegion(scrim.getDesiredGuestRegion());
			result.setMoment(scrim.getMoment());
		}
		
		result.setResolution(scrim.getResolution());
		result.setSeason(scrim.getSeason());
		result.setConfirmed(scrim.isConfirmed());
		result.setGuest(scrim.getGuest());
		result.setCreatorTeamResult(scrim.getCreatorTeamResult());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Scrim reconstructInvite(Scrim result, BindingResult binding) {
		Assert.notNull(result);
		
		Scrim scrim;
		
		scrim = this.create(result.getCreator().getVideogame());
		
		result.setResolution(scrim.getResolution());
		result.setSeason(scrim.getSeason());
		result.setConfirmed(scrim.isConfirmed());
		result.setDesiredGuestRegion(result.getGuest().getRegion());
		result.setCreatorTeamResult(scrim.getCreatorTeamResult());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Scrim reconstructJoin(Scrim result, BindingResult binding) {
		Assert.notNull(result);
		
		Scrim scrim;
		
		scrim = this.findOne(result.getId());
		
		result.setCreator(scrim.getCreator());
		result.setDesiredGuestRegion(scrim.getDesiredGuestRegion());
		result.setMoment(scrim.getMoment());
		result.setResolution(scrim.getResolution());
		result.setSeason(scrim.getSeason());
		result.setConfirmed(scrim.isConfirmed());
		result.setCreatorTeamResult(scrim.getCreatorTeamResult());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Collection<Scrim> getTeamScrims(Team team) {
		Assert.notNull(team);
		
		Collection<Scrim> result;
		
		result = this.scrimRepository.getTeamScrims(team.getId());
		
		return result;
	}
	
	public Scrim createScrimAnnouncement(Team creatorTeam, Date moment, Region region) {
		Assert.notNull(creatorTeam);
		Assert.notNull(moment);
		Assert.notNull(region);
		Assert.isTrue(moment.after(new Date()));
		
		Player principal;
		principal = actorService.checkIsPlayer();
		Assert.isTrue(principal.equals(creatorTeam.getLeader().getPlayer()));
		
		Scrim result;
		
		result = this.create(creatorTeam.getVideogame());
		Assert.notNull(result);
		
		result.setCreator(creatorTeam);
		result.setMoment(moment);
		result.setDesiredGuestRegion(region);
		
		result = this.save(result);
		Assert.notNull(result);
		
		return result;
	}
	
	public Scrim createScrimInvitation(Team creatorTeam, Team guestTeam, Date moment) {
		Assert.notNull(creatorTeam);
		Assert.notNull(guestTeam);
		Assert.notNull(moment);
		Assert.isTrue(moment.after(new Date()));
		Assert.isTrue(guestTeam.getVideogame().equals(creatorTeam.getVideogame()));
		
		Player principal;
		principal = actorService.checkIsPlayer();
		Assert.isTrue(principal.equals(creatorTeam.getLeader().getPlayer()));
		
		Scrim result;
		
		result = this.create(creatorTeam.getVideogame());
		Assert.notNull(result);
		
		result.setCreator(creatorTeam);
		result.setMoment(moment);
		result.setGuest(guestTeam);
		result.setDesiredGuestRegion(guestTeam.getRegion());
		
		result = this.save(result);
		Assert.notNull(result);
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Scrim Invitation: '" + result.getCreator().getName() + "'";
		subject += " -- ";
		subject += "Invitaci�n de Scrim: '" + result.getCreator().getName() + "'";
		
		body = "You have been invited to play in a scrim in " + result.getMoment() + " with your team '" + result.getGuest().getName() + "' by '" + result.getCreator().getName() + "'.";
		body += "\n -- \n";
		body += "Has sido invitado para jugar una scrim el d�a " + result.getMoment() + " con tu equipo '" + result.getGuest().getName() + "' por '" + result.getCreator().getName() + "'.";
		
		recipients = new ArrayList<Actor>();
		recipients.add(result.getGuest().getLeader().getPlayer());
		
		messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
		
		return result;
	}
	
	public Scrim acceptScrim(Scrim scrim, Team guestTeam) {
		Assert.notNull(guestTeam);
		Assert.notNull(scrim);
		
		Assert.isTrue(!guestTeam.equals(scrim.getCreator()));
		Assert.isTrue(scrim.getGuest() == null || scrim.getGuest().equals(guestTeam));
		Assert.isTrue(guestTeam.getVideogame().equals(scrim.getSeason().getVideogame()));

		Assert.isTrue(guestTeam.getRegion().equals(scrim.getDesiredGuestRegion()));
		Assert.isNull(scrim.isConfirmed());
		
		Player principal;
		principal = actorService.checkIsPlayer();
		Assert.isTrue(guestTeam.getLeader().getPlayer().equals(principal));
		
		Scrim result;
		
		scrim.setConfirmed(true);
		scrim.setGuest(guestTeam);

		result = this.save(scrim);
		Assert.notNull(result);
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Scrim Accepted: '" + guestTeam.getName() + "'";
		subject += " -- ";
		subject += "Scrim Aceptada: '" + guestTeam.getName() + "'";
		
		body = "The invitation to play in the scrim in " + scrim.getMoment() + " has been accepted by '" + guestTeam.getName() + "'.";
		body += "\n -- \n";
		body += "La invitaci�n para jugar en la scrim del d�a " + scrim.getMoment() + " ha sido aceptada por '" + guestTeam.getName() + "'.";
		
		recipients = new ArrayList<Actor>();
		recipients.add(scrim.getCreator().getLeader().getPlayer());
		
		messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
		
		// Creator team followers
		
		String subjectCreator;
		String bodyCreator;
		Collection<Actor> recipientsCreator;
		
		subjectCreator = "[Following] '" + scrim.getCreator().getName() + "' is going to play a scrim";
		subjectCreator += " -- ";
		subjectCreator += "[Siguiendo] '" + scrim.getCreator().getName() + "' jugar� una scrim";
		
		bodyCreator = "A scrim is going to be played in " + scrim.getMoment() + " with '" + scrim.getCreator().getName() + "' VS '" + scrim.getGuest().getName() + "'.";
		bodyCreator += "\n -- \n";
		bodyCreator += "Una scrim va a ser jugada el d�a " + scrim.getMoment() + " entre '" + scrim.getCreator().getName() + "' VS '" + scrim.getGuest().getName() + "'.";
		
		recipientsCreator = new ArrayList<Actor>(this.teamService.getTeamFollowers(scrim.getCreator()));
		
		messageService.sendNotification(subjectCreator, bodyCreator, recipientsCreator, Priority.NEUTRAL);
		
		// Guest team followers
		
		String subjectGuest;
		String bodyGuest;
		Collection<Actor> recipientsGuest;
		
		subjectGuest = "[Following] '" + scrim.getGuest().getName() + "' is going to play a scrim";
		subjectGuest += " -- ";
		subjectGuest += "[Siguiendo] '" + scrim.getGuest().getName() + "' jugar� una scrim";
		
		bodyGuest = "A scrim is going to be played in " + scrim.getMoment() + " with '" + scrim.getCreator().getName() + "' VS '" + scrim.getGuest().getName() + "'.";
		bodyGuest += "\n -- \n";
		bodyGuest += "Una scrim va a ser jugada el d�a " + scrim.getMoment() + " entre '" + scrim.getCreator().getName() + "' VS '" + scrim.getGuest().getName() + "'.";
		
		recipientsGuest = new ArrayList<Actor>(this.teamService.getTeamFollowers(scrim.getGuest()));
		
		messageService.sendNotification(subjectGuest, bodyGuest, recipientsGuest, Priority.NEUTRAL);
		
		return result;
	}
	
	public Scrim rejectScrim(Scrim scrim, Team guestTeam) {
		Assert.notNull(guestTeam);
		Assert.notNull(scrim);
		
		Assert.isTrue(scrim.getGuest().equals(guestTeam));
		Assert.isTrue(guestTeam.getVideogame().equals(scrim.getSeason().getVideogame()));
		Assert.isNull(scrim.isConfirmed());
		
		Player principal;
		principal = actorService.checkIsPlayer();
		Assert.isTrue(guestTeam.getLeader().getPlayer().equals(principal));
		
		Scrim result;
		
		scrim.setConfirmed(false);
		scrim.setGuest(guestTeam);

		result = this.save(scrim);
		Assert.notNull(result);
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Scrim Rejected: '" + guestTeam.getName() + "'";
		subject += " -- ";
		subject += "Scrim Rechazada: '" + guestTeam.getName() + "'";
		
		body = "The invitation to play in the scrim in " + scrim.getMoment() + " has been rejected by '" + guestTeam.getName() + "'.";
		body += "\n -- \n";
		body += "La invitaci�n para jugar en la scrim del d�a " + scrim.getMoment() + " ha sido rechazada por '" + guestTeam.getName() + "'.";
		
		recipients = new ArrayList<Actor>();
		recipients.add(scrim.getCreator().getLeader().getPlayer());
		
		messageService.sendNotification(subject, body, recipients, Priority.NEUTRAL);
		
		return result;
	}
	
	public Scrim editScrim(Scrim scrim, String streamingLink) {
		Assert.notNull(scrim);
		Assert.hasText(streamingLink);
		
		Scrim result;
		
		result = this.findOne(scrim.getId());
		Assert.notNull(result);
		
		Assert.isTrue(!result.getStatus().getScrimStatus().equals(ScrimStatus.CLOSED) ||
				result.getStatus().getScrimStatus().equals(ScrimStatus.EXPIRED) ||
				result.getStatus().getScrimStatus().equals(ScrimStatus.CANCELLED));
		
		Player principal;
		principal = actorService.checkIsPlayer();
		Assert.isTrue(result.getCreator().getLeader().getPlayer().equals(principal));
		
		result.setStreamingLink(streamingLink);
		
		result = this.save(scrim);
		Assert.notNull(result);
		
		return result;
	}
	
	public Scrim insertResult(Scrim scrim, Team team, Result scoreResult) {
		Assert.notNull(scrim);
		Assert.notNull(team);
		Assert.notNull(scoreResult);
		
		Scrim result;
		
		result = this.findOne(scrim.getId());
		Assert.notNull(result);
		
		Assert.isTrue(result.getStatus().getScrimStatus().equals(ScrimStatus.FINISHED));
		
		Player principal;
		principal = actorService.checkIsPlayer();
		Assert.isTrue(result.getCreator().getLeader().getPlayer().equals(principal) ||
				result.getGuest().getLeader().getPlayer().equals(principal));
		
		if (result.getCreator().getLeader().getPlayer().equals(principal)) {
			Assert.isNull(result.getCreatorTeamResult());
			
			result.setCreatorTeamResult(scoreResult);
		} else {
			Assert.isNull(result.getGuestTeamResult());
			
			result.setGuestTeamResult(scoreResult);
		}
		
		if (result.getCreatorTeamResult() != null && result.getGuestTeamResult() != null 
				&& result.getCreatorTeamResult().getResult().equals(result.getGuestTeamResult().getResult()) 
				&& !result.getCreatorTeamResult().getResult().equals(Result.DRAW)) {
			Result illogicalResult;
			illogicalResult = new Result();
			illogicalResult.setResult(Result.ILLOGICAL);
			
			result.setCreatorTeamResult(illogicalResult);
			result.setGuestTeamResult(illogicalResult);
		}
		
		result = this.save(scrim);
		Assert.notNull(result);
		
		// Creator team followers
		
		String resultString;
		if (scoreResult.getResult().equals(Result.WIN)) resultString = "VICTORIA";
		else if (scoreResult.getResult().equals(Result.DRAW)) resultString = "EMPATE";
		else if (scoreResult.getResult().equals(Result.LOSE)) resultString = "DERROTA";
		else resultString = "IL�GICO";
		
		String subjectCreator;
		String bodyCreator;
		Collection<Actor> recipientsCreator;
		
		subjectCreator = "[Following] '" + scrim.getCreator().getName() + "' result added to scrim";
		subjectCreator += " -- ";
		subjectCreator += "[Siguiendo] '" + scrim.getCreator().getName() + "' resultado a�adido a scrim";
		
		bodyCreator = "The " + scoreResult.getResult() + " result has been introduced to the scrim which is going to be played in " + scrim.getMoment() + " for '" + scrim.getCreator().getName() + "' VS '" + scrim.getGuest().getName() + "'.";
		bodyCreator += "\n -- \n";
		bodyCreator += "El resultado " + resultString + " ha sido introducido en la scrim que va a ser jugada el d�a " + scrim.getMoment() + " entre '" + scrim.getCreator().getName() + "' VS '" + scrim.getGuest().getName() + "'.";
		
		recipientsCreator = new ArrayList<Actor>(this.teamService.getTeamFollowers(scrim.getCreator()));
		
		messageService.sendNotification(subjectCreator, bodyCreator, recipientsCreator, Priority.NEUTRAL);
		
		// Guest team followers
		
		String subjectGuest;
		String bodyGuest;
		Collection<Actor> recipientsGuest;
		
		subjectGuest = "[Following] '" + scrim.getCreator().getName() + "' result added to scrim";
		subjectGuest += " -- ";
		subjectGuest += "[Siguiendo] '" + scrim.getCreator().getName() + "' resultado a�adido a scrim";
		
		bodyGuest = "The " + scoreResult.getResult() + " result has been introduced to the scrim which is going to be played in " + scrim.getMoment() + " for '" + scrim.getCreator().getName() + "' VS '" + scrim.getGuest().getName() + "'.";
		bodyGuest += "\n -- \n";
		bodyGuest += "El resultado " + resultString + " ha sido introducido en la scrim que va a ser jugada el d�a " + scrim.getMoment() + " entre '" + scrim.getCreator().getName() + "' VS '" + scrim.getGuest().getName() + "'.";
		
		recipientsGuest = new ArrayList<Actor>(this.teamService.getTeamFollowers(scrim.getGuest()));
		
		messageService.sendNotification(subjectGuest, bodyGuest, recipientsGuest, Priority.NEUTRAL);
		
		return result;
	}
	
	public Scrim askResolution(Scrim scrim, Resolution resolution) {
		Assert.notNull(scrim);
		Assert.notNull(resolution);
		
		Scrim result;
		
		result = this.findOne(scrim.getId());
		Assert.notNull(result);
		
		Player principal;
		principal = actorService.checkIsPlayer();
		Assert.isTrue(result.getCreator().getLeader().getPlayer().equals(principal) ||
				result.getGuest().getLeader().getPlayer().equals(principal));
		
		if (result.getCreator().getLeader().getPlayer().equals(principal)) {
			Assert.isTrue(this.playerService.getPremiumPlayersInTeam(result.getCreator().getId()).size() != 0);
		} else {
			Assert.isTrue(this.playerService.getPremiumPlayersInTeam(result.getGuest().getId()).size() != 0);
		}
		
		Assert.isTrue(result.getCreatorTeamResult().getResult().equals(Result.ILLOGICAL) && result.getGuestTeamResult().getResult().equals(Result.ILLOGICAL));
		Assert.isNull(scrim.getResolution());
		
		result.setResolution(resolution);
		
		result = this.save(result);
		Assert.notNull(result);
		
		String subject;
		String body;
		Collection<Actor> recipients;
		
		subject = "Resolution created for scrim: " + result.getCreator().getName() + " VS " + result.getGuest().getName();
		subject += " -- ";
		subject += "Resoluci�n creada para scrim: " + result.getCreator().getName() + " VS " + result.getGuest().getName();
		
		body = "A resolution has been created for scrim: " + result.getCreator().getName() + " VS " + result.getGuest().getName() + 
				", with date: " + result.getMoment();
		body += "\n -- \n";
		body += "Una resoluci�n ha sido creada para la scrim: " + result.getCreator().getName() + " VS " + result.getGuest().getName() + 
				", con fecha: " + result.getMoment();		
		
		recipients = new ArrayList<Actor> (new ArrayList<Actor>(playerService.getPlayersByScrim(result.getId())));
		
		messageService.sendNotification(subject, body, recipients, Priority.HIGH);
		
		return result;
	}
	
	public Scrim resolveScrim(Scrim scrim, Result creatorResult, Result guestResult) {
		Assert.notNull(scrim);
		Assert.notNull(creatorResult);
		Assert.notNull(guestResult);
		
		Scrim result;
		
		result = this.findOne(scrim.getId());
		Assert.notNull(result);
		
		actorService.checkIsReferee();
		
		Assert.notNull(result.getResolution());
		
		result.setCreatorTeamResult(creatorResult);
		result.setGuestTeamResult(guestResult);
		
		result = this.save(result);
		Assert.notNull(result);
		
		return result;
	}

	public void flush() {
		scrimRepository.flush();
	}

}
