package services;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.MessageRepository;
import domain.Actor;
import domain.Folder;
import domain.FolderType;
import domain.Message;
import domain.Priority;

@Service
@Transactional
public class MessageService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private MessageRepository messageRepository;
	
	// Validator---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private FolderService folderService;
	
	@Autowired
	private ConfigurationService configurationService;
	
	// Constructor ---------------------------------------------------
	
	public MessageService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Message create(Folder folder, Actor sender, Collection<Actor> recipients){
		Message result;
		
		result = new Message();
		result.setFolder(folder);
		result.setSender(sender);
		result.setRecipients(recipients);
		
		return result;
	}
	
	public Message findOne(int messageId) {
		Assert.isTrue(messageId != 0);
		
		Message result;
		
		result = messageRepository.findOne(messageId);
		
		return result;
	}
	
	public Collection<Message> findAll() {		
		Collection<Message> result;
		
		result = messageRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Message save(Message message) {
		Assert.notNull(message);
		
		Actor principal;
		principal = actorService.findPrincipal();
		
		if(message.getId() == 0){
			Assert.isTrue(principal.equals(message.getSender())); //Debe estar logueado el que env�a el mensaje

		} else{
			Assert.isTrue(principal.equals(message.getFolder().getActor())); //Debe estar logueado el due�o de la carpeta en la que est� el mensaje			
		}
		
		Message result;
				
		message.setMoment(new Date(System.currentTimeMillis() - 1));
		
		result = messageRepository.save(message);
		
		if(!result.getFolder().getMessages().contains(message)){ //Actualizar la lista de mensajes de la carpeta contenedora
			folderService.addMessageToFolder(result.getFolder(), result);
		}
		
		return result;
	}
	
	public void delete(Message message){
		Assert.notNull(message);
		
		Actor principal;
		principal = actorService.findPrincipal();
		
		Assert.isTrue(principal.equals(message.getFolder().getActor())); //Debe estar logueado el due�o de la carpeta en la que est� el mensaje

		if(message.getFolder().getType().getFolderType().equals(FolderType.TRASHBOX)){ //Si est� en la carpeta Trashbox
			Folder folder;
			
			folder = message.getFolder();
			folder.getMessages().remove(message);
			
			folderService.save(folder);//Actualizar lista de mensajes
			
			message.setFolder(null);
			messageRepository.delete(message); //Borrar mensaje definitivamente
			
		} else{ //En caso contrario
			Folder trashbox;
			
			trashbox = folderService.findTrashboxByActorId(principal.getId());
			
			moveMessage(message.getId(), trashbox.getId()); //Mover mensaje a la Trashbox
		}
	}
	
	// Other business methods ---------------------------------------------------
	
	public void moveMessage(Integer messageId, Integer folderId){
		Actor principal;
		principal = actorService.findPrincipal();
		
		Assert.notNull(messageId);
		Assert.notNull(folderId);
		
		Message message;
		Folder folder, oldFolder;
		
		message = messageRepository.findOne(messageId);
		folder = folderService.findOne(folderId);
		oldFolder = message.getFolder();
		Assert.notNull(message);
		Assert.notNull(folder);
		Assert.notNull(oldFolder);
		
		Assert.isTrue(principal.equals(folder.getActor())); //Debe estar logueado como el due�o de la carpeta
		Assert.isTrue(oldFolder.getActor().equals(folder.getActor())); //No se puede cambiar de due�o
		
		oldFolder.getMessages().remove(message); //Actualizar las listas de mensajes de la carpeta antigua
		folderService.save(oldFolder);
		
		message.setFolder(folder); //Actualizar carpeta del mensaje
		this.save(message);
	}
	
	public void sendMessage(String subject, String body, Collection<Actor> recipients, String priority){ //El que env�a el mensaje es el que est� logueado
		Assert.notNull(subject);
		Assert.notNull(body);
		Assert.notNull(recipients);
		
		Actor principal;
		principal = actorService.findPrincipal();
		
		Iterator<Actor> it = recipients.iterator();
		while(it.hasNext()){ //Crear una copia para cada receptor
			Folder folder;
			Message message;
			Actor actor;
			
			actor = it.next();
			folder = folderService.findInboxByActorId(actor.getId());
			message = this.create(folder, principal, recipients);
			
			message.setSubject(subject);
			message.setBody(body);
			message.setPriority(new Priority());
			message.getPriority().setPriority(priority);
			message.setIsSpam(false);
			
			if(configurationService.isSpam(message)){
				folder = folderService.findSpamboxByActorId(actor.getId());
				message.setFolder(folder);
				message.setIsSpam(true);
			}

			this.save(message); //Guardar el mensaje
		}
		
		Message message; //Copia para el outbox
		Folder outbox;
		
		outbox = folderService.findOutboxByActorId(principal.getId());
		Assert.notNull(outbox);
		
		message = this.create(outbox, principal, recipients);
		
		message.setSubject(subject);
		message.setBody(body);
		message.setPriority(new Priority());
		message.getPriority().setPriority(priority);
		message.setIsSpam(false);
		
		this.save(message); //Guardar el mensaje		
	}
	
	public void sendNotification(String subject, String body, Collection<Actor> recipients, String priority){ //El que env�a la notificaci�n es el que est� logueado
		Assert.notNull(subject);
		Assert.notNull(body);
		Assert.notNull(recipients);
		
		Actor principal;
		principal = actorService.findPrincipal();
		
		Iterator<Actor> it = recipients.iterator();
		while(it.hasNext()){ //Crear una copia para cada receptor
			Folder folder;
			Message message;
			Actor actor;
			
			actor = it.next();
			folder = folderService.findNotificationBoxByActorId(actor.getId());
			message = this.create(folder, principal, recipients);
			
			message.setSubject(subject);
			message.setBody(body);
			message.setPriority(new Priority());
			message.getPriority().setPriority(priority);
			message.setIsSpam(false);

			this.save(message); //Guardar el mensaje
		}
	}
	
	public Collection<Message> findPaginatedMessagesByFolderId(Integer folderId, Integer page){
		Assert.notNull(folderId);
		
		Collection<Message> result;
		
		result = messageRepository.findPaginatedMessagesByFolderId(folderId, new PageRequest(page, 10)).getContent();
		Assert.notNull(result);
		
		return result;		
	}

	public Message reconstruct(Message message, BindingResult binding) {
		validator.validate(message, binding);
		
		return message;
	}

	public void flush() {
		messageRepository.flush();
	}
}
