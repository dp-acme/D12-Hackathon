package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.GameAccountRepository;
import domain.Actor;
import domain.Administrator;
import domain.GameAccount;
import domain.Player;
import domain.Team;
import domain.Videogame;

@Service
@Transactional
public class GameAccountService {

	// Managed repository ----------------------------------------

	@Autowired
	private GameAccountRepository gameAccountRepository;

	// Supporting services ---------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private RegionService regionService;
	
	// Constructor -----------------------------------------------
	
	public GameAccountService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------

	public GameAccount create(Player player) {
		Assert.notNull(player);
		
		GameAccount result;
		
		result = new GameAccount ();
		result.setPlayer(player);
		result.setTeams(new ArrayList<Team>());
		result.setRegion(regionService.getInternational());
		
		return result;
	}

	public Collection<GameAccount> findAll() {
		Collection<GameAccount> result;

		result = this.gameAccountRepository.findAll();

		return result;
	}

	public GameAccount findOne(int gameAccountId) {
		Assert.isTrue(gameAccountId != 0);

		GameAccount result;

		result = this.gameAccountRepository.findOne(gameAccountId);

		return result;
	}

	public GameAccount save(GameAccount gameAccount) {
		Assert.notNull(gameAccount);
		
		Actor principal;
		principal = actorService.findPrincipal();

		if (!(principal instanceof Administrator)) {
			Assert.isTrue(gameAccount.getPlayer().equals(principal));
		}
		
		if (gameAccount.getId() == 0) {
			Assert.isNull(this.getGameAccountForPlayerAndVideogame(gameAccount.getPlayer(), gameAccount.getVideogame()));
		}
		
		GameAccount result;
		
		result = this.gameAccountRepository.save(gameAccount);
		Assert.notNull(result);
		
		return result;
	}

	public void delete(GameAccount gameAccount) {
		Assert.notNull(gameAccount);
		Assert.isTrue(gameAccount.getPlayer().equals(actorService.checkIsPlayer()));

		Collection<Team> teams;
				
		teams = this.teamService.getTeamsOfGameAccount(gameAccount);
		
		// Eliminar los equipos en los que el leader sea gameAccount y no haya mas teammates
		// Dar el leader a otro teammate del equipo si el gameAccount es el leader
		// Salir de los equipos en los que tenga invitation (sea teammate)
		
		for (Team team : teams) {
			if (team.getLeader().equals(gameAccount)) {
				List<GameAccount> teammates;
				
				teammates = new ArrayList<GameAccount> (teamService.getTeammates(team, false));
				
				// Eliminar los equipos en los que el leader sea gameAccount y no haya mas teammates
				if (teammates.size() == 0) {
					teamService.delete(team);
				} // Dar el leader a otro teammate del equipo si el gameAccount es el leader
				else {
					teamService.changeTeammateLeader(team, teammates.get(0), false);
				}
			} // Salir de los equipos en los que tenga invitation (sea teammate)
			else {
				teamService.kickTeammate(team, gameAccount);
			}
		}

		this.gameAccountRepository.delete(gameAccount);
	}

	// Other methods ---------------------------------------------

	@Autowired
	private Validator validator;
	
	public GameAccount reconstruct(GameAccount result, BindingResult binding) {
		GameAccount gameAccount;
		
		if (result.getId() != 0) {
			gameAccount = this.findOne(result.getId());
		
			Assert.notNull(gameAccount);
			
			result.setPlayer(gameAccount.getPlayer());
			result.setVideogame(gameAccount.getVideogame());
		} else {
			Player player;
			
			player = this.actorService.checkIsPlayer();
			
			gameAccount = this.create(player);
			
			result.setPlayer(player);
		}

		result.setTeams(gameAccount.getTeams());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public GameAccount getGameAccountForPlayerAndVideogame(Player player, Videogame videogame) {
		Assert.notNull(player);
		Assert.notNull(videogame);
		
		GameAccount result;
		
		result = this.gameAccountRepository.getGameAccountForPlayerAndVideogame(player.getId(), videogame.getId());
		
		return result;
	}
	
	public Collection<GameAccount> getGameAccountsForPlayer (Player player) {
		Assert.notNull(player);
		
		Collection<GameAccount> result;
		
		result = this.gameAccountRepository.getGameAccountsForPlayer (player.getId());
		
		return result;
	}
	
	public Collection<GameAccount> getGameAccountsNotTeam(Team team) {
		Assert.notNull(team);
		
		Collection<GameAccount> result;
		
		result = this.gameAccountRepository.getGameAccountsNotTeam (team.getId());
		
		return result;
	}

	public void flush() {
		gameAccountRepository.flush();
	}

}
