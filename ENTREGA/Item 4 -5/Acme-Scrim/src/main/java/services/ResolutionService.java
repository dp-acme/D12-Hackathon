package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ResolutionRepository;

import domain.Actor;
import domain.Player;
import domain.Priority;
import domain.Referee;
import domain.Resolution;
import domain.Result;
import domain.Scrim;
import domain.ScrimStatus;

@Service
@Transactional
public class ResolutionService {

	@Autowired
	private ResolutionRepository resolutionRepository;
	
	@Autowired
	private ScrimService scrimService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PlayerService playerService;
	
	public Resolution findOne(int id){
		Assert.isTrue(id != 0);
		
		Resolution result;
		
		result = resolutionRepository.findOne(id);
		
		return result;
	}
	
	public Collection<Resolution> findAll(){
		Collection<Resolution> result; 
		
		result = resolutionRepository.findAll();
		
		Assert.notNull(result);
		
		return result;
	}
	
	public Resolution create(int scrimId){
		Resolution result;
		Scrim scrim;
		
		scrim = scrimService.findOne(scrimId);
		result = new Resolution();
		result.setScrim(scrim);
		
		return result;
	}

	public Resolution save(Resolution resolution){
		Player player; 
		Scrim scrim;
		Resolution result;
		
		scrim = resolution.getScrim();
		player = actorService.checkIsPlayer();
	
		Assert.isTrue(scrim.getCreator().getLeader().getPlayer().equals(player) ^ scrim.getGuest().getLeader().getPlayer().equals(player));	
		Assert.isNull(scrim.getResolution());
		Assert.isTrue(scrim.getStatus().getScrimStatus().equals(ScrimStatus.CLOSED));
		
		if (scrim.getCreator().getLeader().getPlayer().equals(player)) {
			Assert.isTrue(this.playerService.getPremiumPlayersInTeam(scrim.getCreator().getId()).size() != 0);
		} else {
			Assert.isTrue(this.playerService.getPremiumPlayersInTeam(scrim.getGuest().getId()).size() != 0);
		}
		
		Assert.isTrue(scrim.getCreatorTeamResult().getResult().equals(Result.ILLOGICAL) && scrim.getGuestTeamResult().getResult().equals(Result.ILLOGICAL));
		
		result = resolutionRepository.save(resolution);
		
		if(resolution.getId() == 0){
			scrimService.askResolution(scrim, result);
		}
		
		return result;
	}
	
	public void delete (Resolution resolution) {
		Assert.notNull(resolution);
		
		Scrim scrim;
		
		scrim = resolution.getScrim();
		scrim.setResolution(null);
		
		this.scrimService.save(scrim);
		
		this.resolutionRepository.delete(resolution);
	}
	
	public Resolution addReferee(Resolution resolution, Referee referee){
		Referee principal;
		Resolution result = null; 
		
		Assert.notNull(resolution);
		Assert.notNull(referee);
		
		principal = actorService.checkIsReferee();
		Assert.isTrue(principal.equals(referee));
		
		
		if(resolution.getReferee() == null){
			resolution.setReferee(referee);
			result = resolutionRepository.save(resolution);
		}
		
		return result;
	}
	
	public Scrim setScrimResult(int resolutionId, Result resultCreator , Result resultGuest){
		Referee principal;
		Scrim result; 
		Resolution resolution;
				
		resolution = findOne(resolutionId);
		principal = actorService.checkIsReferee();
		result = resolution.getScrim();
		
		Assert.isTrue((resultCreator.getResult().equals(Result.DRAW) && resultGuest.getResult().equals(Result.DRAW)) 
				|| (resultCreator.getResult().equals(Result.WIN) && resultGuest.getResult().equals(Result.LOSE))  
				|| (resultCreator.getResult().equals(Result.LOSE) && resultGuest.getResult().equals(Result.WIN)));
		
		Assert.isTrue(resolution.getReferee().equals(principal));
		Assert.isTrue(result.getResolution().getReferee().equals(principal));

		resolution.setAttended(true);
		
		resolutionRepository.save(resolution);
		
		result = scrimService.resolveScrim(result, resultCreator, resultGuest);

		messageService.sendNotification("Resolution information [RESOLVED] -- Información de resolución [RESUELTO]", 
			"A referee "+principal.getName()+" has solved the problem with the results of the scrim: " + result.getCreator().getName() +" VS "
			+result.getCreator().getName() + "\n -- \n" + "El árbitro "+principal.getName()+" ha resuelto el problema con los resultados de la scrim: " + result.getCreator().getName() +" VS "
			+result.getCreator().getName(), new ArrayList<Actor>(playerService.getPlayersByScrim(result.getId())), Priority.NEUTRAL) ;
		
		return result;
	}
	
	public Collection<Resolution> getPendingResolutions(){
		return resolutionRepository.getPendingResolutions();
	}
	
	public Collection<Resolution> getSolvedResolutions() {
		return resolutionRepository.getSolvedResolutions();
	}

	
	public void flush() {
		resolutionRepository.flush();
	}


}
