package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ReportRepository;
import domain.Actor;
import domain.Configuration;
import domain.Player;
import domain.Priority;
import domain.Report;
import domain.Scrim;
import domain.ScrimStatus;

@Service
@Transactional
public class ReportService {
	
	@Autowired
	private ReportRepository reportRepository;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PlayerService playerService;

	@Autowired
	private ScrimService scrimService;
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private RefereeService refereeService;
	
	
	public Report findOne(int id){
		Assert.isTrue(id != 0);
		
		Report result;
		
		result = reportRepository.findOne(id);
		
		return result;
	}
	
	public Collection<Report> findAll(){
		Collection<Report> result; 
		
		result = reportRepository.findAll();
		
		Assert.notNull(result);
		
		return result;
	}
	
	public Report create(int playerId, int scrimId){
		Report result;
		Player player; 
		Scrim scrim; 
		
		Assert.isTrue(playerId > 0);
		Assert.isTrue(scrimId > 0);

		scrim = scrimService.findOne(scrimId);
		player = playerService.findOne(playerId);
		
		result = new Report();
		result.setScrim(scrim);
		result.setPlayer(player);
		
		return result;
	}
	
	public Report save(Report report){
		Player principal;
		Report result;
		
		Assert.notNull(report);
		
		principal = actorService.checkIsPlayer();
		
		Assert.isTrue(report.getScrim().getCreator().getLeader().getPlayer().equals(principal) ^ report.getScrim().getGuest().getLeader().getPlayer().equals(principal));
		Assert.isTrue(playerService.getPlayersByScrim(report.getScrim().getId()).contains(report.getPlayer()));		
		Assert.isTrue(getReportsByPlayerAndScrim(report.getPlayer().getId(), report.getScrim().getId()).size() == 0);
		Assert.isTrue(report.getScrim().getStatus().getScrimStatus().equals(ScrimStatus.FINISHED));

		if(report.getScrim().getCreator().getLeader().getPlayer().equals(principal)) {
			Assert.isTrue(playerService.getPlayersInTeam(report.getScrim().getGuest().getId()).contains(report.getPlayer()));
		} else if(report.getScrim().getGuest().getLeader().getPlayer().equals(principal)) {
			Assert.isTrue(playerService.getPlayersInTeam(report.getScrim().getCreator().getId()).contains(report.getPlayer()));
		}
		
		result = reportRepository.save(report);
		
		reportRepository.flush();
		
		if (report.getId() == 0) {
			Configuration config = configurationService.find();
			Assert.notNull(config);
			if (getReportsByPlayer(result.getPlayer().getId()).size() == config.getMinimumReports()) {
				// Send Notification
				
				String subject;
				String body;
				Collection<Actor> recipients;
				
				subject = result.getPlayer().getUserAccount() + " reached minimum reports to be banned";
				subject += " -- ";
				subject += result.getPlayer().getUserAccount() + " ha llegado al m�nimo de reportes para ser baneado";
				
				body = result.getPlayer().getUserAccount() + " reached minimum reports to be banned: " + config.getMinimumReports() + " reports.";
				body += "\n -- \n";
				body += result.getPlayer().getUserAccount() + " ha llegado al m�nimo de reportes para ser baneado: " + config.getMinimumReports() + " reportes.";
				
				recipients = new ArrayList<Actor> (new ArrayList<Actor>(refereeService.findAll()));
				
				messageService.sendNotification(subject, body, recipients, Priority.HIGH);
			}
		}
		
		return result;
	}
	
	public void delete(Report report) {
		Assert.notNull(report);
		
		this.reportRepository.delete(report);
	}
	
	public Collection<Report> getReportsByPlayer(int userId){
		return reportRepository.getReportsByPlayer(userId);
	}
	
	public Collection<Report> getReportsByPlayerAndScrim(int userId, int scrimId){
		return reportRepository.getReportsByPlayerAndScrim(userId, scrimId);
	}
	
	public Collection<Player> getPlayersByNumOfReports(long i){
		return reportRepository.getPlayersByNumOfReports(i);
	}

	public Collection<Player> getReportedPlayersOfScrim(Scrim scrim) {
		Assert.notNull(scrim);
		
		return reportRepository.getReportedPlayersOfScrim(scrim.getId());
	}
	
	public Collection<Report> getReportsOfScrim(Scrim scrim) {
		Assert.notNull(scrim);
		
		return reportRepository.getReportsOfScrim(scrim.getId());
	}

	public void flush() {
		reportRepository.flush();
	}

}
