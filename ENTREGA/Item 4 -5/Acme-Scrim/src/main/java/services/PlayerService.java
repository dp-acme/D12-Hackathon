package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.PlayerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Player;
import domain.Team;
import form.RegisterActor;

@Service
@Transactional
public class PlayerService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private PlayerRepository playerRepository;
	
	//Supporting Services--------------------------------------------------
	
	@Autowired
	private PremiumSubscriptionService premiumSubscriptionService;
	
	@Autowired
	private FolderService folderService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor ---------------------------------------------------
	public PlayerService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------
	
	public Player create() {

		Player result;

		result = new Player();
		result.setTeamsFollowed(new ArrayList<Team>());

		return result;
	}

	public Player findOne(int playerId) {
		Assert.isTrue(playerId != 0);

		Player result;

		result = playerRepository.findOne(playerId);

		return result;
	}

	public Collection<Player> findAll() {
		Collection<Player> result;

		result = playerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Player save(Player player) {
		Assert.notNull(player);
		Player result;
		UserAccount userAccount;
			
		if (player.getId() != 0) {
			userAccount = LoginService.getPrincipal();
			Assert.isTrue(player.getUserAccount().equals(userAccount));
			result = playerRepository.save(player);
		} else {
			Md5PasswordEncoder encoder;

			
			Assert.isTrue(!LoginService.isAuthenticated());
			
			encoder = new Md5PasswordEncoder();
			player.getUserAccount().setPassword(
					encoder.encodePassword(player.getUserAccount().getPassword(),
							null));
			result = playerRepository.save(player);
			folderService.createSystemFolders(actorService.findOne(result.getId()));
		}
		
		return result;
	}

	// Other business methods -------------------------------------------------

	@Autowired
	private Validator validator;

	public Player reconstruct(RegisterActor formObject, BindingResult binding) {
		Player result;
		Authority authority;

		authority = new Authority();
		result = create();
		result.setEmail(formObject.getEmail());
		result.setName(formObject.getName());
		result.setSurname(formObject.getSurname());
		authority.setAuthority(Authority.PLAYER);
		formObject.getUserAccount().addAuthority(authority);
		formObject.getUserAccount().setAccountNonLocked(true);
		result.setUserAccount(formObject.getUserAccount());

		validator.validate(formObject, binding);

		return result;
	}
	
	public Player reconstructEdit(Player player, BindingResult binding) {
		Player result;
		
		result = this.findOne(player.getId());
		
		if(result.getPremiumSubscription() != null){
			player.setPremiumSubscription(player.getPremiumSubscription());
		}
		player.setTeamsFollowed(result.getTeamsFollowed());
		player.setUserAccount(result.getUserAccount());

		validator.validate(player, binding);

		return player;
	}

	public void flush() {
		playerRepository.flush();
	}
	
	public Boolean isPremium(int playerId){
		Boolean result;
		
		result = premiumSubscriptionService.hasAnyPremiumSubscription(playerId);
	
		return result;
	}
	
	public Collection<Player> getPlayersByScrim(int scrimId){
		Collection<Player> result;
		
		result = playerRepository.getPlayersByScrim(scrimId);
		
		return result;
	}
	
	public Collection<Player> getPremiumPlayersByScrim(int scrimId){
		Collection<Player> result;
		
		result = playerRepository.getPremiumPlayersByScrim(scrimId);
		
		return result;
	}
	
	public Collection<Player> getPremiumPlayersInTeam(int teamId){
		Collection<Player> result;
		
		result = playerRepository.getPremiumPlayersInTeam(teamId);
		
		return result;
	}
	
	public Collection<Player> getPlayersInTeam(int teamId){
		Collection<Player> result;
		
		result = playerRepository.getPlayersInTeam(teamId);
		
		return result;
	}
}
