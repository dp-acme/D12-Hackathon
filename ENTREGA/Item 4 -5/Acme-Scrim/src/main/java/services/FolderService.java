package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.FolderRepository;
import domain.Actor;
import domain.Folder;
import domain.FolderType;
import domain.Message;

@Service
@Transactional
public class FolderService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private FolderRepository folderRepository;
	
	// Validator ---------------------------------------------------
	
	@Autowired
	Validator validator;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private MessageService messageService;
	
	// Constructor ---------------------------------------------------
	
	public FolderService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Folder create(Folder parent, Actor actor){
		Folder result;
		
		result = new Folder();
		result.setActor(actor);
		result.setParent(parent);
		result.setChildren(new ArrayList<Folder>());
		result.setMessages(new ArrayList<Message>());
		
		return result;
	}
	
	public Folder findOne(int folderId) {
		Assert.isTrue(folderId != 0);
		
		Folder result;
		
		result = folderRepository.findOne(folderId);
		
		return result;
	}
	
	public Collection<Folder> findAll() {		
		Collection<Folder> result;
		
		result = folderRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Folder save(Folder folder) {
		Assert.notNull(folder);
		Assert.isTrue(folder.getParent() == null || folder.getParent().getType().getFolderType().equals(FolderType.USERBOX));
		
		if(folder.getId() != 0){
			Folder oldFolder;
			
			oldFolder = this.findOne(folder.getId());
			Assert.notNull(oldFolder);
			Assert.isTrue(folder.getType() == null || folder.getType().equals(folder.getType()));
			Assert.isTrue(folder.getName() == null || folder.getName().equals(folder.getName()));
		}
		
		Actor principal;
		principal = actorService.findPrincipal();
		
		Assert.isTrue(principal.equals(folder.getActor())); //Debe estar logueado el due�o de la carpeta
		
		Folder result;
		
		result = folderRepository.save(folder);
		
		if(result.getParent() != null && !result.getParent().getChildren().contains(result)){ //Actualizar la lista de carpetas del padre
			Folder parent;
			
			parent = result.getParent();
			parent.getChildren().add(result);
			
			folderRepository.save(parent);
		}
		
		return result;
	}
	
	public void delete(Folder folder){
		Assert.notNull(folder);
		
		Folder parent;
		Actor principal;
		principal = actorService.findPrincipal();
		
		Assert.isTrue(principal.equals(folder.getActor())); //Debe estar logueado el due�o de la carpeta
		Assert.isTrue(folder.getType().getFolderType().equals(FolderType.USERBOX)); //Solo se pueden borrar carpetas que haya creado el usuario
		
		parent = folder.getParent();
		
		if(parent != null){
			parent.getChildren().remove(folder); //Actualizar las carpetas del padre	
			folder.setParent(null);

			folderRepository.save(folder);
			folderRepository.save(parent);
		}
		
		ArrayList<Message> messages = new ArrayList<>(folder.getMessages());
		Iterator<Message> messageIt = messages.iterator(); //Borrar los mensajes de la carpeta
		while(messageIt.hasNext()){
			messageService.delete(messageIt.next());
		}
		
		ArrayList<Folder> folders = new ArrayList<>(folder.getChildren());
		Iterator<Folder> folderIt = folders.iterator(); //Borrar las carpetas hijo
		while(folderIt.hasNext()){
			this.delete(folderIt.next());
		}
		
		folderRepository.delete(folder);
	}
	
	// Other business methods ---------------------------------------------------

	public void createSystemFolder(String name, String type, Actor actor){
		Folder folder;
		
		folder = this.create(null, actor);
		folder.setName(name);
		folder.setType(new FolderType());
		folder.getType().setFolderType(type);
		
		folderRepository.save(folder);
	}
	
	public void createSystemFolders(Actor actor){
		createSystemFolder("Inbox", FolderType.INBOX, actor);
		createSystemFolder("Outbox", FolderType.OUTBOX, actor);
		createSystemFolder("Notifications", FolderType.NOTIFICATIONBOX, actor);
		createSystemFolder("Trash", FolderType.TRASHBOX, actor);
		createSystemFolder("Spam", FolderType.SPAMBOX, actor);
	}
	
	void addMessageToFolder(Folder folder, Message message){
		Assert.notNull(folder);
		Assert.notNull(message);
		
		folder.getMessages().add(message);
		folderRepository.save(folder);
	}
	
	Boolean isChildOf(Folder folder, Folder check){ //Comprobar si check est� entre los hijos de folder (o es folder)
		if(folder.equals(check)){
			return true;
		}
		
		for(Folder f : folder.getChildren()){
			if(isChildOf(f, check)){
				return true;
			}
		}
		
		return false;
	}
	
	public void moveFolder(Integer folderId, Integer newParentId){
		Actor principal;
		principal = actorService.findPrincipal();
		
		Assert.notNull(folderId);
		
		Folder folder;
		Folder parent, oldParent;
		
		folder = folderRepository.findOne(folderId);
		Assert.notNull(folder);
		
		oldParent = folder.getParent();
		
		if(newParentId != null){
			parent = folderRepository.findOne(newParentId);
			Assert.notNull(parent);
			
			Assert.isTrue(!isChildOf(folder, parent)); //Comprobar que no se est� moviendo a uno de los hijos
			Assert.isTrue(!parent.equals(oldParent)); //No puedes mover al mismo sitio

			Assert.isTrue(oldParent == null || oldParent.getActor().equals(parent.getActor())); //No se puede cambiar de due�o
			Assert.isTrue(principal.equals(parent.getActor())); //Debe estar logueado como el due�o de la carpeta padre
			Assert.isTrue(parent.getType().getFolderType().equals(FolderType.USERBOX)); //Se debe mover a una userbox
			
		} else{	
			parent = null;
		}
				
		Assert.isTrue(principal.equals(folder.getActor())); //Debe estar logueado como el due�o de la carpeta

		if(oldParent != null){
			oldParent.getChildren().remove(folder); //Actualizar las listas de carpetas de la carpeta padre antigua
			folderRepository.save(oldParent);	
		}
		
		folder.setParent(parent); //Actualizar carpeta
		this.save(folder);
	}
	
	public Folder findTrashboxByActorId(Integer actorId){
		Assert.notNull(actorId);
		
		Folder result;
		
		result = folderRepository.findTrashboxByActorId(actorId);
		Assert.notNull(result);
		
		return result;
	}

	public Folder findInboxByActorId(Integer actorId) {
		Assert.notNull(actorId);
		
		Folder result;
		
		result = folderRepository.findInboxByActorId(actorId);
		Assert.notNull(result);
		
		return result;
	}

	public Folder findNotificationBoxByActorId(Integer actorId) {
		Assert.notNull(actorId);
		
		Folder result;
		
		result = folderRepository.findNotificationBoxByActorId(actorId);
		Assert.notNull(result);
		
		return result;
	}

	public Folder findOutboxByActorId(Integer actorId) {
		Assert.notNull(actorId);
		
		Folder result;
		
		result = folderRepository.findOutboxByActorId(actorId);
		Assert.notNull(result);
		
		return result;
	}

	public Folder findSpamboxByActorId(Integer actorId) {
		Assert.notNull(actorId);
		
		Folder result;
		
		result = folderRepository.findSpamboxByActorId(actorId);
		Assert.notNull(result);
		
		return result;
	}

	public Collection<Folder> findParentFoldersByActorId(Integer actorId) {
		Assert.notNull(actorId);
		
		Collection<Folder> result;
		
		result = folderRepository.findParentFoldersByActorId(actorId);
		Assert.notNull(result);
		
		return result;
	}

	public Folder reconstruct(Folder folder, Actor actor, Folder parent, BindingResult binding) {
		Folder result;
		
		result = folder;
		
		if(result.getId() == 0){ //Creaci�n
			result.setActor(actor);
			result.setParent(parent);
			result.setChildren(new ArrayList<Folder>());
			result.setMessages(new ArrayList<Message>());
			result.setType(new FolderType());
			result.getType().setFolderType(FolderType.USERBOX);
		
		} else{ //Edici�n
			Folder oldFolder = this.findOne(folder.getId());
			Assert.notNull(oldFolder);
			
			result.setVersion(oldFolder.getVersion());
			result.setActor(oldFolder.getActor());
			result.setChildren(oldFolder.getChildren());
			result.setMessages(oldFolder.getMessages());
			result.setParent(oldFolder.getParent());
			result.setType(oldFolder.getType());
		}
		
		validator.validate(result, binding);
		
		return result;
	}

	public void validate(Folder folder, BindingResult binding) {
		validator.validate(folder, binding);
	}

	public void flush() {
		folderRepository.flush();
	}
}
