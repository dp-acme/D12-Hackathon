package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.FinderRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Finder;
import domain.Scrim;

@Service
@Transactional
public class FinderService {

	// Managed repository --------------------------------------
	
	@Autowired
	private FinderRepository finderRepository;

	// Supporting services -------------------------------------
	@Autowired
	private Validator validator;
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor ---------------------------------------------------

	public FinderService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Finder create() {
		UserAccount userAccount;
		Actor actor;
		Finder result;
		
		userAccount = LoginService.getPrincipal();
		actor = actorService.findByUserAccountId(userAccount.getId());
		Assert.isInstanceOf(Actor.class, actor);
		
		result = new Finder();
		
		result.setActor((Actor)actor);
		result.setScrims(new ArrayList<Scrim>());
		result.setLastSaved(new Date(System.currentTimeMillis() - 1));
		
		return result;
	}

	public Finder findOne(int finderId) {
		Assert.isTrue(finderId != 0);

		Finder result;

		result = finderRepository.findOne(finderId);

		return result;
	}
	
	public Collection<Finder> findAll() {
		Collection<Finder> result;

		result = finderRepository.findAll();

		return result;
	}

	public Finder save(Finder finder) {
		Assert.notNull(finder);
		
		Finder result;
		Collection<Scrim> scrims;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		
		Assert.isTrue(finder.getActor().getUserAccount().equals(principal));
				
		if (finder.getDateEnd() != null && finder.getDateStart() != null)
			Assert.isTrue(finder.getDateEnd().after(finder.getDateStart()));
	
		scrims = new ArrayList<Scrim>(filterScrimsByCriteria(finder));
		finder.setScrims(scrims);
		
		finder.setLastSaved(new Date(System.currentTimeMillis() - 1));

		result = finderRepository.save(finder);
		
		if (finder.getId() == 0) {
			Actor actor;
			
			actor = (Actor)actorService.findByUserAccountId(principal.getId());
			
			actorService.save(actor);
		}
		
		return result;
	}
	
	public void delete(Finder finder) {
		Assert.notNull(finder);
		
		UserAccount userAccount;
		
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(finder.getActor().getUserAccount().equals(userAccount));
		Assert.isTrue(finderRepository.exists(finder.getId()));
		
		actorService.save(finder.getActor());
		
		finderRepository.delete(finder);
	}
	
	//Other business methods -----------------------------------
	
	private boolean sameFinderData(Finder finder1, Finder finder2){
		boolean res = true;

		if(finder1.getDateStart() != null){
			res = finder1.getDateStart().equals(finder2.getDateStart());
		
		} else{
			res = finder2.getDateStart() == null;
		}
		
		if(res){
			if(finder1.getDateEnd() != null){
				res = finder1.getDateEnd().equals(finder2.getDateEnd());
				
			} else{
				res = finder2.getDateEnd() == null;
			}	
		}
		
		if(res){
			if(finder1.getKeyWord() != null){
				res = finder1.getKeyWord().equals(finder2.getKeyWord());
				
			} else{
				res = finder2.getKeyWord() == null;
			}	
		}
		
		if(res){
			if(finder1.getVideogame() != null){
				res = finder1.getVideogame().equals(finder2.getVideogame());
				
			} else{
				res = finder2.getVideogame() == null;
			}	
		}
		
		if(res){
			if(finder1.getRegion() != null){
				res = finder1.getRegion().equals(finder2.getRegion());
				
			} else{
				res = finder2.getRegion() == null;
			}	
		}
		
		return res;
	}
	
	public Finder updateFinder(Finder finder, Finder newFinder){
		Finder result;
		
		result = finder;
		
		if(sameFinderData(finder, newFinder)){ //Si tiene los mismos t�rminos de b�squeda
			Date lastMoment, newMoment;
			Integer interval;
			
			interval = configurationService.find().getFinderCacheTime();
			
			lastMoment = DateUtils.addHours(finder.getLastSaved(), interval);
			newMoment = newFinder.getLastSaved();
									
			if(lastMoment.after(newMoment)){
				result = this.save(finder);
			}
			
		} else{ //Actualizar finder
			finder.setDateStart(newFinder.getDateStart());
			finder.setDateEnd(newFinder.getDateEnd());
			finder.setKeyWord(newFinder.getKeyWord());
			finder.setVideogame(newFinder.getVideogame());
			finder.setRegion(newFinder.getRegion());

			
			result = this.save(finder);			
		}
		
		return result;
	}
	
	
	public Finder reconstruct(Finder finder,  BindingResult binding){
		Finder result;
		
		result = finder;
		
		if(result.getId() == 0){
			result.setActor(actorService.findPrincipal());
			result.setScrims(new ArrayList<Scrim>());
			
		}else{
			result = findOne(finder.getId());
			finder.setActor(result.getActor());
		}
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Collection<Scrim> filterScrimsByCriteria(final Finder finder) {
		String keyWord;
		Date dateStart, dateEnd;

		Collection<Scrim> result;

		keyWord = finder.getKeyWord();
		dateStart = finder.getDateStart();
		dateEnd = finder.getDateEnd();

		if (keyWord == null)
			keyWord = "";
		if (dateStart == null)
			dateStart = new Date(0);
		if (dateEnd == null)
			dateEnd = new Date(Long.MAX_VALUE);

		if(finder.getRegion()==null&&finder.getVideogame()==null){
			result = finderRepository.filterScrimsByCriteriaFinder('%' + keyWord + '%', dateStart, dateEnd, new PageRequest(0, this.configurationService.find().getFinderResults())).getContent();
		}else if(finder.getRegion()==null){
			result = finderRepository.filterScrimsByCriteriaFinder('%' + keyWord + '%', dateStart, dateEnd, finder.getVideogame(),new PageRequest(0, this.configurationService.find().getFinderResults())).getContent();
		}else if(finder.getVideogame()==null){
			result = finderRepository.filterScrimsByCriteriaFinder('%' + keyWord + '%', dateStart, dateEnd, finder.getRegion(), new PageRequest(0, this.configurationService.find().getFinderResults())).getContent();
		}else{
			result = finderRepository.filterScrimsByCriteriaFinder('%' + keyWord + '%', dateStart, dateEnd, finder.getVideogame(), finder.getRegion(), new PageRequest(0, this.configurationService.find().getFinderResults())).getContent();
		}
		Assert.notNull(result);

		return result;
	}

	public Collection<Scrim> filterScrimsByCriteria(String keyWord, Date dateStart, Date dateEnd) {
		Collection<Scrim> result;

		if (keyWord == null)
			keyWord = "";
		if (dateStart == null)
			dateStart = new Date(0);
		if (dateEnd == null)
			dateEnd = new Date(Long.MAX_VALUE);		

		result = finderRepository.filterScrimsByCriteriaFinder('%' + keyWord + '%', dateStart, dateEnd, new PageRequest(0, this.configurationService.find().getFinderResults())).getContent();
		return result;
	}
	
	public Finder getFinderFromActor(){
		Actor principal;
		
		principal = actorService.findPrincipal();
		
		return finderRepository.getFinderFromActor(principal);
	}

	public void flush() {
		finderRepository.flush();
	}
}
