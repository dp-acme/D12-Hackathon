package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.Validator;

import repositories.SocialNetworkRepository;
import domain.SocialIdentity;
import domain.SocialNetwork;

@Service
@Transactional
public class SocialNetworkService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private SocialNetworkRepository socialNetworkRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private SocialIdentityService socialIdentityService;
	
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Constructor ---------------------------------------------------
	
	public SocialNetworkService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public SocialNetwork create(){
		SocialNetwork result;
		
		result = new SocialNetwork();
		
		return result;
	}
	
	public SocialNetwork findOne(int socialNetworkId) {
		Assert.isTrue(socialNetworkId != 0);
		
		SocialNetwork result;
		
		result = socialNetworkRepository.findOne(socialNetworkId);
		
		return result;
	}
	
	public Collection<SocialNetwork> findAll() {		
		Collection<SocialNetwork> result;
		
		try{
			result = socialNetworkRepository.findAll();
		}
		catch(Throwable e){
			result = null;
		}
		
		Assert.notNull(result);
		
		return result;
	}
	
	public SocialNetwork save(SocialNetwork socialNetwork) {
		Assert.notNull(socialNetwork);
		SocialNetwork result;
		actorService.checkIsAdministrator();//Debes estar autenticado como admin
		
		result = socialNetworkRepository.save(socialNetwork);
		
		return result;
	}
	
	public void delete(int socialNetworkId){
		SocialNetwork socialNetwork;
		
		socialNetwork = this.findOne(socialNetworkId);
		Assert.notNull(socialNetwork);

		actorService.checkIsAdministrator();
		
		ArrayList<SocialIdentity> socialIdentities = new ArrayList<>(socialIdentityService.getSocialIdentitiesBySocialNetwork(socialNetworkId));
		Iterator<SocialIdentity> socialIdentityIt = socialIdentities.iterator(); //Borrar los mensajes de la carpeta
		while(socialIdentityIt.hasNext()){
			socialIdentityService.delete(socialIdentityIt.next().getId());
		}
		
		socialNetworkRepository.delete(socialNetwork);
	}
	
	
	// Other business methods ---------------------------------------------------
	
	
	public void flush() {
		socialNetworkRepository.flush();
	}
}
