package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Videogame;

@Component
@Transactional
public class VideogameToStringConverter implements Converter<Videogame, String>{
	
	@Override
	public String convert(Videogame source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}
	
}
