package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.ForumRepository;
import domain.Forum;

@Component
@Transactional
public class StringToForumConverter implements Converter<String, Forum>{
	
	@Autowired
	ForumRepository forumRepository;
	
	@Override
	public Forum convert(String source) {
		Forum result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = forumRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
