package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.GameAccountRepository;
import domain.GameAccount;

@Component
@Transactional
public class StringToGameAccountConverter implements Converter<String, GameAccount>{
	
	@Autowired
	GameAccountRepository gameAccountRepository;
	
	@Override
	public GameAccount convert(String source) {
		GameAccount result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = gameAccountRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
