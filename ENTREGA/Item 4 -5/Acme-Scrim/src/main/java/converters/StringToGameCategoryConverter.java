package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.GameCategoryRepository;
import domain.GameCategory;

@Component
@Transactional
public class StringToGameCategoryConverter implements Converter<String, GameCategory>{
	
	@Autowired
	GameCategoryRepository gameCategoryRepository;
	
	@Override
	public GameCategory convert(String source) {
		GameCategory result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = gameCategoryRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
