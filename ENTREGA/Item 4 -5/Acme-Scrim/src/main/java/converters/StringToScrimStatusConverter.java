package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.ScrimStatus;

@Component
@Transactional
public class StringToScrimStatusConverter implements Converter<String, ScrimStatus>{
	
	@Override
	public ScrimStatus convert(String source) {
		ScrimStatus result;
		result = new ScrimStatus();

		if(source.equals(ScrimStatus.CANCELLED)){
			result.setScrimStatus(ScrimStatus.CANCELLED);
			
		} else if(source.equals(ScrimStatus.CLOSED)){
			result.setScrimStatus(ScrimStatus.CLOSED);
			
		} else if(source.equals(ScrimStatus.CONFIRMED)){
			result.setScrimStatus(ScrimStatus.CONFIRMED);
			
		} else if(source.equals(ScrimStatus.EXPIRED)){
			result.setScrimStatus(ScrimStatus.EXPIRED);
			
		} else if(source.equals(ScrimStatus.FINISHED)){
			result.setScrimStatus(ScrimStatus.FINISHED);
			
		} else if(source.equals(ScrimStatus.PENDING)){
			result.setScrimStatus(ScrimStatus.PENDING);
			
		} else{
			throw new IllegalArgumentException("Invalid domain.ScrimStatus format \"" + source + "\"");
		}
		
		return result;
	}

}
