package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Permission;

@Component
@Transactional
public class PermissionToStringConverter implements Converter<Permission, String>{
	
	@Override
	public String convert(Permission source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}
	
}
