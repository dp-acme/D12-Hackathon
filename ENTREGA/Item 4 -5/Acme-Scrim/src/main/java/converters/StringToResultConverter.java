package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Result;

@Component
@Transactional
public class StringToResultConverter implements Converter<String, Result>{
	
	@Override
	public Result convert(String source) {
		Result result;
		result = new Result();

		if(source.equals(Result.DRAW)){
			result.setResult(Result.DRAW);
			
		} else if(source.equals(Result.ILLOGICAL)){
			result.setResult(Result.ILLOGICAL);
			
		} else if(source.equals(Result.LOSE)){
			result.setResult(Result.LOSE);
		
		} else if(source.equals(Result.WIN)){
			result.setResult(Result.WIN);
			
		} else{
			throw new IllegalArgumentException("Invalid domain.Result format \"" + source + "\"");
		}
		
		return result;
	}

}
