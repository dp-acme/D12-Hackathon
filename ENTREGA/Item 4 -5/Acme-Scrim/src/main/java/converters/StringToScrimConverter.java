package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.ScrimRepository;
import domain.Scrim;

@Component
@Transactional
public class StringToScrimConverter implements Converter<String, Scrim>{
	
	@Autowired
	ScrimRepository scrimRepository;
	
	@Override
	public Scrim convert(String source) {
		Scrim result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = scrimRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
