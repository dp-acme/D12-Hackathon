package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Referee;

@Component
@Transactional
public class RefereeToStringConverter implements Converter<Referee, String>{
	
	@Override
	public String convert(Referee source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}
	
}
