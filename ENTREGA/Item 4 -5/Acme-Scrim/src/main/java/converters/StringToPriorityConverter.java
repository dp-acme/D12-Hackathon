package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Priority;

@Component
@Transactional
public class StringToPriorityConverter implements Converter<String, Priority>{
	
	@Override
	public Priority convert(String source) {
		Priority result;
		result = new Priority();

		if(source.equals(Priority.HIGH)){
			result.setPriority(Priority.HIGH);
			
		} else if(source.equals(Priority.NEUTRAL)){
			result.setPriority(Priority.NEUTRAL);
			
		} else if(source.equals(Priority.LOW)){
			result.setPriority(Priority.LOW);
			
		} else{
			throw new IllegalArgumentException("Invalid domain.Priority format \"" + source + "\"");
		}
		
		return result;
	}

}
