package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.SponsorRepository;
import domain.Sponsor;

@Component
@Transactional
public class StringToRefereeConverter implements Converter<String, Sponsor>{
	
	@Autowired
	SponsorRepository sponsorRepository;
	
	@Override
	public Sponsor convert(String source) {
		Sponsor result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = sponsorRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
