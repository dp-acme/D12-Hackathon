package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.ForumCategory;

@Component
@Transactional
public class ForumCategoryToStringConverter implements Converter<ForumCategory, String>{
	
	@Override
	public String convert(ForumCategory source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}
	
}
