package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Result;

@Component
@Transactional
public class ResultToStringConverter implements Converter<Result, String>{
	
	@Override
	public String convert(Result source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getResult());

		return result;
	}
	
}
