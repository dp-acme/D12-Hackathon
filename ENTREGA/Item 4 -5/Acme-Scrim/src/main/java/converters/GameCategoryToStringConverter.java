package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.GameCategory;

@Component
@Transactional
public class GameCategoryToStringConverter implements Converter<GameCategory, String>{
	
	@Override
	public String convert(GameCategory source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}
	
}
