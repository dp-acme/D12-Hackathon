package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.SocialNetworkRepository;
import domain.SocialNetwork;

@Component
@Transactional
public class StringToSocialNetworkConverter implements Converter<String, SocialNetwork>{
	
	@Autowired
	SocialNetworkRepository socialNetworkRepository;
	
	@Override
	public SocialNetwork convert(String source) {
		SocialNetwork result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = socialNetworkRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
