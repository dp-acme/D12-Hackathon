package form;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import domain.GameCategory;

public class VideogameForm {

	private String name;
	private String description;
	private String image;
	private String seasonName;
	private Collection<GameCategory> gameCategories;

	@NotBlank
	public String getSeasonName() {
		return seasonName;
	}

	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotBlank
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@NotNull
	public Collection<GameCategory> getGameCategories() {
		return gameCategories;
	}

	public void setGameCategories(Collection<GameCategory> gameCategories) {
		this.gameCategories = gameCategories;
	}

}
