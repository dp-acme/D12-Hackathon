package controllers.season;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SeasonService;
import services.VideogameService;
import controllers.AbstractController;
import domain.Scrim;
import domain.Season;
import domain.Videogame;

@Controller
@RequestMapping("/season")
public class SeasonController extends AbstractController {

	// Services -------------------------------------
	@Autowired
	private SeasonService seasonService;

	@Autowired
	private VideogameService videogameService;

	// Constructor -----------------------------------
	public SeasonController() {
		super();
	}

	// Display --------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam int seasonId) {
		ModelAndView result;
		Season season;

		season = this.seasonService.findOne(seasonId);
		Assert.notNull(season);

		Videogame videogame;
		videogame = season.getVideogame();

		Collection<Season> seasons = this.seasonService
				.seasonsOfVideoGame(videogame.getId());

		result = new ModelAndView("season/display");
		result.addObject("season", season);
		result.addObject("seasons", seasons);

		return result;
	}

	// List -----------------------------------------
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam int videogameId,
			@RequestParam(required = false) Integer seasonId) {
		// Creamos el objeto a devolver
		ModelAndView result;

		// COmporbamo si el videogame no esta en la bd
		Videogame videogame;
		videogame = this.videogameService.findOne(videogameId);
		Assert.notNull(videogame);

		Collection<Season> temporadas = videogame.getSeasons();
		Season seasonAMostrar = null;
		Collection<Scrim> scrimsActuales = new ArrayList<Scrim>();
		Collection<Scrim> scrimsPasadas = new ArrayList<Scrim>();

		if (seasonId != null) {
			seasonAMostrar = this.seasonService.findOne(seasonId);
			temporadas.remove(seasonAMostrar);
			scrimsActuales = this.seasonService
					.findScrimsNoPassedOfSeason(seasonAMostrar);
			scrimsPasadas = this.seasonService
					.findScrimsPassedOfSeason(seasonAMostrar);

		} else {
			Season lista = this.seasonService
					.getLastSeasonOfVideogame(videogameId);
			List<Season> listaSea = new ArrayList<Season>();
			listaSea.add(lista);
			if (!listaSea.isEmpty()) {
				seasonAMostrar = lista;
				scrimsActuales = this.seasonService
						.findScrimsNoPassedOfSeason(seasonAMostrar);
				scrimsPasadas = this.seasonService
						.findScrimsPassedOfSeason(seasonAMostrar);
				temporadas.remove(seasonAMostrar);
			}
		}
		// Inicializamos la variable a devolver
		result = new ModelAndView("season/list");

		// Al modelo y a la vista le a�adimos los siguientes atributos
		result.addObject("seasons", temporadas);
		result.addObject("currentSeason", seasonAMostrar);
		result.addObject("requestURI", "season/list.do");
		result.addObject("scrimsPasadas", scrimsPasadas);
		result.addObject("scrimsNoPasadas", scrimsActuales);

		// Devolvemos el resultado
		return result;

	}

}