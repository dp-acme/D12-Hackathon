/*
 * AbstractController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Random;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.AdvertisementService;
import services.ConfigurationService;
import services.PremiumSubscriptionService;
import domain.Actor;
import domain.Advertisement;
import domain.Configuration;
import domain.Player;

@Controller
public class AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private AdvertisementService advertisementService;

	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private PremiumSubscriptionService premiumSubscriptionService;
	
	@Autowired
	private ActorService actorService;

	// Panic handler ----------------------------------------------------------

	@ExceptionHandler(Throwable.class)
	public ModelAndView panic(final Throwable oops) {
		ModelAndView result;

		result = new ModelAndView("misc/panic");
		result.addObject("name", ClassUtils.getShortName(oops.getClass()));
		result.addObject("exception", oops.getMessage());
		result.addObject("stackTrace", ExceptionUtils.getStackTrace(oops));

		return result;
	}

	@ModelAttribute
	public void everyRequest(WebRequest request, Model model) {
		Configuration config;
		String banner;
		String adImage;
		String adURL;
		boolean isSystemAd = false;
		
		// Logo de la p�gina
		config = this.configurationService.find();
		banner = config.getBanner();

		// Anuncio
		int randomInt;
		
		Random random = new Random();
		randomInt = random.nextInt(100);
		
		if (randomInt < config.getAdRatio() || this.advertisementService.findAll().size()<=0) {
			isSystemAd = true;
			adImage = config.getAdImage();
			adURL = "welcome/advertise.do";
		} else {
			Advertisement advertisement;
			
			advertisement = this.advertisementService.findOneRandom();
			
			adImage = advertisement.getImage();
			adURL = advertisement.getWeb();
		}
		
		//Premium Subscription
		if(LoginService.isAuthenticated()){
			Actor principal = this.actorService.findPrincipal();
			if(principal instanceof domain.Player && premiumSubscriptionService.hasAnyPremiumSubscription(((Player) principal).getId())) {
				model.addAttribute("isPremium", true);
			}
		}
		
		model.addAttribute("isSystemAd", isSystemAd);
		model.addAttribute("adImage", adImage);
		model.addAttribute("adURL", adURL);
		model.addAttribute("banner", banner);
	}
}
