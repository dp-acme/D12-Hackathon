package controllers.videogame;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ForumCategoryService;
import services.GameCategoryService;
import services.VideogameService;
import controllers.AbstractController;
import domain.ForumCategory;
import domain.GameCategory;
import domain.Manager;
import domain.Videogame;
import form.VideogameForm;

@Controller
@RequestMapping("/videogame/manager")
public class VideogameManagerController extends AbstractController {

	// Services ----------------------------------------
	@Autowired
	private VideogameService videogameService;

	@Autowired
	private ActorService actorService;

	@Autowired
	private ForumCategoryService forumCategoryService;
	
	@Autowired
	private GameCategoryService gameCategoryService;

	// COntructor --------------------------------------
	public VideogameManagerController() {
		super();
	}

	// Create ------------------------------------------
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		VideogameForm videogameForm;
		
		this.actorService.checkIsManager();
		
		videogameForm = new VideogameForm();

		result = createEditModelAndView(videogameForm);
		
		return result;
	}

	// List -----------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		// Creamos el objeto a devolver
		ModelAndView result;

		Manager manager = this.actorService.checkIsManager();

		// Obtenemos los videogames del sistema
		Collection<Videogame> videogames;
		videogames = manager.getmyVideogames();

		// Inicializamos la variable a devolver
		result = new ModelAndView("videogame/manager/list");

		// Al modelo y a la vista le a�adimos los siguientes atributos
		result.addObject("videogames", videogames);
		result.addObject("requestURI", "videogame/manager/list.do");

		// Devolvemos el resultado
		return result;

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(
			@ModelAttribute(value = "videogameForm") VideogameForm videogameForm,
			BindingResult binding) {
		ModelAndView result;
		Videogame videogame;
		this.actorService.checkIsManager();
		
		videogame = videogameService.reconstruct(videogameForm,
				binding);

		if (binding.hasErrors()) {
			result = createEditModelAndView(videogameForm,
					"videogame.commit.error");
		} else {
			try {
				videogame = this.videogameService.saveNewVideogame(videogame, videogameForm.getSeasonName(), 
						 videogameForm.getGameCategories());
				
				result = new ModelAndView(
						"redirect:/videogame/display.do?videogameId="
								+ videogame.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(videogameForm,
						"videogame.commit.error");
			}
		}

		return result;
	}
	
	private ModelAndView createEditModelAndView(VideogameForm videogameForm) {
		ModelAndView result;

		result = createEditModelAndView(videogameForm, null);

		return result;
	}

	private ModelAndView createEditModelAndView(VideogameForm videogameForm,
			String messageCode) {
		ModelAndView result;

		Collection<ForumCategory> categories = this.forumCategoryService
				.findAll();
		Collection<GameCategory> gameCategories = this.gameCategoryService
				.findAll();

		result = new ModelAndView("videogame/manager/create");

		result.addObject("videogameForm", videogameForm);
		result.addObject("message", messageCode);
		result.addObject("categoryToSelect", categories);
		result.addObject("categoryToSelect2", gameCategories);
		
		return result;
	}
}