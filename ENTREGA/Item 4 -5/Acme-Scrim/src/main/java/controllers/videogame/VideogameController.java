package controllers.videogame;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.GameAccountService;
import services.GameCategoryService;
import services.VideogameService;
import controllers.AbstractController;
import domain.Actor;
import domain.GameAccount;
import domain.Player;
import domain.Videogame;

@Controller
@RequestMapping("/videogame")
public class VideogameController extends AbstractController {
	
	// Services -------------------------------------
	
	@Autowired
	private VideogameService videogameService;
	
	@Autowired
	private GameCategoryService gameCategoryService;

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private GameAccountService gameAccountService;
	
	// Contructor -----------------------------------
	
	public VideogameController() {
		super();
	}
	
	// Display ---------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam int videogameId) {
		ModelAndView result;
		Videogame videogame;
		Actor principal;
		
		videogame = this.videogameService.findOne(videogameId);
		Assert.notNull(videogame);

		result = new ModelAndView("videogame/display");

		if(LoginService.isAuthenticated()){
			principal = this.actorService.findPrincipal();
			
			if (principal instanceof Player) {
				Player player;
				
				player = (Player) principal;
				
				GameAccount gameAccount = null;
				
				gameAccount = this.gameAccountService.getGameAccountForPlayerAndVideogame(player, videogame);
				
				result.addObject("gameAccount", gameAccount);
			}
		}
		
		result.addObject("videogame", videogame);

		return result;
	}

	// List -----------------------------------------
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(required = false) Integer gameCategoryId) {
		// Creamos el objeto a devolver
		ModelAndView result;

		// Obtenemos los videogames del sistema
		Collection<Videogame> videogames = new ArrayList<Videogame>();
		if(gameCategoryId !=null){
			videogames = this.gameCategoryService.findVideogamesOfGameCategory(gameCategoryId);
		}else{
			videogames = this.videogameService.findAll();
		}

		// Inicializamos la variable a devolver
		result = new ModelAndView("videogame/list");
		
		// Al modelo y a la vista le a�adimos los siguientes atributos
		result.addObject("videogames", videogames);
		result.addObject("requestURI", "videogame/list.do");

		// Devolvemos el resultado
		return result;

	}

}