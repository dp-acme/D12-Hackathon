package controllers.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CommentService;
import controllers.AbstractController;
import domain.Comment;


@Controller
@RequestMapping("/comment/player")
public class CommentController extends AbstractController{
	
	// Services------------------------------------------------------------
	
	@Autowired
	private CommentService commentService;
	
	// Constructor--------------------------------------------------------------------
	
	public CommentController() {
		super();
	}

	// Create ----------------------------------------------------------------
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView create(@RequestParam(required = true) int scrimId, @ModelAttribute("comment") Comment comment, BindingResult binding){
		Comment commentRes;
		ModelAndView result;

		commentRes = commentService.reconstruct(comment, scrimId, binding);
		
		if(binding.hasErrors()){			
			result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrimId);
			result.addObject("message", "scrim.comment.error");
		} else {
			try{
				commentService.save(commentRes);
				result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + commentRes.getScrim().getId());
			} catch (Throwable oops) {				
				result = new ModelAndView("redirect:/scrim/display.do?scrimId=" + scrimId);
				result.addObject("message", "scrim.comment.error");	
			}
		}

		return result;
	}
	
}
