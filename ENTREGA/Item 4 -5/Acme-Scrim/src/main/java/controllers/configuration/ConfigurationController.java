package controllers.configuration;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ConfigurationService;
import services.RegionService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Configuration;
import domain.Region;

@Controller
@RequestMapping("/configuration")
public class ConfigurationController extends AbstractController{
	// Services ---------------------------------------------------------------

	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RegionService regionService;
	
	// Constructor -----------------------------------------------------------

	public ConfigurationController() {
		super();
	}
	
	@RequestMapping("/display")
	public ModelAndView display(){
		Assert.isTrue(LoginService.isAuthenticated());
		
		Actor principal;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);
		
		ModelAndView result;
		Collection<Region> regions;
		
		regions = this.regionService.findAll();
						
		result = new ModelAndView("configuration/display");
		result.addObject("configuration", configurationService.find());
		result.addObject("regions", regions);
		
		return result;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Configuration configuration, BindingResult binding){
		Assert.isTrue(LoginService.isAuthenticated());
		Assert.isTrue(configuration.getId() != 0);
		
		Actor principal;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);
		
		configuration = configurationService.reconstruct(configuration, binding);
		
		ModelAndView result;
		
		if(binding.hasErrors()){
			result = new ModelAndView("configuration/display");
			result.addObject("configuration", configuration);
			result.addObject("showEdit", true);
			
		} else{
			try{
				configurationService.save(configuration);
				result = new ModelAndView("redirect:/configuration/display.do");
				
			} catch (Throwable oops) {
				result = new ModelAndView("configuration/display");
				result.addObject("configuration", configuration);
				result.addObject("showEdit", true);
				result.addObject("message", "configuration.commit.error");
			}	
		}
				
		return result;
	}
}
