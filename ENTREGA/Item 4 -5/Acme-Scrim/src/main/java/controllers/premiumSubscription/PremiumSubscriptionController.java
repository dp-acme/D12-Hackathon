package controllers.premiumSubscription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.PremiumSubscriptionService;
import controllers.AbstractController;
import domain.PremiumSubscription;

@Controller
@RequestMapping("/premiumSubscription")
public class PremiumSubscriptionController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private PremiumSubscriptionService premiumSubscriptionService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor -----------------------------------------------------------

	public PremiumSubscriptionController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/create")
	public ModelAndView create() {
		ModelAndView result;
		PremiumSubscription premiumSubscription;
		
		actorService.checkIsPlayer();
		Assert.isTrue(!premiumSubscriptionService.hasAnyPremiumSubscription(actorService.findPrincipal().getId()));
	
		premiumSubscription = premiumSubscriptionService.create();
		
		Assert.notNull(premiumSubscription);
		result = new ModelAndView("premiumSubscription/create");
		result.addObject("premiumSubscription", premiumSubscription);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView save(PremiumSubscription premiumSubscription, BindingResult binding) {
		ModelAndView result;
		actorService.checkIsPlayer();
		Assert.isTrue(!premiumSubscriptionService.hasAnyPremiumSubscription(actorService.findPrincipal().getId()));
		
		premiumSubscription = premiumSubscriptionService.reconstruct(premiumSubscription, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("premiumSubscription/create");
			result.addObject("premiumSubscription", premiumSubscription);
		} else{
			if(!premiumSubscriptionService.checkCreditCard(premiumSubscription.getCreditCard().getExpirationMonth(), premiumSubscription.getCreditCard().getExpirationYear())){
				result = new ModelAndView("premiumSubscription/create");
				result.addObject("premiumSubscription", premiumSubscription);
				result.addObject("creditCardMessage", "premiumSubscription.commit.error.creditCardDate");
				return result;
			}
			try{
				premiumSubscriptionService.save(premiumSubscription);
				
				result = new ModelAndView("redirect:/");
		
			} catch (Throwable oops) {
				result = new ModelAndView("premiumSubscription/create");
				result.addObject("premiumSubscription", premiumSubscription);
				result.addObject("message", "premiumSubscription.commit.error");
			}
		}	
		return result;
	}
}
