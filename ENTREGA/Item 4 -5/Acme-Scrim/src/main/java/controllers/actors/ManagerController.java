package controllers.actors;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ManagerService;
import services.PermissionService;
import services.SocialIdentityService;
import controllers.AbstractController;
import domain.Manager;
import domain.Permission;
import domain.Player;
import form.RegisterActor;

@Controller
@RequestMapping("/manager")
public class ManagerController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private ManagerService managerService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private SocialIdentityService socialIdentityService;
	// Constructor -----------------------------------------------------------

	public ManagerController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required = false, value = "managerId") Integer managerId){
		ModelAndView result;
		Manager manager;

		manager = managerId == null? this.actorService.checkIsManager():this.managerService.findOne(managerId);
		
		if(managerId != null){
			Assert.isTrue(actorService.findPrincipal() instanceof domain.Administrator || actorService.findPrincipal().getId() == managerId);
		}
		
		Assert.notNull(manager);
		
		result = new ModelAndView("actor/manager/display");
		if(managerId == null || (LoginService.isAuthenticated()&& actorService.findPrincipal().getId() == managerId)){
			result.addObject("profile", true);
		}
		
		if(manager.getUserAccount().isAccountNonLocked()){
			result.addObject("isBanned", false);
		}else{
			result.addObject("isBanned", true);
		}
		
		result.addObject("requestURI", "/manager/display.do");
		result.addObject("actor", manager);
		result.addObject("editURI", "manager/edit.do?managerId=");
		result.addObject("socialIdentities", socialIdentityService.getSocialIdentitiesByActor(manager.getId()));
		
		return result;
	}
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView result;
		Collection<Manager> managers;
		
		actorService.checkIsAdministrator();
		
		managers = managerService.findAll();
		result = new ModelAndView("actor/manager/list");
		result.addObject("actors", managers);
		result.addObject("headTitle", "Managers");
		result.addObject("displayURI", "manager/display.do?managerId=");
		result.addObject("requestURI", "manager/list.do");

		return result;
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		actorService.checkIsPlayer();
		ModelAndView result;
		RegisterActor formObject;
		Player player;
		
		player = (Player) actorService.findPrincipal();
		
		if(permissionService.hasManagerPermission(player.getId())){
			Permission permission = permissionService.getPermission(player.getId());
			if(permission.getExpirationDate().before(new Date())){
				permissionService.delete(permission.getId());
			}
		}
		if(!permissionService.hasManagerPermission(player.getId())){
			result = new ModelAndView("actor/manager/create");
			result.addObject("hasPermission", false);
			result.addObject("requestURI", "manager/create.do");
		}else{
			formObject = new RegisterActor();
			result = this.createEditModelAndView(formObject);
			result.addObject("backURI", "");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("registerActor") RegisterActor registerActor, BindingResult binding){
		ModelAndView result;
		Manager manager;
		
		if(!registerActor.getTermsAccepted()){
			result = createEditModelAndView(registerActor);
			result.addObject("showMessageTerms", true);
			return result;
		}
		else if(!registerActor.getPassword().equals(registerActor.getUserAccount().getPassword())){
			result = createEditModelAndView(registerActor);
			result.addObject("showMessageNotMatchingPasswords", true);
			return result;
		}
		
		manager = managerService.reconstruct(registerActor, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(registerActor);
			
		} else { 
			try {
				managerService.save(manager);
				result = new ModelAndView("redirect:/");
				
			} catch (Throwable oops) {
				result = createEditModelAndView(registerActor, "actor.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required = true) Integer managerId) {
		actorService.checkIsManager();
		Manager manager;
		Manager principal;
		ModelAndView result;
		
		manager = managerService.findOne(managerId);
		principal = (Manager)actorService.findPrincipal();
		
		Assert.notNull(manager);
		Assert.isTrue(principal.equals(manager));
		
		result = new ModelAndView("actor/manager/edit");
		result.addObject("registerActor", manager);
		result.addObject("requestURI", "manager/edit.do");
		result.addObject("backURI", "manager/display.do");
		result.addObject("edit", true);

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="registerActor") Manager manager, BindingResult binding){
		Assert.notNull(manager);
		actorService.checkIsManager();
		Manager principal;
		ModelAndView result;
		
		principal = (Manager)actorService.findPrincipal();
		Assert.notNull(manager);
		Assert.isTrue(principal.getId() == manager.getId());
		
		manager = managerService.reconstructEdit(manager, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("actor/manager/edit");
			result.addObject("registerActor", manager);
			result.addObject("requestURI", "manager/edit.do");
			result.addObject("edit", true);
		} else { 
			try {
				manager = managerService.save(manager);
				
				result = new ModelAndView("redirect:/manager/display.do");
				
			} catch (Throwable oops) {
				result = new ModelAndView("actor/manager/edit");
				result.addObject("registerActor", manager);
				result.addObject("requestURI", "manager/edit.do");
				result.addObject("message", "actor.commit.error");
				result.addObject("edit", true);
			}
		}
		return result;
	}
	
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(RegisterActor registerActor) {
		ModelAndView result; 
		
		result = createEditModelAndView(registerActor, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(RegisterActor registerActor, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/manager/create");
		result.addObject("registerActor", registerActor);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "manager/create.do");
		
		return result;
	}

}
