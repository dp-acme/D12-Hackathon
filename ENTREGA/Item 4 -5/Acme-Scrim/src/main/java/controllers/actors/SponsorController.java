package controllers.actors;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.SocialIdentityService;
import services.SponsorService;
import controllers.AbstractController;
import domain.Sponsor;
import form.RegisterActor;

@Controller
@RequestMapping("/sponsor")
public class SponsorController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private SponsorService sponsorService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private SocialIdentityService socialIdentityService;
	
	// Constructor -----------------------------------------------------------

	public SponsorController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required = false, value = "sponsorId") Integer sponsorId){
		ModelAndView result;
		Sponsor sponsor;

		sponsor = sponsorId == null? this.actorService.checkIsSponsor():this.sponsorService.findOne(sponsorId);
		
		if(sponsorId != null){
			Assert.isTrue(actorService.findPrincipal() instanceof domain.Administrator || actorService.findPrincipal().getId() == sponsorId);
		}
		
		Assert.notNull(sponsor);
		
		result = new ModelAndView("actor/sponsor/display");
		if(sponsorId == null || (LoginService.isAuthenticated()&& actorService.findPrincipal().getId() == sponsorId)){
			result.addObject("profile", true);
		}
		
		if(sponsor.getUserAccount().isAccountNonLocked()){
			result.addObject("isBanned", false);
		}else{
			result.addObject("isBanned", true);
		}
		
		result.addObject("requestURI", "/sponsor/display.do");
		result.addObject("actor", sponsor);
		result.addObject("editURI", "sponsor/edit.do?sponsorId=");
		result.addObject("socialIdentities", socialIdentityService.getSocialIdentitiesByActor(sponsor.getId()));
		
		return result;
	}
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView result;
		Collection<Sponsor> sponsors;
		
		actorService.checkIsAdministrator();
		
		sponsors = sponsorService.findAll();
		result = new ModelAndView("actor/sponsor/list");
		result.addObject("actors", sponsors);
		result.addObject("headTitle", "Sponsors");
		result.addObject("displayURI", "sponsor/display.do?sponsorId=");
		result.addObject("requestURI", "sponsor/list.do");

		return result;
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		RegisterActor formObject;
		
		formObject = new RegisterActor();
		result = this.createEditModelAndView(formObject);
		result.addObject("backURI", "");

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("registerActor") RegisterActor registerActor, BindingResult binding){
		ModelAndView result;
		Sponsor sponsor;
		
		if(!registerActor.getTermsAccepted()){
			result = createEditModelAndView(registerActor);
			result.addObject("showMessageTerms", true);
			return result;
		}
		else if(!registerActor.getPassword().equals(registerActor.getUserAccount().getPassword())){
			result = createEditModelAndView(registerActor);
			result.addObject("showMessageNotMatchingPasswords", true);
			return result;
		}
		
		sponsor = sponsorService.reconstruct(registerActor, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(registerActor);
			
		} else { 
			try {
				sponsorService.save(sponsor);
				result = new ModelAndView("redirect:/");
				
			} catch (Throwable oops) {
				result = createEditModelAndView(registerActor, "actor.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required = true) Integer sponsorId) {
		actorService.checkIsSponsor();
		Sponsor sponsor;
		Sponsor principal;
		ModelAndView result;
		
		sponsor = sponsorService.findOne(sponsorId);
		principal = (Sponsor)actorService.findPrincipal();
		
		Assert.notNull(sponsor);
		Assert.isTrue(principal.equals(sponsor));
		
		result = new ModelAndView("actor/sponsor/edit");
		result.addObject("registerActor", sponsor);
		result.addObject("requestURI", "sponsor/edit.do");
		result.addObject("backURI", "sponsor/display.do");
		result.addObject("edit", true);

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="registerActor") Sponsor sponsor, BindingResult binding){
		Assert.notNull(sponsor);
		actorService.checkIsSponsor();
		Sponsor principal;
		ModelAndView result;
		
		principal = (Sponsor)actorService.findPrincipal();
		Assert.notNull(sponsor);
		Assert.isTrue(principal.getId() == sponsor.getId());
		
		sponsor = sponsorService.reconstructEdit(sponsor, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("actor/sponsor/edit");
			result.addObject("registerActor", sponsor);
			result.addObject("requestURI", "sponsor/edit.do");
			result.addObject("edit", true);
		} else { 
			try {
				sponsor = sponsorService.save(sponsor);
				
				result = new ModelAndView("redirect:/sponsor/display.do");
				
			} catch (Throwable oops) {
				result = new ModelAndView("actor/sponsor/edit");
				result.addObject("registerActor", sponsor);
				result.addObject("requestURI", "sponsor/edit.do");
				result.addObject("message", "actor.commit.error");
				result.addObject("edit", true);
			}
		}
		return result;
	}
	
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(RegisterActor registerActor) {
		ModelAndView result; 
		
		result = createEditModelAndView(registerActor, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(RegisterActor registerActor, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/sponsor/create");
		result.addObject("registerActor", registerActor);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "sponsor/create.do");
		
		return result;
	}

}
