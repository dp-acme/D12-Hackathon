package controllers.actors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import controllers.AbstractController;
import domain.Actor;

@Controller
@RequestMapping("/actor")
public class ActorController extends AbstractController {
	
	// Services ---------------------------------------------------------------	
	@Autowired
	private ActorService actorService;
	
	// Constructor -----------------------------------------------------------

	public ActorController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/followTeam")
	public ModelAndView follow(@RequestParam(required = true) Integer teamId) {
		ModelAndView result;
		
		Assert.isTrue(LoginService.isAuthenticated());
			
		actorService.followTeam(teamId);
		
		result = new ModelAndView("redirect:/team/display.do?teamId=" + teamId);
		
		return result;
	}
	
	@RequestMapping(value = "/unfollowTeam")
	public ModelAndView unfollow(@RequestParam(required = true) Integer teamId) {
		ModelAndView result;
		
		Assert.isTrue(LoginService.isAuthenticated());
			
		actorService.unfollowTeam(teamId);
		
		result = new ModelAndView("redirect:/team/display.do?teamId=" + teamId);
		
		return result;
	}
	
	@RequestMapping(value = "/listTeamsFollowed")
	public ModelAndView list() {
		ModelAndView result;
		
		Assert.isTrue(LoginService.isAuthenticated());
		
		result = new ModelAndView("actor/listTeamsFollowed");
		result.addObject("teams", actorService.findPrincipal().getTeamsFollowed());
		result.addObject("requestURI", "actor/listTeamsFollowed.do");
		
		return result;
	}
	
	@RequestMapping(value = "/ban")
	public ModelAndView ban(@RequestParam(required = true) Integer actorId) {
		ModelAndView result;
		Actor actor;
		String redirectURI = "";
		
		actorService.checkIsAdministrator();
		actor = actorService.findOne(actorId);
		
		if(actor instanceof domain.Player){
			redirectURI = "player/display.do?playerId=" + actorId;
		}else if(actor instanceof domain.Manager){
			redirectURI = "manager/display.do?managerId=" + actorId;
		}else if(actor instanceof domain.Sponsor){
			redirectURI = "sponsor/display.do?sponsorId=" + actorId;
		}else if(actor instanceof domain.Referee){
			redirectURI = "referee/display.do?refereeId=" + actorId;
		}
		
		actorService.banActor(actorId);
		
		result = new ModelAndView("redirect:/" + redirectURI);
		return result;
	}
	
	@RequestMapping(value = "/unban")
	public ModelAndView unban(@RequestParam(required = true) Integer actorId) {
		ModelAndView result;
		Actor actor;
		String redirectURI = "";
		
		actorService.checkIsAdministrator();
		actor = actorService.findOne(actorId);
		
		if(actor instanceof domain.Player){
			redirectURI = "player/display.do?playerId=" + actorId;
		}else if(actor instanceof domain.Manager){
			redirectURI = "manager/display.do?managerId=" + actorId;
		}else if(actor instanceof domain.Sponsor){
			redirectURI = "sponsor/display.do?sponsorId=" + actorId;
		}else if(actor instanceof domain.Referee){
			redirectURI = "referee/display.do?refereeId=" + actorId;
		}
		
		actorService.unbanActor(actorId);
		
		result = new ModelAndView("redirect:/" + redirectURI);
		return result;
	}
}
