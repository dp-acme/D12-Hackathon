package controllers.actors;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ConfigurationService;
import services.PermissionService;
import services.PlayerService;
import services.RefereeService;
import services.ReportService;
import services.ResolutionService;
import services.SocialIdentityService;
import controllers.AbstractController;
import domain.Actor;
import domain.Manager;
import domain.Permission;
import domain.Player;
import domain.Referee;
import domain.Report;
import domain.Resolution;
import form.RegisterActor;

@Controller
@RequestMapping("/referee")
public class RefereeController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private RefereeService refereeService;
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ReportService reportService;
	
	@Autowired
	private ResolutionService resolutionService;
	
	@Autowired
	private PlayerService playerService;

	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private SocialIdentityService socialIdentityService;
	
	// Constructor -----------------------------------------------------------

	public RefereeController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required = false, value = "refereeId") Integer refereeId){
		ModelAndView result;
		Referee referee;

		referee = refereeId == null? this.actorService.checkIsReferee():this.refereeService.findOne(refereeId);
		
		if(refereeId != null){
			Assert.isTrue((actorService.findPrincipal() instanceof Manager || actorService.findPrincipal() instanceof domain.Administrator || actorService.findPrincipal().getId() == refereeId));
		}
		
		Assert.notNull(referee);
		
		result = new ModelAndView("actor/referee/display");
		if(refereeId == null || (LoginService.isAuthenticated()&& actorService.findPrincipal().getId() == refereeId)){
			result.addObject("profile", true);
		}
		
		if(referee.getUserAccount().isAccountNonLocked()){
			result.addObject("isBanned", false);
		}else{
			result.addObject("isBanned", true);
		}
		
		result.addObject("requestURI", "/referee/display.do");
		result.addObject("actor", referee);
		result.addObject("editURI", "referee/edit.do?refereeId=");
		result.addObject("socialIdentities", socialIdentityService.getSocialIdentitiesByActor(referee.getId()));
		
		return result;
	}
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView result;
		Collection<Referee> referees;
		
		Assert.isTrue((actorService.findPrincipal() instanceof Manager || actorService.findPrincipal() instanceof domain.Administrator));
		
		referees = refereeService.findAll();
		result = new ModelAndView("actor/referee/list");
		result.addObject("actors", referees);
		result.addObject("headTitle", "Referees");
		result.addObject("displayURI", "referee/display.do?refereeId=");
		result.addObject("requestURI", "referee/list.do");

		return result;
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		actorService.checkIsPlayer();
		ModelAndView result;
		RegisterActor formObject;
		Player player;
		
		player = (Player) actorService.findPrincipal();
		
		if(permissionService.hasRefereePermission(player.getId())){
			Permission permission = permissionService.getPermission(player.getId());
			if(permission.getExpirationDate().before(new Date())){
				permissionService.delete(permission.getId());
			}
		}
		if(!permissionService.hasRefereePermission(player.getId())){
			result = new ModelAndView("actor/referee/create");
			result.addObject("hasPermission", false);
			result.addObject("requestURI", "referee/create.do");
		}else{
			formObject = new RegisterActor();
			result = this.createEditModelAndView(formObject);
			result.addObject("backURI", "");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("registerActor") RegisterActor registerActor, BindingResult binding){
		ModelAndView result;
		Referee referee;
		
		if(!registerActor.getTermsAccepted()){
			result = createEditModelAndView(registerActor);
			result.addObject("showMessageTerms", true);
			return result;
		}
		else if(!registerActor.getPassword().equals(registerActor.getUserAccount().getPassword())){
			result = createEditModelAndView(registerActor);
			result.addObject("showMessageNotMatchingPasswords", true);
			return result;
		}
		
		referee = refereeService.reconstruct(registerActor, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(registerActor);
			
		} else { 
			try {
				refereeService.save(referee);
				result = new ModelAndView("redirect:/");
				
			} catch (Throwable oops) {
				result = createEditModelAndView(registerActor, "actor.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required = true) Integer refereeId) {
		actorService.checkIsReferee();
		Referee referee;
		Referee principal;
		ModelAndView result;
		
		referee = refereeService.findOne(refereeId);
		principal = (Referee)actorService.findPrincipal();
		
		Assert.notNull(referee);
		Assert.isTrue(principal.equals(referee));
		
		result = new ModelAndView("actor/referee/edit");
		result.addObject("registerActor", referee);
		result.addObject("requestURI", "referee/edit.do");
		result.addObject("backURI", "referee/display.do");
		result.addObject("edit", true);

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="registerActor") Referee referee, BindingResult binding){
		Assert.notNull(referee);
		actorService.checkIsReferee();
		Referee principal;
		ModelAndView result;
		
		principal = (Referee)actorService.findPrincipal();
		Assert.notNull(referee);
		Assert.isTrue(principal.getId() == referee.getId());
		
		referee = refereeService.reconstructEdit(referee, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("actor/referee/edit");
			result.addObject("registerActor", referee);
			result.addObject("requestURI", "referee/edit.do");
			result.addObject("edit", true);
		} else { 
			try {
				referee = refereeService.save(referee);
				
				result = new ModelAndView("redirect:/referee/display.do");
				
			} catch (Throwable oops) {
				result = new ModelAndView("actor/referee/edit");
				result.addObject("registerActor", referee);
				result.addObject("requestURI", "referee/edit.do");
				result.addObject("message", "actor.commit.error");
				result.addObject("edit", true);
			}
		}
		return result;
	}
	
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(RegisterActor registerActor) {
		ModelAndView result; 
		
		result = createEditModelAndView(registerActor, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(RegisterActor registerActor, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/referee/create");
		result.addObject("registerActor", registerActor);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "referee/create.do");
		
		return result;
	}
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard(@RequestParam(required = false) Integer playerId){
		Collection<Report> reports;
		Collection<Resolution> resolutionsS;
		Collection<Resolution> resolutionsP;
		Collection<Player> baneablePlayers;
		Collection<Actor> bannedPlayers;
		Set<Player> reportedPlayers = new HashSet<Player>();
		
		ModelAndView result;
		result = new ModelAndView();
		
		if (playerId != null) {
			reports = reportService.getReportsByPlayer(playerId);
			result.addObject("reports", reports);
		}
		
		resolutionsS = resolutionService.getSolvedResolutions();
		resolutionsP = resolutionService.getPendingResolutions();
		
		baneablePlayers = reportService.getPlayersByNumOfReports((long) configurationService.find().getMinimumReports());
		bannedPlayers = actorService.getBannedActors();
				
		for(Report r : reportService.findAll()){
			reportedPlayers.add(r.getPlayer());
		}
		
		result.addObject("resolutionsP", resolutionsP);
		result.addObject("resolutionsS", resolutionsS);
		result.addObject("areports", reportService.findAll());
		result.addObject("baneables", baneablePlayers);
		result.addObject("banned", bannedPlayers);
		result.addObject("playersReported", reportedPlayers);

		return result;
	}
	
	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public ModelAndView ban(@RequestParam(required = true) Integer playerId){
		Player player;
		ModelAndView result;
		
		actorService.checkIsReferee();
		
		player = playerService.findOne(playerId);
		
		try{
			result = new ModelAndView("redirect:/referee/dashboard.do");
			actorService.banActor(player.getId());
		}catch (Throwable e) {
			result = new ModelAndView("redirect:/referee/dashboard.do");
		}
		return result;
	}
	

	@RequestMapping(value = "/unban", method = RequestMethod.GET)
	public ModelAndView unban(@RequestParam(required = true) Integer playerId){
		Player player;
		ModelAndView result;
		
		actorService.checkIsReferee();
		
		player = playerService.findOne(playerId);
		
		try{
			result = new ModelAndView("redirect:/referee/dashboard.do");
			actorService.unbanActor(player.getId());
		}catch (Throwable e) {
			result = new ModelAndView("redirect:/referee/dashboard.do");
		}
		return result;
	}

}
