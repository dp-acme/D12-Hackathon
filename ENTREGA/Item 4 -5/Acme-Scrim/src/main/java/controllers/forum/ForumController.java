package controllers.forum;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ForumCategoryService;
import services.ForumMessageService;
import services.ForumService;
import services.VideogameService;
import controllers.AbstractController;
import domain.Forum;
import domain.ForumCategory;
import domain.ForumMessage;
import domain.Videogame;

@Controller
@RequestMapping("/forum")
public class ForumController extends AbstractController{
	// Services ---------------------------------------------------------------

	@Autowired
	private ForumCategoryService forumCategoryService;
	
	@Autowired
	private ForumMessageService forumMessageService;
	
	@Autowired
	private ForumService forumService;
	
	@Autowired
	private VideogameService videogameService;
	
	
	// Constructor -----------------------------------------------------------

	public ForumController() {
		super();
	}
	
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(required = true) Integer videogameId, @RequestParam(required = false) Integer forumCategoryId){
		
		Boolean isOwner;
		ModelAndView result;
		Videogame videogame;
		
						
		result = new ModelAndView("forum/list");
		isOwner=false;
		videogame=videogameService.findOne(videogameId);
	
		Assert.notNull(videogame);
		if(LoginService.isAuthenticated()&&videogame.getManager().getUserAccount().equals(LoginService.getPrincipal()))
			isOwner=true;
		
		
		Collection<ForumCategory> rootForumCategory = new ArrayList<ForumCategory>();
		rootForumCategory.add(forumCategoryService.findRootForumCategory(videogame));
		
		result.addObject("rootForumCategory",rootForumCategory );
		result.addObject("forumCategoryWithParent", forumCategoryService.create(null));
		result.addObject("forums",forumService.findForumsByCategory(forumCategoryId));
		if(forumCategoryId!=null){
			ForumCategory forumCat;
		
			forumCat = forumCategoryService.findOne(forumCategoryId);
			Assert.notNull(forumCat);

			result.addObject("selected",forumCat);
		}
		result.addObject("videogameId",videogameId);
		result.addObject("isOwner",isOwner);


		
		return result;
	}
	
	@RequestMapping(value = "/createCategory", method = RequestMethod.POST, params = "createWithParent")
	public ModelAndView create(@RequestParam(value = "parentId", required = false) Integer parentId,
							    @ModelAttribute("forumCategoryWithParent") ForumCategory forumCategory, BindingResult binding){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		ForumCategory parent;
		
		Boolean isOwner=false;
		
		parent = forumCategoryService.findOne(parentId);
		Assert.notNull(parent);
		
		forumCategory = forumCategoryService.reconstruct(forumCategory, parent, binding);
		
		if(LoginService.isAuthenticated()&&forumCategory.getVideogame().getManager().getUserAccount().equals(LoginService.getPrincipal()))
			isOwner=true;
		
		if(binding.hasErrors()){
			result = new ModelAndView("forum/list");
			Collection<ForumCategory> rootForumCategory = new ArrayList<ForumCategory>();
			rootForumCategory.add(forumCategoryService.findRootForumCategory(parent.getVideogame()));
			
			result.addObject("rootForumCategory",rootForumCategory );
			result.addObject("forumCategoryWithParent", forumCategory);
			result.addObject("parentId", forumCategory.getParent().getId());
			result.addObject("shownForms", "withParent");
			result.addObject("videogameId",forumCategory.getVideogame().getId());
			result.addObject("isOwner",isOwner);
			result.addObject("message", "org.hibernate.validator.constraints.NotBlank.message");


		} else{
			try{
				forumCategoryService.save(forumCategory);
				
				result = new ModelAndView("redirect:/forum/list.do?videogameId="+parent.getVideogame().getId());
	
			} catch (Throwable oops) {
				result = new ModelAndView("forum/list");
				Collection<ForumCategory> rootForumCategory = new ArrayList<ForumCategory>();
				rootForumCategory.add(forumCategoryService.findRootForumCategory(parent.getVideogame()));
				
				result.addObject("rootForumCategory",rootForumCategory );
				
				result.addObject("forumCategoryWithParent", forumCategory);
				result.addObject("shownForms", "withParent");
				result.addObject("parentId", forumCategory.getParent().getId());
				result.addObject("videogameId",forumCategory.getVideogame().getId());
				result.addObject("isOwner",isOwner);
				result.addObject("message", "forumCategory.commit.error");
			}
		}						
		
		return result;
	}
	
	@RequestMapping("/deleteCategory")
	public ModelAndView delete(@RequestParam(value = "forumCategoryId", required = true) Integer forumCategoryId){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		ForumCategory forumCategory;
		Videogame videogame;
		Boolean isOwner=false;
		
		forumCategory = forumCategoryService.findOne(forumCategoryId);
		Assert.notNull(forumCategory);
		videogame=forumCategory.getVideogame();
		
		if(LoginService.isAuthenticated()&&videogame.getManager().getUserAccount().equals(LoginService.getPrincipal()))
			isOwner=true;
		
		
		try{
			forumCategoryService.delete(forumCategory);
			result = new ModelAndView("redirect:/forum/list.do?videogameId="+videogame.getId());
	
		} catch (Throwable oops) {
			result = new ModelAndView("forum/list");
			Collection<ForumCategory> rootForumCategory = new ArrayList<ForumCategory>();
			rootForumCategory.add(forumCategoryService.findRootForumCategory(videogame));
			
			result.addObject("rootForumCategory",rootForumCategory );
			result.addObject("forumCategoryWithParent", forumCategoryService.create(videogame));
			result.addObject("videogameId",videogame.getId());
			result.addObject("isOwner",isOwner);
			result.addObject("message", "forumCategory.commit.error");
		}
		
		return result;
	}
	
	@RequestMapping("/editCategory")
	public ModelAndView edit(ForumCategory forumCategory, BindingResult binding){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Boolean isOwner=false;
	
		Assert.notNull(forumCategory);
				
		forumCategory = forumCategoryService.reconstruct(forumCategory, null, binding);
		
		if(LoginService.isAuthenticated()&&forumCategory.getVideogame().getManager().getUserAccount().equals(LoginService.getPrincipal()))
			isOwner=true;
		
		if(binding.hasErrors()){
			result = new ModelAndView("forum/list");

			Collection<ForumCategory> rootForumCategory = new ArrayList<ForumCategory>();
			rootForumCategory.add(forumCategoryService.findRootForumCategory(forumCategory.getVideogame()));
			
			result.addObject("rootForumCategory",rootForumCategory );
			result.addObject("forumCategoryWithParent", forumCategoryService.create(forumCategory.getVideogame()));
			
			result.addObject("videogameId",forumCategory.getVideogame().getId());
			result.addObject("isOwner",isOwner);
			
			
			result.addObject("message", "org.hibernate.validator.constraints.NotBlank.message");
			
		} else{
			try{
				forumCategoryService.save(forumCategory);
				result = new ModelAndView("redirect:/forum/list.do?videogameId="+forumCategory.getVideogame().getId());
		
			} catch (Throwable oops) {
				result = new ModelAndView("forum/list");
				Collection<ForumCategory> rootForumCategory = new ArrayList<ForumCategory>();
				rootForumCategory.add(forumCategoryService.findRootForumCategory(forumCategory.getVideogame()));
				
				result.addObject("rootForumCategory",rootForumCategory );
				result.addObject("forumCategoryWithParent", forumCategoryService.create(forumCategory.getVideogame()));
				result.addObject("videogameId",forumCategory.getVideogame().getId());
				result.addObject("isOwner",isOwner);
				result.addObject("message", "forumCategory.commit.error");
			}	
		}
		
		return result;
	}
	
	@RequestMapping("/display")
	public ModelAndView display(@RequestParam(required = true) Integer forumId){
		
		ModelAndView result;
		Forum forum;
		Boolean isAuthenticated;
		
		result = new ModelAndView("forum/display");
		forum = forumService.findOne(forumId);
		
		Collection<ForumMessage> parentMessages = new ArrayList<ForumMessage>();
		parentMessages.addAll(forumMessageService.findByForum(forumId));
		isAuthenticated = false;
		if(LoginService.isAuthenticated()){
			isAuthenticated=true;
		}
		Boolean isOwner=false;
		if(LoginService.isAuthenticated()&&forumService.getVideogameFromForum(forum).getManager().getUserAccount().equals(LoginService.getPrincipal()))
			isOwner=true;
		result.addObject("forumMessageWithoutParent", forumMessageService.create(forum, null));
		result.addObject("forumMessageWithParent", forumMessageService.create(forum, null));
		result.addObject("isAuthenticated", isAuthenticated);
		result.addObject("isOwner",isOwner);
		result.addObject("videogameId",forumService.getVideogameFromForum(forum).getId());


		result.addObject("parentMessages",parentMessages);
		
		return result;
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required=true)Integer videogameId){
		ModelAndView result;
		Videogame videogame;
		Forum forum;
		
		videogame = videogameService.findOne(videogameId);

		Assert.isTrue(videogame.getManager().getUserAccount().equals(LoginService.getPrincipal()));
		
		forum = forumService.create(videogame);

		result = new ModelAndView("forum/create");
		result.addObject("forumCategories", forumCategoryService.findForumCategoriesByVideogame(videogame));
		result.addObject("forum", forum);
		result.addObject("videogameId", videogame.getId());


		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("forum") Forum forum, BindingResult binding){
		ModelAndView result;
		Forum forumReconstruct;
		Videogame videogame;
		
 		videogame =forumService.getVideogameFromForum(forum);
		
		forumReconstruct = forumService.reconstruct(forum, videogame, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("forum/edit");
			result.addObject("forum",forum);
			result.addObject("forumCategories", forumCategoryService.findForumCategoriesByVideogame(videogame));
			
		} else { 
			try {
				forumService.save(forumReconstruct);
				result = new ModelAndView("redirect:/forum/list.do?videogameId="+videogame.getId());
				
			} catch (Throwable oops) {
				result = new ModelAndView("forum/edit");
				result.addObject("forum",forum);
				result.addObject("forumCategories", forumCategoryService.findForumCategoriesByVideogame(videogame));
				result.addObject("message","forumCategory.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public ModelAndView editS(@RequestParam(required=true)Integer forumId){
		ModelAndView result;
		Forum forum;
		
		forum = forumService.findOne(forumId);

		Assert.isTrue(forumService.getVideogameFromForum(forum).getManager().getUserAccount().equals(LoginService.getPrincipal()));
		
		result = new ModelAndView("forum/edit");
		result.addObject("forumCategories", forumCategoryService.findForumCategoriesByVideogame(forumService.getVideogameFromForum(forum)));
		result.addObject("forum", forum);
		result.addObject("videogameId", forumService.getVideogameFromForum(forum).getId());


		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView saveEdit(@ModelAttribute("forum") Forum forum, BindingResult binding){
		ModelAndView result;
		Forum forumReconstruct;
		Videogame videogame;
		
 		videogame =forumService.getVideogameFromForum(forum);
		
		forumReconstruct = forumService.reconstruct(forum, videogame, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("forum/edit");
			result.addObject("forum",forum);
			result.addObject("forumCategories", forumCategoryService.findForumCategoriesByVideogame(forumService.getVideogameFromForum(forum)));

			
		} else { 
			try {
				forumService.save(forumReconstruct);
				result = new ModelAndView("redirect:/forum/list.do?videogameId="+videogame.getId());
				
			} catch (Throwable oops) {
				result = new ModelAndView("forum/create");
				result.addObject("forum",forum);
				result.addObject("forumCategories", forumCategoryService.findForumCategoriesByVideogame(forumService.getVideogameFromForum(forum)));
				result.addObject("message","forumCategory.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping("/delete")
	public ModelAndView deleteForum(@RequestParam(value = "forumId", required = true) Integer forumId){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Forum forum;
		Videogame videogame;
		
		
		forum = forumService.findOne(forumId);
		Assert.notNull(forum);
		videogame=forumService.getVideogameFromForum(forum);

		
		try{
			forumService.delete(forum);
			result = new ModelAndView("redirect:/forum/list.do?videogameId="+videogame.getId());
	
		} catch (Throwable oops) {
			result = new ModelAndView("forum/list");
			Collection<ForumCategory> rootForumCategory = new ArrayList<ForumCategory>();
			rootForumCategory.add(forumCategoryService.findRootForumCategory(videogame));
			
			result.addObject("rootForumCategory",rootForumCategory );
			result.addObject("forumCategoryWithParent", forumCategoryService.create(videogame));
			result.addObject("message", "forumCategory.commit.error");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/createMessage", method = RequestMethod.POST, params = "createWithParent")
	public ModelAndView create(@RequestParam(value = "parentId", required = false) Integer parentId,
							    @ModelAttribute("forumMessageWithParent") ForumMessage forumMessage, BindingResult binding){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		ForumMessage parent;
			
		parent = forumMessageService.findOne(parentId);
		Assert.notNull(parent);
		forumMessage = forumMessageService.reconstruct(forumMessage, parent, binding);
		Boolean isOwner=false;
		if(LoginService.isAuthenticated()&&forumService.getVideogameFromForum(forumMessage.getForum()).getManager().getUserAccount().equals(LoginService.getPrincipal()))
			isOwner=true;
		
		
		if(binding.hasErrors()){
			result = new ModelAndView("forum/display");

			Collection<ForumMessage> parentMessages = new ArrayList<ForumMessage>();
			parentMessages.addAll(forumMessageService.findByForum(forumMessage.getForum().getId()));
			
			result.addObject("forumMessageWithoutParent", forumMessageService.create(forumMessage.getForum(), null));
			result.addObject("forumMessageWithParent", forumMessageService.create(forumMessage.getForum(), null));
			result.addObject("parentMessages",parentMessages);
			result.addObject("isAuthenticated", true);
			result.addObject("isOwner",isOwner);
			result.addObject("videogameId",forumService.getVideogameFromForum(forumMessage.getForum()).getId());

			result.addObject("shownForms", "withParent");
			result.addObject("message", "org.hibernate.validator.constraints.NotBlank.message");


		} else{
			try{
				forumMessageService.save(forumMessage);
				
				result = new ModelAndView("redirect:display.do?forumId="+forumMessage.getForum().getId());
	
			} catch (Throwable oops) {
				result = new ModelAndView("forum/display");
				Collection<ForumMessage> parentMessages = new ArrayList<ForumMessage>();
				parentMessages.addAll(forumMessageService.findByForum(forumMessage.getForum().getId()));
				
				
				result.addObject("forumMessageWithoutParent", forumMessageService.create(forumMessage.getForum(), null));
				result.addObject("forumMessageWithParent", forumMessageService.create(forumMessage.getForum(), null));
				result.addObject("parentMessages",parentMessages);
				result.addObject("isAuthenticated", true);
				result.addObject("isOwner",isOwner);
				result.addObject("videogameId",forumService.getVideogameFromForum(forumMessage.getForum()).getId());

				result.addObject("shownForms", "withParent");
				result.addObject("message", "forumCategory.commit.error");
			}
		}						
		
		return result;
	}
	
	@RequestMapping(value = "/createMessage", method = RequestMethod.POST, params = "createWithoutParent")
	public ModelAndView create(
			@ModelAttribute("forumMessageWithoutParent") ForumMessage forumMessage,
			BindingResult binding) {
		Assert.isTrue(LoginService.isAuthenticated());

		ModelAndView result;
		Boolean isOwner=false;

		forumMessage = forumMessageService.reconstruct(forumMessage, null,
				binding);
		if(LoginService.isAuthenticated()&&forumService.getVideogameFromForum(forumMessage.getForum()).getManager().getUserAccount().equals(LoginService.getPrincipal()))
			isOwner=true;

		if (binding.hasErrors()) {
			result = new ModelAndView("forum/display");

			Collection<ForumMessage> parentMessages = new ArrayList<ForumMessage>();
			parentMessages.addAll(forumMessageService.findByForum(forumMessage
					.getForum().getId()));
			result.addObject("isOwner",isOwner);
			result.addObject("forumMessageWithoutParent",
					forumMessageService.create(forumMessage.getForum(), null));
			result.addObject("forumMessageWithParent",
					forumMessageService.create(forumMessage.getForum(), null));
			result.addObject("parentMessages", parentMessages);
			result.addObject("isAuthenticated", true);
			result.addObject("videogameId",forumService.getVideogameFromForum(forumMessage.getForum()).getId());

			result.addObject("shownForms", "withParent");
			result.addObject("message",
					"org.hibernate.validator.constraints.NotBlank.message");

		} else {
			try {
				forumMessageService.save(forumMessage);

				result = new ModelAndView("redirect:display.do?forumId="
						+ forumMessage.getForum().getId());

			} catch (Throwable oops) {
				result = new ModelAndView("forum/display");
				Collection<ForumMessage> parentMessages = new ArrayList<ForumMessage>();
				parentMessages.addAll(forumMessageService
						.findByForum(forumMessage.getForum().getId()));

				result.addObject("forumMessageWithoutParent",
						forumMessageService.create(forumMessage.getForum(),
								null));
				result.addObject("forumMessageWithParent", forumMessageService
						.create(forumMessage.getForum(), null));
				result.addObject("parentMessages", parentMessages);
				result.addObject("isOwner",isOwner);
				result.addObject("videogameId",forumService.getVideogameFromForum(forumMessage.getForum()).getId());

				result.addObject("shownForms", "withParent");
				result.addObject("message", "forumCategory.commit.error");
			}
		}

		return result;
	}
	@RequestMapping("/deleteMessage")
	public ModelAndView deleteMessage(@RequestParam(value = "forumMessageId", required = true) Integer forumMessageId){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		ForumMessage forumMessage;
		Forum forum;
		Boolean isOwner=false;
		
		forumMessage = forumMessageService.findOne(forumMessageId);
		Assert.notNull(forumMessage);
		
		if(LoginService.isAuthenticated()&&forumService.getVideogameFromForum(forumMessage.getForum()).getManager().getUserAccount().equals(LoginService.getPrincipal()))
			isOwner=true;
		
		forum= forumMessage.getForum();
		try{
			forumMessageService.delete(forumMessage);
			result = new ModelAndView("redirect:/forum/display.do?forumId="+forum.getId());
	
		} catch (Throwable oops) {
			result = new ModelAndView("forum/display");
			Collection<ForumMessage> parentMessages = new ArrayList<ForumMessage>();
			parentMessages.addAll(forumMessageService
					.findByForum(forumMessage.getForum().getId()));

			result.addObject("forumMessageWithoutParent",
					forumMessageService.create(forumMessage.getForum(),
							null));
			result.addObject("forumMessageWithParent", forumMessageService
					.create(forumMessage.getForum(), null));
			result.addObject("parentMessages", parentMessages);
			result.addObject("videogameId",forumService.getVideogameFromForum(forumMessage.getForum()).getId());

			result.addObject("shownForms", "withParent");
			result.addObject("isOwner",isOwner);
			result.addObject("message", "forumCategory.commit.error");
		}
		
	return result;
}
}
