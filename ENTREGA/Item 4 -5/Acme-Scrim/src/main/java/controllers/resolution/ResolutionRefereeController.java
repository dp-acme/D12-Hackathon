package controllers.resolution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ResolutionService;
import controllers.AbstractController;
import domain.Referee;
import domain.Resolution;
import domain.Result;

@Controller
@RequestMapping("/resolution/referee")
public class ResolutionRefereeController extends AbstractController{
	
	@Autowired
	private ResolutionService resolutionService;

	@Autowired
	private ActorService actorService;

	@RequestMapping(value = "/resolve")
	public ModelAndView create(@RequestParam(required = true) Integer resolutionId, @RequestParam(required = true) Integer teamResult){
		Resolution resolution;
		ModelAndView result;
		Result team1;
		Result team2;
		Referee principal;
		
		resolution = resolutionService.findOne(resolutionId);
		Assert.notNull(resolution);
		
		principal = actorService.checkIsReferee();
		
		team1 = new Result();
		team2 = new Result();
		
		switch(teamResult){
		case 0: {team1.setResult(Result.LOSE); team2.setResult(Result.WIN); break;}
		case 1: {team1.setResult(Result.WIN); team2.setResult(Result.LOSE); break;}
		default : {team1.setResult(Result.DRAW); team2.setResult(Result.DRAW);}
		}
		
		try{
			resolution = resolutionService.addReferee(resolution, principal);
			
			if(resolution != null && resolution.getReferee().equals(principal)){
				resolutionService.setScrimResult(resolution.getId(), team1, team2);
				result = new ModelAndView("redirect:/scrim/display.do?scrimId="+resolution.getScrim().getId());
			}else{
				result = new ModelAndView("redirect:/referee/dashboard.do");
			}	
				
		} catch (Throwable oops) {				
			result = new ModelAndView("redirect:/referee/dashboard.do");
		}
		
		return result;
	}
	
}
