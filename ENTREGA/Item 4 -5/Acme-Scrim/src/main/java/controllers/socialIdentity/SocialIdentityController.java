package controllers.socialIdentity;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.SocialIdentityService;
import controllers.AbstractController;
import domain.SocialIdentity;
import domain.SocialNetwork;

@Controller
@RequestMapping("/socialIdentity")
public class SocialIdentityController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private SocialIdentityService socialIdentityService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor -----------------------------------------------------------

	public SocialIdentityController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/create")
	public ModelAndView create(@RequestParam(required = false) Integer socialIdentityId) {
		ModelAndView result;
		SocialIdentity socialIdentity;
		Collection<SocialNetwork> socialNetworks;
		
		Assert.isTrue(LoginService.isAuthenticated());
		socialNetworks = socialIdentityService.getAvSocialNetwork(actorService.findPrincipal().getId());
		
		if(socialIdentityId != null){
			socialNetworks.add(socialIdentityService.findOne(socialIdentityId).getSocialNetwork());
			socialIdentity = socialIdentityService.findOne(socialIdentityId);
		}else{
			socialIdentity = socialIdentityService.create();
		}
		
		Assert.notNull(socialIdentity);
		result = new ModelAndView("socialIdentity/create");
		result.addObject("backURI", this.getBackUri());
		result.addObject("socialIdentity", socialIdentity);
		result.addObject("socialNetworks", socialNetworks);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView save(SocialIdentity socialIdentity, BindingResult binding) {
		ModelAndView result;
		Collection<SocialNetwork> socialNetworks;
		Assert.isTrue(LoginService.isAuthenticated());
		
		socialIdentity = socialIdentityService.reconstruct(socialIdentity, binding);
		socialNetworks = socialIdentityService.getAvSocialNetwork(actorService.findPrincipal().getId());
		
		if(socialIdentity.getId() != 0){
			socialNetworks.add(socialIdentityService.findOne(socialIdentity.getId()).getSocialNetwork());
			Assert.isTrue(actorService.findPrincipal().getId() == socialIdentity.getActor().getId());
		}
		
		Assert.isTrue(socialNetworks.contains(socialIdentity.getSocialNetwork()));
		
		if(binding.hasErrors()){
			result = new ModelAndView("socialIdentity/create");
			result.addObject("socialIdentity", socialIdentity);
			result.addObject("socialNetworks", socialNetworks);
			
		} else{
			try{
				socialIdentityService.save(socialIdentity);
				
				result = new ModelAndView("redirect:/" + this.getBackUri());
		
			} catch (Throwable oops) {
				result = new ModelAndView("socialIdentity/create");
				result.addObject("socialIdentity", socialIdentity);
				result.addObject("socialNetworks", socialNetworks);
				result.addObject("message", "socialIdentity.commit.error");
			}
		}	
		return result;
	}
	
	@RequestMapping(value = "/delete")
	public ModelAndView delete(@RequestParam(required = true) Integer socialIdentityId) {
		ModelAndView result;
		SocialIdentity socialIdentity;
		
		Assert.isTrue(LoginService.isAuthenticated());
		
		socialIdentity = socialIdentityService.findOne(socialIdentityId);
		Assert.notNull(socialIdentity);
		Assert.isTrue(actorService.findPrincipal().getId() == socialIdentity.getActor().getId());
		
		socialIdentityService.delete(socialIdentityId);
		
		result = new ModelAndView("redirect:/" + this.getBackUri());
		
		return result;
	}
	
	private String getBackUri(){
		String result;
		
		result = "";
		if(actorService.findPrincipal() instanceof domain.Manager)result = "manager/display.do";
		else if(actorService.findPrincipal() instanceof domain.Player)result = "player/display.do";
		else if(actorService.findPrincipal() instanceof domain.Referee)result = "referee/display.do";
		else if(actorService.findPrincipal() instanceof domain.Sponsor)result = "sponsor/display.do";
		
		return result;
	}
}
