/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.dashboard;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import controllers.AbstractController;
import domain.Player;
import domain.Videogame;

@Controller
@RequestMapping("/dashboard")
public class DashboardController extends AbstractController {

	// Services -----------------------------------------------------------
	
	@Autowired
	AdministratorService administratorService;
	
	// Constructors -----------------------------------------------------------

	public DashboardController() {
		super();
	}

	@RequestMapping("/display")
	public ModelAndView display() {
		String[]getAvgMaxMinSdVideogamesPerManager, getAvgMaxMinSdSeasonsPerVideogame, 
		getAvgMaxMinSdScrimsPerTeam, getAvgMaxMinSdTeamsPerPlayer, 
		getAvgMaxMinSdPlayersPerTeam;
		Integer round;
		
		round = 2;
		
		String ratioOfPlayersWithForumMessages, ratioOfCancelledScrims;
		
		ModelAndView result;

		result = new ModelAndView("dashboard");
		
		getAvgMaxMinSdVideogamesPerManager = administratorService.roundTo(administratorService.getAvgMaxMinSdVideogamesPerManager(), round);
		getAvgMaxMinSdSeasonsPerVideogame = administratorService.roundTo(administratorService.getAvgMaxMinSdSeasonsPerVideogame(), round);
		getAvgMaxMinSdScrimsPerTeam = administratorService.roundTo(administratorService.getAvgMaxMinSdScrimsPerTeam(), round);
		getAvgMaxMinSdTeamsPerPlayer = administratorService.roundTo(administratorService.getAvgMaxMinSdTeamsPerPlayer(), round);
		getAvgMaxMinSdPlayersPerTeam = administratorService.roundTo(administratorService.getAvgMaxMinSdPlayersPerTeam(), round);

		ratioOfPlayersWithForumMessages = administratorService.roundTo(administratorService.ratioOfPlayersWithForumMessages(),round);
		ratioOfCancelledScrims = administratorService.roundTo(administratorService.ratioOfCancelledScrims(), round);
		
		result.addObject("getAvgMaxMinSdVideogamesPerManager", getAvgMaxMinSdVideogamesPerManager);
		result.addObject("getAvgMaxMinSdSeasonsPerVideogame", getAvgMaxMinSdSeasonsPerVideogame);
		result.addObject("getAvgMaxMinSdScrimsPerTeam", getAvgMaxMinSdScrimsPerTeam);
		result.addObject("getAvgMaxMinSdTeamsPerPlayer", getAvgMaxMinSdTeamsPerPlayer);
		result.addObject("getAvgMaxMinSdPlayersPerTeam", getAvgMaxMinSdPlayersPerTeam);
		
		
		result.addObject("ratioOfPlayersWithForumMessages", ratioOfPlayersWithForumMessages);
		result.addObject("ratioOfCancelledScrims", ratioOfCancelledScrims);
		
		result.addObject("getTeamsWithMoreScrims", administratorService.getTeamsWithMoreScrims());
		result.addObject("getPlayersWithMoreForumMessages", administratorService.getPlayersWithMoreForumMessages());
		result.addObject("playersWithMoreTeams", administratorService.playersWithMoreTeams());
		
		return result;
	}
	
	@RequestMapping("/displayVideogames")
	public ModelAndView displayVideogames(@RequestParam(required = true) Integer page) {
		
		ModelAndView result;
		Collection<Videogame> videogames;
		Collection<Player> players;
		Integer i;
		Boolean last;

		result = new ModelAndView("dashboard/displayVideogames");
		
		
		Pageable pageable = new PageRequest(page,5);
		
		videogames = administratorService.getVideogames(pageable);
		i = 0;
		for(Videogame v:videogames){
			players = administratorService.playersWithMoreTeamsPerVideogame(v);
			result.addObject("players"+i,players );
			result.addObject("videogame"+i,v );
			i++;
		}
		
		last = false;
		if(videogames.size()<5){
			last=true;
		}
		
		result.addObject("last", last);
		
		return result;
	}

}