package controllers.message;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.FolderService;
import services.MessageService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Message;

@Controller
@RequestMapping("/message")
public class MessageController extends AbstractController{
	
	// Services ---------------------------------------------------------------

	@Autowired
	private FolderService folderService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor -----------------------------------------------------------

	public MessageController() {
		super();
	}
	
	@RequestMapping("/create")
	public ModelAndView create(Message message, BindingResult binding,
							   @RequestParam(value = "broadcastNotification", required = false, defaultValue = "false") Boolean bNotification){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Actor principal;
		
		principal = actorService.findPrincipal();
		
		Assert.isTrue(!bNotification || principal instanceof Administrator);
		
		Assert.notNull(message);
		
		message = messageService.reconstruct(message, binding);
		
		if(binding.hasFieldErrors("subject") || binding.hasFieldErrors("body") ||
		  (!bNotification && binding.hasFieldErrors("recipients")) || 
		   binding.hasFieldErrors("priority")){
			result = new ModelAndView("folder/list");
			result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
			result.addObject("folderWithoutParent", folderService.create(null, null));
			result.addObject("folderWithParent", folderService.create(null, null));
			result.addObject("newMessage", message);
			result.addObject("actors", actorService.findAll());
			result.addObject("message", "folder.commit.error");
			
		} else{
			try{
				if(!bNotification){
					messageService.sendMessage(message.getSubject(), message.getBody(), message.getRecipients(), message.getPriority().getPriority());					
				
				} else{
					messageService.sendNotification(message.getSubject(), message.getBody(), actorService.findAll(), message.getPriority().getPriority());					
				}
				
				
				result = new ModelAndView("redirect:/folder/list.do");
		
			} catch (Throwable oops) {
				result = new ModelAndView("folder/list");
				result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
				result.addObject("folderWithoutParent", folderService.create(null, null));
				result.addObject("folderWithParent", folderService.create(null, null));
				result.addObject("newMessage", message);
				result.addObject("actors", actorService.findAll());
				result.addObject("message", "folder.commit.error");
			}	
		}
		
		return result;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(value = "messageId", required = true) Integer messageId){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Message message;
		Actor principal;
		
		principal = actorService.findPrincipal();
		message = messageService.findOne(messageId);
		
		Assert.notNull(message);
		Assert.isTrue(message.getFolder().getActor().equals(principal)); //Comprobar credenciales
		
		try{
			messageService.delete(message);
			result = new ModelAndView("redirect:/folder/list.do");
	
		} catch (Throwable oops) {
			result = new ModelAndView("folder/list");
			result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
			result.addObject("folderWithoutParent", folderService.create(null, null));
			result.addObject("folderWithParent", folderService.create(null, null));
			result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
			result.addObject("actors", actorService.findAll());
			result.addObject("message", "folder.commit.error");
		}	
		
		return result;
	}
	
	@RequestMapping("/move")
	public ModelAndView move(@RequestParam(value = "messageId", required = true) Integer messageId,
							 @RequestParam(value = "folderId", required = true) Integer folderId){
		Assert.isTrue(LoginService.isAuthenticated());
		
		ModelAndView result;
		Actor principal;
		
		principal = actorService.findPrincipal();
				
		try{
			messageService.moveMessage(messageId, folderId);
			result = new ModelAndView("redirect:/folder/list.do");
	
		} catch (Throwable oops) {
			result = new ModelAndView("folder/list");
			result.addObject("parentFolders", folderService.findParentFoldersByActorId(principal.getId()));
			result.addObject("folderWithoutParent", folderService.create(null, null));
			result.addObject("folderWithParent", folderService.create(null, null));
			result.addObject("newMessage", messageService.create(null, null, new ArrayList<Actor>()));
			result.addObject("actors", actorService.findAll());
			result.addObject("message", "folder.commit.error");
		}	
		
		return result;
	}
}
