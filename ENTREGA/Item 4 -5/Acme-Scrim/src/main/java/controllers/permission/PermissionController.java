package controllers.permission;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.PermissionService;
import controllers.AbstractController;
import domain.Permission;
import domain.Player;

@Controller
@RequestMapping("/permission")
public class PermissionController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor -----------------------------------------------------------

	public PermissionController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/create")
	public ModelAndView create() {
		ModelAndView result;
		Permission permission;
		Collection<Player> players;
		
		actorService.checkIsAdministrator();
		permission = permissionService.create();
		players = permissionService.findPlayersWithNoPermission();
		
		if(players.size() == 0){
			result = new ModelAndView("permission/create");
			result.addObject("noPlayers", true);
			return result;
		}
		
		result = new ModelAndView("permission/create");
		result.addObject("permission", permission);
		result.addObject("players", players);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView save(Permission permission, BindingResult binding) {
		ModelAndView result;
		Collection<Player> players;
		
		actorService.checkIsAdministrator();
		Assert.isTrue(permissionService.findPlayersWithNoPermission().contains(permission.getPlayer()));
		
		players = permissionService.findPlayersWithNoPermission();
		permission = permissionService.reconstruct(permission, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("permission/create");
			result.addObject("permission", permission);
			result.addObject("players", players);
			
		} else{
			try{
				permissionService.save(permission);
				
				result = new ModelAndView("redirect:/");
		
			} catch (Throwable oops) {
				result = new ModelAndView("permission/create");
				result.addObject("permission", permission);
				result.addObject("players", players);
				result.addObject("message", "permission.commit.error");
			}
		}
				
		return result;
	}
}
