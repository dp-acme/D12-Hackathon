package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Finder extends DomainEntity{

	// Constructor
	
	public Finder() {
		super();
	}
	
	// Attributes
	
	private String keyWord;
	private Date dateStart;
	private Date dateEnd;
	private Date lastSaved;

	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateEnd() {
		return dateEnd;
	}
	
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastSaved() {
		return lastSaved;
	}
	public void setLastSaved(Date dateStart) {
		this.lastSaved = dateStart;
	}
	
	
	// Relationships
	
	private Actor actor;
	private Videogame videogame;
	private Region region;
	private Collection<Scrim> scrims;
	
	@Valid
	@ManyToMany
	public Collection<Scrim> getScrims() {
		return scrims;
	}
	public void setScrims(Collection<Scrim> scrims) {
		this.scrims = scrims;
	}
	
	@Valid
	@OneToOne
	public Actor getActor() {
		return  this.actor;
	}
	
	public void setActor(Actor actor) {
		this.actor = actor;
	}
	
	@Valid
	@ManyToOne
	public Videogame getVideogame() {
		return  this.videogame;
	}
	
	public void setVideogame(Videogame videogame) {
		this.videogame = videogame;
	}
	
	@Valid
	@ManyToOne
	public Region getRegion() {
		return  this.region;
	}
	
	public void setRegion(Region region) {
		this.region = region;
	}
	
}
