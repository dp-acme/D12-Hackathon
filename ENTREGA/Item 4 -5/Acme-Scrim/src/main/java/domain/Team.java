package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="name")})
public class Team extends DomainEntity {

	// Constructor
	
	public Team() {
		super ();
	}
	
	// Attributes
	
	private String name;
	private String web;
	private String logo;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@URL
	public String getWeb() {
		return web;
	}
	public void setWeb(String web) {
		this.web = web;
	}
	
	@URL
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	// Relationships
	
	private Region region;
	private GameAccount leader;
	private Collection<Invitation> invitations;
	private Videogame videogame;
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public GameAccount getLeader() {
		return leader;
	}
	public void setLeader(GameAccount leader) {
		this.leader = leader;
	}
	
	@Valid
	@NotNull
	@OneToMany(mappedBy = "team")
	public Collection<Invitation> getInvitations() {
		return invitations;
	}
	public void setInvitations(Collection<Invitation> invitations) {
		this.invitations = invitations;
	}
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Videogame getVideogame() {
		return videogame;
	}
	public void setVideogame(Videogame videogame) {
		this.videogame = videogame;
	}
	
}
