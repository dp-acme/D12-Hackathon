package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class GameCategory extends DomainEntity {

	// Constructor
	public GameCategory() {
		super();
	}

	// Attributes
	private String name;

	// GET/SET Methods

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// Relationships
	
	private Collection<Videogame> videogames;
	private GameCategory parent;
	private Collection<GameCategory> children;
	
	@Valid
	@ManyToMany
	public Collection<Videogame> getVideogames() {
		return videogames;
	}
	public void setVideogames(Collection<Videogame> videogames) {
		this.videogames = videogames;
	}
	
	@Valid
	@ManyToOne(optional=true)
	public GameCategory getParent() {
		return parent;
	}
	public void setParent(GameCategory parent) {
		this.parent = parent;
	}

	@NotNull
	@Valid
	@OneToMany(mappedBy = "parent")
	public Collection<GameCategory> getChildren() {
		return children;
	}

	public void setChildren(Collection<GameCategory> children) {
		this.children = children;
	}

}
