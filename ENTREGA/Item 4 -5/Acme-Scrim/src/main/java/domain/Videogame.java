package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="name")})
public class Videogame extends DomainEntity {

	// Constructor
	public Videogame() {
		super();
	}

	// Attributes
	private String name;
	private String description;
	private String image;

	// GET/ SET Methods

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotBlank
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@URL
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	// Relationships

	private Manager manager;
	private Collection<Season> seasons;
	private Collection<ForumCategory> forumCategories;

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	@Valid
	@NotNull
	@OneToMany(cascade = CascadeType.ALL, mappedBy="videogame")
	public Collection<Season> getSeasons() {
		return seasons;
	}

	public void setSeasons(Collection<Season> seasons) {
		this.seasons = seasons;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "videogame")
	public Collection<ForumCategory> getForumCategories() {
		return forumCategories;
	}

	public void setForumCategories(Collection<ForumCategory> forumCategories) {
		this.forumCategories = forumCategories;
	}

}
