package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Embeddable
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="result")})
public class Result {

	// Constructor
	
	public Result() {
		super ();
	}
	
	// Values -----------------------------------------------------------------

	public static final String	WIN			= "WIN";
	public static final String	LOSE		= "LOSE";
	public static final String	DRAW		= "DRAW";
	public static final String	ILLOGICAL	= "ILLOGICAL";

	// Attributes -------------------------------------------------------------

	private String result;

	@NotBlank
	@Pattern(regexp = "^" + Result.WIN + "|" + Result.LOSE + "|" + Result.DRAW + "|" + Result.ILLOGICAL + "$")
	public String getResult() {
		return this.result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
}
