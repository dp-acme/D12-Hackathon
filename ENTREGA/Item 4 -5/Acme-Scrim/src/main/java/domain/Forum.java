package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Forum extends DomainEntity {

	// Constructor
	
	public Forum() {
		super();
	}
	
	// Attributes
	
	private String name;
	private String description;

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	// Relationships
	private Collection<ForumCategory> forumCategories;

	@NotNull
	@Valid
	@ManyToMany
	public Collection<ForumCategory> getForumCategories() {
		return forumCategories;
	}
	public void setForumCategories(Collection<ForumCategory> forumCategories) {
		this.forumCategories = forumCategories;
	}
}
