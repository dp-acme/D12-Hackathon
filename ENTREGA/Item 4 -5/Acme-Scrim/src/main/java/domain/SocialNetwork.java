package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class SocialNetwork extends DomainEntity {

	// Constructor
	public SocialNetwork() {
		super();
	}

	// Attributes
	private String name;
	private String image;

	// Methods GET/ SET
	
	@NotBlank
	public String getName() {
		return this.name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	
	@URL
	public String getImage() {
		return this.image;
	}
	public void setImage(final String link) {
		this.image = link;
	}

}
