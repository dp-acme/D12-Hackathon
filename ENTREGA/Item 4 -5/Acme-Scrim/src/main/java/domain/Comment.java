package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Comment extends DomainEntity {
	
	private String comments;
	
	public Comment(){
		super();
	}

	@NotBlank
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	private Scrim scrim;
	private Player creator;

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Scrim getScrim() {
		return scrim;
	}

	public void setScrim(Scrim scrim) {
		this.scrim = scrim;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Player getCreator() {
		return creator;
	}

	public void setCreator(Player creator) {
		this.creator = creator;
	}
	
	
	
	
}
