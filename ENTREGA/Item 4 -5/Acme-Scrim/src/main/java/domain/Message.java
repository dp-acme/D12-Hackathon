package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="moment")})
public class Message extends DomainEntity {
	
	// Constructor
	
	public Message() {
		super();
	}

	// Attributes
	
	private Date moment;
	private String subject;
	private String body;
	private Priority priority;
	private Boolean isSpam;
	
	@NotNull
	@Past
	public Date getMoment() {
		return moment;
	}
	public void setMoment(Date moment) {
		this.moment = moment;
	}

	@NotBlank
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@NotBlank
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	@Valid
	@NotNull
	public Priority getPriority() {
		return priority;
	}
	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	@NotNull
	public Boolean getIsSpam() {
		return isSpam;
	}
	public void setIsSpam(Boolean isSpam) {
		this.isSpam = isSpam;
	}
	
	// Relationships
	
	private Actor sender;
	private Collection<Actor> recipients;
	private Folder folder;

	@Valid
	@NotNull
	@ManyToOne
	public Actor getSender() {
		return sender;
	}
	public void setSender(Actor sender) {
		this.sender = sender;
	}
	
	@Valid
	@NotNull
	@ManyToMany
	public Collection<Actor> getRecipients() {
		return recipients;
	}
	public void setRecipients(Collection<Actor> recipients) {
		this.recipients = recipients;
	}
	
	@Valid
	@NotNull
	@ManyToOne
	public Folder getFolder() {
		return folder;
	}
	public void setFolder(Folder folder) {
		this.folder = folder;
	}
}
