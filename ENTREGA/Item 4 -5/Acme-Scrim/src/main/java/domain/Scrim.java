package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="moment")})
public class Scrim extends DomainEntity {

	// Constructor
	
	public Scrim() {
		super();
	}
	
	// Attributes
	
	private Date moment;
	private String streamingLink;
	private Result creatorTeamResult;
	private Result guestTeamResult;
	private Boolean confirmed;
	private ScrimStatus status;
	
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return moment;
	}
	public void setMoment(Date moment) {
		this.moment = moment;
	}
	
	@URL
	public String getStreamingLink() {
		return streamingLink;
	}
	public void setStreamingLink(String streamingLink) {
		this.streamingLink = streamingLink;
	}
	
	@Valid
	@AttributeOverrides({
		@AttributeOverride(name = "result", column = @Column(name = "creatorTeamResult")),
	})
	public Result getCreatorTeamResult() {
		return creatorTeamResult;
	}
	public void setCreatorTeamResult(Result creatorTeamResult) {
		this.creatorTeamResult = creatorTeamResult;
	}
	
	@Valid
	@AttributeOverrides({
		@AttributeOverride(name = "result", column = @Column(name = "guestTeamResult")),
	})
	public Result getGuestTeamResult() {
		return guestTeamResult;
	}
	public void setGuestTeamResult(Result guestTeamResult) {
		this.guestTeamResult = guestTeamResult;
	}
	
	public Boolean isConfirmed() {
		return confirmed;
	}
	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}

	@Transient
	public ScrimStatus getStatus() {
		this.status = new ScrimStatus();
		
		if (this.isConfirmed() == null && this.getMoment().after(new Date())) {
			this.status.setScrimStatus(ScrimStatus.PENDING);
		} else if ((this.getGuest() == null || this.isConfirmed() == null) && this.getMoment().before(new Date())) {
			this.status.setScrimStatus(ScrimStatus.EXPIRED);
		} else if (this.getGuest() != null && this.getMoment().after(new Date()) && this.isConfirmed()) {
			this.status.setScrimStatus(ScrimStatus.CONFIRMED);
		} else if (this.getGuest() != null && !this.isConfirmed()) {
			this.status.setScrimStatus(ScrimStatus.CANCELLED);
		} else if (this.getGuest() != null && this.getMoment().before(new Date()) && this.isConfirmed() && 
				(this.getGuestTeamResult() == null || this.getCreatorTeamResult() == null)) {
			this.status.setScrimStatus(ScrimStatus.FINISHED);
		} else if (this.getGuestTeamResult() != null && this.getCreatorTeamResult() != null) {
			this.status.setScrimStatus(ScrimStatus.CLOSED);
		}
		
		return this.status;
	}
	public void setStatus(ScrimStatus status) {
		this.status = status;
	}
	
	// Relationships
	
	private Team creator;
	private Team guest;
	private Region desiredGuestRegion;
	private Season season;
	private Resolution resolution;
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Team getCreator() {
		return creator;
	}
	public void setCreator(Team creator) {
		this.creator = creator;
	}
	
	@Valid
	@ManyToOne(optional = true)
	public Team getGuest() {
		return guest;
	}
	public void setGuest(Team guest) {
		this.guest = guest;
	}
	
	@Valid
	@ManyToOne(optional = true)
	public Region getDesiredGuestRegion() {
		return desiredGuestRegion;
	}
	public void setDesiredGuestRegion(Region region) {
		this.desiredGuestRegion = region;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Season getSeason() {
		return season;
	}
	public void setSeason(Season season) {
		this.season = season;
	}
	
	@Valid
	@OneToOne(optional = true, mappedBy = "scrim")
	public Resolution getResolution() {
		return resolution;
	}
	public void setResolution(Resolution resolution) {
		this.resolution = resolution;
	}
	
}
