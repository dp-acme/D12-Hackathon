package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import security.UserAccount;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="userAccount_id")})
public abstract class Actor extends DomainEntity {

	// Constructor
	
	public Actor() {
		super();
	}
	
	// Attributes
	
	private String name;
	private String surname;
	private String email;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@Email
	@NotBlank
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	// Relationship
	
	private UserAccount userAccount;
	private Collection<Team> teamsFollowed;
	
	@NotNull
	@Valid
	@OneToOne(cascade = CascadeType.ALL)
	public UserAccount getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	@NotNull
	@Valid
	@OneToMany()
	public Collection<Team> getTeamsFollowed() {
		return teamsFollowed;
	}
	public void setTeamsFollowed(Collection<Team> teamsFollowed) {
		this.teamsFollowed = teamsFollowed;
	}

}
