package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

	// Constructor

	public Manager() {
		super();
	}

	// Attributes

	// Relationships

	private Collection<Videogame> myVideogames;

	@Valid
	@OneToMany(mappedBy = "manager")
	public Collection<Videogame> getmyVideogames() {
		return myVideogames;
	}

	public void setmyVideogames(Collection<Videogame> myVideogames) {
		this.myVideogames = myVideogames;
	}
}
