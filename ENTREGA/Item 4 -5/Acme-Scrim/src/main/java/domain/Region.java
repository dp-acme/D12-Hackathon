package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = { @Index(columnList ="englishName, spanishName, abbreviation, colorDisplay")})
public class Region extends DomainEntity {

	// Constructor
	
	public Region() {
		super();
	}

	// Attributes

	private String englishName;
	private String spanishName;
	private String abbreviation;
	private String colorDisplay;
	
	@NotBlank
	@Column(unique = true)
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	
	@NotBlank
	@Column(unique = true)
	public String getSpanishName() {
		return spanishName;
	}
	public void setSpanishName(String spanishName) {
		this.spanishName = spanishName;
	}
	
	@NotBlank
	@Column(unique = true)
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	
	@NotBlank
	@Column(unique = true)
	public String getColorDisplay() {
		return colorDisplay;
	}
	public void setColorDisplay(String colorDisplay) {
		this.colorDisplay = colorDisplay;
	}

}
