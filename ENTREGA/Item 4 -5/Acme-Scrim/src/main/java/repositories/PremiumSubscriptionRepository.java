package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.PremiumSubscription;

@Repository
public interface PremiumSubscriptionRepository extends JpaRepository<PremiumSubscription, Integer>{
	
	@Query("select p from PremiumSubscription p where p.player.id = ?1")
	PremiumSubscription findPremiumSubscription(int playerId);
	
}