package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Player;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Integer>{
	
//	@Query("select g.player from GameAccount g where " +
//			"g in (select inv.teammate from Invitation inv " +
//			"where inv in (select s.creator.invitations from Scrim s " +
//			"where s.creator in (select g1.teams from GameAccount g1 " +
//			"where g1.player.id = ?2) " +
//			"or s.creator.leader.player.id = ?2 " +
//			"and s.id = ?1) " +
//			"or inv in (select s3.guest.invitations from Scrim s3 " +
//			"where s3.guest in (select g5.teams from GameAccount g5 " +
//			"where g5.player.id = ?2) " +
//			"or s3.guest.leader.player.id = ?2 " +
//			"and s3.id = ?1) "+
//			"and inv.moment is not null) " +
//			"or g = (select s1.creator.leader from Scrim s1 " +
//			"where s1.creator in (select g3.teams from GameAccount g3 " +
//			"where g3.player.id = ?2) " +
//			"or s1.creator.leader.player.id = ?2 " +
//			"and s1.id = ?1) " +
//			"or g = (select s2.guest.leader from Scrim s2 " +
//			"where s2.guest in (select g4.teams from GameAccount g4 " +
//			"where g4.player.id = ?2 " +
//			"and s2.id = ?1))")
//	Collection<Player> getPlayersByScrimAndPlayer(int scrimId, int playerId);

	@Query("select g.player from GameAccount g " +
			"where g in (select inv.teammate from Invitation inv " +
			"where inv.team = (select s.creator from Scrim s " +
			"where s.id = ?1) " +
			"or inv.team = (select s2.guest from Scrim s2 " +
			"where s2.id = ?1)) " +
			"or g in (select t.leader from Team t " +
			"where t = (select s3.creator from Scrim s3 " +
			"where s3.id=?1) " +
			"or t=(select s4.guest from Scrim s4 " +
			"where s4.id=?1))")
	Collection<Player> getPlayersByScrim(int scrimId);
	
	@Query("select g.player from GameAccount g " +
			"where (g in (select inv.teammate from Invitation inv " +
			"where inv.team = (select s.creator from Scrim s " +
			"where s.id = ?1) " +
			"or inv.team = (select s2.guest from Scrim s2 " +
			"where s2.id = ?1)) " +
			"or g in (select t.leader from Team t " +
			"where t = (select s3.creator from Scrim s3 " +
			"where s3.id=?1) " +
			"or t=(select s4.guest from Scrim s4 " +
			"where s4.id=?1))) " +
			"and g.player.premiumSubscription is not null")
	Collection<Player> getPremiumPlayersByScrim(int scrimId);

	@Query("select g.player from GameAccount g " +
			"where g in (select inv.teammate from Invitation inv " +
			"where inv.team.id = ?1) " +
			"or g in (select t.leader from Team t " +
			"where t.id = ?1)")
	Collection<Player> getPlayersInTeam(int teamId);
	
	@Query("select g.player from GameAccount g " +
			"where g in (select inv.teammate from Invitation inv " +
			"where inv.team.id = ?1) " +
			"or g in (select t.leader from Team t " +
			"where t.id = ?1) and g.player.premiumSubscription is not null")
	Collection<Player> getPremiumPlayersInTeam(int teamId);
	
}