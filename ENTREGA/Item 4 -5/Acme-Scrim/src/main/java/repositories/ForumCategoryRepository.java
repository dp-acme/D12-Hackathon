package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.ForumCategory;
import domain.Videogame;

@Repository
public interface ForumCategoryRepository extends JpaRepository<ForumCategory, Integer>{
	
	@Query("select v.forumCategories.size from Videogame v where v.id=?1")
	Double forumCategoriesSize(int videogameId);
	
	@Query("select f from ForumCategory f where (?1 member of f.childrenCategories)")
	ForumCategory parentCategory(int forumCategoryId);
	
	@Query("select f from ForumCategory f where f.name = 'FORUM' AND f.videogame=?1")
	ForumCategory findRootForumCategory(Videogame videogame);

	@Query("select f from ForumCategory f where  f.videogame=?1")
	Collection<ForumCategory> findForumCategoriesByVideogame(Videogame videogame);
	
}