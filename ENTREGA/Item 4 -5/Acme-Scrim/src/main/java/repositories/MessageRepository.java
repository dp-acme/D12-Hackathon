package repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer>{

	@Query("select m from Message m where m.folder.id = ?1 order by m.moment desc")
	Page<Message> findPaginatedMessagesByFolderId(Integer folderId, Pageable pageable);
}