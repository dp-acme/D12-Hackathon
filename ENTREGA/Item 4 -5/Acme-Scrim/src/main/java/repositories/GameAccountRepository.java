package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.GameAccount;

@Repository
public interface GameAccountRepository extends JpaRepository<GameAccount, Integer>{

	@Query("select gameAccount from GameAccount gameAccount where gameAccount.player.id = ?1 AND gameAccount.videogame.id = ?2")
	GameAccount getGameAccountForPlayerAndVideogame(int playerId, int videogameId);
	
	@Query("select gameAccount from GameAccount gameAccount where gameAccount.player.id = ?1")
	Collection<GameAccount> getGameAccountsForPlayer(int playerId);
	
	@Query("select gameAccount from GameAccount gameAccount where gameAccount.videogame.id = (select team.videogame from Team team where team.id = ?1) AND " +
			"gameAccount NOT IN (select inv.teammate from Invitation inv where inv.team.id = ?1) AND " +
			"gameAccount != (select team.leader from Team team where team.id = ?1)")
	Collection<GameAccount> getGameAccountsNotTeam(int teamId);
	
}