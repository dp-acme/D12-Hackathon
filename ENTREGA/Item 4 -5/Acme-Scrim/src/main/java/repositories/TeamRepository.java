package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.GameAccount;
import domain.Player;
import domain.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer>{

	@Query("select inv.teammate from Invitation inv where inv.team.id = ?1")
	Collection<GameAccount> getTeammates(int teamId);
	
	@Query("select inv.teammate from Invitation inv where inv.team.id = ?1 AND inv.moment != null")
	Collection<GameAccount> getRealTeammates(int teamId);
	
	@Query("select inv.teammate.player from Invitation inv where inv.team.id = ?1")
	Collection<Player> getPlayerTeammates(int teamId);
	
	@Query("select t from Team t where t.leader.id = ?1 OR t = (select inv.team from Invitation inv where inv.teammate.id = ?1)")
	Collection<Team> getTeamsOfGameAccount(int gameAccountId);
	
	@Query("select t from Team t where t.videogame.id = ?1")
	Collection<Team> getTeamsOfVideogame(int videogameId);
	
	@Query("select t, cast((select count(*) from Review r where r.team=t AND r.positive=true AND r.team.videogame.id=?1)as float) from Team t " +
			"where t.videogame.id = ?1 order by(cast((select count(*) from Review r where r.team=t AND r.positive=true AND r.team.videogame.id=?1)as float)) desc")
	Collection<Object[]> getTeamsOrderedByLikes(int videogameId); 
	
	@Query("select t,cast((select count(s) from Scrim s where(s.guest=t AND s.guestTeamResult.result='WIN' AND s.guest.videogame.id=?1)" +
			"OR(s.creator=t AND s.creatorTeamResult.result='WIN')AND s.creator.videogame.id=?1)as float) from Team t " +
			"where t.videogame.id = ?1 order by (cast((select count(s) from Scrim s where(s.guest=t AND s.guestTeamResult.result='WIN' AND s.guest.videogame.id=?1)" +
			"OR(s.creator=t AND s.creatorTeamResult.result='WIN')AND s.creator.videogame.id=?1)as float)) desc")
	Collection<Object[]> getTeamsOrderedByWonGames(int videogameId);
	
	@Query("select a from Actor a where (select t from Team t where t.id = ?1) IN ELEMENTS (a.teamsFollowed)")
	Collection<Actor> getTeamFollowers(int teamId);
}