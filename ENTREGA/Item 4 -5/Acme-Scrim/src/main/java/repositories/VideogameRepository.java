package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.GameAccount;
import domain.GameCategory;
import domain.Player;
import domain.Team;
import domain.Videogame;

@Repository
public interface VideogameRepository extends JpaRepository<Videogame, Integer>{

	// Lista de GameCategories que tiene un Videogame
	@Query("select gc from GameCategory gc where ?1 in elements(gc.videogames)")
	Collection<GameCategory> listOfGameCategories(int videogameId);
	
	// Lista de Teams que tiene un Videogame
	@Query("select t from Team t where t.videogame.id = ?1")
	Collection<Team> listOfTeams(int videogameId);
	
	// GameAccount asociada a un videogame
	@Query("select g from GameAccount g where g.videogame.id = ?1")
	Collection<GameAccount> listOfGameAccountsOfVideogame (int videogameId);
	
	@Query("select g.player from GameAccount g where g.videogame.id = ?1")
	Collection<Player> listOfPlayersOfVideogame (int videogameId);
	
	@Query("select vg from Videogame vg where vg NOT IN (select gA.videogame from GameAccount gA where gA.player.id = ?1)")
	Collection<Videogame> getVideogamesWithoutGameAccount (int playerId);
	
	@Query("select vg from Videogame vg where vg IN (select gA.videogame from GameAccount gA where gA.player.id = ?1)")
	Collection<Videogame> getVideogamesWithGameAccount (int playerId);
	
}