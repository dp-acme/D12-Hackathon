package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{
	
	@Query("select c from Comment c where c.creator.id = ?1 and c.scrim.id = ?2")
	Collection<Comment> getCommentsByPlayerAndScrim(int playerId, int scrimId);
	
	@Query("select c from Comment c where c.scrim.id = ?1")
	Collection<Comment> getCommentsByScrim(int scrimId);


}
