package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.GameAccount;
import domain.Region;
import domain.Scrim;
import domain.Team;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer>{

	@Query("select region from Region region where region.englishName = 'International'")
	Region getInternational();
	
	@Query("select team from Team team where team.region.id = ?1")
	Collection<Team> getTeamsWithRegion(int regionId);
	
	@Query("select gameAccount from GameAccount gameAccount where gameAccount.region.id = ?1")
	Collection<GameAccount> getGameAccountsWithRegion(int regionId);
	
	@Query("select scrim from Scrim scrim where scrim.desiredGuestRegion.id = ?1")
	Collection<Scrim> getScrimsWithRegion(int regionId);
	
	@Query("select region from Region region where region.englishName like ?2 AND region.id != ?1")
	Region uniqueEnglishName(int regionId, String englishName);
	
	@Query("select region from Region region where region.spanishName like ?2 AND region.id != ?1")
	Region uniqueSpanishName(int regionId, String spanishName);
	
	@Query("select region from Region region where region.abbreviation like ?2 AND region.id != ?1")
	Region uniqueAbbreviation(int regionId, String abbreviation);
	
	@Query("select region from Region region where region.colorDisplay like ?2 AND region.id != ?1")
	Region uniqueColor(int regionId, String color);
	
}