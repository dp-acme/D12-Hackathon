package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.Review;

public interface ReviewRepository extends JpaRepository<Review, Integer>{

	@Query("select l from Review l where l.team.id = ?1")
	Collection<Review> getLikesByTeam(int teamId);
	
	@Query("select l from Review l where l.scrim.id = ?1")
	Collection<Review> getLikesByScrim(int scrimId);
	
	@Query("select l from Review l where l.scrim.id = ?1 and l.team.id = ?2")
	Collection<Review> getLikesByScrimAndTeam(int scrimId, int teamId);
	
	@Query("select count(*) from Review l where l.team.id = ?1 AND l.positive = true")
	int getNumberLikesByTeam(int teamId);
	
	@Query("select count(*) from Review l where l.team.id = ?1 AND l.positive = false")
	int getNumberDislikesByTeam(int teamId);
	
}
