<?xml version="1.0" encoding="UTF-8"?>

<!-- 
 * security.xml
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 -->

<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:security="http://www.springframework.org/schema/security"	
	xsi:schemaLocation="
		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd		
        http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security-3.2.xsd
    ">

	<!-- Security infrastructure -->

	<bean id="loginService" class="security.LoginService" />

	<bean id="passwordEncoder" class="org.springframework.security.authentication.encoding.Md5PasswordEncoder" />

	<!-- Access control -->

	<security:http auto-config="true" use-expressions="true">
		<security:intercept-url pattern="/" access="permitAll" /> 

		<security:intercept-url pattern="/favicon.ico" access="permitAll" /> 
		<security:intercept-url pattern="/images/**" access="permitAll" /> 
		<security:intercept-url pattern="/scripts/**" access="permitAll" /> 
		<security:intercept-url pattern="/styles/**" access="permitAll" /> 

		<security:intercept-url pattern="/views/misc/index.jsp" access="permitAll" /> 

		<security:intercept-url pattern="/security/login.do" access="permitAll" /> 
		<security:intercept-url pattern="/security/loginFailure.do" access="permitAll" /> 

		<security:intercept-url pattern="/welcome/index.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/faq.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/aboutUs.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/termsAndConditions.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/advertise.do" access="permitAll" />
		
		<security:intercept-url pattern="/scrim/display.do" access="permitAll" />
		<security:intercept-url pattern="/scrim/list.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/scrim/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/scrim/edit.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/scrim/join.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/scrim/invite.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/scrim/acceptInvitation.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/scrim/rejectInvitation.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/scrim/win.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/scrim/draw.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/scrim/lose.do" access="hasRole('PLAYER')" />

		<security:intercept-url pattern="/advertisement/sponsor/list.do" access="hasRole('SPONSOR')" />
		<security:intercept-url pattern="/advertisement/sponsor/display.do" access="hasRole('SPONSOR')" />
		<security:intercept-url pattern="/advertisement/sponsor/create.do" access="hasRole('SPONSOR')" />
		<security:intercept-url pattern="/advertisement/sponsor/edit.do" access="hasRole('SPONSOR')" />
		<security:intercept-url pattern="/advertisement/sponsor/delete.do" access="hasRole('SPONSOR')" />
		
		<security:intercept-url pattern="/season/manager/list.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/season/manager/create.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/season/manager/finished.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/season/display.do" access="permitAll" />
		<security:intercept-url pattern="/season/list.do" access="permitAll" />
		
		<security:intercept-url pattern="/videogame/display.do" access="permitAll" />
		<security:intercept-url pattern="/videogame/manager/create.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/videogame/list.do" access="permitAll" />
		<security:intercept-url pattern="/videogame/manager/list.do" access="hasRole('MANAGER')" />
		
		<security:intercept-url pattern="/player/create.do" access="isAnonymous()" />
		<security:intercept-url pattern="/manager/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/referee/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/sponsor/create.do" access="isAnonymous()" />
		
		<security:intercept-url pattern="/player/askRefereePermission.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/player/askManagerPermission.do" access="hasRole('PLAYER')" />
		
		<security:intercept-url pattern="/player/edit.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/referee/edit.do" access="hasRole('REFEREE')" />
		<security:intercept-url pattern="/manager/edit.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/sponsor/edit.do" access="hasRole('SPONSOR')" />
		
		<security:intercept-url pattern="/player/list.do" access="permitAll" />
		<security:intercept-url pattern="/referee/list.do" access="hasAnyRole('MANAGER', 'ADMIN')" />
		<security:intercept-url pattern="/manager/list.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/sponsor/list.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/player/display.do" access="permitAll" />
		<security:intercept-url pattern="/referee/display.do" access="hasAnyRole('REFEREE', 'MANAGER', 'ADMIN')" />
		<security:intercept-url pattern="/manager/display.do" access="hasAnyRole('MANAGER', 'ADMIN')" />
		<security:intercept-url pattern="/sponsor/display.do" access="hasAnyRole('SPONSOR', 'ADMIN')" />
		
		<security:intercept-url pattern="/actor/followTeam.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/actor/unfollowTeam.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/actor/listTeamsFollowed.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/actor/ban.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/actor/unban.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/premiumSubscription/create.do" access="hasRole('PLAYER')" />
		
		<security:intercept-url pattern="/permission/create.do" access="hasRole('ADMIN')" />

		<security:intercept-url pattern="/folder/list.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/folder/create.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/folder/delete.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/folder/move.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/folder/edit.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/message/create.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/message/delete.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/message/move.do" access="isAuthenticated()" />
		
		<security:intercept-url pattern="/review/player/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/comment/player/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/report/player/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/referee/dashboard.do" access="hasRole('REFEREE')" />
		<security:intercept-url pattern="/report/player/edit.do" access="hasRole('PLAYER')" />
		
		<security:intercept-url pattern="/resolution/player/edit.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/resolution/player/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/resolution/referee/resolve.do" access="hasRole('REFEREE')" />
		<security:intercept-url pattern="/referee/ban.do" access="hasRole('REFEREE')" />
		<security:intercept-url pattern="/referee/unban.do" access="hasRole('REFEREE')" />
		
		<security:intercept-url pattern="/socialNetwork/create.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/socialNetwork/list.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/socialNetwork/delete.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/socialIdentity/create.do" access="isAuthenticated()" />
		<security:intercept-url pattern="/socialIdentity/delete.do" access="isAuthenticated()" />
		
		<security:intercept-url pattern="/forum/list.do" access="permitAll" />
		<security:intercept-url pattern="/forum/createCategory.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/forum/editCategory.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/forum/deleteCategory.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/forum/create.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/forum/edit.do" access="hasRole('MANAGER')" />	
		<security:intercept-url pattern="/forum/delete.do" access="hasRole('MANAGER')" />	
		<security:intercept-url pattern="/forum/deleteMessage.do" access="hasRole('MANAGER')" />			
		<security:intercept-url pattern="/forum/createMessage.do" access="isAuthenticated()" />	
		<security:intercept-url pattern="/forum/display.do" access="permitAll" />
		
		<security:intercept-url pattern="/dashboard/display.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/dashboard/displayVideogames.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/gameCategory/list.do" access="permitAll" />
		<security:intercept-url pattern="/gameCategory/admin/edit.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/gameCategory/admin/create.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/gameCategory/admin/delete.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/configuration/display.do" access="hasRole('ADMIN')" />		
		<security:intercept-url pattern="/configuration/edit.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/region/edit.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/region/create.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/team/display.do" access="permitAll" />
		<security:intercept-url pattern="/team/list.do" access="permitAll" />
		<security:intercept-url pattern="/team/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/team/edit.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/team/kick.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/team/invite.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/team/changeLeader.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/team/exit.do" access="hasRole('PLAYER')" />
		
		<security:intercept-url pattern="/gameAccount/list.do" access="permitAll" />
		<security:intercept-url pattern="/gameAccount/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/gameAccount/edit.do" access="hasRole('PLAYER')" />
		
		<security:intercept-url pattern="/invitation/create.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/invitation/accept.do" access="hasRole('PLAYER')" />
		<security:intercept-url pattern="/invitation/reject.do" access="hasRole('PLAYER')" />
		
		<security:intercept-url pattern="/**" access="hasRole('NONE')" />

		<security:form-login 
			login-page="/security/login.do"
			password-parameter="password" 
			username-parameter="username"
			authentication-failure-url="/security/loginFailure.do" />

		<security:logout 
			logout-success-url="/" 
			invalidate-session="true" />
	</security:http>

</beans>