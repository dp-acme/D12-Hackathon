<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ page import="java.text.DecimalFormat" %>


<script type="text/javascript">
function showTables(divName) {
	   
	var systems = ["videogame0", "videogame1","videogame3", "videogame4"];

	for (i = 0; i < systems.length; i++) { 
	    if(systems[i] == divName){
	    	 document.getElementById(divName).style.display = "block";
	    }else{
	    	 document.getElementById(systems[i]).style.display = "none";
	    }
	}
}

</script>
	<a href="dashboard/display.do"><spring:message code="dashboard.videogames.backToDashboard"/></a>
	<br>

<jstl:if test="${not empty videogame0 }">
<button onclick="showTables('videogame0')"><spring:message text="${videogame0.getName() }" /></button>
</jstl:if><jstl:if test="${not empty videogame1 }">
<button onclick="showTables('videogame1')"><spring:message text="${videogame1.getName() }" /></button>
</jstl:if> <jstl:if test="${not empty videogame2 }">
<button onclick="showTables('videogame2')"><spring:message text="${videogame2.getName() }" /></button>
</jstl:if> <jstl:if test="${not empty videogame3 }">
<button onclick="showTables('videogame3')"><spring:message text="${videogame3.getName() }" /></button>
</jstl:if> <jstl:if test="${not empty videogame4 }">
<button onclick="showTables('videogame4')"><spring:message text="${videogame4.getName() }" /></button>
</jstl:if>


<div id="videogame0" style="display: block;"> 
<h2><spring:message text="${videogame0.getName() }" /></h2>


<display:table id="row" name="players0" requestURI="dashboard/displayVideogames.do" pagesize="5">
	<display:column titleKey="dashboard.name" sortable="true" >
			<a  href="player/display.do?playerId=${row.id}">
				<spring:message text="${row.name }" />
			</a>
	</display:column>
	<display:column property="surname" titleKey="dashboard.surname" sortable="true" />
	<display:column property="userAccount.username" titleKey="dashboard.username" sortable="true" />
	</display:table>
</div>

<div id="videogame1" style="display: none;"> 
<h2><spring:message text="${videogame1.getName()  }" /></h2>


<display:table id="row" name="players1" requestURI="dashboard/displayVideogames.do" pagesize="5">
	<display:column titleKey="dashboard.name" sortable="true" >
			<a  href="player/display.do?playerId=${row.id}">
				<spring:message text="${row.name }" />
			</a>
	</display:column>
	<display:column property="surname" titleKey="dashboard.surname" sortable="true" />
	<display:column property="userAccount.username" titleKey="dashboard.username" sortable="true" />
	</display:table>
</div>

<div id="videogame2" style="display: none;"> 
<h2><spring:message text="${videogame2.getName()  }" /></h2>


<display:table id="row" name="players2" requestURI="dashboard/displayVideogames.do" pagesize="5">
	<display:column titleKey="dashboard.name" sortable="true" >
			<a  href="player/display.do?playerId=${row.id}">
				<spring:message text="${row.name }" />
			</a>
	</display:column>
	<display:column property="surname" titleKey="dashboard.surname" sortable="true" />
	<display:column property="userAccount.username" titleKey="dashboard.username" sortable="true" />
	</display:table>
</div>

<div id="videogame3" style="display: none;"> 
<h2><spring:message text="${videogame3.getName()  }" /></h2>


<display:table id="row" name="players3" requestURI="dashboard/displayVideogames.do" pagesize="5">
	<display:column titleKey="dashboard.name" sortable="true" >
			<a  href="player/display.do?playerId=${row.id}">
				<spring:message text="${row.name }" />
			</a>
	</display:column>
	<display:column property="surname" titleKey="dashboard.surname" sortable="true" />
	<display:column property="userAccount.username" titleKey="dashboard.username" sortable="true" />
	</display:table>
</div>

<div id="videogame4" style="display: none;"> 
<h2><spring:message text="${videogame4.getName()  }" /></h2>


<display:table id="row" name="players4" requestURI="dashboard/displayVideogames.do" pagesize="5">
	<display:column titleKey="dashboard.name" sortable="true" >
			<a  href="player/display.do?playerId=${row.id}">
				<spring:message text="${row.name }" />
			</a>
	</display:column>
	<display:column property="surname" titleKey="dashboard.surname" sortable="true" />
	<display:column property="userAccount.username" titleKey="dashboard.username" sortable="true" />
	</display:table>
</div>

<br>

<jstl:if test="${!last }">
	<a href="dashboard/displayVideogames.do?page=${param.page+1}"><spring:message code="dashboard.videogames.next"/></a>
</jstl:if>

<jstl:if test="${param.page>0}">
<div style="margin-left: 5em">
	<a href="dashboard/displayVideogames.do?page=${param.page-1}"><spring:message code="dashboard.videogames.back"/></a>
</div>
</jstl:if>



