<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ page import="java.text.DecimalFormat" %>


<script type="text/javascript">
function showTables(divName) {
	   
	var systems = ["statistics", "getTeamsWithMoreScrims","getPlayersWithMoreForumMessages", "playersWithMoreTeams"];

	for (i = 0; i < systems.length; i++) { 
	    if(systems[i] == divName){
	    	 document.getElementById(divName).style.display = "block";
	    }else{
	    	 document.getElementById(systems[i]).style.display = "none";
	    }
	}
}

</script>
	<a href="dashboard/displayVideogames.do?page=0"><spring:message code="dashboard.videogames.goToVideogameDashboard"/></a>
<br>

<button onclick="showTables('statistics')"><spring:message code="dashboard.statistics" /></button>
<button onclick="showTables('getTeamsWithMoreScrims')"><spring:message code="dashboard.getTeamsWithMoreScrims" /></button>
<button onclick="showTables('getPlayersWithMoreForumMessages')"><spring:message code="dashboard.getPlayersWithMoreForumMessages" /></button>
<button onclick="showTables('playersWithMoreTeams')"><spring:message code="dashboard.playersWithMoreTeams" /></button>

<div id="statistics" style="display: block;"> 
<h2><spring:message code="dashboard.statistics" /></h2>

<ul>

	<li>	
		<b><spring:message code="dashboard.ratioOfPlayersWithForumMessages" />:</b>
		<jstl:out value="${ratioOfPlayersWithForumMessages}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.ratioOfCancelledScrims" />:</b>
		<jstl:out value="${ratioOfCancelledScrims}" />
	</li>
</ul>

<table> 
	<tr style="background-color: yellow;">
		<th><spring:message code="dashboard.table.property" /></th>
		<th><spring:message code="dashboard.table.average" /></th>
		<th><spring:message code="dashboard.table.sd" /></th>
		<th><spring:message code="dashboard.table.min" /></th>
		<th><spring:message code="dashboard.table.max" /></th>
		
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.getAvgMaxMinSdVideogamesPerManager" /></td>
		<td><jstl:out value="${getAvgMaxMinSdVideogamesPerManager[0]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdVideogamesPerManager[3]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdVideogamesPerManager[1]}" /></td>	
		<td><jstl:out value="${getAvgMaxMinSdVideogamesPerManager[2]}" /></td>
		
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.getAvgMaxMinSdSeasonsPerVideogame" /></td>
		<td><jstl:out value="${getAvgMaxMinSdSeasonsPerVideogame[0]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdSeasonsPerVideogame[3]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdSeasonsPerVideogame[1]}" /></td>	
		<td><jstl:out value="${getAvgMaxMinSdSeasonsPerVideogame[2]}" /></td>
		
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.getAvgMaxMinSdScrimsPerTeam" /></td>
		<td><jstl:out value="${getAvgMaxMinSdScrimsPerTeam[0]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdScrimsPerTeam[3]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdScrimsPerTeam[1]}" /></td>	
		<td><jstl:out value="${getAvgMaxMinSdScrimsPerTeam[2]}" /></td>
		
	</tr>
	
		<tr>
		<td><spring:message code="dashboard.getAvgMaxMinSdTeamsPerPlayer" /></td>
		<td><jstl:out value="${getAvgMaxMinSdTeamsPerPlayer[0]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdTeamsPerPlayer[3]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdTeamsPerPlayer[1]}" /></td>	
		<td><jstl:out value="${getAvgMaxMinSdTeamsPerPlayer[2]}" /></td>
		
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.getAvgMaxMinSdPlayersPerTeam" /></td>
		<td><jstl:out value="${getAvgMaxMinSdPlayersPerTeam[0]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdPlayersPerTeam[3]}" /></td>
		<td><jstl:out value="${getAvgMaxMinSdPlayersPerTeam[1]}" /></td>	
		<td><jstl:out value="${getAvgMaxMinSdPlayersPerTeam[2]}" /></td>
		
	</tr>
</table>
</div>

<div id="getTeamsWithMoreScrims" style="display: none;"> 
<h2><spring:message code="dashboard.getTeamsWithMoreScrims" /></h2>


<display:table id="row" name="getTeamsWithMoreScrims" requestURI="dashboard/display.do" pagesize="5">
	<display:column titleKey="dashboard.name" sortable="true" >
			<a  href="team/display.do?teamId=${row.id}">
				<spring:message text="${row.name }" />
			</a>
	</display:column></display:table>
</div>

<div id="getPlayersWithMoreForumMessages" style="display: none;"> 
<h2><spring:message code="dashboard.getPlayersWithMoreForumMessages" /></h2>

<display:table id="row" name="getPlayersWithMoreForumMessages" requestURI="dashboard/display.do" pagesize="5">
	<display:column titleKey="dashboard.name" sortable="true" >
			<a  href="player/display.do?playerId=${row.id}">
				<spring:message text="${row.name }" />
			</a>
	</display:column>
	<display:column property="surname" titleKey="dashboard.surname" sortable="true" />
	<display:column property="userAccount.username" titleKey="dashboard.username" sortable="true" />
</display:table>

</div>

<div id="playersWithMoreTeams" style="display: none;"> 

<h2><spring:message code="dashboard.playersWithMoreTeams" /></h2>

<display:table id="row" name="playersWithMoreTeams" requestURI="dashboard/display.do" pagesize="5">
	<display:column titleKey="dashboard.name" sortable="true" >
			<a  href="player/display.do?playerId=${row.id}">
				<spring:message text="${row.name }" />
			</a>
	</display:column>	
	<display:column property="surname" titleKey="dashboard.surname" sortable="true" />
	<display:column property="userAccount.username" titleKey="dashboard.username" sortable="true" />
</display:table>
</div>
