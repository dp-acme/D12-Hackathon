<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:set var="action" value="scrim/edit.do" />
<jstl:if test="${scrim.getId() == 0}">
	<jstl:set var="action" value="scrim/create.do" />
</jstl:if>
<jstl:if test="${scrim.getGuest() != null}">
	<jstl:set var="action" value="scrim/invite.do" />
</jstl:if>

<form:form modelAttribute="scrim" action="${action}">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	
	<jstl:if test="${scrim.getGuest() != null}">
		<form:hidden path="guest" />
		<spring:message code="scrim.edit.guest" /><jstl:out value="${scrim.getGuest().getName()}" />
	</jstl:if>	
	
	<acme:select items="${teams}" itemLabel="name" code="scrim.edit.creator" path="creator" required="required" />
	
	<jstl:if test="${scrim.getGuest() == null}">
		<jstl:choose>
			<jstl:when test="${cookie.language.value == 'es'}">
				<jstl:set value="spanishName" var="regionLabel" />
			</jstl:when>
			<jstl:otherwise>
				<jstl:set value="englishName" var="regionLabel" />
			</jstl:otherwise>
		</jstl:choose>
		<acme:select items="${regions}" itemLabel="${regionLabel}" code="scrim.edit.desiredRegion" path="desiredGuestRegion" required="required" />
	</jstl:if>
	
	<jstl:if test="${scrim.getId() == 0}">
		<acme:textbox code="scrim.edit.moment" path="moment" placeholderCode="scrim.edit.momentPlaceholder" />
		<jstl:if test="${not empty pastMoment}"><div class="error"><spring:message code="scrim.edit.pastMoment" /></div></jstl:if>
	</jstl:if>
	
	<acme:textbox code="scrim.edit.streamingLink" path="streamingLink" />
	
	<br />

	<acme:submit code="scrim.edit.save" name="save" />

</form:form>
