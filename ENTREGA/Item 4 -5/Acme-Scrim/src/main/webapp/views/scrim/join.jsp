<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:if test="${not empty noTeams && noTeams.equals('true')}">
	<spring:message code="scrim.join.noTeams" />
</jstl:if>

<jstl:if test="${empty noTeams}">

	<form:form modelAttribute="scrim" action="scrim/join.do">
		
		<form:hidden path="id" />
		<form:hidden path="version" />
		
		<spring:message code="scrim.join.desiredGuestRegion" /><acme:displayRegion region="${scrim.getDesiredGuestRegion()}" />
		<acme:select items="${teams}" itemLabel="name" code="scrim.join.guest" path="guest" required="required" />
		
		<br />
	
		<acme:submit code="scrim.join.save" name="save" />
		<acme:cancel code="scrim.join.cancel" url="scrim/display.do?scrimId=${scrim.getId()}" />
	
	</form:form>

</jstl:if>