<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@  page import="domain.Scrim" %> 

<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal" />

<script type="text/javascript">
	function like(creator, like) {
	    var elem;
	    if(creator == true)
	    	elem = document.getElementById("creatorBar"); 
	    else
	    	elem = document.getElementById("guestBar"); 
	    
	    if(like == false)
	    	elem.style.backgroundColor = "#9d0000";
	    else
	    	elem.style.backgroundColor = "#4CAF50";
	    
	    var width = 0;
	    var id = setInterval(frame, 10);
	    function frame() {
	        if (width >= 100) {
	            clearInterval(id);
	        } else {
	            width+=2; 
	            elem.style.width = width + '%'; 
	        }
	    }
	}
	
	function sendLike(like){
		window.location.replace("review/player/create.do?scrimId=${param.scrimId}&teamId=${teamToLike}&positive="+like);
	}
</script>

<spring:message var="formatDate" code="scrim.display.datePattern" />
<fmt:formatDate value="${scrim.getMoment()}" pattern="${formatDate}" />

<spring:message code="scrim.display.status" /><span class="${scrim.getStatus().getScrimStatus()}"><spring:message code="${scrim.getStatus().getScrimStatus()}" /></span>

<jstl:if test="${scrim.getCreatorTeamResult() != null && scrim.getCreatorTeamResult().getResult().equals('ILLOGICAL')
	 && scrim.getGuestTeamResult() != null && scrim.getGuestTeamResult().getResult().equals('ILLOGICAL')
	 && hasPremiumPlayerInTeam && scrim.getResolution() == null}">
	<acme:cancel code="scrim.display.resolution" url="resolution/player/create.do?scrimId=${scrim.getId()}" />
</jstl:if>

<jstl:if test="${scrim.getStatus().getScrimStatus().equals('PENDING') && scrim.getGuest() != null 
	&& scrim.getGuest().getLeader().getPlayer().getUserAccount().equals(principal)}">
	<acme:cancel code="scrim.display.accept" url="scrim/acceptInvitation.do?scrimId=${scrim.getId()}" />
	<acme:cancel code="scrim.display.reject" url="scrim/rejectInvitation.do?scrimId=${scrim.getId()}" />
</jstl:if>

<div style="clear: both;">
	<hr />
</div>

<!-- TEAMS INFORMATION -->

<div>
	
	<div style="display: inline-block; float: left; margin-left: 4%; text-align: left;">
	
		<!-- Creator like bar -->
	
		<div id="myProgress">
		  <div id="creatorBar"></div>
		</div>
		
		<jstl:choose> 
			<jstl:when test="${isGuestPlayer && creatorReviewValue == 'null' && scrim.getStatus().getScrimStatus().equals('FINISHED')}">
				<input name="submit" id="like" type="image" src="images/like.png" onclick="sendLike(true);" />
				<input name="submit" id="dislike" type="image" src="images/dislike.png" onclick="sendLike(false);" />
			</jstl:when>
		</jstl:choose>
		
		<jstl:choose>
			<jstl:when test="${creatorReviewValue == 'true'}">
				<script>
					like(true, true);
				</script>
			</jstl:when>
			<jstl:when test="${creatorReviewValue == 'false'}">
				<script>
					like(true, false);
				</script>
			</jstl:when>
		</jstl:choose>
		
		<!-- CREATOR TEAM INFORMATION -->
		
		<h2>
			<spring:message code="scrim.display.creator" />
			<a href="team/display.do?teamId=${scrim.getCreator().getId()}"><jstl:out value="${scrim.getCreator().getName()}" /></a>
			<jstl:if test="${scrim.getCreatorTeamResult() != null}">
				<acme:displayResult result="${scrim.getCreatorTeamResult()}" />
			</jstl:if>
			<jstl:if test="${scrim.getStatus().getScrimStatus().equals('FINISHED') && scrim.getCreatorTeamResult() == null && isCreatorPlayer}">
				<acme:cancel url="scrim/win.do?scrimId=${scrim.getId()}" code="scrim.display.winButton"/>
				<acme:cancel url="scrim/draw.do?scrimId=${scrim.getId()}" code="scrim.display.drawButton"/>
				<acme:cancel url="scrim/lose.do?scrimId=${scrim.getId()}" code="scrim.display.loseButton"/>
			</jstl:if>
		</h2>
		
		<jstl:if test="${!scrim.getStatus().getScrimStatus().equals('CLOSED')}">
			<jstl:forEach items="${creatorTeammates}" var="var" varStatus="varStatus">
			
				<div>
					<jstl:if test="${var.equals(scrim.getCreator().getLeader())}">
						<span class="tooltip">
							<a href="player/display.do?playerId=${var.getPlayer().getId()}"><b><jstl:out value="${var.getNickname()}" /></b></a>
							<span class="tooltiptext tooltip-right">
								<spring:message code="scrim.display.leader" />
							</span>
						</span>
					</jstl:if>
					<jstl:if test="${!var.equals(scrim.getCreator().getLeader())}">
						<a href="player/display.do?playerId=${var.getPlayer().getId()}"><jstl:out value="${var.getNickname()}" /></a>
					</jstl:if>
					
					<jstl:if test="${!reportedPlayers.contains(var.getPlayer()) && isGuestPlayer && scrim.getStatus().getScrimStatus().equals('FINISHED')}">
						<acme:cancel url="report/player/create.do?scrimId=${scrim.getId()}&playerId=${var.getPlayer().getId()}" code="scrim.display.report"/>
					</jstl:if>
				</div>
			
			</jstl:forEach>
		</jstl:if>
		
	</div>
	
	<div style="display: inline-block; float: right; margin-right: 20%; text-align: right;">
		
		<jstl:if test="${scrim.getStatus().getScrimStatus().equals('PENDING') && scrim.getGuest() == null}">
			<acme:cancel code="scrim.display.join" url="scrim/join.do?scrimId=${scrim.getId()}" />
		</jstl:if>
		
		<jstl:if test="${scrim.getGuest() != null}">
		
			<!-- Guest like bar -->
		
			<div id="myProgress">
			  <div id="guestBar"></div>
			</div>
			
			<jstl:choose> 
				<jstl:when test="${isCreatorPlayer && guestReviewValue == 'null' && scrim.getStatus().getScrimStatus().equals('FINISHED')}">
					<input name="submit" id="like" type="image" src="images/like.png" onclick="sendLike(true);"/>
					<input name="submit" id="dislike" type="image" src="images/dislike.png" onclick="sendLike(false);"/>
				</jstl:when>
			</jstl:choose>
			
		<jstl:choose> 
			<jstl:when test="${guestReviewValue == 'true'}">
				<script>
					like(false, true);
				</script>
			</jstl:when>
			<jstl:when test="${guestReviewValue == 'false'}">
				<script>
					like(false, false);
				</script>
			</jstl:when>
		</jstl:choose>
			
			<!-- GUEST TEAM INFORMATION -->
			
			<h2>
				<jstl:if test="${scrim.getStatus().getScrimStatus().equals('FINISHED') && scrim.getGuestTeamResult() == null && isGuestPlayer}">
					<acme:cancel url="scrim/lose.do?scrimId=${scrim.getId()}" code="scrim.display.loseButton"/>
					<acme:cancel url="scrim/draw.do?scrimId=${scrim.getId()}" code="scrim.display.drawButton"/>
					<acme:cancel url="scrim/win.do?scrimId=${scrim.getId()}" code="scrim.display.winButton"/>
				</jstl:if>
				<jstl:if test="${scrim.getGuestTeamResult() != null}">
					<acme:displayResult result="${scrim.getGuestTeamResult()}" />
				</jstl:if>
				<a href="team/display.do?teamId=${scrim.getGuest().getId()}"><jstl:out value="${scrim.getGuest().getName()}" /></a> 
				<spring:message code="scrim.display.guest" />
			</h2>
			
			<jstl:if test="${!scrim.getStatus().getScrimStatus().equals('CLOSED')}">
				<jstl:forEach items="${guestTeammates}" var="var" varStatus="varStatus">
				
					<div>
				
						<jstl:if test="${!reportedPlayers.contains(var.getPlayer()) && isCreatorPlayer && scrim.getStatus().getScrimStatus().equals('FINISHED')}">
							<acme:cancel url="report/player/create.do?scrimId=${scrim.getId()}&playerId=${var.getPlayer().getId()}" code="scrim.display.report"/>
						</jstl:if>
						
						<jstl:if test="${var.equals(scrim.getGuest().getLeader())}">
							<span class="tooltip">
								<a href="player/display.do?playerId=${var.getPlayer().getId()}"><b><jstl:out value="${var.getNickname()}" /></b></a>
								<span class="tooltiptext tooltip-left">
									<spring:message code="scrim.display.leader" />
								</span>
							</span>
						</jstl:if>
						
						<jstl:if test="${!var.equals(scrim.getGuest().getLeader())}">
							<a href="player/display.do?playerId=${var.getPlayer().getId()}"><jstl:out value="${var.getNickname()}" /></a>
						</jstl:if>
						
					</div>
				
				</jstl:forEach>
			</jstl:if>
			
		</jstl:if>

	</div>
</div>

<div style="clear: both;">
	<br />
	<hr />
</div>

<!-- Comments -->

<h3>
	<spring:message code="scrim.display.comments"/>:
</h3>
<jstl:forEach items="${comments}" var="item">
	<fieldset style="display: inline-block; border-radius: 5px 10px; background-color: rgb(173, 232, 255);">
		<b><jstl:out value="${item.creator.name}"/></b><br/>
		<jstl:out value="${item.comments}"/><br/><br/>
	</fieldset><br/>
</jstl:forEach> 

<br/>

<jstl:if test="${isScrimPlayer}">
	<form:form modelAttribute="comment" action="comment/player/create.do?scrimId=${param.scrimId}">
		<form:hidden path="id" />
		<form:hidden path="version" />

		<acme:textarea code="scrim.comment.commentC" path="comments" required="required"/> <br/>
		<acme:submit code="scrim.comment.comment" name="save" /> 
	</form:form>
</jstl:if>

<!-- AUX -->

<style>
#myProgress {
    max-width: 100%;
    background-color: #cecdd1;
}

#guestBar, #creatorBar {
	width: 0%;
    height: 10px;
    background-color: #4CAF50;
    line-height: 30px; 
    color: white; 
}

</style>

