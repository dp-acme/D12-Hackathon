<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>


<fieldset>
	<legend>
		<b><spring:message code="videogame.display.datas" /></b>
	</legend>

	<!-- Name -->
	<h2>
		<jstl:out value="${videogame.getName()}" />
	</h2>

	<!-- Image -->
	<jstl:if test="${not empty videogame.getImage()}">
		<img src="${videogame.getImage()}" width="300" />
		<br />
		<br />
	</jstl:if>
	
	<!-- Description -->
	<b><spring:message code="videogame.display.description" />:</b>
	<jstl:out value="${videogame.getDescription()}" />

	<br /><br />


	<security:authorize access="hasRole('MANAGER')">
		<spring:url var="urlListSeason" value="videogame/manager/list.do" />
		<spring:url var="urlListSeasons"
			value="season/manager/list.do?videogameId=${videogame.getId()}" />
	</security:authorize>

	<security:authentication property="principal" var="principal" />

	<jstl:if test="${principal != videogame.getManager().getUserAccount()}">
		<spring:url var="urlListSeason" value="videogame/list.do" />
		<spring:url var="urlListSeasons"
			value="season/list.do?videogameId=${videogame.getId()}" />
	</jstl:if>

	<!-- Forum -->
	<spring:url var="urlListForum" value="forum/list.do?videogameId=${videogame.getId()}" />
	<a href="${urlListForum}"><spring:message code="videogame.display.Forums" /></a> 
	
	<br /><br />
	
	<!-- Teams -->
	<spring:url var="urlListTeams" value="team/list.do?videogameId=${videogame.getId()}" />
	<a href="${urlListTeams}"><spring:message code="videogame.display.Teams" /></a> 
	<security:authorize access="hasRole('PLAYER')">
		<jstl:if test="${not empty gameAccount}">
			<br /><a href="team/create.do?videogameId=${videogame.getId()}"><spring:message code="videogame.display.createTeam" /></a>
		</jstl:if>
	</security:authorize>
	
	<br /><br />
	
	<!-- Seasons -->
	<a href="${urlListSeasons}"><spring:message code="videogame.display.Seasons" /></a>
	<security:authorize access="hasRole('PLAYER')">
		<jstl:if test="${not empty gameAccount && gameAccount.getTeams().size() != 0}">
			<br /><a href="scrim/create.do?videogameId=${videogame.getId()}"><spring:message code="videogame.display.createScrim" /></a>
		</jstl:if>
	</security:authorize>
	
	<br /><br />
	
	<!-- GameAccounts -->
	<spring:url var="urlListGameAccounts" value="gameAccount/list.do?videogameId=${videogame.getId()}" />
	<a href="${urlListGameAccounts}"><spring:message code="videogame.display.gameAccounts" /></a> 
	<security:authorize access="hasRole('PLAYER')">
		<jstl:if test="${empty gameAccount}">
			<br /><a href="gameAccount/create.do?videogameId=${videogame.getId()}"><spring:message code="videogame.display.createGameAccount" /></a>
		</jstl:if>
	</security:authorize>
	
	<br /><br />
	
</fieldset>