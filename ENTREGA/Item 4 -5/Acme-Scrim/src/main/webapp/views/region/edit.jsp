<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form modelAttribute="region" action="region/edit.do">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	
	<acme:textbox code="region.edit.englishName" path="englishName" required="required" onchange="refreshPreview();" />
	<jstl:if test="${not empty uniqueEnglishName}"><div class="error"><spring:message code="${uniqueEnglishName}" /></div></jstl:if>
	<acme:textbox code="region.edit.spanishName" path="spanishName" required="required" onchange="refreshPreview();" />
	<jstl:if test="${not empty uniqueSpanishName}"><div class="error"><spring:message code="${uniqueSpanishName}" /></div></jstl:if>
	<acme:textbox code="region.edit.abbreviation" path="abbreviation" required="required" onchange="refreshPreview();" />
	<jstl:if test="${not empty uniqueAbbreviation}"><div class="error"><spring:message code="${uniqueAbbreviation}" /></div></jstl:if>
	<acme:textbox type="color" code="region.edit.color" path="colorDisplay" required="required" onchange="refreshPreview();" />
	<jstl:if test="${not empty uniqueColor}"><div class="error"><spring:message code="${uniqueColor}" /></div></jstl:if>

	<br />

	<spring:message code="region.edit.preview" /><span style="font-size: 20px;"><acme:displayRegion region="${region}" /></span>

	<br />
	<br />

	<acme:submit code="region.edit.save" name="save" />
	
	<jstl:if test="${region.getId() != 0}">
		<spring:message code="region.edit.delete.confirm" var="confirmDeleteMessage" />
		<acme:submit code="region.edit.delete" name="delete" onClickCallback="javascript: return confirm('${confirmDeleteMessage}');" />
	</jstl:if>
	
	<acme:cancel code="region.edit.cancel" url="configuration/display.do" />

</form:form>

<script>

function refreshPreview () {
	var englishName = document.getElementById('englishNameTooltip');
	if (englishName != null)
		englishName.innerHTML = document.getElementById('englishName').value;
	
	var spanishNameTooltip = document.getElementById('spanishNameTooltip');
	if (spanishNameTooltip != null)
		spanishNameTooltip.innerHTML = document.getElementById('spanishName').value;
	
	var abbreviation = document.getElementById('regionAbbreviationTooltip');
	if (abbreviation != null)
		abbreviation.innerHTML = document.getElementById('abbreviation').value;
	
	var colorDisplay = document.getElementById('regionTooltip');
	if (colorDisplay != null)
		colorDisplay.style.color = document.getElementById('colorDisplay').value;
}

</script>
