<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<fieldset style="border-radius: 5px 15px; display: inline-block; margin: 0 auto;
				 background-color: #F7F8E0">
	<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="socialIdentity.fieldset" /></h3>
	<hr>
	<form:form modelAttribute="socialIdentity" action="${requestURI}">
	
		<form:hidden path="id"/>
		
		<acme:select items="${socialNetworks}" itemLabel="name" code="socialIdentity.edit.socialNetwork" path="socialNetwork" required="required"/>
		<br/>
		
		<acme:textbox code="socialIdentity.edit.link" path="link" required="required"/>
		<br/>
		
		<acme:submit code="socialIdentity.edit.create" name="save" />
		
		<acme:cancel code="socialIdentity.edit.cancel" url="${backURI}" />
		
	</form:form>
</fieldset>