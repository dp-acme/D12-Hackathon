<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<security:authorize access="hasRole('SPONSOR')">

	<!-- Abrimos el formulario de edicion/ creacion -->

	<form:form action="advertisement/sponsor/edit.do"
		modelAttribute="advertisement">
		<br />

		<form:hidden path="id" />
		<form:hidden path="version" />

		<fieldset>
			<legend>
				<h2>
					<b><spring:message code="advertisement.edit.advertisementDatas" />:</b>
				</h2>
			</legend>
			<!-- Indicamos los campos de formulario -->
			<br /> <b><acme:textbox code="advertisement.edit.image"
					path="image" required="required" /></b> <br /> <b><acme:textbox
					code="advertisement.edit.web" path="web" required="required" /></b> <br />

		</fieldset>
		<br />

		<fieldset>
			<legend>
				<h2>
					<b><spring:message code="advertisement.edit.creditCardDatas" />:</b>
				</h2>
			</legend>
			<br /> <b><acme:textbox code="advertisement.edit.holderName"
					path="creditCard.holderName" /></b> <br /> <b><acme:textbox
					code="advertisement.edit.brandName" path="creditCard.brandName"
					required="required" /></b> <br /> <b><acme:textbox
					code="advertisement.edit.creditCardNumber" path="creditCard.number"
					required="required" /></b> <br /> <b><acme:textbox type="number"
					code="advertisement.edit.expirationMonth"
					path="creditCard.expirationMonth"
					placeholderCode="advertisement.edit.expirationMonth.placeholder"
					required="required" /></b> <br /> <b><acme:textbox type="number"
					code="advertisement.edit.expirationYear"
					path="creditCard.expirationYear"
					placeholderCode="advertisement.edit.expirationYear.placeholder"
					required="required" /></b> <br /> <b><acme:textbox
					code="advertisement.edit.cvv" path="creditCard.cvv"
					placeholderCode="advertisement.edit.cvv.placeholder"
					required="required" /></b> <br />

		</fieldset>


		<jstl:if
			test="${creditCardMessage != null && creditCardMessage != ''}">
			<p class="error">
				<spring:message code="${creditCardMessage}" />
			</p>
		</jstl:if>

		<br />

		<!-- Botones del formulario -->
		<acme:submit name="save" code="advertisement.edit.save" />

		<acme:cancel url="advertisement/sponsor/list.do"
			code="advertisement.edit.cancel" />
	</form:form>

</security:authorize>
