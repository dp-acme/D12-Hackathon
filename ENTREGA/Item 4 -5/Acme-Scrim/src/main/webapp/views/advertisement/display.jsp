<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>


<security:authorize access="hasRole('SPONSOR')">
	<a
		href="advertisement/sponsor/edit.do?advertisementId=${advertisement.getId()}">
		<spring:message code="advertisement.list.editAdvertisement" />
	</a>
</security:authorize>


<fieldset>
	<legend>
		<h2>
			<b><spring:message code="advertisement.display.infoAdvertisement" />:</b>
		</h2>
	</legend>
	<!-- Image -->
	<b><spring:message code="advertisement.display.image" />:</b> <img
		src="${advertisement.getImage()}" width="300" /> <br /> <br />

	<!-- Web -->
	<b><spring:message code="advertisement.display.web" />:</b>
	<jstl:out value="${advertisement.getWeb()}" />

	<br /> <br />

	<!-- Sponsor -->
	<b><spring:message code="advertisement.display.sponsor" />:</b>
	<jstl:out value="${advertisement.getSponsor().getName()}" />

	<br /> <br />

</fieldset>