<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="org.springframework.web.util.HtmlUtils" %> 
<%@page import="java.util.*" %> 
<%@page import="domain.Player" %> 
<%@page import="domain.Report" %> 

<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<script>
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }
 

function showTables(divName) {
	   
	var systems = ["reports", "resolutions"];

	for (i = 0; i < systems.length; i++) { 
	    if(systems[i] == divName){
	    	 document.getElementById(divName).style.display = "block";
	    }else{
	    	 document.getElementById(systems[i]).style.display = "none";
	    }
	}
}
 
</script>

<jstl:if test="${resolved}">
	<button onclick="showTables('reports')"><spring:message code="referee.dashboard.reports" /></button>
	<script>alert("asdasdsd2");</script>
</jstl:if>

<button onclick="showTables('reports')"><spring:message code="referee.dashboard.reports" /></button>
<button onclick="showTables('resolutions')"><spring:message code="referee.dashboard.resolution" /></button>
<br/><br/>
<div id ="reports" style="display: block;">
<b><spring:message code="referee.dashboard.rplayers" /></b>

<display:table id="row" name="playersReported" requestURI="referee/dashboard.do" pagesize="5">
	<jstl:set var="rnum" value="0" />
	<jstl:forEach  var="rep" items="${areports}">
		<jstl:if test="${rep.player eq row}">
				<jstl:set var="rnum" value="${rnum + 1}" />
		</jstl:if>
	</jstl:forEach>
	
	<display:column titleKey="referee.dashboard.rplayers" sortable="true"><a href ="referee/dashboard.do?playerId=${row.id}"><jstl:out value="${row.name}"/></a></display:column>
	<display:column titleKey="referee.dashboard.rnum" sortable="true"><jstl:out value="${rnum}"/></display:column>
	
	<jstl:set var="contains" value="false" />
		<jstl:forEach var="item" items="${baneables}">
 			 <jstl:if test="${item eq row}">
  			 <jstl:set var="contains" value="true" />
  		</jstl:if>
	</jstl:forEach>
	
	<display:column titleKey="referee.dashboard.ban">	
			<jstl:choose>
				<jstl:when test="${contains == true and row.userAccount.accountNonLocked == true}">
						<a href="referee/ban.do?playerId=${row.id}"><spring:message code="referee.dashboard.ban" /></a>
				</jstl:when>
				<jstl:when test="${row.userAccount.accountNonLocked == false}">
						<a href="referee/unban.do?playerId=${row.id}"><spring:message code="referee.dashboard.unban" /></a>
				</jstl:when>
			</jstl:choose>
		</display:column>
	
</display:table>

<jstl:if test="${param.playerId != null}">
<b><spring:message code="referee.dashboard.reports" /></b>

<display:table id="row" name="reports" requestURI="referee/dashboard.do" pagesize="5">
		<display:column titleKey="referee.dashboard.scrim"><a  href ="scrim/display.do?scrimId=${row.scrim.id}"><script>document.write(escapeHtml('${row.scrim.creator.name} vs ${row.scrim.guest.name}'))</script></a></display:column>
		<display:column property="comments" titleKey="referee.dashboard.comment"/>		
</display:table>
</jstl:if>	
</div>

<div id ="resolutions" style = "display: none;">
<b><spring:message code="referee.dashboard.resolutionP" /></b>

<display:table id="row" name="resolutionsP" requestURI="referee/dashboard.do" pagesize="5">
	<display:column titleKey="referee.dashboard.scrim"><a  href ="scrim/display.do?scrimId=${row.scrim.id}"><script>document.write(escapeHtml('${row.scrim.creator.name} vs ${row.scrim.guest.name}'))</script></a></display:column>
	<display:column property="comments" titleKey="referee.dashboard.comment"/>
	<display:column titleKey="referee.dashboard.resolve">
		<a href="resolution/referee/resolve.do?resolutionId=${row.id}&teamResult=1"><spring:message code="referee.dashboard.win" /></a>
		<a href="resolution/referee/resolve.do?resolutionId=${row.id}&teamResult=2"><spring:message code="referee.dashboard.draw" /></a>
		<a href="resolution/referee/resolve.do?resolutionId=${row.id}&teamResult=0"><spring:message code="referee.dashboard.lose" /></a>
	</display:column>
</display:table>
<br/><br/>
<b><spring:message code="referee.dashboard.resolutionS" /></b>

<display:table id="row" name="resolutionsS" requestURI="referee/dashboard.do" pagesize="5">
	<display:column titleKey="referee.dashboard.scrim"><a  href ="scrim/display.do?scrimId=${row.scrim.id}"><script>document.write(escapeHtml('${row.scrim.creator.name} vs ${row.scrim.guest.name}'))</script></a></display:column>
	<display:column property="comments" titleKey="referee.dashboard.comment"/>
	<display:column titleKey="referee.dashboard.resolved">${row.scrim.creatorTeamResult.result}</display:column>
</display:table>
</div>