<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:set value="RGB(255, 255, 190)" var="bgColor"/>

<div style="overflow: auto;">

	<script>
		function switchViews(){
			view = document.getElementById("view");
			edit = document.getElementById("edit");
			
			if(view.style.display == 'none'){
				view.style.display = 'inline-block';
				edit.style.display = 'none';
				
			} else{
				view.style.display = 'none';
				edit.style.display = 'inline-block';			
			}
		}
	</script>
	
	<fieldset id='view' style="border-radius: 5px 15px; display: inline-block; float: left; background-color: ${bgColor}">
		<b><spring:message code="configuration.banner" />:</b> <jstl:out value="${configuration.banner}" /><br>
		<b><spring:message code="configuration.tabooWords" />:</b>  <jstl:out value="${configuration.tabooWords}" /><br>
		<b><spring:message code="configuration.minimumReports" />:</b>  <jstl:out value="${configuration.minimumReports}" /><br>
		<b><spring:message code="configuration.adRatio" />:</b>  <jstl:out value="${configuration.adRatio}" /><br>
		<b><spring:message code="configuration.adImage" />:</b>  <jstl:out value="${configuration.adImage}" /><br>
		<b><spring:message code="configuration.finderCacheTime" />:</b>  <jstl:out value="${configuration.finderCacheTime}" /><br>
		<b><spring:message code="configuration.finderResults" />:</b>  <jstl:out value="${configuration.finderResults}" /><br>
		
		<hr>
		
		<button onClick="switchViews()">
			<spring:message code="configuration.edit" />
		</button>
	</fieldset>
	
	<fieldset id='edit' style="border-radius: 5px 15px; display: none; float: left; background-color: ${bgColor}">
		<form:form modelAttribute="configuration" action="configuration/edit.do">
			<form:hidden path="id" />
			<form:hidden path="version" />
			
			<acme:textbox code="configuration.banner" path="banner" />
			<acme:textbox code="configuration.tabooWords" path="tabooWords" />
			<acme:textbox type="number" code="configuration.minimumReports" path="minimumReports" />
			<acme:textbox type="number" code="configuration.adRatio" path="adRatio" />
			<acme:textbox code="configuration.adImage" path="adImage" />
			<acme:textbox type="number" code="configuration.finderCacheTime" path="finderCacheTime" />
			<acme:textbox type="number" code="configuration.finderResults" path="finderResults" />
		</form:form>
		
		<hr>
		
		<button onClick="switchViews()">
			<spring:message code="configuration.cancel" />
		</button>
		
		<button onClick="document.getElementById('configuration').submit()">
			<spring:message code="configuration.submit" />
		</button>
	</fieldset>
	
	<jstl:if test="${showEdit != null}">
		<script>
			switchViews();
		</script>
	</jstl:if>

</div>

<br />

<h3><spring:message code="configuration.regions" /></h3>

<display:table requestURI="configuration/display.do" name="regions" id="row" pagesize="5">

	<display:column titleKey="configuration.region.preview">
		<acme:displayRegion region="${row}" />
	</display:column>
	
	<display:column titleKey="configuration.region.englishName" property="englishName" />
	
	<display:column titleKey="configuration.region.spanishName" property="spanishName" />
	
	<display:column titleKey="configuration.region.abbreviation" property="abbreviation" />
	
	<display:column titleKey="configuration.region.colorDisplay">
		<span style="color: ${row.getColorDisplay()};"><jstl:out value="${row.getColorDisplay()}" /></span>
	</display:column>
	
	<display:column>
		<jstl:if test="${row.getEnglishName() != 'International'}">
			<a href="region/edit.do?regionId=${row.getId()}"><spring:message code="configuration.region.edit" /></a>
		</jstl:if>
	</display:column>

</display:table>

<acme:cancel url="region/create.do" code="configuration.region.create" />
