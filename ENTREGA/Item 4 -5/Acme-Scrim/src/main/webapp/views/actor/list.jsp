<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<fieldset style="border-radius: 5px 15px; display: inline-block; margin: 0 auto; width: 700px;
			 background-color: #F7F8E0">
<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><jstl:out value="${headTitle}"/></h3>
<hr>

<display:table name="actors" id="row" requestURI="${requestURI}" pagesize="5">
	
	<display:column property="userAccount.username" titleKey="actor.list.username" sortable="true" />
	
	<display:column property="name" titleKey="actor.list.name" sortable="true" />
	
	<display:column property="surname" titleKey="actor.list.surname" sortable="true" />
	
	<spring:message code="actor.list.profile" var="profileHeader" />
	<display:column title="" sortable="false">
			<spring:url value="${displayURI.concat(row.id)}"
				var="displayActor" />
			<a href="${displayActor}"> <jstl:out value="${profileHeader}" />
			</a>
	</display:column>
</display:table>

<acme:cancel url="" code="actor.list.back"/>
</fieldset>
