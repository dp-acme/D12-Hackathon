<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<fieldset style="border-radius: 5px 15px; display: inline-block; margin: 0 auto; width: 600px;
				 background-color: #F7F8E0">
	<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="socialNetwork.fieldset" /></h3>
	<hr>

<display:table name="socialNetworks" id="row" requestURI="${requestURI}"
	pagesize="5">


	<display:column property="name" titleKey="socialNetwork.list.name"/>
	<display:column	titleKey="socialNetwork.list.image" >
		<jstl:if test="${not empty row.image}">
			<img src="${row.image}" style = "max-height: 100px; max-width: 100px;"/>
		</jstl:if>
	</display:column>

	<%--Mostramos la columna de display --%>

	<security:authorize access="hasRole('ADMIN')">
		<display:column title="" sortable="false">
			<a href="socialNetwork/create.do?socialNetworkId=${row.getId()}"> <spring:message
					code="socialNetwork.list.edit" />
			</a>
		</display:column>
		
		<display:column title="" sortable="false">
			<a href="socialNetwork/delete.do?socialNetworkId=${row.getId()}"> <spring:message
					code="socialNetwork.list.delete" />
			</a>
		</display:column>
	</security:authorize>

</display:table>

<a href="socialNetwork/create.do"> <spring:message code="socialNetwork.list.create" /></a>

<br/>

<acme:cancel url="" code="socialNetwork.list.cancel"/>

<br />
</fieldset>
