<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form modelAttribute="gameAccount" action="gameAccount/edit.do">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	
	<jstl:if test="${gameAccount.getId() == 0}">
		<acme:select items="${videogames}" itemLabel="name" code="gameAccount.edit.videogame" path="videogame" required="required" />
	</jstl:if>
	
	<br />
	
	<jstl:choose>
		<jstl:when test="${cookie.language.value == 'es'}">
			<jstl:set value="spanishName" var="regionLabel" />
		</jstl:when>
		<jstl:otherwise>
			<jstl:set value="englishName" var="regionLabel" />
		</jstl:otherwise>
	</jstl:choose>
	<acme:select items="${regions}" itemLabel="${regionLabel}" code="gameAccount.edit.region" path="region" required="true" />
	<acme:textbox code="gameAccount.edit.nickname" path="nickname" required="required" />

	<br />

	<acme:submit code="gameAccount.edit.save" name="save" />
	
	<jstl:if test="${gameAccount.getId() != 0}">
		<spring:message code="gameAccount.edit.delete.confirm" var="confirmDeleteMessage" />
		<acme:submit code="gameAccount.edit.delete" name="delete" onClickCallback="javascript: return confirm('${confirmDeleteMessage}');" />
	</jstl:if>
	
</form:form>

