<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%@page import="org.springframework.web.util.HtmlUtils" %> 
<%@page import="java.util.Collection" %>
<%@page import="java.util.Iterator" %>
<%@page import="domain.Folder" %>
<%@page import="domain.FolderType" %>

<div style="overflow: auto;">

<spring:message code="folder.column.dateFormat" var="dateFormat"/>
<jstl:set value="${parentFolders}" var="f" />

<spring:message code="folder.delete.message" var="deleteFolderMessage" />
<spring:message code="message.delete.message" var="deleteMessageMessage" />

<% 
	@SuppressWarnings("unchecked")
	Collection<Folder> parentFolders = (Collection<Folder>)pageContext.getAttribute("f"); 
	HashMap<String, String> messages = new HashMap<String, String>();
	
	messages.put("deleteFolderMessage", (String)pageContext.getAttribute("deleteFolderMessage"));
	messages.put("deleteMessageMessage", (String)pageContext.getAttribute("deleteMessageMessage"));

	Integer selectedFolder = null;
	
	if(request.getParameter("folderId") != null){
		selectedFolder = Integer.valueOf(request.getParameter("folderId"));
	}
%>

<%! 
	String safeHTML(String str){ //Funci�n para hacer HTML seguro
		return HtmlUtils.htmlEscape(str);
	}
%>

<%!
	String toJavasScriptArray(Collection<Folder> folders, Boolean includeZero){
		String res = "[" + (includeZero? "0, " : "");
		
		Iterator<Folder> it = folders.iterator();
		while(it.hasNext()){
			res += it.next().getId();
			
			if(it.hasNext()){
				res += ", ";
			}
		}
		
		return res + "]";
	}

	void AddFolders(Collection<Folder> allowed, Collection<Folder> checking, Folder forbidden, Boolean includeSystem){
		for(Folder f : checking){
			if(!f.equals(forbidden) && (includeSystem || f.getType().getFolderType().equals(FolderType.USERBOX))){
				allowed.add(f);
				AddFolders(allowed, f.getChildren(), forbidden, includeSystem);
			}
		}
	}

	String foldersToMove(Folder folder, Collection<Folder> parentFolders){ //Obtener un array de JavaScript con los ids de las carpetas a las que se puede mover la carpeta dada
		String res = "[";
		ArrayList<Folder> allowed = new ArrayList<Folder>();
		
		if(folder.getParent() != null){ //Se puede mover a la ra�z si no est� en ella
			res += "0, ";
		}
		
		AddFolders(allowed, parentFolders, folder, false);
		allowed.remove(folder.getParent());
		
		for(int i = 0; i < allowed.size(); i ++){
			if(i != 0){
				res += ", ";
			}
			
			res += allowed.get(i).getId();
		}
		
		return res + "]";
	}
%>
	
<jstl:set value="${param.folderId}" var="currentFolder"/>

<% 
	Integer currentFolder = null;
	
	if(pageContext.getAttribute("currentFolder") != null){
		currentFolder = Integer.valueOf((String)pageContext.getAttribute("currentFolder"));
	}

	Collection<Folder> allFolders = new ArrayList<Folder>(); //Lista de todas 
	AddFolders(allFolders, parentFolders, null, false);
	
	Collection<Folder> allFoldersIncludingSystem = new ArrayList<Folder>();
	AddFolders(allFoldersIncludingSystem, parentFolders, null, true);	
%>

<%! 
	String allFoldersExceptCurrent(Collection<Folder> allFolders, Integer currentFolder){
		String res = "[";
		
		for(Folder f : allFolders){
			if(res.length() > 1){
				res += ", ";
			}
			
			if(f.getId() != currentFolder){
				res += f.getId();
			}
		}
		
		return res + "]";
	}

	String displayFolders(Collection<Folder> folders, Collection<Folder> parentFolders, Integer selectedFolder){
		String res = "";
		
		res += "<div style='margin-left: 0px; margin-right: 0px;'>";
		
		Iterator<Folder> it = folders.iterator();
		while(it.hasNext()){
			Folder f = it.next();
			
			res += "<img id='arrow_folder_" + f.getId() + "' src='images/flechaDerecha.png' " + 
					   "width='12px' height='12px' style='visibility: " +
					   (f.getChildren().size() > 0? "visible" : "hidden") + 
					   "; cursor: pointer;'" +
					   "onClick='switchChildrenVisibility(" + f.getId() + ")' />&nbsp;";
				
			
			res += "<div id='folder_" + f.getId() + "_data' style='display: inline-block;'>";
			
			if(selectedFolder != null && selectedFolder.equals(f.getId())){
				res += "<b>";
			}
			
			res += "<a id='folder_" + f.getId() + "_name' href='folder/list.do?folderId=" + f.getId() + "&page=1'>";
			res += safeHTML(f.getName());
			res += "<a>";

			if(selectedFolder != null && selectedFolder.equals(f.getId())){
				res += "</b>";
			}
			
			res += "</div>";
			
			if(f.getType().getFolderType().equals(FolderType.USERBOX)){ //Icono para carpetas creadas por el usuario
				res += "&nbsp;<img id='addChild_folder_" + f.getId() + "' src='images/crearCarpeta.png' " + 
						   "width='12px' height='12px'" + 
						   "style='cursor: pointer;'"+
						   "onClick='showFolderFormP(" + f.getId() + ")'/>";
						   
				res += "<form method='POST' style='display: inline-block;' id='editionForm_" + f.getId() + 
						"' action='folder/edit.do'>"; //Formulario de edici�n
				
				res += "<div id='edition_form_id_" + f.getId() + "' style='display: none;'>";
				res += "<input type='hidden' id='id' name='id'>";
				res += "</div>";	
						
				res += "<div id='edition_form_name_" + f.getId() + "' style='display: none;'>";
				res += "<input type='hidden' id='name' name='name'>";
				res += "</div>";				
				
				res += "&nbsp;<img id='edit_folder_" + f.getId() + "' src='images/editarCarpeta.png' " + 
						   "width='12px' height='12px'" + 
						   "style='cursor: pointer;'"+
						   "onClick=\"showEditionForm(" + f.getId() + ", '" + safeHTML(f.getName()) + "')\"/>";
						
				res += "<div id='cancel_edit_folder_div_" + f.getId() + "' style='display: none;'>"; //Div auxiliar para ocultar el espacio
				res += "&nbsp;<img id='cancel_edit_folder_" + f.getId() + "' src='images/cancelarEdicionCarpeta.png' " + 
						   "width='12px' height='12px'" + 
						   "style='cursor: pointer;'"+
						   "onClick=\"hideEditionForm(" + f.getId() + ", '" + safeHTML(f.getName()) + "')\"/>";
				res += "</div>";
				res += "</form>";
						   
				res += "&nbsp;<img id='delete_folder_" + f.getId() + "' src='images/borrarCarpeta.png' " + 
						   "width='12px' height='12px'" + 
						   "style='cursor: pointer;'"+
						   "onClick='deleteFolder(" + f.getId() + ")'/>";
						   
				res += "&nbsp;<img id='move_folder_" + f.getId() + "' src='images/moverCarpeta.png' " + 
						   "width='12px' height='12px'" + 
						   "style='cursor: pointer;'"+
						   "onClick='showMoveButtons(" + f.getId() + ", " + foldersToMove(f, parentFolders) + ")'/>";
			}
			
			res += "&nbsp;<img id='move_folder_to_" + f.getId() + "' src='images/moverCarpeta2.png' " + 
					   "width='12px' height='12px'" + 
					   "style='cursor: pointer; display: none;'"+
					   "onClick='showMoveButtons(" + f.getId() + ", " + foldersToMove(f, parentFolders) + ")'/>";
			
			if(f.getChildren().size() > 0){ //Poner los hijos
				res += "<div id='folder_" + f.getId() + "_children' style='display: none; margin-left: 20px; margin-bottom: 5px;'>";
				res += displayFolders(f.getChildren(), parentFolders, selectedFolder);
				res += "</div>";
			}
			
			if(it.hasNext()){				
				res += "<br id='imgPadding" + f.getId() + "' style='display: inherit;'>";
			}
		}
			
		res += "</div>";

		return res;
	}
%>

<!-- BARRA DE CARPETAS -->

<script>
	var allFolders = <%= toJavasScriptArray(allFoldersIncludingSystem, false) %>
	var allFoldersAndRoot = <%= toJavasScriptArray(allFoldersIncludingSystem, true) %>

	Array.prototype.contains = function(obj) {
	    var i = this.length;
	    while (i--) {
	        if (this[i] === obj) {
	            return true;
	        }
	    }
	    return false;
	};
	
	function hideFolderFormWP(){
		document.getElementById('createFolderWithoutParent').style.display = 'none';
		document.getElementById('imgPadding').style.display = 'none';
	}
	
	function hideFolderFormP(){
		document.getElementById('createFolderWithParent').style.display = 'none';
		document.getElementById('imgPadding').style.display = 'none';
	}	

	function hideMoveButtons(){
		for(var i = 0; i < allFolders.length; i += 1){
			document.getElementById("move_folder_to_" + allFolders[i]).style.display = "none";				
		}
	}
	
	function showFolderFormWP(){
		hideFolderFormP();
		hideMoveButtons();
		document.getElementById('createFolderWithoutParent').style.display = 'inline-block';
		document.getElementById('imgPadding').style.display = 'inherit';
	}
	
	function showFolderFormP(id){
		hideFolderFormWP();
		hideMoveButtons();
		document.getElementById('folderWithParent').action = 'folder/create.do?parentId=' + id;
		document.getElementById('createFolderWithParent').style.display = 'inline-block';
		document.getElementById('imgPadding').style.display = 'inherit';
	}
	
	function switchChildrenVisibility(id){
		element = document.getElementById('folder_' + id + "_children");	
		
		if(element.style.display == 'none'){
			element.style.display = 'inherit';
			element = document.getElementById('arrow_folder_' + id).src = "images/flechaAbajo.png";	
			document.getElementById('imgPadding' + id).style.display = 'none';

		} else{
			element.style.display = 'none';			
			element = document.getElementById('arrow_folder_' + id).src = "images/flechaDerecha.png";	
			document.getElementById('imgPadding' + id).style.display = 'inherit';
		}
	}
	
	function deleteFolder(id){
		if(confirm('<%= messages.get("deleteFolderMessage") %>')){
			relativeRedir("folder/delete.do?folderId=" + id);
		}
	}
	
	function deleteMessage(id){
		if(confirm('<%= messages.get("deleteMessageMessage") %>')){
			relativeRedir("message/delete.do?messageId=" + id);
		}
	}

	function showMoveButtons(id, allowedFolders){
		for(var i = 0; i < allFoldersAndRoot.length; i += 1){
			if(allowedFolders.contains(allFoldersAndRoot[i])){
				button = document.getElementById("move_folder_to_" + allFoldersAndRoot[i]);

				if(allFoldersAndRoot[i] != 0){
					button.setAttribute("onClick", "javascript: relativeRedir('folder/move.do?folderId=" + id + "&parentId=" + allFoldersAndRoot[i] + "')");
				
				} else{
					button.setAttribute("onClick", "javascript: relativeRedir('folder/move.do?folderId=" + id + "')");					
				}
				
				button.style.display = "inline";
			
			} else{
				document.getElementById("move_folder_to_" + allFoldersAndRoot[i]).style.display = "none";				
			}
		}
	}

	function showMoveMessageButtons(id, allowedFolders){
		for(var i = 0; i < allFolders.length; i += 1){
			if(allowedFolders.contains(allFolders[i])){
				button = document.getElementById("move_folder_to_" + allFolders[i]);

				button.setAttribute("onClick", "javascript: relativeRedir('message/move.do?messageId=" + id + "&folderId=" + allFolders[i] + "')");
				button.style.display = "inline";
			
			} else{
				document.getElementById("move_folder_to_" + allFolders[i]).style.display = "none";				
			}
		}
	}
	
	function submitEditionForm(id){
		name = document.getElementById("name_" + id).value;
		nameHidden = document.getElementById("edition_form_name_" + id).childNodes[0];
		idHidden = document.getElementById("edition_form_id_" + id).childNodes[0];
		
		nameHidden.value = name; //Copiar los datos al form
		idHidden.value = id;
				
		document.getElementById("editionForm_" + id).submit(); //Hacer submit
	}

	function showEditionForm(id, name){
		var input = document.createElement("input"); //Crear el input de texto
		input.setAttribute('type', "text");
		input.setAttribute('id', "name_" + id);
		input.setAttribute('value', name);
						
		document.getElementById("folder_" + id + "_data").appendChild(input); //A�adir el form a la vista
		document.getElementById("folder_" + id + "_name").style.display = "none"; //Ocultar el nombre de la carpeta
		document.getElementById("cancel_edit_folder_div_" + id).style.display = "inline-block"; //Mostrar el bot�n de cancelaci�n
		document.getElementById("edit_folder_" + id).src = "images/confirmarEdicionCarpeta.png"; //Cambiar el icono del bot�n de edici�n por confirmaci�n
		document.getElementById("edit_folder_" + id).setAttribute("onClick", "submitEditionForm(" + id + ")");
	}	
	
	function hideEditionForm(id, name){
		var input = document.getElementById("name_" + id);
		input.parentNode.removeChild(input); //Eliminar formulario
		document.getElementById("folder_" + id + "_name").style.display = "inline-block"; //Ocultar el nombre de la carpeta
		document.getElementById("cancel_edit_folder_div_" + id).style.display = "none"; //Ocultar el bot�n de cancelaci�n		
		document.getElementById("edit_folder_" + id).src = "images/editarCarpeta.png"; //Cambiar el icono del bot�n de confirmaci�n por edici�n
		document.getElementById("edit_folder_" + id).setAttribute("onClick", "showEditionForm(" + id + ", '" + name + "')");
	}
	
	function hideWriteMessageView(){
		document.getElementById('newMessageColumn').style.display = 'none';	
	}
	
	function hideWriteNotificationView(){
		if(document.getElementById('newNotificationColumn') != null){
			document.getElementById('newNotificationColumn').style.display = 'none';				
		}
	}
	
	function showWriteMessageView(){
		hideWriteNotificationView();
		element = document.getElementById('newMessageColumn');	
		
		if(element.style.display == 'none'){
			element.style.display = 'inherit';
			document.getElementById('messageColumn').style.display = 'none';

		} else{
			element.style.display = 'none';			
			document.getElementById('messageColumn').style.display = 'inline-block';
		}
	}
	
	function showWriteNotificationView(){
		hideWriteMessageView();
		element = document.getElementById('newNotificationColumn');	
		
		if(element.style.display == 'none'){
			element.style.display = 'inherit';
			document.getElementById('messageColumn').style.display = 'none';

		} else{
			element.style.display = 'none';			
			document.getElementById('messageColumn').style.display = 'inline-block';
		}
	}
</script>

<jstl:set value="RGB(255, 255, 190)" var="bgColor"/>

<fieldset style="border-radius: 5px 15px; display: inline-block; float: left;
				 background-color: ${bgColor}">
	<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="folder.column.folders" /></h3>
	<hr>

	<div id='folderTree' style="margin-left: 0px; margin-right: 15px;">
	
		<div id='createFolderWithoutParent' style="display: none;">
			<form:form action="folder/create.do" modelAttribute="folderWithoutParent">
				<acme:textbox path="name" code="folder.name" required="required"/>
				
				<div style="margin-top: 5px;">
					<acme:submit name="createWithoutParent" code="folder.create"/>
					<spring:message code="folder.cancel" var="cancelButton" />
					<input type="button" value="${cancelButton}" onclick="hideFolderFormWP()">
				</div>
			</form:form>
		</div>

		<div id='createFolderWithParent' style="display: none;">
			<form:form action="folder/create.do" modelAttribute="folderWithParent">
				<acme:textbox path="name" code="folder.name" required="required"/>
				
				<div style="margin-top: 5px;">
					<acme:submit name="createWithParent" code="folder.create"/>
					<spring:message code="folder.cancel" var="cancelButton" />
					<input type="button" value="${cancelButton}" onclick="hideFolderFormP()">
				</div>
			</form:form>		
		</div>
					
		<br id='imgPadding' style="display: none;">
								
		<img src="images/crearCarpeta.png" width="12px" height="12px"
		     style="cursor: pointer; margin-left: 15px; margin-bottom: 3px; margin-top: 10px;"
			 onclick="showFolderFormWP()">
			 
		<img src="images/moverCarpeta2.png" width="12px" height="12px"
			 id ="move_folder_to_0"
		     style="display: none; cursor: pointer; margin-bottom: 3px; margin-top: 10px;"
			 onclick="showFolderFormWP()">
		
		<%
			out.print(displayFolders(parentFolders, parentFolders, selectedFolder));
		%>
		
		<hr>
	</div>
	
	<button onclick="showWriteMessageView()">
	<spring:message code="folder.column.writeMessage" />
	</button>
	
	<security:authorize access="hasRole('ADMIN')">
		<br>
	
		<button onclick="showWriteNotificationView()">
		<spring:message code="folder.column.writeNotification" />
		</button>
	</security:authorize>
</fieldset>

<!-- BARRA DE MENSAJES -->

<jstl:set value="RGB(255, 255, 160)" var="bgColorEven"/>
<jstl:set value="RGB(255, 255, 120)" var="bgColorOdd"/>

<jstl:if test="${messages != null}">
	<fieldset style="border-radius: 5px 15px; display: inline-block; float: left;
					 background-color: ${bgColor}">
		
		<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="folder.column.messages" /></h3>
		<hr>
		
		<jstl:if test="${messages.size() > 0}">
			<fieldset style="border-radius: 5px 10px; display: inline-block; float: left;">
				<b><spring:message code="folder.column.page" /><jstl:out value=": " /></b>
				<jstl:set value="${pages}" var="p"/>
				
				
				<% 
					Integer pages = ((Double)pageContext.getAttribute("p")).intValue();
				
					for(Integer i = 1; i <= pages; i ++){
						if(i != 1){
							out.print(", ");
						}
						
						if(i == Integer.valueOf(request.getParameter("page"))){
							out.print(i);
							
						} else{
							String url = "folder/list.do?";
							
							url += "folderId=" + request.getParameter("folderId") + "&";
							url += "page=" + i + "&";
							
							if(request.getParameter("messageId") != null){
								url += "messageId=" + request.getParameter("messageId");								
							}
							
							out.print("<a href='" + url + "'>" + i + "</a>");							
						}
					}
				%>
			</fieldset>
			
			<div id='messages' style="margin-left: 0px;">
				<table style="display: inline-table; width: 100%; border-collapse: collapse; border: 0px;">
					<tr>
						<th style="text-align: center; border: 0px solid black;"><spring:message code="folder.column.moment" /></th>
						<th style="text-align: center; border: 0px solid black;"><spring:message code="folder.column.priority" /></th>
						<th style="text-align: center; border: 0px solid black;"><spring:message code="folder.column.subject" /></th>
						<th style="text-align: center; border: 0px solid black;"><spring:message code="folder.column.from" /></th>					
					</tr>			
									
						<jstl:forEach items="${messages}" var="m" varStatus="status">
							<jstl:if test="${status.index % 2 == 0}">
								<jstl:set value="${bgColorOdd}" var="rowBgColor"/>
							</jstl:if>
							
							<jstl:if test="${status.index % 2 == 1}">
								<jstl:set value="${bgColorEven}" var="rowBgColor"/>
							</jstl:if>
							
							<tr>
								<td style="border: 0px solid black;">
									<fieldset style="border-radius: 5px; background-color: ${rowBgColor};">
										<fmt:formatDate value="${m.moment}" pattern="${dateFormat}"/>
									</fieldset>
								</td>
								
								<td style="border: 0px solid black;">
									<fieldset style="border-radius: 5px; background-color: ${rowBgColor}">
										<jstl:out value="${m.priority.priority}" />
									</fieldset>
								</td>
								
								<td style="border: 0px solid black;">
									<fieldset style="border-radius: 5px; background-color: ${rowBgColor}">
										<jstl:out value="${m.subject}" />
									</fieldset>
								</td>
								
								<td style="border: 0px solid black;">
									<fieldset style="border-radius: 5px; background-color: ${rowBgColor}">
									<jstl:out value="${m.sender.name} (${m.sender.userAccount.username})" />
									</fieldset>
								</td>
								
								<td style="border: 0px solid black;">
									<fieldset style="border-radius: 5px; background-color: ${rowBgColor}">
										<img src="images/borrarCarpeta.png" width="20" height="20"
										style="cursor: pointer;" onclick="deleteMessage(${m.id})"/>
									</fieldset>
								</td>
								
								<td style="border: 0px solid black;">
									<fieldset style="border-radius: 5px; background-color: ${rowBgColor}">
										<img src="images/moverCarpeta.png" width="20" height="20"
										style="cursor: pointer;" onclick="showMoveMessageButtons(${m.id}, 
										<%= allFoldersExceptCurrent(allFoldersIncludingSystem, currentFolder) %>)"/>
									</fieldset>
								</td>
								
								<td style="border: 0px solid black;">
									<fieldset style="border-radius: 5px; background-color: ${rowBgColor}">
										<a href="folder/list.do?folderId=${param.folderId}&page=${param.page}&messageId=${m.id}">
											<spring:message code="folder.column.show" />
										</a>
									</fieldset>
								</td>
							</tr>
						</jstl:forEach>
				</table>
			</div>
		</jstl:if>
		
		<jstl:if test="${messages.size() == 0}">
			<spring:message code="message.list.empty"/>
		</jstl:if>
	</fieldset>
</jstl:if>

<!-- BARRA DE VISUALIZACI�N -->

<jstl:if test="${currentMessage != null}">
	<fieldset id='messageColumn' style="border-radius: 5px 15px; display: inline-block; float: left;
					 background-color: ${bgColor}">
		<div id='currentMessage' style="margin-left: 0px;">
			<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;">
				<jstl:out value="${currentMessage.subject}"/>
			</h3>
	
			<hr>
			
			<b><spring:message code="folder.column.moment" />:</b>
				<fmt:formatDate value="${currentMessage.moment}" pattern="${dateFormat}"/>
			<br>
			
			<b><spring:message code="folder.column.priority" />:</b>
			<jstl:out value="${currentMessage.priority.priority}"/>	
			<br>
			
			<b><spring:message code="folder.column.from" />:</b>
			<jstl:out value="${currentMessage.sender.name} (${currentMessage.sender.userAccount.username})"/>	
			<br>
					
			<b><spring:message code="folder.column.to" />:</b>
			<jstl:forEach items="${currentMessage.recipients}" var="r" varStatus="status">
				<jstl:if test="${!status.isFirst()}">
					<jstl:out value=", " />
				</jstl:if>
				
				<jstl:out value="${r.name} (${r.userAccount.username})" />			
			</jstl:forEach>
			
			<hr>
			
			<jstl:out value="${currentMessage.body}" />									
		</div>
	</fieldset>
</jstl:if>

<!-- BARRA DE REDACCI�N -->

<fieldset id='newMessageColumn' style="border-radius: 5px 15px; display: none; float: left;
					 background-color: ${bgColor}">
	<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="folder.column.newMessage" /></h3>
	<hr>
	
	<form:form action="message/create.do" modelAttribute="newMessage">
		<acme:textbox code="folder.column.subject" path="subject" required="required"/>
		<acme:textarea code="folder.column.body" path="body" required="required"/>
		
		
		<spring:message code="folder.column.priority.high" var="highPriority" />
		<spring:message code="folder.column.priority.neutral" var="neutralPriority" />
		<spring:message code="folder.column.priority.low" var="lowPriority" />
		<spring:message code="folder.column.priority" var="priorityLabel" />
		<form:label path="priority">
		${priorityLabel}
		</form:label>
		<form:select path="priority">
			<form:option value="HIGH" label="${highPriority}" />
			<form:option value="NEUTRAL" label="${neutralPriority}" />
			<form:option value="LOW" label="${lowPriority}" />
		</form:select>
		<form:errors path="priority" />

		<br>
		
		<spring:message code="folder.column.to" var="recipientsLabel" />
		<form:label path="recipients">
		${recipientsLabel}
		</form:label>
		<form:select path="recipients" multiple="multiple">
			<form:options items="${actors}" itemLabel="name" itemValue="id"/>
		</form:select>
		<form:errors path="recipients" />
		
		<br>
		
		<acme:submit name="send" code="folder.column.send"/>
	</form:form>
</fieldset>

<!-- BARRA DE REDACCI�N DE NOTIFICACIONES -->

<security:authorize access="hasRole('ADMIN')">
	<fieldset id='newNotificationColumn' style="border-radius: 5px 15px; display: none; float: left;
						 background-color: ${bgColor}">
		<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="folder.column.newNotification" /></h3>
		<hr>
		
		<form:form action="message/create.do?broadcastNotification=true" modelAttribute="newMessage">
			<acme:textbox code="folder.column.subject" path="subject" required="required"/>
			<acme:textarea code="folder.column.body" path="body" required="required"/>
			
			
			<spring:message code="folder.column.priority.high" var="highPriority" />
			<spring:message code="folder.column.priority.neutral" var="neutralPriority" />
			<spring:message code="folder.column.priority.low" var="lowPriority" />
			<spring:message code="folder.column.priority" var="priorityLabel" />
			<form:label path="priority">
			${priorityLabel}
			</form:label>
			<form:select path="priority">
				<form:option value="HIGH" label="${highPriority}" />
				<form:option value="NEUTRAL" label="${neutralPriority}" />
				<form:option value="LOW" label="${lowPriority}" />
			</form:select>
			<form:errors path="priority" />
					
			<br>
			
			<acme:submit name="send" code="folder.column.send"/>
		</form:form>
	</fieldset>
</security:authorize>

</div>

<!-- SCRIPTS FINALES -->

<jstl:if test="${shownForms != null && shownForms.equals(\"withoutParent\")}">
	<script>
		showFolderFormWP();
	</script>
</jstl:if>

<jstl:if test="${shownForms != null && shownForms.equals(\"withParent\")}">
	<jstl:set value="${param.parentId}" var="parentId" />

	<script>
		showFolderFormP(<%= (String)pageContext.getAttribute("parentId") %>);
	</script>
</jstl:if>

<jstl:if test="${not empty param.folderId}">
	<%
		Folder selected = null;
	
		for(Folder f : allFolders){ //Obtener carpeta con el id dado por par�metro
			if(f.getId() == selectedFolder){
				selected = f;
				break;
			}
		}
		
		if(selected != null){
			selected = selected.getParent();
			
			while(selected != null){ //Abrir la jerarqu�a
	%>
	
	<script>
		switchChildrenVisibility(<%= selected.getId() %>);
	</script>
	
	<%	
				selected = selected.getParent(); //Actualizar carpeta actual
			} 
		}
	%>
</jstl:if>