<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

	<form:form  action="forum/create.do" modelAttribute="forum">
		<form:hidden path="id" />
		<form:hidden path="version" />
		
		<acme:textbox code="forumCategory.name" path="name" required="required"/>
		<br>
		
		<acme:textbox code="forumCategory.description" path="description" required="required"/>
		<br>
		
		<form:label path="forumCategories">
			<spring:message code="forumCategory.categories" />
		</form:label>
		<form:select path="forumCategories" multiple="multiple" itemValue="id" >
			<form:options items="${forumCategories}" itemLabel="name" itemValue="id"/>
		</form:select>
		<form:errors path="forumCategories" />
	
		<br>
		
		<acme:submit name="save" code="forum.edit.save"/>
		<acme:cancel url="/forum/list.do?videogameId=${videogameId }" code="forum.edit.cancel"/>
		
		
</form:form>