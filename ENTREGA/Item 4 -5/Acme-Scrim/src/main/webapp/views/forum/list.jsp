<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%@page import="org.springframework.web.util.HtmlUtils" %> 
<%@page import="java.util.Collection" %>
<%@page import="java.util.Iterator" %>
<%@page import="domain.ForumCategory" %>

<jstl:set value="${rootForumCategory}" var="f" />
<jstl:set value="${isOwner}" var="isOwner" />
<jstl:if test="${not empty param.forumCategoryId}">
	<jstl:set value="${selected}" var="selected" />
</jstl:if>



<spring:message code="forumCategory.delete.message" var="deleteForumCategoryMessage" />

<% 
	@SuppressWarnings("unchecked")
	Collection<ForumCategory> rootForumCategory = (Collection<ForumCategory>)pageContext.getAttribute("f"); 
	HashMap<String, String> messages = new HashMap<String, String>();
	Boolean isOwner = (Boolean)pageContext.getAttribute("isOwner"); 
	
	messages.put("deleteForumCategoryMessage", (String)pageContext.getAttribute("deleteForumCategoryMessage"));
%>

<%! 
	String safeHTML(String str){ //Funci�n para hacer HTML seguro
		return HtmlUtils.htmlEscape(str);
	}
%>

<%!
	String toJavasScriptArray(Collection<ForumCategory> forumCategories){
		String res = "[0, ";
		
		Iterator<ForumCategory> it = forumCategories.iterator();
		while(it.hasNext()){
			res += it.next().getId();
			
			if(it.hasNext()){
				res += ", ";
			}
		}
		
		return res + "]";
	}

%>

<% 
	Collection<ForumCategory> allForumCategories = new ArrayList<ForumCategory>(); //Lista de todas 
%>

<%! 
	String displayForumCategories(Collection<ForumCategory> forumCategories,Boolean isOwner){
		String res = "";
		
		res += "<div style='margin-left: 0px; margin-right: 0px;'>";
		
		Iterator<ForumCategory> it = forumCategories.iterator();
		while(it.hasNext()){
			ForumCategory f = it.next();
			
			res += "<img id='arrow_forumCategory_" + f.getId() + "' src='images/flechaDerecha.png' " + 
					   "width='12px' height='12px' style='visibility: " +
					   (f.getChildrenCategories().size() > 0? "visible" : "hidden") + 
					   "; cursor: pointer;'" +
					   "onClick='switchChildrenVisibility(" + f.getId() + ")' />&nbsp;";
				
			
			res += "<div id='forumCategory_" + f.getId() + "_data' style='display: inline-block;'>";
			res += "<span id='forumCategory_" + f.getId() + "_name'>";
			res += "<a href='forum/list.do?videogameId="+f.getVideogame().getId()+"&forumCategoryId="+f.getId()+"'>";
			res += safeHTML(f.getName());
			res += "</a>";
			res += "</span>";
			res += "</div>";
			
			if(isOwner){
				
				res += "&nbsp;<img id='addChild_forumCategory_" + f.getId() + "' src='images/crearCarpeta.png' " + 
						   "width='12px' height='12px'" + 
						   "style='cursor: pointer;'"+
						   "onClick='showForumCategoryFormP(" + f.getId() + ")'/>";
				
			if(f.getParent()!=null){ 
					res += "<form method='POST' style='display: inline-block;' id='editionForm_" + f.getId() + 
							"' action='forum/editCategory.do'>"; //Formulario de edici�n
					
					res += "<div id='edition_form_id_" + f.getId() + "' style='display: none;'>";
					res += "<input type='hidden' id='id' name='id'>";
					res += "</div>";	
							
					res += "<div id='edition_form_name_" + f.getId() + "' style='display: none;'>";
					res += "<input type='hidden' id='name' name='name'>";
					res += "</div>";				
					
					res += "&nbsp;<img id='edit_forumCategory_" + f.getId() + "' src='images/editarCarpeta.png' " + 
							   "width='12px' height='12px'" + 
							   "style='cursor: pointer;'"+
							   "onClick=\"showEditionForm(" + f.getId() + ", '" + safeHTML(f.getName()) + "')\"/>";
							
					res += "<div id='cancel_edit_forumCategory_div_" + f.getId() + "' style='display: none;'>"; //Div auxiliar para ocultar el espacio
					res += "&nbsp;<img id='cancel_edit_forumCategory_" + f.getId() + "' src='images/cancelarEdicionCarpeta.png' " + 
							   "width='12px' height='12px'" + 
							   "style='cursor: pointer;'"+
							   "onClick=\"hideEditionForm(" + f.getId() + ", '" +safeHTML(f.getName()) + "')\"/>";
					res += "</div>";
					res += "</form>";
							   
					res += "&nbsp;<img id='delete_forumCategory_" + f.getId() + "' src='images/borrarCarpeta.png' " + 
							   "width='12px' height='12px'" + 
							   "style='cursor: pointer;'"+
							   "onClick='deleteForumCategory(" + f.getId() + ")'/>";
				}
			}
			 if(f.getChildrenCategories().size() > 0){ //Poner los hijos
				 res += "<div id='forumCategory_" + f.getId() + "_children' style='display: none; margin-left: 20px; margin-bottom: 5px;'>";
				res += displayForumCategories(f.getChildrenCategories(),isOwner);
				res += "</div>";
			} 
			
			if(it.hasNext()){				
				res += "<br id='imgPadding" + f.getId() + "' style='display: inherit;'>";
			}
		}
			
		res += "</div>";

		return res;
	}
%>

<!-- BARRA DE CARPETAS -->
<script>
	function switchChildrenVisibility(id){
		element = document.getElementById('forumCategory_' + id + "_children");	
		
		if(element.style.display == 'none'){
			element.style.display = 'inherit';
			element = document.getElementById('arrow_forumCategory_' + id).src = "images/flechaAbajo.png";	
			document.getElementById('imgPadding' + id).style.display = 'none';
	
		} else{
			element.style.display = 'none';			
			element = document.getElementById('arrow_forumCategory_' + id).src = "images/flechaDerecha.png";	
			document.getElementById('imgPadding' + id).style.display = 'inherit';
		}
	}
	
	function deleteForumCategory(id){
		if(confirm('<%= messages.get("deleteForumCategoryMessage") %>')){
			relativeRedir("forum/deleteCategory.do?forumCategoryId=" + id);
		}
	}
	
	function showForumCategoryFormP(id){
		document.getElementById('forumCategoryWithParent').action = 'forum/createCategory.do?parentId=' + id;
		document.getElementById('createForumCategoryWithParent').style.display = 'inline-block';
		document.getElementById('imgPadding').style.display = 'inherit';
	}
	
	function hideForumCategoryFormP(){
		document.getElementById('createForumCategoryWithParent').style.display = 'none';
		document.getElementById('imgPadding').style.display = 'none';
	}	
	
	function submitEditionForm(id){
		name = document.getElementById("name_" + id).value;
		nameHidden = document.getElementById("edition_form_name_" + id).childNodes[0];
		idHidden = document.getElementById("edition_form_id_" + id).childNodes[0];
		
		nameHidden.value = name; //Copiar los datos al form
		idHidden.value = id;
				
		document.getElementById("editionForm_" + id).submit(); //Hacer submit
	}
	
	function showEditionForm(id, name){
		var input = document.createElement("input"); //Crear el input de texto
		input.setAttribute('id', "name_" + id);
		input.setAttribute('value', name);
						
		document.getElementById("forumCategory_" + id + "_data").appendChild(input); //A�adir el form a la vista
		document.getElementById("forumCategory_" + id + "_name").style.display = "none"; //Ocultar el nombre de la carpeta
		document.getElementById("cancel_edit_forumCategory_div_" + id).style.display = "inline-block"; //Mostrar el bot�n de cancelaci�n
		document.getElementById("edit_forumCategory_" + id).src = "images/confirmarEdicionCarpeta.png"; //Cambiar el icono del bot�n de edici�n por confirmaci�n
		document.getElementById("edit_forumCategory_" + id).setAttribute("onClick", "submitEditionForm(" + id + ")");
	}	
	
	function hideEditionForm(id, name){
		var input = document.getElementById("name_" + id);
		input.parentNode.removeChild(input); //Eliminar formulario
		document.getElementById("forumCategory_" + id + "_name").style.display = "inline-block"; //Ocultar el nombre de la carpeta

		document.getElementById("cancel_edit_forumCategory_div_" + id).style.display = "none"; //Ocultar el bot�n de cancelaci�n		
		document.getElementById("edit_forumCategory_" + id).src = "images/editarCarpeta.png"; //Cambiar el icono del bot�n de confirmaci�n por edici�n
		document.getElementById("edit_forumCategory_" + id).setAttribute("onClick", "showEditionForm(" + id + ", '" + name + "')");
	}
	
</script>
<div>
<jstl:if test="${isOwner }" >		
	<a href="forum/create.do?videogameId=${videogameId }" style="display:inherit;float:left">
		<spring:message code="forumCategory.create.forum"/></a>
</jstl:if><br>
<div style="border-radius: 5px 15px; display: inline-block; margin-left: 15px;">
	<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="forumCategory.categories"/></h3>
	<hr>

	<div id='forumCategoryTree' style="margin-left: 0px; margin-right: 15px;">
		
		<div id='createForumCategoryWithParent' style="display: none;">
			<form:form action="forum/createCategory.do" modelAttribute="forumCategoryWithParent">
				<acme:textbox code="forumCategory.name" path="name" required="required"/>
				
				
				<div style="margin-top: 5px;">
					<acme:submit name="createWithParent" code="forumCategory.create"/>
					<spring:message code="forumCategory.cancel" var="cancelButton" />
					<input type="button" value="${cancelButton}" onclick="hideForumCategoryFormP()">
				</div>
			</form:form>		
		</div>
					
		<br id='imgPadding' style="display: none;">
		
		<%
			out.print(displayForumCategories(rootForumCategory,isOwner));
		%>
		
	</div>
</div>
	
		<spring:message code="forumCategory.edit" var="edit" />
		<spring:message code="forumCategory.delete" var="delete" />
		
		<div style="display: inline-block;margin-left: 25px;">
			<jstl:if test="${!forums.isEmpty() }">
				<display:table id="row" name="forums" requestURI="forum/list.do" pagesize="5" style="display: inline-block; float: left;">
				<display:column titleKey="forumCategory.name" sortable="true" >
					<a  href="forum/display.do?forumId=${row.id}">
						<spring:message text="${row.name }" />
					</a>
				</display:column>
				<display:column property="description" titleKey="forumCategory.description" sortable="true"/>
			<jstl:if test="${isOwner}">
				<display:column >
					<a  href="forum/edit.do?forumId=${row.id}">
						<spring:message code="forumCategory.edit" />
					</a>
				</display:column>
				<display:column >
					<a  href="forum/delete.do?forumId=${row.getId()}">
						<spring:message code="forumCategory.delete" />
					</a>
				</display:column>			
			</jstl:if>
			</display:table>
		</jstl:if>
	</div>
</div>

<!-- SCRIPTS FINALES -->
<jstl:if test="${not empty param.forumCategoryId}">
	<%
		ForumCategory selected = (ForumCategory)pageContext.getAttribute("selected"); 

		if(selected != null){
			selected = selected.getParent();
			
			while(selected != null){ //Abrir la jerarqu�a
	%>
	
	<script>
		switchChildrenVisibility(<%= selected.getId() %>);
	</script>
	
	<%	
				selected = selected.getParent(); //Actualizar carpeta actual
			} 
		}
	%>
</jstl:if>
