<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<fieldset style="border-radius: 5px 15px; display: inline-block; margin: 0 auto;
				 background-color: #F7F8E0">
	<h3 style="text-align: center; margin-bottom: 5px; margin-top: 5px;"><spring:message code="premiumSubscription.fieldset" /></h3>
	<hr>
	<form:form modelAttribute="premiumSubscription" action="${requestURI}">
		
		<acme:textbox code="premiumSubscription.edit.holderName" path="creditCard.holderName" required="required" />
		<br />
		
		<acme:textbox code="premiumSubscription.edit.brandName" path="creditCard.brandName" required="required" />
		<br />
	
		<acme:textbox code="premiumSubscription.edit.creditCardNumber" path="creditCard.number" required="required" />
		<br />
	
		<acme:textbox type="number" code="premiumSubscription.edit.expirationMonth" path="creditCard.expirationMonth" required="required" placeholderCode="premiumSubscription.edit.expirationMonth.placeholder" />
		<br />
		
		<acme:textbox type="number" code="premiumSubscription.edit.expirationYear" path="creditCard.expirationYear" required="required" placeholderCode="premiumSubscription.edit.expirationYear.placeholder" />
		<br />
		
		<acme:textbox code="premiumSubscription.edit.cvv" path="creditCard.cvv" required="required" placeholderCode="premiumSubscription.edit.cvv.placeholder" />
		<br />
		
		<jstl:if test="${creditCardMessage != null && creditCardMessage != ''}">
			<p class="error"><spring:message code="${creditCardMessage}"/></p>
		</jstl:if>
		<br />
		
		<!-- Botones del formulario -->
		<acme:submit name="save" code="premiumSubscription.edit.save" />
		<acme:cancel url="" code="premiumSubscription.edit.cancel" />
		
	</form:form>
</fieldset>