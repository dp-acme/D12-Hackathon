<%@ tag language="java" body-content="empty"%>

<%-- Taglibs --%>

<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%-- Attributes --%>

<%@ attribute name="region" required="true" type="domain.Region"%>
<%@ attribute name="tooltipTextClass" required="false"%>


<jstl:if test="${tooltipTextClass == null}">
	<jstl:set var="tooltipTextClass" value="tooltiptext tooltip-top" />
</jstl:if>

<%-- Definition --%>
<span id="regionTooltip" class="tooltip" style="color: ${region.getColorDisplay()};">
	<span id="regionAbbreviationTooltip"><jstl:out value="${region.getAbbreviation()}" /></span>
	<span class="${tooltipTextClass}">
		<jstl:choose>
			<jstl:when test="${cookie.language.value == 'es'}">
				<span id="spanishNameTooltip">${region.getSpanishName()}</span>
			</jstl:when>
			<jstl:otherwise>
				<span id="englishNameTooltip">${region.getEnglishName()}</span>
			</jstl:otherwise>
		</jstl:choose>
	</span>
</span>