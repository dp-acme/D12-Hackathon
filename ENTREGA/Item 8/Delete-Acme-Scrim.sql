start transaction;

use `Acme-Scrim`;

revoke all privileges on `Acme-Scrim`.* from 'acme-user'@'%';
revoke all privileges on `Acme-Scrim`.* from 'acme-manager'@'%';

drop user 'acme-user'@'%';
drop user 'acme-manager'@'%';

drop database `Acme-Scrim`;

commit;