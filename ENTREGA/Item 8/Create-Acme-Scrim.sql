﻿start transaction;

create database `acme-scrim`;

use `acme-scrim`;

create user 'acme-user'@'%' identified by password '*4F10007AADA9EE3DBB2CC36575DFC6F4FDE27577';
create user 'acme-manager'@'%' identified by password '*FDB8CD304EB2317D10C95D797A4BD7492560F55F';

grant select, insert, update, delete on `acme-scrim`.* to 'acme-user'@'%';

grant select, insert, update, delete, create, drop, references, index, alter, 
        create temporary tables, lock tables, create view, create routine, 
        alter routine, execute, trigger, show view
    on `acme-scrim`.* to 'acme-manager'@'%';

-- MySQL dump 10.13  Distrib 5.5.29, for Win64 (x86)
--
-- Host: localhost    Database: acme-scrim
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor_team`
--

DROP TABLE IF EXISTS `actor_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actor_team` (
  `Actor_id` int(11) NOT NULL,
  `teamsFollowed_id` int(11) NOT NULL,
  UNIQUE KEY `UK_368t0cvvdnkd8j70pmeq3bqw3` (`teamsFollowed_id`),
  CONSTRAINT `FK_368t0cvvdnkd8j70pmeq3bqw3` FOREIGN KEY (`teamsFollowed_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AdministratorUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  CONSTRAINT `FK_idt4b4u259p6vs4pyr9lax4eg` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (5488,0,'admin@admin.admin','admin','adminSurname',5464);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advertisement`
--

DROP TABLE IF EXISTS `advertisement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `brandName` varchar(255) DEFAULT NULL,
  `cvv` int(11) DEFAULT NULL,
  `expirationMonth` int(11) DEFAULT NULL,
  `expirationYear` int(11) DEFAULT NULL,
  `holderName` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `sponsor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dr486rxf9c2nw6qrxjijijaya` (`sponsor_id`),
  CONSTRAINT `FK_dr486rxf9c2nw6qrxjijijaya` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `scrim_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rqb1hyr2sw3xgojh4lutrler` (`creator_id`),
  KEY `FK_m0kruwirsnd4dbyjrubidhjl8` (`scrim_id`),
  CONSTRAINT `FK_m0kruwirsnd4dbyjrubidhjl8` FOREIGN KEY (`scrim_id`) REFERENCES `scrim` (`id`),
  CONSTRAINT `FK_rqb1hyr2sw3xgojh4lutrler` FOREIGN KEY (`creator_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `adImage` varchar(255) DEFAULT NULL,
  `adRatio` double DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `finderCacheTime` int(11) DEFAULT NULL,
  `finderResults` int(11) DEFAULT NULL,
  `minimumReports` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES (5463,0,'https://i.imgur.com/BFUMlsc.jpg',50,'https://i.imgur.com/LxD75MA.png',1,5,1);
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuration_taboowords`
--

DROP TABLE IF EXISTS `configuration_taboowords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration_taboowords` (
  `Configuration_id` int(11) NOT NULL,
  `tabooWords` varchar(255) DEFAULT NULL,
  KEY `FK_jflyxemijdnhx9v7hc8aodm6g` (`Configuration_id`),
  CONSTRAINT `FK_jflyxemijdnhx9v7hc8aodm6g` FOREIGN KEY (`Configuration_id`) REFERENCES `configuration` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration_taboowords`
--

LOCK TABLES `configuration_taboowords` WRITE;
/*!40000 ALTER TABLE `configuration_taboowords` DISABLE KEYS */;
INSERT INTO `configuration_taboowords` VALUES (5463,'polla'),(5463,'sex'),(5463,'sexo'),(5463,'penis'),(5463,'cialis'),(5463,'hack'),(5463,'script'),(5463,'eloboost');
/*!40000 ALTER TABLE `configuration_taboowords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finder`
--

DROP TABLE IF EXISTS `finder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finder` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `dateEnd` datetime DEFAULT NULL,
  `dateStart` datetime DEFAULT NULL,
  `keyWord` varchar(255) DEFAULT NULL,
  `lastSaved` datetime DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `videogame_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7f0105dml03xclaeyvev9cdia` (`region_id`),
  KEY `FK_rmgwcnhmje8mm1kgkj5efsmwl` (`videogame_id`),
  CONSTRAINT `FK_rmgwcnhmje8mm1kgkj5efsmwl` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`),
  CONSTRAINT `FK_7f0105dml03xclaeyvev9cdia` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `finder_scrim`
--

DROP TABLE IF EXISTS `finder_scrim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finder_scrim` (
  `Finder_id` int(11) NOT NULL,
  `scrims_id` int(11) NOT NULL,
  KEY `FK_g4ap2huvqu3wfu0t2fa58evi6` (`scrims_id`),
  KEY `FK_gba0n62yqn87mu5yt90y4q2rc` (`Finder_id`),
  CONSTRAINT `FK_gba0n62yqn87mu5yt90y4q2rc` FOREIGN KEY (`Finder_id`) REFERENCES `finder` (`id`),
  CONSTRAINT `FK_g4ap2huvqu3wfu0t2fa58evi6` FOREIGN KEY (`scrims_id`) REFERENCES `scrim` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `folder`
--

DROP TABLE IF EXISTS `folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `folderType` varchar(255) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_e6lcmpm09goh6x4n16fbq5uka` (`parent_id`),
  CONSTRAINT `FK_e6lcmpm09goh6x4n16fbq5uka` FOREIGN KEY (`parent_id`) REFERENCES `folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder`
--

LOCK TABLES `folder` WRITE;
/*!40000 ALTER TABLE `folder` DISABLE KEYS */;
INSERT INTO `folder` VALUES (5575,0,'Inbox','INBOX',5488,NULL),(5576,0,'Outbox','OUTBOX',5488,NULL),(5577,0,'Notifications','NOTIFICATIONBOX',5488,NULL),(5578,0,'Trash','TRASHBOX',5488,NULL),(5579,0,'Spam','SPAMBOX',5488,NULL);
/*!40000 ALTER TABLE `folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder_message`
--

DROP TABLE IF EXISTS `folder_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder_message` (
  `Folder_id` int(11) NOT NULL,
  `messages_id` int(11) NOT NULL,
  KEY `FK_5nh3mwey9bw25ansh2thcbcdh` (`messages_id`),
  KEY `FK_dwna03p0i8so6ov91ouups81r` (`Folder_id`),
  CONSTRAINT `FK_dwna03p0i8so6ov91ouups81r` FOREIGN KEY (`Folder_id`) REFERENCES `folder` (`id`),
  CONSTRAINT `FK_5nh3mwey9bw25ansh2thcbcdh` FOREIGN KEY (`messages_id`) REFERENCES `message` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forum`
--

DROP TABLE IF EXISTS `forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forum_forumcategory`
--

DROP TABLE IF EXISTS `forum_forumcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum_forumcategory` (
  `Forum_id` int(11) NOT NULL,
  `forumCategories_id` int(11) NOT NULL,
  KEY `FK_60nr4j692fwhkywmwokb1itrl` (`forumCategories_id`),
  KEY `FK_kokog4rs8dqr06r4cx0o7lnxr` (`Forum_id`),
  CONSTRAINT `FK_kokog4rs8dqr06r4cx0o7lnxr` FOREIGN KEY (`Forum_id`) REFERENCES `forum` (`id`),
  CONSTRAINT `FK_60nr4j692fwhkywmwokb1itrl` FOREIGN KEY (`forumCategories_id`) REFERENCES `forumcategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forumcategory`
--

DROP TABLE IF EXISTS `forumcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forumcategory` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `videogame_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_mi6lsi2byuoxmhywf205cdjd7` (`name`),
  KEY `FK_luwbhm8skpe5ca4p5rncrn5it` (`parent_id`),
  KEY `FK_ea7y5bdgg67kxghplja2rs1le` (`videogame_id`),
  CONSTRAINT `FK_ea7y5bdgg67kxghplja2rs1le` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`),
  CONSTRAINT `FK_luwbhm8skpe5ca4p5rncrn5it` FOREIGN KEY (`parent_id`) REFERENCES `forumcategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forummessage`
--

DROP TABLE IF EXISTS `forummessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forummessage` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `body` varchar(255) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `actor_id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6hthpjwyc9i1behds6b5jxr9b` (`forum_id`),
  KEY `FK_34qn4xvyxsyqmn59dinbq2g0o` (`parent_id`),
  CONSTRAINT `FK_34qn4xvyxsyqmn59dinbq2g0o` FOREIGN KEY (`parent_id`) REFERENCES `forummessage` (`id`),
  CONSTRAINT `FK_6hthpjwyc9i1behds6b5jxr9b` FOREIGN KEY (`forum_id`) REFERENCES `forum` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gameaccount`
--

DROP TABLE IF EXISTS `gameaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameaccount` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `videogame_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_31tsu9ewe98mla4d1c5c5rf76` (`player_id`),
  KEY `FK_l9p667qxso7w2i2u7o1ybe409` (`region_id`),
  KEY `FK_tk5qhgefha4663jj09g40dyq2` (`videogame_id`),
  CONSTRAINT `FK_tk5qhgefha4663jj09g40dyq2` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`),
  CONSTRAINT `FK_31tsu9ewe98mla4d1c5c5rf76` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`),
  CONSTRAINT `FK_l9p667qxso7w2i2u7o1ybe409` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gamecategory`
--

DROP TABLE IF EXISTS `gamecategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamecategory` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sfr7x9y9at2hlew31p2b7xauw` (`parent_id`),
  CONSTRAINT `FK_sfr7x9y9at2hlew31p2b7xauw` FOREIGN KEY (`parent_id`) REFERENCES `gamecategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamecategory`
--

LOCK TABLES `gamecategory` WRITE;
/*!40000 ALTER TABLE `gamecategory` DISABLE KEYS */;
INSERT INTO `gamecategory` VALUES (5482,0,'VIDEOGAME',NULL);
/*!40000 ALTER TABLE `gamecategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamecategory_videogame`
--

DROP TABLE IF EXISTS `gamecategory_videogame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamecategory_videogame` (
  `GameCategory_id` int(11) NOT NULL,
  `videogames_id` int(11) NOT NULL,
  KEY `FK_k42u5qam14inqcnolbnpn7nja` (`videogames_id`),
  KEY `FK_4s2tr7s7hu7n47c7a239spmcy` (`GameCategory_id`),
  CONSTRAINT `FK_4s2tr7s7hu7n47c7a239spmcy` FOREIGN KEY (`GameCategory_id`) REFERENCES `gamecategory` (`id`),
  CONSTRAINT `FK_k42u5qam14inqcnolbnpn7nja` FOREIGN KEY (`videogames_id`) REFERENCES `videogame` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hibernate_sequences`
--

DROP TABLE IF EXISTS `hibernate_sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_next_hi_value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequences`
--

LOCK TABLES `hibernate_sequences` WRITE;
/*!40000 ALTER TABLE `hibernate_sequences` DISABLE KEYS */;
INSERT INTO `hibernate_sequences` VALUES ('DomainEntity',2);
/*!40000 ALTER TABLE `hibernate_sequences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitation`
--

DROP TABLE IF EXISTS `invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invitation` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `team_id` int(11) NOT NULL,
  `teammate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_sx5u548knvttm1et11tek4w98` (`moment`),
  KEY `FK_5cbniothyor3oy2f4lv680xkb` (`team_id`),
  KEY `FK_jw2d07bfi5lf8cn8awr1rh3fy` (`teammate_id`),
  CONSTRAINT `FK_jw2d07bfi5lf8cn8awr1rh3fy` FOREIGN KEY (`teammate_id`) REFERENCES `gameaccount` (`id`),
  CONSTRAINT `FK_5cbniothyor3oy2f4lv680xkb` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ManagerUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  CONSTRAINT `FK_84bmmxlq61tiaoc7dy7kdcghh` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `body` varchar(255) DEFAULT NULL,
  `isSpam` bit(1) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_6x5ou3k3m43529vy5p43fnbb4` (`moment`),
  KEY `FK_7t1ls63lqb52igs4ms20cf94t` (`folder_id`),
  CONSTRAINT `FK_7t1ls63lqb52igs4ms20cf94t` FOREIGN KEY (`folder_id`) REFERENCES `folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_actor`
--

DROP TABLE IF EXISTS `message_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_actor` (
  `Message_id` int(11) NOT NULL,
  `recipients_id` int(11) NOT NULL,
  KEY `FK_2340xdahcha0b5cyr6bxhq6ji` (`Message_id`),
  CONSTRAINT `FK_2340xdahcha0b5cyr6bxhq6ji` FOREIGN KEY (`Message_id`) REFERENCES `message` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `expirationDate` date DEFAULT NULL,
  `isReferee` bit(1) DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5np14bftvr4mpq8gjrrpcxjlc` (`player_id`),
  CONSTRAINT `FK_5np14bftvr4mpq8gjrrpcxjlc` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  `premiumSubscription_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PlayerUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  KEY `FK_jf4ev7hu15fuegd1hl7nfjwau` (`premiumSubscription_id`),
  CONSTRAINT `FK_tkebx5co6g69vs716a1t01mq7` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`),
  CONSTRAINT `FK_jf4ev7hu15fuegd1hl7nfjwau` FOREIGN KEY (`premiumSubscription_id`) REFERENCES `premiumsubscription` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `premiumsubscription`
--

DROP TABLE IF EXISTS `premiumsubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `premiumsubscription` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `brandName` varchar(255) DEFAULT NULL,
  `cvv` int(11) DEFAULT NULL,
  `expirationMonth` int(11) DEFAULT NULL,
  `expirationYear` int(11) DEFAULT NULL,
  `holderName` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_l2oqg11of7llcd1eh5gss99n7` (`player_id`),
  CONSTRAINT `FK_l2oqg11of7llcd1eh5gss99n7` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `referee`
--

DROP TABLE IF EXISTS `referee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referee` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `RefereeUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  CONSTRAINT `FK_poaxqb6i26ly4p9nkegehjagr` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `abbreviation` varchar(255) DEFAULT NULL,
  `colorDisplay` varchar(255) DEFAULT NULL,
  `englishName` varchar(255) DEFAULT NULL,
  `spanishName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dxj3h3jo4yuvptpwv301sw2rv` (`abbreviation`),
  UNIQUE KEY `UK_r6xmydirdrff3uo301rhechjg` (`colorDisplay`),
  UNIQUE KEY `UK_cl4sklxbgoe0ydf69o2id5c6s` (`englishName`),
  UNIQUE KEY `UK_mg9vxrpln1hi5hag7f4dn733q` (`spanishName`),
  KEY `UK_lq1axdi2r5olblqyyxkp5w1it` (`englishName`,`spanishName`,`abbreviation`,`colorDisplay`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (5525,0,'INT','#808080','International','Internacional'),(5526,0,'EU','#0000ff','Europe','Europa'),(5527,0,'NA','#ff0000','North America','Norteamérica'),(5528,0,'CN','#cc6600','China','China'),(5529,0,'KR','#008000','Korea','Korea');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  `scrim_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_keewtfe4fyti2gun6l0f434h4` (`player_id`),
  KEY `FK_m705d66b6eu1mrodeg1xthhnk` (`scrim_id`),
  CONSTRAINT `FK_m705d66b6eu1mrodeg1xthhnk` FOREIGN KEY (`scrim_id`) REFERENCES `scrim` (`id`),
  CONSTRAINT `FK_keewtfe4fyti2gun6l0f434h4` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resolution`
--

DROP TABLE IF EXISTS `resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolution` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `attended` bit(1) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `referee_id` int(11) DEFAULT NULL,
  `scrim_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mmah9bnnlugv004q5xki6ueta` (`scrim_id`),
  KEY `FK_q85jk9coiedqk81kgmhgh5xob` (`referee_id`),
  CONSTRAINT `FK_mmah9bnnlugv004q5xki6ueta` FOREIGN KEY (`scrim_id`) REFERENCES `scrim` (`id`),
  CONSTRAINT `FK_q85jk9coiedqk81kgmhgh5xob` FOREIGN KEY (`referee_id`) REFERENCES `referee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `positive` bit(1) NOT NULL,
  `scrim_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sb2pk5qbx3wctnyaerfg8vnbw` (`scrim_id`),
  KEY `FK_oupp3rpdwyl9njhumoe7kvmj2` (`team_id`),
  CONSTRAINT `FK_oupp3rpdwyl9njhumoe7kvmj2` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  CONSTRAINT `FK_sb2pk5qbx3wctnyaerfg8vnbw` FOREIGN KEY (`scrim_id`) REFERENCES `scrim` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `scrim`
--

DROP TABLE IF EXISTS `scrim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scrim` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `confirmed` bit(1) DEFAULT NULL,
  `creatorTeamResult` varchar(255) DEFAULT NULL,
  `guestTeamResult` varchar(255) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `streamingLink` varchar(255) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `desiredGuestRegion_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `season_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_6erghudh9nd7qc9gi8t5u8p36` (`moment`),
  KEY `FK_hb76o09msqhvtvs3tm197mkjq` (`creator_id`),
  KEY `FK_brcr6vmdc1g6f8ycanr7i8a68` (`desiredGuestRegion_id`),
  KEY `FK_ntl9swke41goy4j04laym5f7l` (`guest_id`),
  KEY `FK_3f7j8yenhleq7w79qcxn6n9rb` (`season_id`),
  CONSTRAINT `FK_3f7j8yenhleq7w79qcxn6n9rb` FOREIGN KEY (`season_id`) REFERENCES `season` (`id`),
  CONSTRAINT `FK_brcr6vmdc1g6f8ycanr7i8a68` FOREIGN KEY (`desiredGuestRegion_id`) REFERENCES `region` (`id`),
  CONSTRAINT `FK_hb76o09msqhvtvs3tm197mkjq` FOREIGN KEY (`creator_id`) REFERENCES `team` (`id`),
  CONSTRAINT `FK_ntl9swke41goy4j04laym5f7l` FOREIGN KEY (`guest_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `season`
--

DROP TABLE IF EXISTS `season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `season` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `finished` bit(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `videogame_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_1svo5lg501nuo950c7omtj2f9` (`startDate`),
  KEY `FK_iqci6ggin1l06x25p1ss1b3mu` (`videogame_id`),
  CONSTRAINT `FK_iqci6ggin1l06x25p1ss1b3mu` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `socialidentity`
--

DROP TABLE IF EXISTS `socialidentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialidentity` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `actor_id` int(11) NOT NULL,
  `socialNetwork_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_q9cst2d9rfx34qm9ae2pmj4kj` (`socialNetwork_id`),
  CONSTRAINT `FK_q9cst2d9rfx34qm9ae2pmj4kj` FOREIGN KEY (`socialNetwork_id`) REFERENCES `socialnetwork` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `socialnetwork`
--

DROP TABLE IF EXISTS `socialnetwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialnetwork` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sponsor`
--

DROP TABLE IF EXISTS `sponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsor` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `SponsorUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  CONSTRAINT `FK_okfx8h0cn4eidh8ng563vowjc` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `leader_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `videogame_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_qp2b64w1p994jswu2fgfqa5yd` (`name`),
  KEY `FK_rpp7024f7vud2cjvyrj0w7eol` (`leader_id`),
  KEY `FK_ih61m8b3n92ppgh5u7xko78w7` (`region_id`),
  KEY `FK_jaxxb63et8h2iw6edraguerwh` (`videogame_id`),
  CONSTRAINT `FK_jaxxb63et8h2iw6edraguerwh` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`),
  CONSTRAINT `FK_ih61m8b3n92ppgh5u7xko78w7` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`),
  CONSTRAINT `FK_rpp7024f7vud2cjvyrj0w7eol` FOREIGN KEY (`leader_id`) REFERENCES `gameaccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `useraccount`
--

DROP TABLE IF EXISTS `useraccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useraccount` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `accountNonLocked` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_csivo9yqa08nrbkog71ycilh5` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useraccount`
--

LOCK TABLES `useraccount` WRITE;
/*!40000 ALTER TABLE `useraccount` DISABLE KEYS */;
INSERT INTO `useraccount` VALUES (5464,0,'','21232f297a57a5a743894a0e4a801fc3','admin');
/*!40000 ALTER TABLE `useraccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useraccount_authorities`
--

DROP TABLE IF EXISTS `useraccount_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useraccount_authorities` (
  `UserAccount_id` int(11) NOT NULL,
  `authority` varchar(255) DEFAULT NULL,
  KEY `FK_b63ua47r0u1m7ccc9lte2ui4r` (`UserAccount_id`),
  CONSTRAINT `FK_b63ua47r0u1m7ccc9lte2ui4r` FOREIGN KEY (`UserAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useraccount_authorities`
--

LOCK TABLES `useraccount_authorities` WRITE;
/*!40000 ALTER TABLE `useraccount_authorities` DISABLE KEYS */;
INSERT INTO `useraccount_authorities` VALUES (5464,'ADMIN'),(5465,'PLAYER'),(5466,'PLAYER'),(5467,'REFEREE'),(5468,'REFEREE'),(5469,'SPONSOR'),(5470,'SPONSOR'),(5471,'MANAGER'),(5472,'MANAGER'),(5473,'SPONSOR'),(5474,'PLAYER'),(5475,'PLAYER'),(5476,'PLAYER'),(5477,'PLAYER'),(5478,'PLAYER'),(5479,'PLAYER'),(5480,'PLAYER'),(5481,'PLAYER');
/*!40000 ALTER TABLE `useraccount_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videogame`
--

DROP TABLE IF EXISTS `videogame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videogame` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `manager_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_qhjs904m2m9q0vd5c4iyib029` (`name`),
  KEY `FK_gj45cwew2bqe15u7eilaotmk` (`manager_id`),
  CONSTRAINT `FK_gj45cwew2bqe15u7eilaotmk` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-08 17:51:20
commit;