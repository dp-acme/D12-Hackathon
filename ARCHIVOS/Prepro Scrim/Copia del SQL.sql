﻿-- MySQL dump 10.13  Distrib 5.5.29, for Win64 (x86)
--
-- Host: localhost    Database: acme-scrim
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor_team`
--

DROP TABLE IF EXISTS `actor_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actor_team` (
  `Actor_id` int(11) NOT NULL,
  `teamsFollowed_id` int(11) NOT NULL,
  UNIQUE KEY `UK_368t0cvvdnkd8j70pmeq3bqw3` (`teamsFollowed_id`),
  CONSTRAINT `FK_368t0cvvdnkd8j70pmeq3bqw3` FOREIGN KEY (`teamsFollowed_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actor_team`
--

LOCK TABLES `actor_team` WRITE;
/*!40000 ALTER TABLE `actor_team` DISABLE KEYS */;
/*!40000 ALTER TABLE `actor_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AdministratorUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  CONSTRAINT `FK_idt4b4u259p6vs4pyr9lax4eg` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (8696,0,'admin@admin.admin','admin','adminSurname',8672);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advertisement`
--

DROP TABLE IF EXISTS `advertisement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `brandName` varchar(255) DEFAULT NULL,
  `cvv` int(11) DEFAULT NULL,
  `expirationMonth` int(11) DEFAULT NULL,
  `expirationYear` int(11) DEFAULT NULL,
  `holderName` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `sponsor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dr486rxf9c2nw6qrxjijijaya` (`sponsor_id`),
  CONSTRAINT `FK_dr486rxf9c2nw6qrxjijijaya` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertisement`
--

LOCK TABLES `advertisement` WRITE;
/*!40000 ALTER TABLE `advertisement` DISABLE KEYS */;
INSERT INTO `advertisement` VALUES (8832,0,'VISA',132,11,20,'Sponsor1','4981561349465266','https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Coca-Cola_bottle_cap.svg/1200px-Coca-Cola_bottle_cap.svg.png','https://www.cocacola.es/es/home/',8712),(8833,0,'VISA',132,11,20,'Sponsor1','4981561349465266','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROLHH63LBcyOvWnpaanKZkoxYLCyu0VepU0IMieU_8gGO_th6QbA','http://www.kia.com/es/es/',8712),(8834,0,'VISA',132,11,20,'Sponsor1','4981561349465266','https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Logo_Corte_Ingl%C3%A9s.svg/1200px-Logo_Corte_Ingl%C3%A9s.svg.png','https://www.elcorteingles.es/',8712),(8835,0,'SANTANDER',304,2,21,'Sponsor2','4626566531600704','http://www.marketingnews.es/siteresources/files/666/44.jpg','https://www.mediamarkt.es/',8713),(8836,0,'VISA',112,9,21,'Sponsor3','4537888250900943','http://www.vegaehijos.com/imgs/img_prueba0214173204.jpg','http://www.vegaehijos.com/',8714),(8837,0,'SANTANDER',304,2,21,'Sponsor2','4626566531600704','https://i.pinimg.com/originals/88/8b/9a/888b9ab46e6af6a2d2ca063bda972d71.jpg','http://www.loreal.es/marcas/l%E2%80%99or%C3%A9al-luxe',8713);
/*!40000 ALTER TABLE `advertisement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `scrim_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rqb1hyr2sw3xgojh4lutrler` (`creator_id`),
  KEY `FK_m0kruwirsnd4dbyjrubidhjl8` (`scrim_id`),
  CONSTRAINT `FK_m0kruwirsnd4dbyjrubidhjl8` FOREIGN KEY (`scrim_id`) REFERENCES `scrim` (`id`),
  CONSTRAINT `FK_rqb1hyr2sw3xgojh4lutrler` FOREIGN KEY (`creator_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (8885,0,'Muy divertida la partida =)',8700,8879),(8886,0,'Gg, wp!!',8701,8879),(8887,0,'Vaya paliza...',8704,8880),(8888,0,'Al menos se intentó',8706,8880),(8889,0,'Lo importante es participar :)',8708,8880);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `adImage` varchar(255) DEFAULT NULL,
  `adRatio` double DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `finderCacheTime` int(11) DEFAULT NULL,
  `finderResults` int(11) DEFAULT NULL,
  `minimumReports` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES (8671,0,'https://i.imgur.com/BFUMlsc.jpg',50,'https://i.imgur.com/LxD75MA.png',1,5,1);
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuration_taboowords`
--

DROP TABLE IF EXISTS `configuration_taboowords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration_taboowords` (
  `Configuration_id` int(11) NOT NULL,
  `tabooWords` varchar(255) DEFAULT NULL,
  KEY `FK_jflyxemijdnhx9v7hc8aodm6g` (`Configuration_id`),
  CONSTRAINT `FK_jflyxemijdnhx9v7hc8aodm6g` FOREIGN KEY (`Configuration_id`) REFERENCES `configuration` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration_taboowords`
--

LOCK TABLES `configuration_taboowords` WRITE;
/*!40000 ALTER TABLE `configuration_taboowords` DISABLE KEYS */;
INSERT INTO `configuration_taboowords` VALUES (8671,'polla'),(8671,'sex'),(8671,'sexo'),(8671,'penis'),(8671,'cialis'),(8671,'hack'),(8671,'script'),(8671,'eloboost');
/*!40000 ALTER TABLE `configuration_taboowords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finder`
--

DROP TABLE IF EXISTS `finder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finder` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `dateEnd` datetime DEFAULT NULL,
  `dateStart` datetime DEFAULT NULL,
  `keyWord` varchar(255) DEFAULT NULL,
  `lastSaved` datetime DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `videogame_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7f0105dml03xclaeyvev9cdia` (`region_id`),
  KEY `FK_rmgwcnhmje8mm1kgkj5efsmwl` (`videogame_id`),
  CONSTRAINT `FK_rmgwcnhmje8mm1kgkj5efsmwl` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`),
  CONSTRAINT `FK_7f0105dml03xclaeyvev9cdia` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finder`
--

LOCK TABLES `finder` WRITE;
/*!40000 ALTER TABLE `finder` DISABLE KEYS */;
/*!40000 ALTER TABLE `finder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finder_scrim`
--

DROP TABLE IF EXISTS `finder_scrim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finder_scrim` (
  `Finder_id` int(11) NOT NULL,
  `scrims_id` int(11) NOT NULL,
  KEY `FK_g4ap2huvqu3wfu0t2fa58evi6` (`scrims_id`),
  KEY `FK_gba0n62yqn87mu5yt90y4q2rc` (`Finder_id`),
  CONSTRAINT `FK_gba0n62yqn87mu5yt90y4q2rc` FOREIGN KEY (`Finder_id`) REFERENCES `finder` (`id`),
  CONSTRAINT `FK_g4ap2huvqu3wfu0t2fa58evi6` FOREIGN KEY (`scrims_id`) REFERENCES `scrim` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finder_scrim`
--

LOCK TABLES `finder_scrim` WRITE;
/*!40000 ALTER TABLE `finder_scrim` DISABLE KEYS */;
/*!40000 ALTER TABLE `finder_scrim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder`
--

DROP TABLE IF EXISTS `folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `folderType` varchar(255) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_e6lcmpm09goh6x4n16fbq5uka` (`parent_id`),
  CONSTRAINT `FK_e6lcmpm09goh6x4n16fbq5uka` FOREIGN KEY (`parent_id`) REFERENCES `folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder`
--

LOCK TABLES `folder` WRITE;
/*!40000 ALTER TABLE `folder` DISABLE KEYS */;
INSERT INTO `folder` VALUES (8738,0,'Inbox','INBOX',8700,NULL),(8739,0,'Outbox','OUTBOX',8700,NULL),(8740,0,'Notifications','NOTIFICATIONBOX',8700,NULL),(8741,0,'Trash','TRASHBOX',8700,NULL),(8742,0,'Spam','SPAMBOX',8700,NULL),(8743,0,'Inbox','INBOX',8701,NULL),(8744,0,'Outbox','OUTBOX',8701,NULL),(8745,0,'Notifications','NOTIFICATIONBOX',8701,NULL),(8746,0,'Trash','TRASHBOX',8701,NULL),(8747,0,'Spam','SPAMBOX',8701,NULL),(8748,0,'Inbox','INBOX',8702,NULL),(8749,0,'Outbox','OUTBOX',8702,NULL),(8750,0,'Notifications','NOTIFICATIONBOX',8702,NULL),(8751,0,'Trash','TRASHBOX',8702,NULL),(8752,0,'Spam','SPAMBOX',8702,NULL),(8753,0,'Inbox','INBOX',8703,NULL),(8754,0,'Outbox','OUTBOX',8703,NULL),(8755,0,'Notifications','NOTIFICATIONBOX',8703,NULL),(8756,0,'Trash','TRASHBOX',8703,NULL),(8757,0,'Spam','SPAMBOX',8703,NULL),(8758,0,'Inbox','INBOX',8704,NULL),(8759,0,'Outbox','OUTBOX',8704,NULL),(8760,0,'Notifications','NOTIFICATIONBOX',8704,NULL),(8761,0,'Trash','TRASHBOX',8704,NULL),(8762,0,'Spam','SPAMBOX',8704,NULL),(8763,0,'Inbox','INBOX',8705,NULL),(8764,0,'Outbox','OUTBOX',8705,NULL),(8765,0,'Notifications','NOTIFICATIONBOX',8705,NULL),(8766,0,'Trash','TRASHBOX',8705,NULL),(8767,0,'Spam','SPAMBOX',8705,NULL),(8768,0,'Inbox','INBOX',8706,NULL),(8769,0,'Outbox','OUTBOX',8706,NULL),(8770,0,'Notifications','NOTIFICATIONBOX',8706,NULL),(8771,0,'Trash','TRASHBOX',8706,NULL),(8772,0,'Spam','SPAMBOX',8706,NULL),(8773,0,'Inbox','INBOX',8707,NULL),(8774,0,'Outbox','OUTBOX',8707,NULL),(8775,0,'Notifications','NOTIFICATIONBOX',8707,NULL),(8776,0,'Trash','TRASHBOX',8707,NULL),(8777,0,'Spam','SPAMBOX',8707,NULL),(8778,0,'Inbox','INBOX',8708,NULL),(8779,0,'Outbox','OUTBOX',8708,NULL),(8780,0,'Notifications','NOTIFICATIONBOX',8708,NULL),(8781,0,'Trash','TRASHBOX',8708,NULL),(8782,0,'Spam','SPAMBOX',8708,NULL),(8783,0,'Inbox','INBOX',8696,NULL),(8784,0,'Outbox','OUTBOX',8696,NULL),(8785,0,'Notifications','NOTIFICATIONBOX',8696,NULL),(8786,0,'Trash','TRASHBOX',8696,NULL),(8787,0,'Spam','SPAMBOX',8696,NULL),(8788,0,'Inbox','INBOX',8715,NULL),(8789,0,'Outbox','OUTBOX',8715,NULL),(8790,0,'Notifications','NOTIFICATIONBOX',8715,NULL),(8791,0,'Trash','TRASHBOX',8715,NULL),(8792,0,'Spam','SPAMBOX',8715,NULL),(8793,0,'Inbox','INBOX',8716,NULL),(8794,0,'Outbox','OUTBOX',8716,NULL),(8795,0,'Notifications','NOTIFICATIONBOX',8716,NULL),(8796,0,'Trash','TRASHBOX',8716,NULL),(8797,0,'Spam','SPAMBOX',8716,NULL),(8798,0,'Inbox','INBOX',8710,NULL),(8799,0,'Outbox','OUTBOX',8710,NULL),(8800,0,'Notifications','NOTIFICATIONBOX',8710,NULL),(8801,0,'Trash','TRASHBOX',8710,NULL),(8802,0,'Spam','SPAMBOX',8710,NULL),(8803,0,'Inbox','INBOX',8711,NULL),(8804,0,'Outbox','OUTBOX',8711,NULL),(8805,0,'Notifications','NOTIFICATIONBOX',8711,NULL),(8806,0,'Trash','TRASHBOX',8711,NULL),(8807,0,'Spam','SPAMBOX',8711,NULL),(8808,0,'Inbox','INBOX',8712,NULL),(8809,0,'Outbox','OUTBOX',8712,NULL),(8810,0,'Notifications','NOTIFICATIONBOX',8712,NULL),(8811,0,'Trash','TRASHBOX',8712,NULL),(8812,0,'Spam','SPAMBOX',8712,NULL),(8813,0,'Inbox','INBOX',8713,NULL),(8814,0,'Outbox','OUTBOX',8713,NULL),(8815,0,'Notifications','NOTIFICATIONBOX',8713,NULL),(8816,0,'Trash','TRASHBOX',8713,NULL),(8817,0,'Spam','SPAMBOX',8713,NULL),(8818,0,'Inbox','INBOX',8714,NULL),(8819,0,'Outbox','OUTBOX',8714,NULL),(8820,0,'Notifications','NOTIFICATIONBOX',8714,NULL),(8821,0,'Trash','TRASHBOX',8714,NULL),(8822,0,'Spam','SPAMBOX',8714,NULL),(8823,0,'Inbox','INBOX',8709,NULL),(8824,0,'Outbox','OUTBOX',8709,NULL),(8825,0,'Notifications','NOTIFICATIONBOX',8709,NULL),(8826,0,'Trash','TRASHBOX',8709,NULL),(8827,0,'Spam','SPAMBOX',8709,NULL),(8828,0,'test','USERBOX',8709,NULL),(8829,0,'test2','USERBOX',8709,8828);
/*!40000 ALTER TABLE `folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder_message`
--

DROP TABLE IF EXISTS `folder_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder_message` (
  `Folder_id` int(11) NOT NULL,
  `messages_id` int(11) NOT NULL,
  KEY `FK_5nh3mwey9bw25ansh2thcbcdh` (`messages_id`),
  KEY `FK_dwna03p0i8so6ov91ouups81r` (`Folder_id`),
  CONSTRAINT `FK_dwna03p0i8so6ov91ouups81r` FOREIGN KEY (`Folder_id`) REFERENCES `folder` (`id`),
  CONSTRAINT `FK_5nh3mwey9bw25ansh2thcbcdh` FOREIGN KEY (`messages_id`) REFERENCES `message` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder_message`
--

LOCK TABLES `folder_message` WRITE;
/*!40000 ALTER TABLE `folder_message` DISABLE KEYS */;
INSERT INTO `folder_message` VALUES (8738,8830),(8743,8831);
/*!40000 ALTER TABLE `folder_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum`
--

DROP TABLE IF EXISTS `forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum`
--

LOCK TABLES `forum` WRITE;
/*!40000 ALTER TABLE `forum` DISABLE KEYS */;
INSERT INTO `forum` VALUES (8726,0,'Forum con 2 categorias','Forum1'),(8727,0,'Forum con categoria FORUM','Forum2'),(8728,0,'Forum con una categoria','Forum3');
/*!40000 ALTER TABLE `forum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_forumcategory`
--

DROP TABLE IF EXISTS `forum_forumcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum_forumcategory` (
  `Forum_id` int(11) NOT NULL,
  `forumCategories_id` int(11) NOT NULL,
  KEY `FK_60nr4j692fwhkywmwokb1itrl` (`forumCategories_id`),
  KEY `FK_kokog4rs8dqr06r4cx0o7lnxr` (`Forum_id`),
  CONSTRAINT `FK_kokog4rs8dqr06r4cx0o7lnxr` FOREIGN KEY (`Forum_id`) REFERENCES `forum` (`id`),
  CONSTRAINT `FK_60nr4j692fwhkywmwokb1itrl` FOREIGN KEY (`forumCategories_id`) REFERENCES `forumcategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_forumcategory`
--

LOCK TABLES `forum_forumcategory` WRITE;
/*!40000 ALTER TABLE `forum_forumcategory` DISABLE KEYS */;
INSERT INTO `forum_forumcategory` VALUES (8726,8724),(8726,8725),(8727,8717),(8728,8724);
/*!40000 ALTER TABLE `forum_forumcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forumcategory`
--

DROP TABLE IF EXISTS `forumcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forumcategory` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `videogame_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_luwbhm8skpe5ca4p5rncrn5it` (`parent_id`),
  KEY `FK_ea7y5bdgg67kxghplja2rs1le` (`videogame_id`),
  CONSTRAINT `FK_ea7y5bdgg67kxghplja2rs1le` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`),
  CONSTRAINT `FK_luwbhm8skpe5ca4p5rncrn5it` FOREIGN KEY (`parent_id`) REFERENCES `forumcategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forumcategory`
--

LOCK TABLES `forumcategory` WRITE;
/*!40000 ALTER TABLE `forumcategory` DISABLE KEYS */;
INSERT INTO `forumcategory` VALUES (8717,1,'FORUM',NULL,8838),(8718,1,'FORUM',NULL,8839),(8719,1,'FORUM',NULL,8840),(8720,1,'FORUM',NULL,8841),(8721,1,'FORUM',NULL,8842),(8722,1,'FORUM',NULL,8843),(8723,1,'Hijo de categoría FORUM',8717,8838),(8724,1,'Hijo de categoría FORUM del videogame1',8717,8838),(8725,1,'Hijo de categoría forumCategoryChildren1 del videogame1',8723,8838);
/*!40000 ALTER TABLE `forumcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forummessage`
--

DROP TABLE IF EXISTS `forummessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forummessage` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `body` varchar(255) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `actor_id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6hthpjwyc9i1behds6b5jxr9b` (`forum_id`),
  KEY `FK_34qn4xvyxsyqmn59dinbq2g0o` (`parent_id`),
  CONSTRAINT `FK_34qn4xvyxsyqmn59dinbq2g0o` FOREIGN KEY (`parent_id`) REFERENCES `forummessage` (`id`),
  CONSTRAINT `FK_6hthpjwyc9i1behds6b5jxr9b` FOREIGN KEY (`forum_id`) REFERENCES `forum` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forummessage`
--

LOCK TABLES `forummessage` WRITE;
/*!40000 ALTER TABLE `forummessage` DISABLE KEYS */;
INSERT INTO `forummessage` VALUES (8729,0,'Message on forum1','2012-10-10 10:00:00',8701,8726,NULL),(8730,0,'Message on forum1 replying to forumMessage1','2012-10-10 10:00:00',8710,8726,8729),(8731,0,'Message on forum1 replying to forumMessage2','2012-10-10 10:00:00',8700,8726,8730),(8732,0,'Message on forum1 replying to forumMessage1','2012-10-10 10:00:00',8700,8726,8729);
/*!40000 ALTER TABLE `forummessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameaccount`
--

DROP TABLE IF EXISTS `gameaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameaccount` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `videogame_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_31tsu9ewe98mla4d1c5c5rf76` (`player_id`),
  KEY `FK_l9p667qxso7w2i2u7o1ybe409` (`region_id`),
  KEY `FK_tk5qhgefha4663jj09g40dyq2` (`videogame_id`),
  CONSTRAINT `FK_tk5qhgefha4663jj09g40dyq2` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`),
  CONSTRAINT `FK_31tsu9ewe98mla4d1c5c5rf76` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`),
  CONSTRAINT `FK_l9p667qxso7w2i2u7o1ybe409` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameaccount`
--

LOCK TABLES `gameaccount` WRITE;
/*!40000 ALTER TABLE `gameaccount` DISABLE KEYS */;
INSERT INTO `gameaccount` VALUES (8851,0,'videogame2Player1',8700,8735,8839),(8852,0,'videogame2Player2',8701,8733,8839),(8853,0,'videogame2Player3',8702,8734,8839),(8854,0,'videogame2Player4',8703,8736,8839),(8855,0,'videogame6Player3',8702,8737,8843),(8856,0,'videogame6Player4',8703,8733,8843),(8857,0,'videogame6Player5',8704,8734,8843),(8858,0,'videogame6Player6',8705,8733,8843),(8859,0,'videogame6Player7',8706,8733,8843),(8860,0,'videogame6Player8',8707,8735,8843);
/*!40000 ALTER TABLE `gameaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamecategory`
--

DROP TABLE IF EXISTS `gamecategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamecategory` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sfr7x9y9at2hlew31p2b7xauw` (`parent_id`),
  CONSTRAINT `FK_sfr7x9y9at2hlew31p2b7xauw` FOREIGN KEY (`parent_id`) REFERENCES `gamecategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamecategory`
--

LOCK TABLES `gamecategory` WRITE;
/*!40000 ALTER TABLE `gamecategory` DISABLE KEYS */;
INSERT INTO `gamecategory` VALUES (8690,0,'VIDEOGAME',NULL),(8691,0,'Deportes',8690),(8692,0,'Acción',8690),(8693,0,'Fútbol',8691),(8694,0,'Fórmula 1',8691),(8695,0,'Lucha',8692);
/*!40000 ALTER TABLE `gamecategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamecategory_videogame`
--

DROP TABLE IF EXISTS `gamecategory_videogame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamecategory_videogame` (
  `GameCategory_id` int(11) NOT NULL,
  `videogames_id` int(11) NOT NULL,
  KEY `FK_k42u5qam14inqcnolbnpn7nja` (`videogames_id`),
  KEY `FK_4s2tr7s7hu7n47c7a239spmcy` (`GameCategory_id`),
  CONSTRAINT `FK_4s2tr7s7hu7n47c7a239spmcy` FOREIGN KEY (`GameCategory_id`) REFERENCES `gamecategory` (`id`),
  CONSTRAINT `FK_k42u5qam14inqcnolbnpn7nja` FOREIGN KEY (`videogames_id`) REFERENCES `videogame` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamecategory_videogame`
--

LOCK TABLES `gamecategory_videogame` WRITE;
/*!40000 ALTER TABLE `gamecategory_videogame` DISABLE KEYS */;
INSERT INTO `gamecategory_videogame` VALUES (8690,8838),(8690,8839),(8690,8840),(8690,8841),(8690,8842),(8690,8843),(8691,8838),(8691,8839),(8691,8842),(8692,8840),(8692,8841),(8693,8839),(8694,8842),(8695,8840);
/*!40000 ALTER TABLE `gamecategory_videogame` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequences`
--

DROP TABLE IF EXISTS `hibernate_sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_next_hi_value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequences`
--

LOCK TABLES `hibernate_sequences` WRITE;
/*!40000 ALTER TABLE `hibernate_sequences` DISABLE KEYS */;
INSERT INTO `hibernate_sequences` VALUES ('DomainEntity',1);
/*!40000 ALTER TABLE `hibernate_sequences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitation`
--

DROP TABLE IF EXISTS `invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invitation` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `team_id` int(11) NOT NULL,
  `teammate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5cbniothyor3oy2f4lv680xkb` (`team_id`),
  KEY `FK_jw2d07bfi5lf8cn8awr1rh3fy` (`teammate_id`),
  CONSTRAINT `FK_jw2d07bfi5lf8cn8awr1rh3fy` FOREIGN KEY (`teammate_id`) REFERENCES `gameaccount` (`id`),
  CONSTRAINT `FK_5cbniothyor3oy2f4lv680xkb` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitation`
--

LOCK TABLES `invitation` WRITE;
/*!40000 ALTER TABLE `invitation` DISABLE KEYS */;
INSERT INTO `invitation` VALUES (8865,0,'Invitation Team1 - GameAccount 2','2012-10-10 10:00:00',8861,8852),(8866,0,'Invitation Team2 - GameAccount 4','2012-10-10 10:00:00',8862,8854),(8867,0,'Invitation Team3 - GameAccount 6','2012-10-10 10:00:00',8863,8856),(8868,0,'Invitation Team3 - GameAccount 7','2012-10-10 10:00:00',8863,8857),(8869,0,'Invitation Team4 - GameAccount 9','2012-10-10 10:00:00',8864,8859),(8870,0,'Invitation Team4 - GameAccount 10','2012-10-10 10:00:00',8864,8860);
/*!40000 ALTER TABLE `invitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ManagerUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  CONSTRAINT `FK_84bmmxlq61tiaoc7dy7kdcghh` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
INSERT INTO `manager` VALUES (8715,0,'manager1@manager.manager','manager1','manager1Surname',8679),(8716,0,'manager2@manager.manager','manager2','manager2Surname',8680);
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `body` varchar(255) DEFAULT NULL,
  `isSpam` bit(1) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7t1ls63lqb52igs4ms20cf94t` (`folder_id`),
  CONSTRAINT `FK_7t1ls63lqb52igs4ms20cf94t` FOREIGN KEY (`folder_id`) REFERENCES `folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (8830,0,'Message 1 body','\0','2012-10-10 10:00:00','NEUTRAL','Message 1 subject',8738,8696),(8831,0,'Message 2 body','\0','2013-10-10 10:00:00','NEUTRAL','Message 2 subject',8743,8696);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_actor`
--

DROP TABLE IF EXISTS `message_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_actor` (
  `Message_id` int(11) NOT NULL,
  `recipients_id` int(11) NOT NULL,
  KEY `FK_2340xdahcha0b5cyr6bxhq6ji` (`Message_id`),
  CONSTRAINT `FK_2340xdahcha0b5cyr6bxhq6ji` FOREIGN KEY (`Message_id`) REFERENCES `message` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_actor`
--

LOCK TABLES `message_actor` WRITE;
/*!40000 ALTER TABLE `message_actor` DISABLE KEYS */;
INSERT INTO `message_actor` VALUES (8830,8700),(8831,8701);
/*!40000 ALTER TABLE `message_actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `expirationDate` date DEFAULT NULL,
  `isReferee` bit(1) DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5np14bftvr4mpq8gjrrpcxjlc` (`player_id`),
  CONSTRAINT `FK_5np14bftvr4mpq8gjrrpcxjlc` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (8844,0,'2021-08-05','',8701),(8845,0,'2021-08-05','\0',8702);
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  `premiumSubscription_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PlayerUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  KEY `FK_jf4ev7hu15fuegd1hl7nfjwau` (`premiumSubscription_id`),
  CONSTRAINT `FK_tkebx5co6g69vs716a1t01mq7` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`),
  CONSTRAINT `FK_jf4ev7hu15fuegd1hl7nfjwau` FOREIGN KEY (`premiumSubscription_id`) REFERENCES `premiumsubscription` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
INSERT INTO `player` VALUES (8700,0,'player1@player.player','player1','player1Surname',8673,NULL),(8701,1,'player2@player.player','player2','player2Surname',8674,8846),(8702,1,'player3@player.player','player3','player3Surname',8682,8847),(8703,0,'player4@player.player','player4','player4Surname',8683,NULL),(8704,0,'player5@player.player','player5','player5Surname',8684,NULL),(8705,0,'player6@player.player','player6','player6Surname',8685,NULL),(8706,0,'player7@player.player','player7','player7Surname',8686,NULL),(8707,0,'player8@player.player','player8','player8Surname',8687,NULL),(8708,0,'player9@player.player','player9','player9Surname',8688,NULL),(8709,0,'player10@player.player','player10','player10Surname',8689,NULL);
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `premiumsubscription`
--

DROP TABLE IF EXISTS `premiumsubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `premiumsubscription` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `brandName` varchar(255) DEFAULT NULL,
  `cvv` int(11) DEFAULT NULL,
  `expirationMonth` int(11) DEFAULT NULL,
  `expirationYear` int(11) DEFAULT NULL,
  `holderName` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_l2oqg11of7llcd1eh5gss99n7` (`player_id`),
  CONSTRAINT `FK_l2oqg11of7llcd1eh5gss99n7` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `premiumsubscription`
--

LOCK TABLES `premiumsubscription` WRITE;
/*!40000 ALTER TABLE `premiumsubscription` DISABLE KEYS */;
INSERT INTO `premiumsubscription` VALUES (8846,0,'VISA',132,11,20,'Sponsor1','4981561349465266',8701),(8847,0,'SANTANDER',304,2,21,'Sponsor2','4626566531600704',8702);
/*!40000 ALTER TABLE `premiumsubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referee`
--

DROP TABLE IF EXISTS `referee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referee` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `RefereeUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  CONSTRAINT `FK_poaxqb6i26ly4p9nkegehjagr` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referee`
--

LOCK TABLES `referee` WRITE;
/*!40000 ALTER TABLE `referee` DISABLE KEYS */;
INSERT INTO `referee` VALUES (8710,0,'referee1@referee.referee','referee1','referee1Surname',8675),(8711,0,'referee2@referee.referee','referee2','referee2Surname',8676);
/*!40000 ALTER TABLE `referee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `abbreviation` varchar(255) DEFAULT NULL,
  `colorDisplay` varchar(255) DEFAULT NULL,
  `englishName` varchar(255) DEFAULT NULL,
  `spanishName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dxj3h3jo4yuvptpwv301sw2rv` (`abbreviation`),
  UNIQUE KEY `UK_r6xmydirdrff3uo301rhechjg` (`colorDisplay`),
  UNIQUE KEY `UK_cl4sklxbgoe0ydf69o2id5c6s` (`englishName`),
  UNIQUE KEY `UK_mg9vxrpln1hi5hag7f4dn733q` (`spanishName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (8733,0,'INT','#808080','International','Internacional'),(8734,0,'EU','#0000ff','Europe','Europa'),(8735,0,'NA','#ff0000','North America','Norteamérica'),(8736,0,'CN','#cc6600','China','China'),(8737,0,'KR','#008000','Korea','Korea');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  `scrim_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_keewtfe4fyti2gun6l0f434h4` (`player_id`),
  KEY `FK_m705d66b6eu1mrodeg1xthhnk` (`scrim_id`),
  CONSTRAINT `FK_m705d66b6eu1mrodeg1xthhnk` FOREIGN KEY (`scrim_id`) REFERENCES `scrim` (`id`),
  CONSTRAINT `FK_keewtfe4fyti2gun6l0f434h4` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` VALUES (8892,0,'Estuvo todo el rato insultado...',8702,8879),(8893,0,'Uso trucos para ganar injustamente!!',8700,8879);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resolution`
--

DROP TABLE IF EXISTS `resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolution` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `attended` bit(1) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `referee_id` int(11) DEFAULT NULL,
  `scrim_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mmah9bnnlugv004q5xki6ueta` (`scrim_id`),
  KEY `FK_q85jk9coiedqk81kgmhgh5xob` (`referee_id`),
  CONSTRAINT `FK_mmah9bnnlugv004q5xki6ueta` FOREIGN KEY (`scrim_id`) REFERENCES `scrim` (`id`),
  CONSTRAINT `FK_q85jk9coiedqk81kgmhgh5xob` FOREIGN KEY (`referee_id`) REFERENCES `referee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resolution`
--

LOCK TABLES `resolution` WRITE;
/*!40000 ALTER TABLE `resolution` DISABLE KEYS */;
INSERT INTO `resolution` VALUES (8894,0,'\0','La partida la ganamos nosotros!! Mira : https://k34.kn3.net/6A0AD2808.jpg',NULL,8879);
/*!40000 ALTER TABLE `resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `positive` bit(1) NOT NULL,
  `scrim_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sb2pk5qbx3wctnyaerfg8vnbw` (`scrim_id`),
  KEY `FK_oupp3rpdwyl9njhumoe7kvmj2` (`team_id`),
  CONSTRAINT `FK_oupp3rpdwyl9njhumoe7kvmj2` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  CONSTRAINT `FK_sb2pk5qbx3wctnyaerfg8vnbw` FOREIGN KEY (`scrim_id`) REFERENCES `scrim` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (8890,0,'',8879,8861),(8891,0,'',8879,8862);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scrim`
--

DROP TABLE IF EXISTS `scrim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scrim` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `confirmed` bit(1) DEFAULT NULL,
  `creatorTeamResult` varchar(255) DEFAULT NULL,
  `guestTeamResult` varchar(255) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `streamingLink` varchar(255) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `desiredGuestRegion_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `season_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hb76o09msqhvtvs3tm197mkjq` (`creator_id`),
  KEY `FK_brcr6vmdc1g6f8ycanr7i8a68` (`desiredGuestRegion_id`),
  KEY `FK_ntl9swke41goy4j04laym5f7l` (`guest_id`),
  KEY `FK_3f7j8yenhleq7w79qcxn6n9rb` (`season_id`),
  CONSTRAINT `FK_3f7j8yenhleq7w79qcxn6n9rb` FOREIGN KEY (`season_id`) REFERENCES `season` (`id`),
  CONSTRAINT `FK_brcr6vmdc1g6f8ycanr7i8a68` FOREIGN KEY (`desiredGuestRegion_id`) REFERENCES `region` (`id`),
  CONSTRAINT `FK_hb76o09msqhvtvs3tm197mkjq` FOREIGN KEY (`creator_id`) REFERENCES `team` (`id`),
  CONSTRAINT `FK_ntl9swke41goy4j04laym5f7l` FOREIGN KEY (`guest_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scrim`
--

LOCK TABLES `scrim` WRITE;
/*!40000 ALTER TABLE `scrim` DISABLE KEYS */;
INSERT INTO `scrim` VALUES (8879,0,'','ILLOGICAL','ILLOGICAL','2013-10-24 10:00:00','http://www.twitch.tv',8861,8733,8862,8874),(8880,0,'',NULL,NULL,'2019-10-24 19:00:00',NULL,8863,8733,8864,8878),(8881,0,'',NULL,NULL,'2013-10-24 10:00:00','http://www.twitch.tv',8861,8733,8862,8874),(8882,0,NULL,NULL,NULL,'2019-10-24 10:00:00','http://www.twitch.tv',8861,8733,NULL,8874),(8883,0,'\0',NULL,NULL,'2018-10-24 10:00:00','http://www.twitch.tv',8862,8733,8861,8874),(8884,0,'','ILLOGICAL','ILLOGICAL','2013-10-24 10:00:00','http://www.twitch.tv',8861,8733,8862,8874);
/*!40000 ALTER TABLE `scrim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `season`
--

DROP TABLE IF EXISTS `season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `season` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `finished` bit(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `videogame_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_iqci6ggin1l06x25p1ss1b3mu` (`videogame_id`),
  CONSTRAINT `FK_iqci6ggin1l06x25p1ss1b3mu` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `season`
--

LOCK TABLES `season` WRITE;
/*!40000 ALTER TABLE `season` DISABLE KEYS */;
INSERT INTO `season` VALUES (8871,0,'','Temporada 1','2010-09-05 00:00:00',8838),(8872,0,'','Temporada 2','2010-12-09 00:00:00',8838),(8873,0,'\0','Temporada 3','2018-05-05 00:00:00',8838),(8874,0,'\0','Temporada 1','2012-02-03 00:00:00',8839),(8875,0,'\0','Temporada 1','2013-09-10 00:00:00',8840),(8876,0,'\0','Temporada 1','2016-08-06 00:00:00',8841),(8877,0,'\0','Temporada 1','2016-08-06 00:00:00',8842),(8878,0,'\0','Temporada 1','2016-08-06 00:00:00',8843);
/*!40000 ALTER TABLE `season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialidentity`
--

DROP TABLE IF EXISTS `socialidentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialidentity` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `actor_id` int(11) NOT NULL,
  `socialNetwork_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_q9cst2d9rfx34qm9ae2pmj4kj` (`socialNetwork_id`),
  CONSTRAINT `FK_q9cst2d9rfx34qm9ae2pmj4kj` FOREIGN KEY (`socialNetwork_id`) REFERENCES `socialnetwork` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialidentity`
--

LOCK TABLES `socialidentity` WRITE;
/*!40000 ALTER TABLE `socialidentity` DISABLE KEYS */;
INSERT INTO `socialidentity` VALUES (8848,0,'https://twitter.com/sktelecomfaker?lang=es',8700,8698),(8849,0,'https://www.instagram.com/sktfakerlol/?hl=es',8700,8697),(8850,0,'https://www.facebook.com/MSISpain/',8712,8699);
/*!40000 ALTER TABLE `socialidentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialnetwork`
--

DROP TABLE IF EXISTS `socialnetwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialnetwork` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialnetwork`
--

LOCK TABLES `socialnetwork` WRITE;
/*!40000 ALTER TABLE `socialnetwork` DISABLE KEYS */;
INSERT INTO `socialnetwork` VALUES (8697,0,'https://images.vexels.com/media/users/3/137380/isolated/preview/1b2ca367caa7eff8b45c09ec09b44c16-instagram-icon-logo-by-vexels.png','Instagram'),(8698,0,'https://images.vexels.com/media/users/3/137419/isolated/preview/b1a3fab214230557053ed1c4bf17b46c-logotipo-de-icono-de-twitter-by-vexels.png','Twitter'),(8699,0,'https://images.vexels.com/media/users/3/137253/isolated/preview/90dd9f12fdd1eefb8c8976903944c026-icono-de-icono-de-facebook-by-vexels.png','Facebook');
/*!40000 ALTER TABLE `socialnetwork` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sponsor`
--

DROP TABLE IF EXISTS `sponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsor` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `SponsorUK_cgls5lrufx91ufsyh467spwa3` (`userAccount_id`),
  CONSTRAINT `FK_okfx8h0cn4eidh8ng563vowjc` FOREIGN KEY (`userAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sponsor`
--

LOCK TABLES `sponsor` WRITE;
/*!40000 ALTER TABLE `sponsor` DISABLE KEYS */;
INSERT INTO `sponsor` VALUES (8712,0,'sponsor1@sponsor.sponsor','sponsor1','sponsor1Surname',8677),(8713,0,'sponsor2@sponsor.sponsor','sponsor2','sponsor2Surname',8678),(8714,0,'sponsor3@sponsor.sponsor','sponsor3','sponsor3Surname',8681);
/*!40000 ALTER TABLE `sponsor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `leader_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `videogame_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rpp7024f7vud2cjvyrj0w7eol` (`leader_id`),
  KEY `FK_ih61m8b3n92ppgh5u7xko78w7` (`region_id`),
  KEY `FK_jaxxb63et8h2iw6edraguerwh` (`videogame_id`),
  CONSTRAINT `FK_jaxxb63et8h2iw6edraguerwh` FOREIGN KEY (`videogame_id`) REFERENCES `videogame` (`id`),
  CONSTRAINT `FK_ih61m8b3n92ppgh5u7xko78w7` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`),
  CONSTRAINT `FK_rpp7024f7vud2cjvyrj0w7eol` FOREIGN KEY (`leader_id`) REFERENCES `gameaccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (8861,0,'https://media.glassdoor.com/sqll/34053/team-industries-squarelogo-1442310926913.png','Team1 FIFA18','http://www.team1.com',8851,8733,8839),(8862,0,NULL,'Team2 FIFA18',NULL,8853,8733,8839),(8863,0,NULL,'Team3 RocketLeague',NULL,8855,8733,8843),(8864,0,'https://www.logoshuffle.com/logo/thumb/27bcc064-8f97-439a-98ff-943104445cda?sz=300','Team4 RocketLeague','http://www.team4.com',8858,8733,8843);
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useraccount`
--

DROP TABLE IF EXISTS `useraccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useraccount` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `accountNonLocked` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_csivo9yqa08nrbkog71ycilh5` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useraccount`
--

LOCK TABLES `useraccount` WRITE;
/*!40000 ALTER TABLE `useraccount` DISABLE KEYS */;
INSERT INTO `useraccount` VALUES (8672,0,'','21232f297a57a5a743894a0e4a801fc3','admin'),(8673,0,'','5d2bbc279b5ce75815849d5e3f0533ec','player1'),(8674,0,'','88e77ff74930f37010370c296d14737b','player2'),(8675,0,'','092d469d0a71c8002e32f808b861f9a4','referee1'),(8676,0,'','4f556d5a66f4fe0d058523a7b8e19d91','referee2'),(8677,0,'','42c63ad66d4dc07ed17753772bef96d6','sponsor1'),(8678,0,'','3dc67f80a03324e01b1640f45d107485','sponsor2'),(8679,0,'','c240642ddef994358c96da82c0361a58','manager1'),(8680,0,'','8df5127cd164b5bc2d2b78410a7eea0c','manager2'),(8681,0,'','9cq4146hes52n7bc2d8h90630f7aae8c','sponsor3'),(8682,0,'','1aa3814dca32e4c0b79a2ca047ef1db0','player3'),(8683,0,'','12efaba7fd50f5c66bd295683c0ce2a7','player4'),(8684,0,'','c5aec8b7110bb97bf59ab1a06805ebdd','player5'),(8685,0,'','9d470eed6fd655c08563e806848cdc6d','player6'),(8686,0,'','10b5d73053b6372f33e28589a0285f80','player7'),(8687,0,'','0d4753a9802fe6af7d3bab0c91cb07f3','player8'),(8688,0,'','26a26a749d6d4cbe1da7c2f3a172fea8','player9'),(8689,0,'','d9174af8299b39a7092f4b7683cf8ce2','player10');
/*!40000 ALTER TABLE `useraccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useraccount_authorities`
--

DROP TABLE IF EXISTS `useraccount_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useraccount_authorities` (
  `UserAccount_id` int(11) NOT NULL,
  `authority` varchar(255) DEFAULT NULL,
  KEY `FK_b63ua47r0u1m7ccc9lte2ui4r` (`UserAccount_id`),
  CONSTRAINT `FK_b63ua47r0u1m7ccc9lte2ui4r` FOREIGN KEY (`UserAccount_id`) REFERENCES `useraccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useraccount_authorities`
--

LOCK TABLES `useraccount_authorities` WRITE;
/*!40000 ALTER TABLE `useraccount_authorities` DISABLE KEYS */;
INSERT INTO `useraccount_authorities` VALUES (8672,'ADMIN'),(8673,'PLAYER'),(8674,'PLAYER'),(8675,'REFEREE'),(8676,'REFEREE'),(8677,'SPONSOR'),(8678,'SPONSOR'),(8679,'MANAGER'),(8680,'MANAGER'),(8681,'SPONSOR'),(8682,'PLAYER'),(8683,'PLAYER'),(8684,'PLAYER'),(8685,'PLAYER'),(8686,'PLAYER'),(8687,'PLAYER'),(8688,'PLAYER'),(8689,'PLAYER');
/*!40000 ALTER TABLE `useraccount_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videogame`
--

DROP TABLE IF EXISTS `videogame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videogame` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `manager_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gj45cwew2bqe15u7eilaotmk` (`manager_id`),
  CONSTRAINT `FK_gj45cwew2bqe15u7eilaotmk` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videogame`
--

LOCK TABLES `videogame` WRITE;
/*!40000 ALTER TABLE `videogame` DISABLE KEYS */;
INSERT INTO `videogame` VALUES (8838,0,'Hearthstone es un juego de cartas de Blizzard.','https://d2q63o9r0h0ohi.cloudfront.net/images/logos/navigation/logo-d365f1e154305df66af7fd09db2e9bec7c1be8a26a0c6bdde8fcea565a1649400f798f2fba69fb98e0d75aa85d654db5fa5b403420779202bccb6d28ba407f7b.png','Hearthstone',8715),(8839,0,'Celebra el Juego del Mundo en EA SPORTS FIFA 18 ','http://www.fifplay.com/img/public/fifa-18-logo-black.png','FIFA 18',8715),(8840,0,'La última entrega de Call of Duty, ambientado en la 2ª guerra mundial.','https://upload.wikimedia.org/wikipedia/commons/6/6f/CoD_WWII_Logo.png','Call of Duty: World War 2',8715),(8841,0,'El juego más competitivo de Blizzard, usa a tus mejores personajes en este shooter.','https://d1u5p3l4wpay3k.cloudfront.net/hots_es_gamepedia/4/4a/OverwatchLogo.jpg','Overwatch',8716),(8842,0,'League of Legends, es uno de los juegos más grandes en el panorama competitivo de los e-sports.','https://lolstatic-a.akamaihd.net/frontpage/apps/prod/playnow-global/en_US/2ba598862d4913d3243c845b5ff216acdd1cf689/assets/img/lol-logo.png','League of Legends',8716),(8843,0,'El juego de fútbol más loco! Con coches radiocontrol, una gran bola, y 2 porterías.','https://vignette.wikia.nocookie.net/logopedia/images/9/90/Rocket_League.svg/revision/latest?cb=20171202171315','Rocket League',8716);
/*!40000 ALTER TABLE `videogame` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-02 11:24:35
